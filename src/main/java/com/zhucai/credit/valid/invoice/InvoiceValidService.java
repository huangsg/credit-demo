package com.zhucai.credit.valid.invoice;

public interface InvoiceValidService {

	String invoiceSign(String url);
	
	InviceValidResult valid(String url);
}
