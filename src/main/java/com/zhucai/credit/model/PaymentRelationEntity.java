package com.zhucai.credit.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * 融资支付关系表
 * @author xjz
 *
 */
@Entity
@Table(name="finance_payment_relation")
public class PaymentRelationEntity extends BaseEntity{

	private static final long serialVersionUID = 1L;
	
	//支付单号
	@Column(nullable = false)
	private String paySingleNo;
	//融资单号
	@Column(nullable = false)
	private String recordNo;
	
	public String getPaySingleNo() {
		return paySingleNo;
	}
	public void setPaySingleNo(String paySingleNo) {
		this.paySingleNo = paySingleNo;
	}
	public String getRecordNo() {
		return recordNo;
	}
	public void setRecordNo(String recordNo) {
		this.recordNo = recordNo;
	}
	
	
	

	
}
