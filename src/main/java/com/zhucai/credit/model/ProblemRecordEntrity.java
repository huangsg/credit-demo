package com.zhucai.credit.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.wordnik.swagger.annotations.ApiModelProperty;
/**
 * 问题记录表
 * @author hsg
 *
 */
@Entity
@Table(name="finance_problem_record")
public class ProblemRecordEntrity extends BaseEntity {

		private static final long serialVersionUID = 1L;
		@ApiModelProperty(value = "问题描述")
		private String problemRemark;
		@ApiModelProperty(value = "问题类型")
		private String problemType;
		@ApiModelProperty(value = "受理人id,")
		private Long operator;
		@ApiModelProperty(value = "受理人联系方式,")
		private String operPhone;
		@ApiModelProperty(value = "问题级别,")
		private String operlevel;
		@ApiModelProperty(value = "问题关联的业务表id,")
		private Long outId;
		@ApiModelProperty(value = "是否被处理过,(0无,1有)")
		private Integer operStatus;
		@ApiModelProperty(value = "操作时间,")
		private Date operTime;
		@Column(nullable = true, length = 500)
		public String getProblemRemark() {
			return problemRemark;
		}
		public void setProblemRemark(String problemRemark) {
			this.problemRemark = problemRemark;
		}
		@Column(nullable = true)
		public Long getOperator() {
			return operator;
		}
		public void setOperator(Long operator) {
			this.operator = operator;
		}
		@Column(nullable = true, length = 25)
		public String getOperPhone() {
			return operPhone;
		}
		public void setOperPhone(String operPhone) {
			this.operPhone = operPhone;
		}
		@Column(nullable = true, length = 25)
		public String getOperlevel() {
			return operlevel;
		}
		public void setOperlevel(String operlevel) {
			this.operlevel = operlevel;
		}
		@Column(nullable = true)
		public Integer getOperStatus() {
			return operStatus;
		}
		public void setOperStatus(Integer operStatus) {
			this.operStatus = operStatus;
		}
		@Column(nullable = true)
		public Long getOutId() {
			return outId;
		}
		public void setOutId(Long outId) {
			this.outId = outId;
		}
		@Column(nullable = true)
		public Date getOperTime() {
			return operTime;
		}
		public void setOperTime(Date operTime) {
			this.operTime = operTime;
		}
		@Column(nullable = true,length = 40)
		public String getProblemType() {
			return problemType;
		}
		public void setProblemType(String problemType) {
			this.problemType = problemType;
		}
		
}
