package com.zhucai.credit.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.zhucai.credit.model.StationMessage;

/**
 * 
 * @author hsg
 * StationMessage
 */
@Repository
public interface StationMessageRepository extends
		PagingAndSortingRepository<StationMessage, Long>,
		JpaSpecificationExecutor<StationMessage> {

	@Query(value = "select s.* from tb_xa_station_message s where s.number=?1 and s.status=1 ", nativeQuery = true)
	public StationMessage findStationMessage(String number);



	





}
