package com.zhucai.credit.service.impl.transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zhucai.credit.bank.bean.BankTransfer;
import com.zhucai.credit.bank.service.IFactoringService;
import com.zhucai.credit.bean.CreditApplication;
import com.zhucai.credit.bean.CreditRecord;
import com.zhucai.credit.common.CommonEnums.CrAprovalStatus;
import com.zhucai.credit.common.CommonEnums.CreditStatus;
import com.zhucai.credit.common.CommonEnums.CreditUnusualStatus;
import com.zhucai.credit.common.CommonEnums.EntType;
import com.zhucai.credit.common.CommonEnums.OperType;
import com.zhucai.credit.common.CommonEnums.PayType;
import com.zhucai.credit.common.CommonEnums.PaymentType;
import com.zhucai.credit.common.CommonEnums.PaymentUse;
import com.zhucai.credit.common.CommonEnums.TradeStatus;
import com.zhucai.credit.common.CommonEnums.ZhuCaiProdType;
import com.zhucai.credit.common.EnumHelper;
import com.zhucai.credit.common.HelpConstant;
import com.zhucai.credit.common.ZhucaiConstant;
import com.zhucai.credit.model.CreditApplicationEntity;
import com.zhucai.credit.model.CreditProductEntity;
import com.zhucai.credit.model.CreditRecordEntrity;
import com.zhucai.credit.model.CreditRecordTimeEntrity;
import com.zhucai.credit.model.EntAccountEntity;
import com.zhucai.credit.model.EntCrProdEntity;
import com.zhucai.credit.model.OperRecordEntity;
import com.zhucai.credit.model.PaymentOrderEntity;
import com.zhucai.credit.model.PaymentPlanEntity;
import com.zhucai.credit.monitor.service.INotificationService;
import com.zhucai.credit.repository.CreditApplicationRepository;
import com.zhucai.credit.repository.CreditInvoiceRepository;
import com.zhucai.credit.repository.CreditProductRepository;
import com.zhucai.credit.repository.CreditRecordRepository;
import com.zhucai.credit.repository.CreditRecordTimeRepository;
import com.zhucai.credit.repository.EntAccountRepository;
import com.zhucai.credit.repository.EntCrProdEntityRepository;
import com.zhucai.credit.repository.OperRecordRepository;
import com.zhucai.credit.repository.PaymentOrderItemRepository;
import com.zhucai.credit.repository.PaymentOrderRepository;
import com.zhucai.credit.repository.PaymentPlanRepository;
import com.zhucai.credit.service.CreditSupplierService;
import com.zhucai.credit.service.impl.loop.OrderLoopService;
import com.zhucai.credit.utils.SqlResultUtil;
import com.zhucai.credit.utils.TimeUtils;

/**
 * 供应商接口实现
 * 
 * @date 2017年12月1日 @user hsg
 */
@Service("creditSupplierService")
@com.alibaba.dubbo.config.annotation.Service(version = "1.0.0")
public class SupplierServiceImplT implements CreditSupplierService {
	private static final Logger logger = LoggerFactory.getLogger(SupplierServiceImplT.class);

	@Autowired
	private EntAccountRepository entAccountRepository;
	@Autowired
	private EntCrProdEntityRepository entCrProdEntityRepository;
	@Autowired
	private CreditApplicationRepository creditApplicationRepository;
	@Autowired
	private OperRecordRepository operRecordRepository;
	@Autowired
	private CreditRecordRepository creditRecordRepository;
	@Autowired
	private CreditInvoiceRepository creditInvoiceRepository;
	@Autowired
	private PaymentOrderRepository paymentOrderRepository;
	@Autowired
	private PaymentPlanRepository paymentPlanRepository;
	@Autowired
	private OrderLoopService orderLoopService;
	@Autowired
	private CreditUserServiceUtil creditUserServiceUtil;
	@Autowired
	private PaymentOrderItemRepository paymentOrderItemRepository;
	@Autowired
	private CreditProductRepository creditProductRepository;
	@Autowired
	private CreditRecordTimeRepository creditRecordTimeRepository;

	@Autowired
	private Map<String, IFactoringService> bankLinkageService;
	
	@Resource(name="EMAIL") 
	private INotificationService emailINotificationService;

	
	/**
	 * 加载供应商筑保通信息以及e点通申请信息
	 * 
	 * @date 2017年12月1日 @user hsg
	 */
	@Override
	public CreditApplication loadIngSupProd(CreditApplication ca) {
		logger.info("### 加载供应商筑保通信息以及e点通申请信息 start ca = {}", ca.toString());

		try {
			// 根据id查询资质申请记录
			CreditApplicationEntity cre = creditApplicationRepository.findOne(ca.getTid());

			if (cre == null) {
				return null;
			}
			BeanUtils.copyProperties(cre, ca);
			OperRecordEntity operRecordEntity = operRecordRepository.findByTid(cre.getOperRecordId());
			if (operRecordEntity != null) {
				ca.setRejectReason(operRecordEntity.getReview());
			}
		} catch (Exception e) {
			logger.debug("加载供应商筑保通信息以及e点通申请信息方法发生错误 errMsg={}", e);
			return null;
		}

		return ca;
	}

	/**
	 * 供应商e点通申请点击我已传递
	 * 
	 * @date 2017年12月3日 @user hsg
	 */
	@Override
	public CreditApplication passOn(CreditApplication ca) {
		logger.info("### 供应商建行e点通申请点击我已传递 start ca={}", ca.toString());
		try {
			CreditApplicationEntity creditApplication = creditApplicationRepository.findOne(ca.getTid());
			if (creditApplication == null) {
				return null;
			}
			creditApplication.setCrAprovalStatus(CrAprovalStatus.DATA_REVIEW.toString());

			Integer coo = creditApplicationRepository.updateCrAprovalStatus(CrAprovalStatus.DATA_REVIEW.toString(), ca.getTid());

			if (coo != 1) {
				throw new RuntimeException("供应商建行e点通申请点击我已传递  返回更新条数异常 coo=" + coo);
			}

			// 新增操作记录
			OperRecordEntity ope = new OperRecordEntity();
			ope.setBizKey(CrAprovalStatus.DATA_REVIEW.toString());
			ope.setModifyDescription("供应商点击我已传递资料");
			ope.setOperator(ca.getUserId());
			ope.setOperatorName(ca.getApplicantName());
			ope.setOperTime(new Date());
			ope.setOperType(OperType.PASS_ON.toString());
			ope.setOutId(creditApplication.getTid());
			ope.setResult(EnumHelper.getDescribe(CrAprovalStatus.DATA_REVIEW));
			ope.setReview("供应商点击我已传递资料");
			operRecordRepository.save(ope);

			BeanUtils.copyProperties(creditApplication, ca);
		} catch (Exception e) {
			logger.error("### 供应商建行e点通申请点击我已传递 发生异常 errMsg = {}", e);
			return null;
		}

		return ca;

	}

	/**
	 * 供应商融资确认
	 * 
	 * @date 2017年12月8日 @user hsg
	 */
	@Override
	public CreditRecord supApproveCredit(CreditRecord cr) {
		logger.info("### 供应商融资确认 start cr={}", cr.toString());
		try {
			CreditRecordEntrity cre = creditRecordRepository.findByTidAndStatusNot(cr.getTid(), HelpConstant.Status.delete);
			if (cre == null) {
				cr.setReturnStatus("FAIL");
				cr.setReturnMessage("确认融资失败:网络异常,请稍后再试!");
				logger.error("tid={},查出的融资记录为空", cr.getTid());
				return cr;
			}
			cr.setRecordNo(cre.getRecordNo());
			EntCrProdEntity crProdEntity = entCrProdEntityRepository.findByEnAccId(cre.getParticipator(), cre.getCrProdId(), EntType.SUPPLIER.toString(),
					ZhuCaiProdType.ZHU_CAI_B_TONG.toString());

			Integer dateTian = TimeUtils.dateTian(cre.getLoanDate(), new Date());

			BigDecimal orderAmt = SqlResultUtil.getOrderAmt(cre.getCreditGetAmount(), dateTian, crProdEntity.getCrProdRate());

			// 查询此融资单是否有正常的支付成功记录
			Integer coo = paymentOrderItemRepository.findByStatusNotAndOrderNoAndNormal(HelpConstant.Status.delete, cre.getRecordNo(), TradeStatus.NORMAL.toString());

			// 新增融资确认历时记录
			creditUserServiceUtil.createRecordTime(cre.getTid(), CreditStatus.WAIT_CONFIRM.toString());

			if (orderAmt.compareTo(BigDecimal.ZERO) == 1 && coo == 0) {
				PaymentOrderEntity createOrder = createOrder(cr);
				cre.setCreditStatus(CreditStatus.WAIT_PAY.toString());
				cre.setCreditUnusualStatus(CreditUnusualStatus.NORMAL.toString());
				cre.setRejectReason(null);
				if (createOrder == null) {
					logger.error("新增订单信息失败，参数msg={}", cr);
				}
			} else {
				// 当支付成功记录大于0,或服务费小于0,直接发送给银行
				BankTransfer bankTransfer = new BankTransfer();
				BeanUtils.copyProperties(cr, bankTransfer);
				CreditProductEntity cpe = creditProductRepository.findByTidAndStatusNot(cre.getCrProdId(), HelpConstant.Status.delete);
				IFactoringService iFactoringService = bankLinkageService.get(cpe.getCrProdType());
				CreditRecord apply = iFactoringService.apply(bankTransfer);
				// 更新为待签合同
				if ("OK".equals(apply.getReturnStatus())) {
					cre.setCreditStatus(CreditStatus.SUBSTITUTE_CONTRACT.toString());
					cre.setCreditUnusualStatus(CreditUnusualStatus.NORMAL.toString());
					cre.setRejectReason(null);
				} else {
					// 新增融资支付历时记录
					creditUserServiceUtil.createRecordTime(cre.getTid(), CreditStatus.WAIT_PAY.toString());
					// 更新为网络异常
					cre.setCreditUnusualStatus(CreditUnusualStatus.NETWORK_ERROR.toString());
					cre.setCreditStatus(CreditStatus.SUMBIT_TO_BANK.toString());
					cre.setRejectReason(apply.getReturnMessage());
					logger.info("更新融资失败，data={},调用建行接口失败，error={}", cre, apply.getReturnMessage());
				}
			}
			creditRecordRepository.saveAndFlush(cre);
			BeanUtils.copyProperties(cre, cr);

			// 新增融资确认操作记录
			OperRecordEntity ope = new OperRecordEntity();
			ope.setModifyDescription("供应商确认融资记录");
			ope.setOperator(cr.getUserId());
			ope.setOperatorName(cr.getUserName());
			ope.setOperTime(new Date());
			ope.setOperType(OperType.TRACK_CREDIT.toString());
			ope.setOutId(cre.getTid());
			ope.setReview("供应商确认融资记录");

			ope.setBizKey(CreditStatus.WAIT_PAY.toString());
			ope.setResult(CreditStatus.WAIT_PAY.toString() + "待支付");
			operRecordRepository.save(ope);
		} catch (Exception e) {
			cr.setReturnStatus("FAIL");
			cr.setReturnMessage("确认融资失败:网络异常,请稍后再试!");
			emailINotificationService.notification("供应商融资确认异常 融资单号="+cr.getRecordNo()+",err="+e);
			logger.error("supApproveCr edit方法发生错误： err={}",  e.getMessage());
			return cr;
		}
		return cr;
	}

	/**
	 * 供应商重新提交
	 * 
	 * @date 2017年12月8日 @user hsg
	 */
	@Override
	public CreditRecord supAgainSubmit(CreditRecord cr) {
		logger.info("### 供应商重新提交（supAgainSubmit）start ，data={}", cr.toString());

		CreditRecordEntrity cre = creditRecordRepository.findByTidAndStatusNot(cr.getTid(), HelpConstant.Status.delete);

		if (cre == null) {
			cr.setReturnStatus("FAIL");
			cr.setReturnMessage("确认融资失败:网络异常,请稍后再试!");
			logger.error("tid={},supAgainSubmit查出的融资记录为空", cr.getTid());
			return cr;
		}
		// 判断是哪个银行产品
		CreditProductEntity cpe = creditProductRepository.findOne(cre.getCrProdId());
		if (cpe == null) {
			cr.setReturnStatus("FAIL");
			cr.setReturnMessage("确认融资失败:获取银行产品失败!");
			logger.error("cr.getCrProdId()={},supAgainSubmit查出的CreditProductEntity为空", cr.getCrProdId());
			emailINotificationService.notification("供应商重新提交银行失败：supAgainSubmit查出的CreditProductEntity为空，cr.getCrProdId()="+cr.getCrProdId());
			return cr;
		}
		try {
			IFactoringService iFactoringService = bankLinkageService.get(cpe.getCrProdType());
			// 调用银行接口
			BankTransfer bankTransfer = new BankTransfer();
			BeanUtils.copyProperties(cr, bankTransfer);
			CreditRecord credit = iFactoringService.apply(bankTransfer);
			// 更新为待签合同
			if ("OK".equals(credit.getReturnStatus())) {
				cre.setCreditStatus(CreditStatus.SUBSTITUTE_CONTRACT.toString());
				cre.setCreditUnusualStatus(CreditUnusualStatus.NORMAL.toString());
				cre.setRejectReason(null);
				Integer cou = creditRecordRepository.updateContract(HelpConstant.Status.delete, cr.getTid());
				if (cou != 1) {
					logger.info("更新融资记失败，data={},调用建行接口成功，ccbdata={}", cre, cr);
				}

			} else {
				cre.setCreditStatus(CreditStatus.SUMBIT_TO_BANK.toString());
				cre.setCreditUnusualStatus(CreditUnusualStatus.NETWORK_ERROR.toString());
				cre.setRejectReason("连接超时，请稍后再试！");
				cr.setReturnStatus("FAIL");
				cr.setReturnMessage("连接超时，请稍后再试！");
				creditRecordRepository.updateBySignContract( cre.getTid(), cre.getCreditUnusualStatus(), credit.getReturnMessage(), cre.getCreditStatus());
				logger.info("更新融资失败，data={},调用建行接口失败，error={}", cre, credit.getReturnMessage());
			}

			// 新增供应商重新提交记录
			OperRecordEntity ope = new OperRecordEntity();
			ope.setModifyDescription("供应重新提交");
			ope.setOperator(cr.getUserId());
			ope.setOperatorName(cr.getUserName());
			ope.setOperTime(new Date());
			ope.setOperType(OperType.SUP_AGAIN_SUBMIT.toString());
			ope.setOutId(cre.getTid());
			ope.setReview("供应商重新提交记录");
			ope.setBizKey(CreditStatus.SUBSTITUTE_CONTRACT.toString());
			ope.setResult(CreditStatus.SUBSTITUTE_CONTRACT.toString() + "待支付");
			operRecordRepository.save(ope);
			logger.info("新增一条操作记录data={}", ope);
		} catch (Exception e) {
			cr.setReturnStatus("FAIL");
			cr.setReturnMessage("提交失败:网络异常,请稍后再试!");
			logger.error("supAgainSubmit方法发生错误：" + e);
			emailINotificationService.notification("供应商重新提交银行接口失败："+e);
			return cr;
		}
		return cr;
	}

	/**
	 * 供应商点击支付零元的时候
	 * 
	 * @date 2017年12月8日 @user hsg
	 */
	@Override
	public void payZeroPlan(Long id) {
		logger.info("### 供应商点击支付但是服务费为零元时 start tid={}", id);
		try {
			CreditRecordEntrity cre = creditRecordRepository.findByTidAndStatusNot(id, HelpConstant.Status.delete);

			if (cre == null) {
				return;
			}

			CreditRecord cr = new CreditRecord();
			BeanUtils.copyProperties(cre, cr);
			// 判断是哪个银行产品
			CreditProductEntity cpe = creditProductRepository.findOne(cre.getCrProdId());
			if (cpe == null) {
				return;
			}
			EntCrProdEntity crProdEntity = entCrProdEntityRepository.findByEnAccId(cre.getParticipator(), cre.getCrProdId(), EntType.SUPPLIER.toString(),
					ZhuCaiProdType.ZHU_CAI_B_TONG.toString());

			Integer dateTian = TimeUtils.dateTian(cre.getLoanDate(), new Date());

			BigDecimal orderAmt = SqlResultUtil.getOrderAmt(cre.getCreditGetAmount(), dateTian, crProdEntity.getCrProdRate());
			if (orderAmt.compareTo(BigDecimal.ZERO) == 1) {
				return;
			}

			IFactoringService iFactoringService = bankLinkageService.get(cpe.getCrProdType());

			// 查询是否有成功的支付历时记录
			List<CreditRecordTimeEntrity> findByCrRecordIdAndRecordStatus = creditRecordTimeRepository.findByCrRecordIdAndRecordStatus(id, CreditStatus.WAIT_PAY.toString());
			if (CollectionUtils.isEmpty(findByCrRecordIdAndRecordStatus)) {
				// 新增融资支付历时记录
				creditUserServiceUtil.createRecordTime(cre.getTid(), CreditStatus.WAIT_PAY.toString());
			}

			// 调用银行接口
			BankTransfer bankTransfer = new BankTransfer();
			BeanUtils.copyProperties(cr, bankTransfer);
			CreditRecord credit = iFactoringService.apply(bankTransfer);
			// 更新为待签合同
			if ("OK".equals(credit.getReturnStatus())) {
				cre.setCreditStatus(CreditStatus.SUBSTITUTE_CONTRACT.toString());
				cre.setCreditUnusualStatus(CreditUnusualStatus.NORMAL.toString());
				cre.setRejectReason(null);
				Integer cou = creditRecordRepository.updateContract(HelpConstant.Status.delete, cr.getTid());
				if (cou != 1) {
					logger.info("更新融资记失败，data={},调用建行接口成功，ccbdata={}", cre, cr);
				}

			} else {
				cre.setCreditStatus(CreditStatus.SUMBIT_TO_BANK.toString());
				cre.setCreditUnusualStatus(CreditUnusualStatus.NETWORK_ERROR.toString());
				cre.setRejectReason("网络异常，请稍后再试！");
				creditRecordRepository.saveAndFlush(cre);
			}

		} catch (Exception e) {
			logger.error("供应商点击支付但是服务费为零元时 payZeroPlan方法发生错误：" + e);
		}
	}

	/**
	 * 新增订单信息
	 * 
	 * @ejb.create-method
	 */
	private PaymentOrderEntity createOrder(CreditRecord cr) {
		logger.info("### 新增订单信息start cr={}", cr.toString());
		CreditRecordEntrity cre = creditRecordRepository.findByTidAndStatusNot(cr.getTid(), HelpConstant.Status.delete);
		if (cre == null) {
			return null;
		}

		Long participator = cre.getParticipator();
		// 查询费率
		EntCrProdEntity ecpe = entCrProdEntityRepository.findByEnAccId(participator, cre.getCrProdId(), EntType.SUPPLIER.toString(), ZhuCaiProdType.ZHU_CAI_B_TONG.toString());
		PaymentOrderEntity paymentOrderEntity = paymentOrderRepository.findPaymentOrderByOrderNo(HelpConstant.Status.delete, PaymentType.FINANCE_CREDIT.toString(),
				cre.getRecordNo());
		if (paymentOrderEntity != null) {
			return null;
		}
		EntAccountEntity eae = entAccountRepository.findByTid(participator);
		if (eae == null) {
			return null;
		}
		PaymentOrderEntity poe = paymentOrderRepository.findByStatusNotAndOrderNo(HelpConstant.Status.delete, cre.getRecordNo());

		if (poe != null) {
			return poe;
		} else {
			poe = new PaymentOrderEntity();

		}
		poe.setPayEnterpriseId(eae.getEnterpriseId());
		poe.setPayEnterpriseName(eae.getEnterpriseName());
		poe.setCollectEnterpriseName("江苏足财电子商务有限公司");
		/** 入参：融资金额、账期、费率 */
		Integer dateTian = TimeUtils.dateTian(cre.getLoanDate(), new Date());
		BigDecimal orderAmt = SqlResultUtil.getOrderAmt(cre.getCreditGetAmount(), dateTian, ecpe.getCrProdRate());
		poe.setOrderAmt(orderAmt);
		poe.setCreateOrderTime(new Date());
		poe.setCreateUser(cr.getUserId());
		poe.setCreateOrderTime(new Date());
		poe.setOrderNo(cre.getRecordNo());
		poe.setPayType(PayType.EMPTY.toString());
		poe.setServiceNo(PaymentType.FINANCE_CREDIT.toString());

		/*
		 * Date fetureDate = TimeUtils.getFetureDate(1,
		 * TimeUtils.YYYYMMDDHHMMSS); poe.setOrderOutTime(fetureDate);
		 */

		poe.setPayEnterpriseName(eae.getEnterpriseName());
		poe.setPayEnterpriseId(eae.getEnterpriseId());
		poe.setPayStatus("01");
		poe.setTradeStatus(TradeStatus.NORMAL.toString());
		poe.setProductName(PaymentUse.SERVICE_CHARGE.toString());
		poe.setProductRemark("筑保通服务费");
		PaymentOrderEntity save = paymentOrderRepository.save(poe);
		logger.info("新增一条订单信息，msg={}", save);
		return save;
	}

	/**
	 * 融资驳回
	 */
	@Override
	public CreditRecord rejectCredit(CreditRecord cr) {
		logger.info("### 融资供应商驳回start cr={}", cr.toString());
		try {
			CreditRecordEntrity cre = creditRecordRepository.findByTidAndStatusNot(cr.getTid(), HelpConstant.Status.delete);
			if (cre == null) {
				cr.setReturnStatus("FAIL");
				cr.setReturnMessage("确认融资失败:网络异常,请稍后再试!");
				logger.error("tid={},查出的融资记录为空", cr.getTid());
				return cr;
			}

			// 驳回操作记录
			OperRecordEntity ope = new OperRecordEntity();
			ope.setModifyDescription("供应商确认融资记录");
			ope.setOperator(cr.getUserId());
			ope.setOperatorName(cr.getUserName());
			ope.setOperTime(new Date());
			ope.setOperType(OperType.TRACK_CREDIT.toString());
			ope.setOutId(cre.getTid());
			ope.setReview("供应商确认融资记录");
			ope.setBizKey(CreditStatus.WAIT_SUBMIT.toString());
			ope.setResult(CreditStatus.WAIT_SUBMIT.toString() + "驳回融资");
			operRecordRepository.save(ope);
			logger.info("新增一条操作记录data={}", ope);

			// 融资记录改为待发起
			cre.setCreditStatus(CreditStatus.WAIT_SUBMIT.toString());
			// 异常状态改为供应商驳回
			cre.setCreditUnusualStatus(CreditUnusualStatus.WAIT_CONFIRM_CANCEL.toString());
			cre.setRejectReason(null);

			Integer updateUnusualStatus = creditRecordRepository.updateUnusualStatus(HelpConstant.Status.delete, cre.getTid(), CreditUnusualStatus.WAIT_CONFIRM_CANCEL.toString(),
					null, CreditStatus.WAIT_SUBMIT.toString());
			if (updateUnusualStatus != 1) {
				throw new RuntimeException("更新融资状态失败");
			}
			logger.info("驳回融资成功，data={}", cre);
			BeanUtils.copyProperties(cre, cr);

			/**
			 * 实际付款状态改为未付款
			 */
			PaymentPlanEntity ppe = paymentPlanRepository.findOne(cre.getPaymentId());
			ppe.setPayState(ZhucaiConstant.PaymentPlanStatus.NOT_PAYMENT_STATUS);
			paymentPlanRepository.updatePayState(cre.getPaymentId(), ZhucaiConstant.PaymentPlanStatus.NOT_PAYMENT_STATUS);

			// 删除融资历时记录
			creditRecordTimeRepository.deleteRecordTimeByRecordId(cre.getTid());

		} catch (Exception e) {
			cr.setReturnStatus("FAIL");
			cr.setReturnMessage("确认融资失败:网络异常,请稍后再试!");
			logger.error("supApproveCredit方法发生错误：err={}",  e);
			emailINotificationService.notification("### 融资单号="+cr.getRecordNo()+"供应商驳回异常err="+e);
			return cr;
		}
		return cr;
	}

	/**
	 * 根据企业id查询合作的采购商id集合
	 * 
	 * @date 2017年12月12日 @user hsg
	 */
	@Override
	public List<Long> finfPurList(Long supId, String entType) {
		List<Long> rsList = new ArrayList<Long>();
		try {
			EntAccountEntity eae = entAccountRepository.findByStatusNotAndEnterpriseIdAndEntType(HelpConstant.Status.delete, supId, entType);
			if (eae == null) {
				return null;
			}
			List<Object[]> objList = creditApplicationRepository.findPurList(HelpConstant.Status.delete, eae.getTid());
			if (objList != null && objList.size() > 0) {
				for (Object[] obj : objList) {
					rsList.add(SqlResultUtil.getSqlResultLong(obj[0]));
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			return null;
		}
		return rsList;
	}

}
