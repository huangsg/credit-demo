package com.zhucai.credit.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.zhucai.credit.model.PaymentPlanEntity;

/**
 * 
 * @author hsg
 *（付款计划的）Repository
 */
@Repository
public interface PaymentPlanRepository extends
		PagingAndSortingRepository<PaymentPlanEntity, Long>,
		JpaSpecificationExecutor<PaymentPlanEntity> {
	 

	@Query(value = "select count(*) from tb_xa_payment_plan txpp where txpp.order_id=?1 and txpp.pay_state <>?2 and txpp.status <>?3",nativeQuery = true)
	public Integer findUnfinishedCountByOrderIdAndStatusNot(Long orderId,Long long1, int delete);

	/**
	 * 根据发票主键id查询计划付款
	 * @param delete
	 * @param tid
	 * @return
	 */
	public PaymentPlanEntity findByStatusNotAndInvoiceFileId(int delete, Long tid);

	/**
	 * 更新计划付款状态
	 * @author hsg
	 * @version 2018年7月16日
	 * @param tid
	 * @param checkingPaymentStatus
	 * @return
	 */
	@Transactional
	@Modifying(clearAutomatically=true)
	@Query(value="update tb_xa_payment_plan set pay_state = ?2 where id =?1",nativeQuery=true)
	public Integer updatePayState(Long tid, int checkingPaymentStatus);

	/**
	 * 更新发票id
	 * @author hsg
	 * @version 2018年7月16日
	 * @param tid
	 * @param checkingPaymentStatus
	 * @return
	 */
	@Transactional
	@Modifying(clearAutomatically=true)
	@Query(value="update tb_xa_payment_plan set invoice_file_id = ?1 where id =?2",nativeQuery=true)
	public Integer updateInvoiceFileId(Long invoiceTid, Long tid);





}
