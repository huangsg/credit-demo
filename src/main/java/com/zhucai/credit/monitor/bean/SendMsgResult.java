package com.zhucai.credit.monitor.bean;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@ApiModel("短信请求结果")
public class SendMsgResult {

	@ApiModelProperty(value = "请求是否成功 0：失败  1：成功")
	private Integer code;

	@ApiModelProperty(value = "请求结果详情")
	private String msg;
	
	public SendMsgResult() {}

	public SendMsgResult(Integer code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
