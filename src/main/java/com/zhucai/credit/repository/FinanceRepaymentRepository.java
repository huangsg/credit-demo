package com.zhucai.credit.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.zhucai.credit.model.FinanceRepaymentEntity;

/**
 * 
 * @author hsg
 *（付款实例）Repository
 */
@Repository
public interface FinanceRepaymentRepository extends
		PagingAndSortingRepository<FinanceRepaymentEntity, Long>,
		JpaSpecificationExecutor<FinanceRepaymentEntity> {


	/**
	 * 根据外部id查询还款记录
	 * @param delete
	 * @param tid
	 * @return
	 * @author hsg
	 */
	@Query(value="select fpr.id,fea.enterprise_name,fpr.payment_time,fpr.payment_amount from finance_payment_record fpr"
			+ " left join finance_ent_account fea on fea.org_num = fpr.org_num and fpr.ent_type = fea.ent_type"
			+ " where fpr.status <> ?1 and fpr.out_id=?2",nativeQuery=true)
	List<Object[]> findByStatusNotAndOutId(int delete, Long tid);

	/**
	 * 根据反馈序号查询还款记录
	 * @param delete
	 * @param serial_num
	 * @return
	 * @author hsg
	 */
	List<FinanceRepaymentEntity> findByStatusNotAndSerialNum(int delete, Integer serial_num);

	/**
	 * 根据反馈序号和发票号查询还款记录
	 * @param delete
	 * @param serial_num
	 * @param invoiceNoCode
	 * @return
	 */
	List<FinanceRepaymentEntity> findByStatusNotAndSerialNumAndInvoiceNoCode(
			int delete, Integer serial_num, String invoiceNoCode);
	





}
