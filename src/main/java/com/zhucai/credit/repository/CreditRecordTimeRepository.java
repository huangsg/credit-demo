package com.zhucai.credit.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.zhucai.credit.model.CreditRecordTimeEntrity;

/**
 * 
 * @author hsg
 * ContractListRepository
 */
@Repository
public interface CreditRecordTimeRepository extends
		PagingAndSortingRepository<CreditRecordTimeEntrity, Long>,
		JpaSpecificationExecutor<CreditRecordTimeEntrity> {


	/**
	 * 根据id逻辑删除
	 * @param delete
	 * @param orderId
	 * @return
	 */
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value="update finance_cr_record_time set status = ?1 where status <> ?1 and id = ?2", nativeQuery=true)
	Integer deleteRecordTimeById(int delete, String id);

	List<CreditRecordTimeEntrity> findByCrRecordIdAndStatusNot(Long recordId,
			int delete);

	/**
	 * 根据融资记录id物理删除
	 * @param delete
	 * @param orderId
	 * @return
	 */
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value="delete from  finance_cr_record_time where  cr_record_id = ?1", nativeQuery=true)
	Integer deleteRecordTimeByRecordId(Long recordId);

	/**
	 * 根据状态和融资id查询历时记录
	 * @version 2018年7月16日
	 * @param recordId
	 * @param recordStatus
	 * @return
	 */
	@Query(value="select * from finance_cr_record_time where cr_record_id = ?1 and recording_time >= "
			+ " (select max(fcrt.recording_time) from finance_cr_record_time fcrt where fcrt.cr_record_id = ?1 "
			+ "and fcrt.recording_type = ?2 and status <> 3)",nativeQuery=true)
	List<CreditRecordTimeEntrity> findByCrRecordIdAndRecordStatus(Long recordId,
			String recordStatus);
	/**
	 * 获取倒叙融资记录
	 * @version 2018年7月16日
	 * @param recordId
	 * @param recordStatus
	 * @return
	 */
	@Query(value="select * from finance_cr_record_time where cr_record_id = ?1  and status <> 3 order by id desc"
			,nativeQuery=true)
	List<CreditRecordTimeEntrity> findRecordTimeInfo(Long recordId);
	/**
	 * 根据融资id查询历时记录
	 * @version 2018年7月16日
	 * @param recordId
	 * @param recordStatus
	 * @return
	 */
	@Query(value="select * from finance_cr_record_time where cr_record_id = ?1",nativeQuery=true)
	List<CreditRecordTimeEntrity> findByCrRecordId(Long recordId);

	/**
	 * 更新还款的时间（取最后成功）
	 * @param delete
	 * @param orderId
	 * @return
	 */
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value="update finance_cr_record_time set recording_time = ?3 where status <> 3 and cr_record_id = ?1 and recording_type = ?2", nativeQuery=true)
	void updateTime(Long tid, String recording_type, Date date);

	/**
	 * 根据融资记录id查询融资历时记录表信息
	 * @param tid
	 * @return
	 */
	@Query(value = "select * from finance_cr_record_time where cr_record_id = ?1 and recording_type =?2 and status <> 3",nativeQuery = true)
	List<CreditRecordTimeEntrity> findByCrRecordIdAndType(Long tid,String timeType);

	
	/**
	 * 根据融资记录id更新历时记录
	 * @param tid
	 * @param remark
	 * @param type
	 * @param payTime
	 * @return
	 */
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value="update finance_cr_record_time set recording_remark = ?2,recording_type = ?3,recording_time = ?4 where cr_record_id = ?1",nativeQuery = true)
	int updateByCrRecordId(Long tid, String remark, String type, Date payTime); 

}
