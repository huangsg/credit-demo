package com.zhucai.credit.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.wordnik.swagger.annotations.ApiModelProperty;
/**
 * 贷款记录
 * @author hsg
 *
 */
@Entity
@Table(name="finance_cr_record")
public class CreditRecordEntrity extends BaseEntity {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "交易编号,(暂定)")
	private String recordNo;
	@ApiModelProperty(value = "报名编号")
	private String appNum;
	@ApiModelProperty(value = "贷款发起者的userid")
	private Long originatorUserId;
	@ApiModelProperty(value = "贷款发起者,(企业id)")
	private Long originatorEnterpriseId;
	@ApiModelProperty(value = "贷款发起者,(采购商的金融id)")
	private Long originator;
	@ApiModelProperty(value = "贷款发起者的组织机构id,(采购商)")
	private String originatorOrgnCode;
	@ApiModelProperty(value = "贷款参与者,（供应商的金融id）")
	private Long participator;
	@ApiModelProperty(value = "投标发起人userId,（供应商）")
	private Long participatorUserId;
	@ApiModelProperty(value = "融资确认userId,（供应商）")
	private String partRoleUserId;
	@ApiModelProperty(value = "贷款参与者的企业id,（供应商）")
	private Long participatorEnterpriseId;
	@ApiModelProperty(value = "贷款参与者的组织机构id,（供应商）")
	private String participatorOrgnCode;
	@ApiModelProperty(value = "贷款服务种类id,")
	private Long crProdId; 
	@ApiModelProperty(value = "付款单号,")
	private String paymentNum;
	@ApiModelProperty(value = "计划付款id,")
	private Long paymentId;
	@ApiModelProperty(value = "合同id,(筑材内部的合同id)")
	private Long contractId;
	@ApiModelProperty(value = "合同编号,(筑材内部的合同编号)")
	private String contractNum;
	@ApiModelProperty(value = "合同编号,(建行的合同)")
	private String contractNumCcb;
	@ApiModelProperty(value = "发票代码和发票号,")
	private String invoiceNoCode;
	@ApiModelProperty(value = "融资订单状态,Enum(CreditStatus)")
	private String creditStatus;
	@ApiModelProperty(value = "融资订单正常（异常）状态,Enum(CreditUnusualStatus)")
	private String creditUnusualStatus;
	@ApiModelProperty(value = "融资订单异常状态的原因,")
	private String rejectReason;
	@ApiModelProperty(value = "采购商和银行贷款金额（融资金额）,")
	private BigDecimal creditAmount;
	@ApiModelProperty(value = "采购商和供应商的产生的交易金额（账款金额）,")
	private BigDecimal creditGetAmount;
	@ApiModelProperty(value = "账款到期日（采购商和供应商的账款到期日）,")
	private Date loanDate;
	@ApiModelProperty(value = "融资到期日（采购商和供应商的账款到期日（建行返回的））,")
	private Date loanDateCcb;
	@ApiModelProperty(value = "已还款金额,")
	private BigDecimal repaymentAmount;
	@ApiModelProperty(value = "采购商已还款金额,")
	private BigDecimal purRepaymentAmount;
	@ApiModelProperty(value = "供应商已还款金额,")
	private BigDecimal supRepaymentAmount;
	@ApiModelProperty(value = "已支取金额,")
	private BigDecimal drawAmount;
	@ApiModelProperty(value = "支取率,")
	private BigDecimal drawRate;
	@ApiModelProperty(value = "交易时间,")
	private Date transactionTime;
	@ApiModelProperty(value = "账期,(单位 天)")
	private Integer creditPeriod;
	@ApiModelProperty(value = "筑材发票信息关联id")
	private Long invoiceTid;
	@ApiModelProperty(value = "项目信息关联id")
	private Long projectTid;
	@ApiModelProperty(value = "部门信息关联id")
	private Long departmentTid;
	
	/**
	 * 工商银行新加
	 * @version 2018年8月25日
	 * @return
	 */
	@ApiModelProperty(value = "为了查询工行信息冗余")
	private String qryfSeqno;
	@ApiModelProperty(value = "为了查询工行信息冗余")
	private String qrySerialNo;
	@ApiModelProperty(value = "筑材网审核时间")
	private Date auditTime;
	@Column(nullable=true)
	public Date getAuditTime() {
		return auditTime;
	}
	public void setAuditTime(Date auditTime) {
		this.auditTime = auditTime;
	}
	@Column(nullable=true,length=40)
	public String getQryfSeqno() {
		return qryfSeqno;
	}
	public void setQryfSeqno(String qryfSeqno) {
		this.qryfSeqno = qryfSeqno;
	}
	@Column(nullable=true,length=40)
	public String getQrySerialNo() {
		return qrySerialNo;
	}
	public void setQrySerialNo(String qrySerialNo) {
		this.qrySerialNo = qrySerialNo;
	}
	@Column(nullable=true,length=40)
	public String getCreditUnusualStatus() {
		return creditUnusualStatus;
	}
	public void setCreditUnusualStatus(String creditUnusualStatus) {
		this.creditUnusualStatus = creditUnusualStatus;
	}
	@Column(nullable=true)
	public Long getDepartmentTid() {
		return departmentTid;
	}
	public void setDepartmentTid(Long departmentTid) {
		this.departmentTid = departmentTid;
	}
	@Column(nullable=true)
	public Long getProjectTid() {
		return projectTid;
	}
	public void setProjectTid(Long projectTid) {
		this.projectTid = projectTid;
	}
	@Column(nullable=true)
	public Long getInvoiceTid() {
		return invoiceTid;
	}
	public void setInvoiceTid(Long invoiceTid) {
		this.invoiceTid = invoiceTid;
	}
	@Column(nullable=true,length=20)
	public String getAppNum() {
		return appNum;
	}
	public void setAppNum(String appNum) {
		this.appNum = appNum;
	}
	@Column(nullable=true,columnDefinition = "decimal(20,2)")
	public BigDecimal getPurRepaymentAmount() {
		return purRepaymentAmount;
	}
	public void setPurRepaymentAmount(BigDecimal purRepaymentAmount) {
		this.purRepaymentAmount = purRepaymentAmount;
	}
	@Column(nullable=true,columnDefinition = "decimal(20,2)")
	public BigDecimal getSupRepaymentAmount() {
		return supRepaymentAmount;
	}
	public void setSupRepaymentAmount(BigDecimal supRepaymentAmount) {
		this.supRepaymentAmount = supRepaymentAmount;
	}
	@Column(nullable=true,length=20)
	public String getOriginatorOrgnCode() {
		return originatorOrgnCode;
	}
	public void setOriginatorOrgnCode(String originatorOrgnCode) {
		this.originatorOrgnCode = originatorOrgnCode;
	}
	@Column(nullable=true,length=12)
	public String getParticipatorOrgnCode() {
		return participatorOrgnCode;
	}
	public void setParticipatorOrgnCode(String participatorOrgnCode) {
		this.participatorOrgnCode = participatorOrgnCode;
	}
	@Column(nullable=true,length=4,columnDefinition = "INT default 0")
	public Integer getCreditPeriod() {
		return creditPeriod;
	}
	public void setCreditPeriod(Integer creditPeriod) {
		this.creditPeriod = creditPeriod;
	}
	@Column(nullable=true,length=20)
	public Long getOriginator() {
		return originator;
	}
	public void setOriginator(Long originator) {
		this.originator = originator;
	}
	@Column(nullable=true,length=20)
	public Long getParticipator() {
		return participator;
	}
	public void setParticipator(Long participator) {
		this.participator = participator;
	}
	@Column(nullable=true,length=20)
	public Long getCrProdId() {
		return crProdId;
	}
	public void setCrProdId(Long crProdId) {
		this.crProdId = crProdId;
	}
	@Column(nullable=true,length=20)
	public String getPaymentNum() {
		return paymentNum;
	}
	public void setPaymentNum(String paymentNum) {
		this.paymentNum = paymentNum;
	}
	@Column(nullable=true,length=20)
	public Long getContractId() {
		return contractId;
	}
	public void setContractId(Long contractId) {
		this.contractId = contractId;
	}
	@Column(nullable=true,length=20)
	public String getContractNum() {
		return contractNum;
	}
	public void setContractNum(String contractNum) {
		this.contractNum = contractNum;
	}
	@Column(nullable=true,length=20)
	public String getCreditStatus() {
		return creditStatus;
	}
	public void setCreditStatus(String creditStatus) {
		this.creditStatus = creditStatus;
	}
	@Column(nullable=true,columnDefinition = "decimal(20,2)")
	public BigDecimal getCreditAmount() {
		return creditAmount;
	}
	public void setCreditAmount(BigDecimal creditAmount) {
		this.creditAmount = creditAmount;
	}
	@Column(nullable = true)
	public Date getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Date transactionTime) {
		this.transactionTime = transactionTime;
	}
	@Column(nullable = true, length = 20)
	public String getRecordNo() {
		return recordNo;
	}
	public void setRecordNo(String recordNo) {
		this.recordNo = recordNo;
	}
	@Column(nullable=true,length=20)
	public Date getLoanDate() {
		return loanDate;
	}
	public void setLoanDate(Date loanDate) {
		this.loanDate = loanDate;
	}
	@Column(nullable=true,columnDefinition = "decimal(20,2)")
	public BigDecimal getRepaymentAmount() {
		return repaymentAmount;
	}
	public void setRepaymentAmount(BigDecimal repaymentAmount) {
		this.repaymentAmount = repaymentAmount;
	}
	@Column(nullable=true,columnDefinition = "decimal(20,2)")
	public BigDecimal getDrawAmount() {
		return drawAmount;
	}
	public void setDrawAmount(BigDecimal drawAmount) {
		this.drawAmount = drawAmount;
	}
	@Column(nullable=true,columnDefinition = "decimal(20,2)")
	public BigDecimal getCreditGetAmount() {
		return creditGetAmount;
	}
	public void setCreditGetAmount(BigDecimal creditGetAmount) {
		this.creditGetAmount = creditGetAmount;
	}
	@Column(nullable=false,length=10)
	public Long getOriginatorUserId() {
		return originatorUserId;
	}
	public void setOriginatorUserId(Long originatorUserId) {
		this.originatorUserId = originatorUserId;
	}
	@Column(nullable=true,length=20)
	public Long getParticipatorUserId() {
		return participatorUserId;
	}
	public void setParticipatorUserId(Long participatorUserId) {
		this.participatorUserId = participatorUserId;
	}
	@Column(nullable=true,length=20)
	public Long getOriginatorEnterpriseId() {
		return originatorEnterpriseId;
	}
	public void setOriginatorEnterpriseId(Long originatorEnterpriseId) {
		this.originatorEnterpriseId = originatorEnterpriseId;
	}
	@Column(nullable=true,length=20)
	public Long getParticipatorEnterpriseId() {
		return participatorEnterpriseId;
	}
	public void setParticipatorEnterpriseId(Long participatorEnterpriseId) {
		this.participatorEnterpriseId = participatorEnterpriseId;
	}
	@Column(nullable=true,length=20)
	public Long getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(Long paymentId) {
		this.paymentId = paymentId;
	}
	@Column(nullable=true,length=100)
	public String getContractNumCcb() {
		return contractNumCcb;
	}
	public void setContractNumCcb(String contractNumCcb) {
		this.contractNumCcb = contractNumCcb;
	}
	@Column(nullable=true,length=20)
	public Date getLoanDateCcb() {
		return loanDateCcb;
	}
	public void setLoanDateCcb(Date loanDateCcb) {
		this.loanDateCcb = loanDateCcb;
	}
	@Column(nullable=true,length=20)
	public String getPartRoleUserId() {
		return partRoleUserId;
	}
	public void setPartRoleUserId(String partRoleUserId) {
		this.partRoleUserId = partRoleUserId;
	}
	@Column(nullable=true,columnDefinition = "decimal(20,2)")
	public BigDecimal getDrawRate() {
		return drawRate;
	}
	public void setDrawRate(BigDecimal drawRate) {
		this.drawRate = drawRate;
	}
	@Column(nullable=true,length=200)
	public String getRejectReason() {
		return rejectReason;
	}
	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}
	@Column(nullable=true,length=50)
	public String getInvoiceNoCode() {
		return invoiceNoCode;
	}
	public void setInvoiceNoCode(String invoiceNoCode) {
		this.invoiceNoCode = invoiceNoCode;
	}
	
}
