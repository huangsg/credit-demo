package com.zhucai.credit.bank.ccbe.bean;

import java.io.Serializable;
import java.math.BigDecimal;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * 生成报名信息查询
 * 6W5501
 * @date 2017年11月28日 @user hsg
 */
public class SignUpInfo implements Serializable{

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "当前最大反馈序号")
	private Integer max_serial_num;
	@ApiModelProperty(value = "报名客户序号")
	private Integer customer_app_serial_num;
	@ApiModelProperty(value = "组织机构代码")
	private String orgn_num;
	@ApiModelProperty(value = "报名编号")
	private String app_num;
	@ApiModelProperty(value = "企业名称")
	private String customer_name;
	@ApiModelProperty(value = "申请人姓名")
	private String applicant_name;
	@ApiModelProperty(value = "申请人固定电话")
	private String applicant_fix_phone;
	@ApiModelProperty(value = "申请人手机")
	private String applicant_phone;
	@ApiModelProperty(value = "在合作方的客户ID")
	private String partner_customer_id;
	@ApiModelProperty(value = "申贷金额")
	private BigDecimal apply_amt;
	@ApiModelProperty(value = "公司简称")
	private String customer_short_name;
	@ApiModelProperty(value = "上年度销售收入（元）")
	private BigDecimal xssr_snd;
	@ApiModelProperty(value = "所属行业名称")
	private String belong_industry;
	@ApiModelProperty(value = "合作企业组织机构号")
	private String Co_Entp_Org_InsNo;
	@ApiModelProperty(value = "平台名称")
	private String Pltfrm_Nm;
	public Integer getMax_serial_num() {
		return max_serial_num;
	}
	public void setMax_serial_num(Integer max_serial_num) {
		this.max_serial_num = max_serial_num;
	}
	public Integer getCustomer_app_serial_num() {
		return customer_app_serial_num;
	}
	public void setCustomer_app_serial_num(Integer customer_app_serial_num) {
		this.customer_app_serial_num = customer_app_serial_num;
	}
	public String getOrgn_num() {
		return orgn_num;
	}
	public void setOrgn_num(String orgn_num) {
		this.orgn_num = orgn_num;
	}
	public String getApp_num() {
		return app_num;
	}
	public void setApp_num(String app_num) {
		this.app_num = app_num;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public String getApplicant_name() {
		return applicant_name;
	}
	public void setApplicant_name(String applicant_name) {
		this.applicant_name = applicant_name;
	}
	public String getApplicant_fix_phone() {
		return applicant_fix_phone;
	}
	public void setApplicant_fix_phone(String applicant_fix_phone) {
		this.applicant_fix_phone = applicant_fix_phone;
	}
	public String getApplicant_phone() {
		return applicant_phone;
	}
	public void setApplicant_phone(String applicant_phone) {
		this.applicant_phone = applicant_phone;
	}
	public String getPartner_customer_id() {
		return partner_customer_id;
	}
	public void setPartner_customer_id(String partner_customer_id) {
		this.partner_customer_id = partner_customer_id;
	}
	public BigDecimal getApply_amt() {
		return apply_amt;
	}
	public void setApply_amt(BigDecimal apply_amt) {
		this.apply_amt = apply_amt;
	}
	public String getCustomer_short_name() {
		return customer_short_name;
	}
	public void setCustomer_short_name(String customer_short_name) {
		this.customer_short_name = customer_short_name;
	}
	public BigDecimal getXssr_snd() {
		return xssr_snd;
	}
	public void setXssr_snd(BigDecimal xssr_snd) {
		this.xssr_snd = xssr_snd;
	}
	public String getBelong_industry() {
		return belong_industry;
	}
	public void setBelong_industry(String belong_industry) {
		this.belong_industry = belong_industry;
	}
	public String getCo_Entp_Org_InsNo() {
		return Co_Entp_Org_InsNo;
	}
	public void setCo_Entp_Org_InsNo(String co_Entp_Org_InsNo) {
		Co_Entp_Org_InsNo = co_Entp_Org_InsNo;
	}
	public String getPltfrm_Nm() {
		return Pltfrm_Nm;
	}
	public void setPltfrm_Nm(String pltfrm_Nm) {
		Pltfrm_Nm = pltfrm_Nm;
	}
	
}
