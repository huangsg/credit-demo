package com.zhucai.credit.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.zhucai.credit.model.Message;

/**
 * 
 * @author hsg
 * Message
 */
@Repository
public interface MessageRepository extends
		PagingAndSortingRepository<Message, Long>,
		JpaSpecificationExecutor<Message> {


	@Query(value="select a.* from tb_xa_message a where a.content = ?1",nativeQuery = true)
    public List<Message> findMessageByContent(String content);

	





}
