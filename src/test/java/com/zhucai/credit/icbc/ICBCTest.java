package com.zhucai.credit.icbc;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.jsoup.nodes.Document;

import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.io.Files;
import com.zhucai.credit.utils.StrongJsoup;

public class ICBCTest {
	public static void main(String[] args) throws Exception {
		List<String> readLines = Files.readLines(new File("icbc/QGYLPAY.txt"), Charsets.UTF_8);

		System.out.println(Joiner.on("\r\n").join(readLines));
		
		String url = "http://127.0.0.1:448/servlet/ICBCCMPAPIReqServlet?userID=lubw.y.4402&PackageID=2&SendTime=20180801";
		StrongJsoup connect = StrongJsoup.connect(url);
		connect.headers(ImmutableMap.of("Content-Type", "application/x-www-form-urlencoded"));
		
		Map<String,String> params = Maps.newHashMap();
		params.put("Version", "0.0.0.1");
		params.put("TransCode","QGYLPAY");
		params.put("BankCode","102");
		params.put("GroupCIS","440290000079081");
		params.put("ID","lubw.y.4402");
		params.put("Cert","");
		params.put("reqData",Joiner.on("\r\n").join(readLines));
		
		connect.getJsoupConn().data(params);
		
		Document post = connect.post();
		String text = post.text();
		System.out.println(text);
		
		Base64 base64 = new Base64();
		String substring = text.substring(text.indexOf("=")+1);
		System.out.println(substring);
		byte[] decode = base64.decode(substring);
		System.out.println(new String(decode,"GBK"));
        for(String l : readLines) {
        	//System.out.println(l);
        }
        
        
        
	}
}
