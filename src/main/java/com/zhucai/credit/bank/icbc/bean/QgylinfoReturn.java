package com.zhucai.credit.bank.icbc.bean;

import java.io.Serializable;
/**
 * 合同签订查询返回实体
 * @author xjz
 *
 */
public class QgylinfoReturn implements Serializable{


	private static final long serialVersionUID = 1L;
	
	//核心企业账号
	private String accNo;
	//会员企业账号
	private String memberAccNo;
	//供应链编号
	private String gylCode;
	//会员企业代码类型
	private String MemberType;
	//会员企业单位代码
	private String memberCode;
	//应付账款状态
	private String status;
	//核心企业CIS代码
	private String accCis;
	//起始日期(YYYYMMDD)
	private String beginDate;
	//结束日期(YYYYMMDD)
	private String endDate;
	//查询下页标识
	private String nextTag;
	//响应备用字段1
	private String repReserved1;
	//响应备用字段2
	private String repReserved2;
	//应付账款序号
	private String dueNo;
	//单位名称
	private String fullName;
	//供应商名称
	private String MemberName;
	//供应商CIS代码
	private String memberCIS;
	//供应商收款账号
	private String memberRecAcc;
	//应付账款金额
	private Long dueAmt;
	//应付账款余额
	private Long dueBal;
	//应付账款生成日期
	private String dueDate;
	//计划付款日期
	private String planDate;
	//应付账款状态
	private Integer rdStatus;
	//凭证种类 01 发票(本期只支持发票)
	private String vouchType;
	//凭证代码
	private String vouchCode;
	//凭证号码
	private String vouchNo;
	//响应备用字段3
	private String repReserved3;
	//响应备用字段4
	private String repReserved4;
	public String getAccNo() {
		return accNo;
	}
	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}
	public String getMemberAccNo() {
		return memberAccNo;
	}
	public void setMemberAccNo(String memberAccNo) {
		this.memberAccNo = memberAccNo;
	}
	public String getGylCode() {
		return gylCode;
	}
	public void setGylCode(String gylCode) {
		this.gylCode = gylCode;
	}
	public String getMemberType() {
		return MemberType;
	}
	public void setMemberType(String memberType) {
		MemberType = memberType;
	}
	public String getMemberCode() {
		return memberCode;
	}
	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAccCis() {
		return accCis;
	}
	public void setAccCis(String accCis) {
		this.accCis = accCis;
	}
	public String getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getNextTag() {
		return nextTag;
	}
	public void setNextTag(String nextTag) {
		this.nextTag = nextTag;
	}
	public String getRepReserved1() {
		return repReserved1;
	}
	public void setRepReserved1(String repReserved1) {
		this.repReserved1 = repReserved1;
	}
	public String getRepReserved2() {
		return repReserved2;
	}
	public void setRepReserved2(String repReserved2) {
		this.repReserved2 = repReserved2;
	}
	public String getDueNo() {
		return dueNo;
	}
	public void setDueNo(String dueNo) {
		this.dueNo = dueNo;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getMemberName() {
		return MemberName;
	}
	public void setMemberName(String memberName) {
		MemberName = memberName;
	}
	public String getMemberCIS() {
		return memberCIS;
	}
	public void setMemberCIS(String memberCIS) {
		this.memberCIS = memberCIS;
	}
	public String getMemberRecAcc() {
		return memberRecAcc;
	}
	public void setMemberRecAcc(String memberRecAcc) {
		this.memberRecAcc = memberRecAcc;
	}
	public Long getDueAmt() {
		return dueAmt;
	}
	public void setDueAmt(Long dueAmt) {
		this.dueAmt = dueAmt;
	}
	public Long getDueBal() {
		return dueBal;
	}
	public void setDueBal(Long dueBal) {
		this.dueBal = dueBal;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getPlanDate() {
		return planDate;
	}
	public void setPlanDate(String planDate) {
		this.planDate = planDate;
	}
	public Integer getRdStatus() {
		return rdStatus;
	}
	public void setRdStatus(Integer rdStatus) {
		this.rdStatus = rdStatus;
	}
	public String getVouchType() {
		return vouchType;
	}
	public void setVouchType(String vouchType) {
		this.vouchType = vouchType;
	}
	public String getVouchCode() {
		return vouchCode;
	}
	public void setVouchCode(String vouchCode) {
		this.vouchCode = vouchCode;
	}
	public String getVouchNo() {
		return vouchNo;
	}
	public void setVouchNo(String vouchNo) {
		this.vouchNo = vouchNo;
	}
	public String getRepReserved3() {
		return repReserved3;
	}
	public void setRepReserved3(String repReserved3) {
		this.repReserved3 = repReserved3;
	}
	public String getRepReserved4() {
		return repReserved4;
	}
	public void setRepReserved4(String repReserved4) {
		this.repReserved4 = repReserved4;
	}
	
	
}
