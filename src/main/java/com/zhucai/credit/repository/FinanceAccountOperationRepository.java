package com.zhucai.credit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.zhucai.credit.model.FinanceAccountOperationEntity;

/**
 * 融资记录Repository
 */
@Repository
public interface FinanceAccountOperationRepository extends 
JpaSpecificationExecutor<FinanceAccountOperationEntity>,
JpaRepository<FinanceAccountOperationEntity,Long> {



@Modifying(clearAutomatically = true)
@Transactional
@Query(value="update finance_account_operation fao set fao.bank_id = ?1 where fao.acc_pur_id= ?2 and fao.acc_sup_id = ?3",nativeQuery=true)
public Integer updateBankId(Long bankId,Long purAccId, Long supAccId);

public FinanceAccountOperationEntity findByAccPurIdAndAccSupIdAndStatusNot(
		Long purId, Long supId, int delete);

@Modifying(clearAutomatically = true)
@Transactional
@Query(value="delete from finance_account_operation  where acc_pur_id=?1 and acc_sup_id = ?2",nativeQuery=true)
public Integer delByPurIdAndSupId(Long purEntId, Long supEntId);

@Transactional
@Query(value="delete from finance_account_operation  where id = ?1",nativeQuery=true)
@Modifying
public Integer delByTid(Long tid);
}
