package com.zhucai.credit.bank.ccbe.bean;

import java.io.Serializable;
import java.util.Date;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * 付款申请信息接收
 * 6W5548
 * @date 2017年11月28日 @user xjz
 */
public class PayApplicationReceiveInfo implements Serializable{


	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "交易请求方组织机构代码")
	private String request_orgn_num;
	@ApiModelProperty(value = "客户来源")
	private String customer_source;
	@ApiModelProperty(value = "合同编号")
	private String contract_num;
	@ApiModelProperty(value = "在合作方客户ID")
	private String cooper_cust_num;
	@ApiModelProperty(value = "客户组织机构代码")
	private String cust_orgn_code;
	@ApiModelProperty(value = "单据号")
	private String bill_num;
	@ApiModelProperty(value = "币种代码")
	private String currency_cd;
	@ApiModelProperty(value = "付款金额")
	private String payment_amt;
	@ApiModelProperty(value = "付款日期")
	private Date payment_date;
	@ApiModelProperty(value = "付款方式")
	private String payment_mode;
	@ApiModelProperty(value = "付款金额（现金）")
	private Double payment_cash_amt;
	@ApiModelProperty(value = "付款金额（银承）")
	private Double payment_silver_amt;
	@ApiModelProperty(value = "账户类型")
	private String account_type;
	public String getRequest_orgn_num() {
		return request_orgn_num;
	}
	public void setRequest_orgn_num(String request_orgn_num) {
		this.request_orgn_num = request_orgn_num;
	}
	public String getCustomer_source() {
		return customer_source;
	}
	public void setCustomer_source(String customer_source) {
		this.customer_source = customer_source;
	}
	public String getContract_num() {
		return contract_num;
	}
	public void setContract_num(String contract_num) {
		this.contract_num = contract_num;
	}
	public String getCooper_cust_num() {
		return cooper_cust_num;
	}
	public void setCooper_cust_num(String cooper_cust_num) {
		this.cooper_cust_num = cooper_cust_num;
	}
	public String getCust_orgn_code() {
		return cust_orgn_code;
	}
	public void setCust_orgn_code(String cust_orgn_code) {
		this.cust_orgn_code = cust_orgn_code;
	}
	
	public Date getPayment_date() {
		return payment_date;
	}
	public void setPayment_date(Date payment_date) {
		this.payment_date = payment_date;
	}
	public String getPayment_mode() {
		return payment_mode;
	}
	public void setPayment_mode(String payment_mode) {
		this.payment_mode = payment_mode;
	}
	
	public Double getPayment_cash_amt() {
		return payment_cash_amt;
	}
	public void setPayment_cash_amt(Double payment_cash_amt) {
		this.payment_cash_amt = payment_cash_amt;
	}
	public Double getPayment_silver_amt() {
		return payment_silver_amt;
	}
	public void setPayment_silver_amt(Double payment_silver_amt) {
		this.payment_silver_amt = payment_silver_amt;
	}
	public String getAccount_type() {
		return account_type;
	}
	public void setAccount_type(String account_type) {
		this.account_type = account_type;
	}
	public String getBill_num() {
		return bill_num;
	}
	public void setBill_num(String bill_num) {
		this.bill_num = bill_num;
	}
	public String getCurrency_cd() {
		return currency_cd;
	}
	public void setCurrency_cd(String currency_cd) {
		this.currency_cd = currency_cd;
	}
	public String getPayment_amt() {
		return payment_amt;
	}
	public void setPayment_amt(String payment_amt) {
		this.payment_amt = payment_amt;
	}
	

}
