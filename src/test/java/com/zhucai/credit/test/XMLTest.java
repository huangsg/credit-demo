package com.zhucai.credit.test;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import com.thoughtworks.xstream.io.xml.XppDriver;

public class XMLTest {
	 public static void main(String[] args) {

	        Map<String,String> map = new HashMap<String,String>();
	        map.put("na_me","chris");
	        map.put("isl_and","faranga");
	        
	        XStream magicApi = new XStream(new XppDriver(new XmlFriendlyNameCoder("_-", "_")));
	        XStream.setupDefaultSecurity(magicApi);
	        magicApi.registerConverter(new MapEntryConverter());
	        magicApi.alias("root", Map.class);

	        String xml = magicApi.toXML(map);
	        System.out.println("Result of tweaked XStream toXml()");
	        System.out.println(xml);

	        Map<String, String> extractedMap = (Map<String, String>) magicApi.fromXML(xml);
	        for(String key : extractedMap.keySet()) {
	        	System.out.println(key);
	        }
	        assert extractedMap.get("na_me").equals("chris");
	        assert extractedMap.get("isl_and").equals("faranga");

	    }

	    public static class MapEntryConverter implements Converter {

	        public boolean canConvert(Class clazz) {
	            return AbstractMap.class.isAssignableFrom(clazz);
	        }

	        public void marshal(Object value, HierarchicalStreamWriter writer, MarshallingContext context) {

	            AbstractMap map = (AbstractMap) value;
	            for (Object obj : map.entrySet()) {
	                Map.Entry entry = (Map.Entry) obj;
	                writer.startNode(entry.getKey().toString());
	                Object val = entry.getValue();
	                if ( null != val ) {
	                    writer.setValue(val.toString());
	                }
	                writer.endNode();
	            }

	        }

	        public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {

	            Map<String, String> map = new HashMap<String, String>();

	            while(reader.hasMoreChildren()) {
	                reader.moveDown();

	                String key = reader.getNodeName(); // nodeName aka element's name
	                String value = reader.getValue();
	                map.put(key, value);

	                reader.moveUp();
	            }

	            return map;
	        }

	    }
}
