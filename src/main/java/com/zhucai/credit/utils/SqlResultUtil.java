package com.zhucai.credit.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * sql返回数据安全获取工具类
 * @author zj
 *
 */
public class SqlResultUtil {
	/**
	 * 获取字符串
	 * @param o
	 * @return
	 */
	public static String getSqlResultString(Object o){
		if(o == null){
			return "";
		}
		return String.valueOf(o);
	}
	/**
	 * 获取组织机构号
	 * @param o
	 * @return
	 */
	public static String getOrgNum(String o){
		String sub_unNo =  o.substring(8, 17);
		StringBuffer orgNum=new StringBuffer();
		orgNum.append(sub_unNo.substring(0, 8));
		orgNum.append("-");
		orgNum.append(sub_unNo.substring(8));
		return orgNum.toString();
	}

	/**
	 * 获取long
	 * @param o
	 * @return
	 */
	public static Long getSqlResultLong(Object o){
		if(getSqlResultString(o) != null){
			try{
				return Long.parseLong(getSqlResultString(o));
			}
			catch(Exception e){
				return null;
			}
		}
		return null;
	}
	 /**
     * 获取平台服务费算法
     * @param creditAmt 融资总金额
     * @param creditPeriod 账期
     * @return
     */
	public static BigDecimal getOrderAmt(BigDecimal creditAmt, Integer creditPeriod,BigDecimal rate){
    	if(creditAmt!=null){
    		//BigDecimal orderAmt = BigDecimal.valueOf(creditAmt.multiply(BigDecimal.valueOf(0.001)).doubleValue());
    		//计算规则：融资总金额  * 年化率(1%) * 账期 /360天(1年) (注：向上保留小数点后两位)
    		BigDecimal orderAmt = creditAmt.multiply(rate).multiply(new BigDecimal(creditPeriod-1)).divide(new BigDecimal(360).multiply(new BigDecimal(100)),2,RoundingMode.CEILING);
    		
    		if(orderAmt.compareTo(BigDecimal.ZERO)==-1){
    			orderAmt = BigDecimal.ZERO;
    		}
    		return orderAmt;
    	}else{
    		return BigDecimal.ZERO;
    	}
    	
    }



	/**
	 * 获取整型
	 * @param o
	 * @return
	 */
	public static Integer getSqlResultInteger(Object o){
		if(getSqlResultString(o) != null){
			try{
				return Integer.parseInt(getSqlResultString(o));
			}
			catch(Exception e){
				return null;
			}
		}
		return null;
	}

	/**
	 * 获取double
	 * @param o
	 * @return
	 */
	public static Double getSqlResultDouble(Object o){
		if(getSqlResultString(o) != null){
			try{
				return Double.parseDouble(getSqlResultString(o));
			}
			catch(Exception e){
				return null;
			}
		}
		return null;
	}

	public static String getSqlResultDateStringToString(Object o){
		if(getSqlResultString(o) != null){
			try{
				if(getSqlResultString(o).length() >= 19){
					return getSqlResultString(o).substring(0,19);
				}
				else if(getSqlResultString(o).length() >= 10){
					return getSqlResultString(o).substring(0,10);
				}
			}
			catch(Exception e){
				return null;
			}
		}
		return null;
	}
	/**
	 * 获取float
	 * @param o
	 * @return
	 */
	public static Float getSqlResultFloat(Object o){
		if(getSqlResultString(o) != null){
			try{
				return Float.parseFloat(getSqlResultString(o));
			}
			catch(Exception e){
				return null;
			}
		}
		return null;
	}
	/**
	 * 获取BigDecimal
	 * @param o
	 * @return
	 */
	public static BigDecimal getSqlResultBigDecimal(Object o){
		if(getSqlResultString(o) != null){
			
			try{
				return new BigDecimal(String.valueOf(o));
			}
			catch(Exception e){
				return BigDecimal.ZERO;
			}
		}
		return BigDecimal.ZERO;
	}

	/**
	 * 获取日期
	 * @param o
	 * @return
	 */
	public static Date getSqlResultDate(Object o){
		if(getSqlResultLong(o) != null){
			try{
				return new Date(getSqlResultLong(o) * 1000);
			}
			catch(Exception e){
				return null;
			}
		}
		else if(getSqlResultString(o) != null){
			try{
				return TimeUtils.getStringToDate(getSqlResultString(o));
			}
			catch(Exception e){
				return null;
			}
		}
		return null;
	}
	/**
	 * 获取日期
	 * @param o
	 * @return
	 */
	public static Date getSqlResultDate2(String o){
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
          Date de = null;
			try {
				de = sdf.parse(o);
			} catch (java.text.ParseException e) {
				return null;
			}
		return de;
	}
	/**
	 * 获取日期
	 * @param o
	 * @return
	 */
	public static Date getSqlResultDate3(String o){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date de = null;
		try {
			de = sdf.parse(o);
		} catch (java.text.ParseException e) {
			return null;
		}
		return de;
	}
	/**
	 * 获取日期
	 * @param o
	 * @return
	 */
	public static Date getSqlResultDate4(String o){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date de = null;
		try {
			de = sdf.parse(o);
		} catch (java.text.ParseException e) {
			return null;
		}
		return de;
	}
	
	
}
