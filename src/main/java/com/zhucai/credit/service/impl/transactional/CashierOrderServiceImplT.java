package com.zhucai.credit.service.impl.transactional;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.zhucai.credit.bean.PaymentOrder;
import com.zhucai.credit.bean.PaymentRelation;
import com.zhucai.credit.common.CommonEnums.EntType;
import com.zhucai.credit.common.CommonEnums.PaymentType;
import com.zhucai.credit.common.CommonEnums.TradeStatus;
import com.zhucai.credit.common.CommonEnums.ZhuCaiProdType;
import com.zhucai.credit.common.HelpConstant;
import com.zhucai.credit.common.MyPage;
import com.zhucai.credit.model.CreditRecordEntrity;
import com.zhucai.credit.model.EntCrProdEntity;
import com.zhucai.credit.model.PaymentOrderEntity;
import com.zhucai.credit.model.PaymentOrderItemEntity;
import com.zhucai.credit.model.PaymentRelationEntity;
import com.zhucai.credit.repository.CreditRecordRepository;
import com.zhucai.credit.repository.EntCrProdEntityRepository;
import com.zhucai.credit.repository.PaymentOrderItemRepository;
import com.zhucai.credit.repository.PaymentOrderRepository;
import com.zhucai.credit.repository.PaymentRelationRepository;
import com.zhucai.credit.service.cashier.CashierOrderService;
import com.zhucai.credit.service.impl.loop.OrderLoopService;
import com.zhucai.credit.util.StringUtilsTo;
import com.zhucai.credit.util.TimeUtils;
import com.zhucai.credit.utils.SqlResultUtil;


/**
 * 收银台订单接口实现
 * @author hsg
 *
 */
@Service("cashierOrderService")
@com.alibaba.dubbo.config.annotation.Service(version="1.0.0")
public class CashierOrderServiceImplT implements CashierOrderService {

	private static final Logger logger = LoggerFactory.getLogger(CreditUserServiceImplT.class);

	@Autowired
	private CreditRecordRepository creditRecordRepository;
	@Autowired
	private PaymentOrderRepository paymentOrderRepository;
	@Autowired
	private OrderLoopService orderLoopService;
	@Autowired
	private EntCrProdEntityRepository entCrProdEntityRepository;
	@Autowired
	private PaymentOrderItemRepository paymentOrderItemRepository;
	@Autowired
	private PaymentRelationRepository paymentRelationRepository;
	
	@Override
	public List<PaymentOrder> findByOrderNo(String OrderNo,Integer accountType) {
		
		logger.info("查询订单信息start,parm={}",OrderNo);
		List<PaymentOrder> poList = new ArrayList<>();
		List<String> recordNoList = new ArrayList<>();
		PaymentRelationEntity relationEntity = paymentRelationRepository.findByPaySingleNo(OrderNo);
		String[] recordArr = relationEntity.getRecordNo().split(",");
		for (String record : recordArr) {
			recordNoList.add(record);
		}
		//根据融资单号查询订单信息
		if(recordNoList.size() != 0) {
			List<Object[]> objList = null;
			List<PaymentOrderEntity> poeList = new ArrayList<>();
			for (String record : recordNoList) {
				// 1采购商,2供应商
				if(accountType == 1) {
					objList = paymentOrderRepository.findAllByOrderNo1(HelpConstant.Status.delete,PaymentType.FINANCE_CREDIT.toString(),record);
					//objList = paymentOrderItemRepository.findAllByOrderNo1(HelpConstant.Status.delete,PaymentType.FINANCE_CREDIT.toString(),record);
				}else {
					objList = paymentOrderRepository.findAllByOrderNo(HelpConstant.Status.delete,PaymentType.FINANCE_CREDIT.toString(),record);
					//objList = paymentOrderItemRepository.findAllByOrderNo(HelpConstant.Status.delete,PaymentType.FINANCE_CREDIT.toString(),record);
				}
				
				
//				fpo.create_user,       fpo.collect_enterprise_name,          fpo.create_order_time,         fpo.discount_amt,           fpo.flow_number," +
//				fpo.order_amt,         fpo.order_no,                         fpo.pay_enterprise_id,         fpo.pay_enterprise_name,    fpo.pay_status,
//				fpo.pay_time,          fpo.pay_type,                         fpo.product_name,              fpo.product_remark,         fpo.service_no,
//				fpo.trade_status,      fpt.cr_prod_bank_name "
				if(objList.size() > 0) {
					for (Object[] obj : objList) {
						PaymentOrderEntity orderEntity = new PaymentOrderEntity();
						orderEntity.setCreateUser(SqlResultUtil.getSqlResultLong(obj[0]));
						orderEntity.setCollectEnterpriseName(SqlResultUtil.getSqlResultString(obj[1]));
						orderEntity.setCreateOrderTime(SqlResultUtil.getSqlResultDate(obj[2]));
						orderEntity.setDiscountAmt(SqlResultUtil.getSqlResultBigDecimal(obj[3]));
						orderEntity.setFlowNumber(SqlResultUtil.getSqlResultString(obj[4]));
						orderEntity.setOrderAmt(SqlResultUtil.getSqlResultBigDecimal(obj[5]));
						orderEntity.setOrderNo(SqlResultUtil.getSqlResultString(obj[6]));
						orderEntity.setPayEnterpriseId(SqlResultUtil.getSqlResultLong(obj[7]));
						orderEntity.setPayEnterpriseName(SqlResultUtil.getSqlResultString(obj[8]));
						orderEntity.setPayStatus(SqlResultUtil.getSqlResultString(obj[9]));
						orderEntity.setPayTime(SqlResultUtil.getSqlResultDate(obj[10]));
						orderEntity.setPayType(SqlResultUtil.getSqlResultString(obj[11]));
						orderEntity.setProductName(SqlResultUtil.getSqlResultString(obj[12]));
						orderEntity.setProductRemark(SqlResultUtil.getSqlResultString(obj[13]));
						orderEntity.setServiceNo(SqlResultUtil.getSqlResultString(obj[14]));
						orderEntity.setTradeStatus(SqlResultUtil.getSqlResultString(obj[15]));
						orderEntity.setFinanceBank(SqlResultUtil.getSqlResultString(obj[16]));
						poeList.add(orderEntity);
					}
				}
			}
			//List<PaymentOrderEntity> poeList = paymentOrderRepository.findPaymentOrderByOrderNoList(HelpConstant.Status.delete,PaymentType.FINANCE_CREDIT.toString(),recordNoList);
			if(poeList.size() == 0) {
				return null;
			}	
			BigDecimal allOrderAmt = BigDecimal.ZERO;
			for (PaymentOrderEntity poe : poeList) {
				PaymentOrder po = new PaymentOrder();
				CreditRecordEntrity cre = creditRecordRepository.findByStatusNotAndRecordNo(HelpConstant.Status.delete, poe.getOrderNo());
				EntCrProdEntity ecpe = entCrProdEntityRepository.findByEnAccId(cre.getParticipator(),cre.getCrProdId(),EntType.SUPPLIER.toString(),ZhuCaiProdType.ZHU_CAI_B_TONG.toString());
				//重新计算订单金额
				BigDecimal orderAmt = SqlResultUtil.getOrderAmt(cre.getCreditGetAmount(), TimeUtils.dateTian(cre.getLoanDate(), new Date()), ecpe.getCrProdRate());
				poe.setOrderAmt(orderAmt);
				poe.setDiscountAmt(orderAmt);
				//将重新计算的订单金额保存到数据库
				int i = paymentOrderRepository.updateOrderAmtByOrderNo(orderAmt,orderAmt,poe.getOrderNo(),3);
				if(i != 1) {
					throw new RuntimeException("重新一算订单金额出现异常");
				}
				PaymentOrderEntity entity = paymentOrderRepository.findByOrderNoAndStatusNot(poe.getOrderNo(),3);
				
				allOrderAmt = allOrderAmt.add(entity.getOrderAmt());
				BeanUtils.copyProperties(entity, po);
				po.setFinanceBank(poe.getFinanceBank());
				po.setPayEnterpriseName(poe.getPayEnterpriseName());
				po.setPayStatus(poe.getPayStatus());
				//判断此订单是否发起线下支付
				if("05".equals(poe.getPayStatus())){
					po.setLinePayBol(true);
				}else{
					po.setLinePayBol(false);
				}
				if(ecpe != null){
					po.setRate(ecpe.getCrProdRate());
				}
				po.setPaySingleNo(OrderNo);
				poList.add(po);
			}
		}
		return poList;
		
		//PaymentOrderEntity poe = paymentOrderRepository.findPaymentOrderByOrderNo(HelpConstant.Status.delete, PaymentType.FINANCE_CREDIT.toString(), OrderNo);
//		
//		CreditRecordEntrity cre = creditRecordRepository.findByStatusNotAndRecordNo(HelpConstant.Status.delete, OrderNo);
//		Long participator = cre.getParticipator();
//		EntCrProdEntity ecpe = entCrProdEntityRepository.findByEnAccId(participator,cre.getCrProdId(),EntType.SUPPLIER.toString());
//		//重新计算订单金额
//		BigDecimal orderAmt = SqlResultUtil.getOrderAmt(cre.getCreditGetAmount(), TimeUtils.dateTian(cre.getLoanDate(), new Date()), ecpe.getCrProdRate());
//		if(poe==null){
//			return null;
//		}
//		poe.setOrderAmt(orderAmt);
//		poe.setDiscountAmt(orderAmt);
//		//将重新计算的订单金额保存到数据库
//		paymentOrderRepository.save(poe);
//		PaymentOrderEntity entity = paymentOrderRepository.findPaymentOrderByOrderNo(HelpConstant.Status.delete, PaymentType.FINANCE_CREDIT.toString(), OrderNo);
//		BeanUtils.copyProperties(entity, po);
//		//判断此订单是否发起线下支付
//		if(cre!=null && cre.getCreditStatus().equals(CreditStatus.OFFLINE_PAY_AFFIRM.toString())){
//			po.setLinePayBol(true);
//		}else{
//			po.setLinePayBol(false);
//		}
//		if(ecpe != null){
//			po.setRate(ecpe.getCrProdRate());
//		}
//		logger.info("查询订单信息end,data={}",poe);
	}

	@Override
	public PaymentOrder findById(Long tid) {
		PaymentOrder po = new PaymentOrder();
		PaymentOrderEntity poe = paymentOrderRepository.findByStatusNotAndTid(HelpConstant.Status.delete,tid);
		if(poe!=null){
			BeanUtils.copyProperties(poe, po);
		}else{
			return null;
		}
		return po;
	}

	/**
	 * 支付成功更新状态
	 * @date 2018-04-27
	 * @param serviceId
	 * @param orderId
	 * @param payMethodName
	 * @param channelSerialNum
	 * @param finishTime
	 */
	@Override
	public void updatePaymentOrder(String serviceId, String orderId, String payMethodCode, String channelSerialNum, Date finishTime) {
		List<String> orderIdList = new ArrayList<>();
		PaymentRelationEntity paymentRelationEntity = paymentRelationRepository.findByPaySingleNo(orderId);
		String[] recordArr = paymentRelationEntity.getRecordNo().split(",");
		for (String str : recordArr) {
			orderIdList.add(str);
		}
		List<PaymentOrderEntity> paymentOrderEntityList = paymentOrderRepository.findPaymentOrderByOrderNoList(HelpConstant.Status.delete, PaymentType.FINANCE_CREDIT.toString(), orderIdList);
		if(paymentOrderEntityList.size() > 0) {
			for (PaymentOrderEntity paymentOrder : paymentOrderEntityList) {
				if(paymentOrder != null && (paymentOrder.getPayStatus().endsWith("01") || paymentOrder.getPayStatus().endsWith("05"))){
					int count = paymentOrderRepository.updatePaymentOrderPaySuccess(serviceId,paymentOrder.getOrderNo(),payMethodCode,channelSerialNum,finishTime);
					if(count != 1){
						logger.info("### update更新结果条数异常,count={}",count);
						throw new RuntimeException("update更新结果条数异常");
					}
				}
				try {
					orderLoopService.changeOrderByRcordNo(paymentOrder.getOrderNo());
				} catch (RuntimeException e) {
					logger.error("### 调用建行签订合同时，发生异常...msg={}",e.getLocalizedMessage());
				}
			}
		}
	}
	
	@Override
	public Page<PaymentOrder> getPayOrderRecordPage(
			PaymentOrder paymentOrderVO) {
		
		List<Object[]> objList=new ArrayList<Object[]>();
	    Calendar calendar = Calendar.getInstance();
	    
 	    SimpleDateFormat format = new SimpleDateFormat("yyy-MM-dd");
	    Date paymentStartTime = paymentOrderVO.getPayStartTime();
	    Date paymentEndTime = paymentOrderVO.getPayEndTime();
	    try {
			if(paymentStartTime !=null){
				String startTimeStr = format.format(paymentStartTime);
				paymentStartTime = format.parse(startTimeStr);
			}
			if(paymentEndTime !=null){
				calendar.setTime(paymentEndTime);
				int day = calendar.get(Calendar.DATE);
				calendar.set(Calendar.DATE, day + 1);
				paymentEndTime = calendar.getTime();
				String endTimeStr = format.format(paymentEndTime);
				paymentEndTime = format.parse(endTimeStr);
			}
		} catch (ParseException e) {
			logger.error("### 查询支付账单信息，日期格式化错误，msg={}",e.getLocalizedMessage());
		}
	    logger.info("### 查询支付账单列表信息，");
	    
	    //查询支付账单条数
	    List<String> payStatusList = null;
	    if(paymentOrderVO.getPayStatus() != null && paymentOrderVO.getPayStatus().length() >= 2) {
	    	payStatusList = new ArrayList<>();
	    	String[] strArr = paymentOrderVO.getPayStatus().split(",");
	    		for (String st : strArr) {
	    			payStatusList.add(st);
				}
	    }
	    if(org.springframework.util.StringUtils.isEmpty(paymentOrderVO.getPayStatus())) {
	    	payStatusList = new ArrayList<>();
	    	payStatusList.add("01");
	    	payStatusList.add("02");
	    	payStatusList.add("03");
	    	payStatusList.add("04");
	    	payStatusList.add("05");
	    }
	    List<String> payTypeList =  null;
	    if(paymentOrderVO.getPayType() != null && paymentOrderVO.getPayType().length() >= 2) {
	    	payTypeList = new ArrayList<>();
	    	String[] strArr = paymentOrderVO.getPayType().split(",");
	    	for (String str : strArr) {
	    		payTypeList.add(str);
			}
	    }
	    if(org.springframework.util.StringUtils.isEmpty(paymentOrderVO.getPayType())) {
	    	payTypeList = new ArrayList<>();
	    	payTypeList.add("EMPTY");
	    	payTypeList.add("UNIONPAY");
	    	payTypeList.add("ALIPAY");
	    	payTypeList.add("OFFLINE_PAYMENT");
	    }
	    Integer counts = paymentOrderRepository.findCountPayRecord(StringUtilsTo.isEmptyToNull(paymentOrderVO.getServiceNo()),
	    		StringUtilsTo.isEmptyToNull(paymentOrderVO.getOrderNo()),
	    		StringUtilsTo.isEmptyToNull(paymentOrderVO.getPayEnterpriseName()),paymentStartTime,paymentEndTime,
	    		payStatusList,
	    		payTypeList,paymentOrderVO.getUserList());
	    
	    logger.info("### 获取账单列表数量...count={}",counts);
	    //查询支付账单信息列表
	    objList=paymentOrderRepository.findPayRecordList(StringUtilsTo.isEmptyToNull(paymentOrderVO.getServiceNo()),
	    		StringUtilsTo.isEmptyToNull(paymentOrderVO.getOrderNo()),
	    		StringUtilsTo.isEmptyToNull(paymentOrderVO.getPayEnterpriseName()),paymentStartTime,paymentEndTime,
	    		payStatusList,
	    		payTypeList,paymentOrderVO.getUserList(),
	    		paymentOrderVO.getNextPage()*paymentOrderVO.getPageSize(), paymentOrderVO.getPageSize());
	    logger.info("### 查询支付账单列表信息完成...data={}",objList);
		List<PaymentOrder> crList = new ArrayList<PaymentOrder>();
		if(!objList.isEmpty()){
			for(Object[] obj:objList){
				PaymentOrder paymentOrder=new PaymentOrder();
				//主键id
				paymentOrder.setTid(SqlResultUtil.getSqlResultLong(obj[0]));
				paymentOrder.setServiceNo(SqlResultUtil.getSqlResultString(obj[1]));
				paymentOrder.setOrderNo(SqlResultUtil.getSqlResultString(obj[2]));
				paymentOrder.setPayEnterpriseName(SqlResultUtil.getSqlResultString(obj[3]));
				paymentOrder.setPayEnterpriseId(SqlResultUtil.getSqlResultString(obj[4]));
				paymentOrder.setCollectEnterpriseName(SqlResultUtil.getSqlResultString(obj[5]));
				paymentOrder.setOrderAmt(SqlResultUtil.getSqlResultBigDecimal(obj[6]));
				paymentOrder.setProductName(SqlResultUtil.getSqlResultString(obj[7]));
				paymentOrder.setDiscountAmt(SqlResultUtil.getSqlResultBigDecimal(obj[8]));
				paymentOrder.setPayTime(SqlResultUtil.getSqlResultDate2(SqlResultUtil.getSqlResultString(obj[9])));
				paymentOrder.setPayStatus(SqlResultUtil.getSqlResultString(obj[10]));
				paymentOrder.setPayType(SqlResultUtil.getSqlResultString(obj[11]));
				paymentOrder.setFlowNumber(SqlResultUtil.getSqlResultString(obj[12]));
				paymentOrder.setTradeStatus(SqlResultUtil.getSqlResultString(obj[13]));
				paymentOrder.setItemId(SqlResultUtil.getSqlResultLong(obj[14]));
				crList.add(paymentOrder);
			}
		}
    	
    	Page<PaymentOrder> page = new MyPage<PaymentOrder>(paymentOrderVO.getNextPage(), paymentOrderVO.getPageSize(), crList, counts);
    	logger.info("### 支付账单列表信息组装完成...");
		return page;
	}

	/**
	 * 根据订单明细id查询订单
	 */
	@Override
	public PaymentOrder findOrderById(Long id) {
		List<Object[]> rsList= paymentOrderRepository.findByItemId(HelpConstant.Status.delete,id);
		if(rsList.isEmpty()){
			return null;
		}
		PaymentOrder paymentOrder = new PaymentOrder();
		for(Object[] obj:rsList){
			//主键id
			paymentOrder.setTid(SqlResultUtil.getSqlResultLong(obj[0]));
			paymentOrder.setServiceNo(SqlResultUtil.getSqlResultString(obj[1]));
			paymentOrder.setOrderNo(SqlResultUtil.getSqlResultString(obj[2]));
			paymentOrder.setPayEnterpriseName(SqlResultUtil.getSqlResultString(obj[3]));
			paymentOrder.setPayEnterpriseId(SqlResultUtil.getSqlResultString(obj[4]));
			paymentOrder.setCollectEnterpriseName(SqlResultUtil.getSqlResultString(obj[5]));
			paymentOrder.setOrderAmt(SqlResultUtil.getSqlResultBigDecimal(obj[6]));
			paymentOrder.setProductName(SqlResultUtil.getSqlResultString(obj[7]));
			paymentOrder.setDiscountAmt(SqlResultUtil.getSqlResultBigDecimal(obj[8]));
			paymentOrder.setPayTime(SqlResultUtil.getSqlResultDate2(SqlResultUtil.getSqlResultString(obj[9])));
			paymentOrder.setPayStatus(SqlResultUtil.getSqlResultString(obj[10]));
			paymentOrder.setPayType(SqlResultUtil.getSqlResultString(obj[11]));
			paymentOrder.setFlowNumber(SqlResultUtil.getSqlResultString(obj[12]));
			paymentOrder.setTradeStatus(SqlResultUtil.getSqlResultString(obj[13]));
			paymentOrder.setItemId(SqlResultUtil.getSqlResultLong(obj[14]));
		}
		return paymentOrder;
	}
	
	/**
	 * 线下支付：支付已确认，更新订单状态
	 * @param serviceId
	 * @param orderId
	 */
	@Override
	public void updatePaymentOrder(String serviceId, String orderId,Long userId) {
		List<String> orderIdList = new ArrayList<>();
		PaymentRelationEntity paymentRelationEntity = paymentRelationRepository.findByPaySingleNo(orderId);
		String[] recordArr = paymentRelationEntity.getRecordNo().split(",");
		for (String str : recordArr) {
			orderIdList.add(str);
		}
		List<PaymentOrderEntity> poeList = paymentOrderRepository.findPaymentOrderByOrderNoList(HelpConstant.Status.delete,serviceId,orderIdList);
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String offLineFolwNumber = format.format(new Date());
		for (PaymentOrderEntity paymentOrder : poeList) {
			if(paymentOrder != null && paymentOrder.getPayStatus().endsWith("01")){
				Integer count = creditRecordRepository.updatePayLine(HelpConstant.Status.delete,paymentOrder.getOrderNo());
				if(count!=1){
					throw new RuntimeException("修改融资状态为线下支付待确认发返回条数异常");
				}
				PaymentOrderItemEntity createPaymentOrderItemEntity = createPaymentOrderItemEntity(paymentOrder,userId,offLineFolwNumber);
			
				PaymentOrderItemEntity entity = paymentOrderItemRepository.findByOrderIdAndPayTypeAndPayStatus(paymentOrder.getOrderNo(),"OFFLINE_PAYMENT","01");
				if(entity == null) {
					//新增一条线下订单明细
					paymentOrderItemRepository.save(createPaymentOrderItemEntity);
				}else {
					//明细已存在，修改明细的支付状态
					paymentOrderItemRepository.updatePayStatusByOrderId(paymentOrder.getOrderNo(),"OFFLINE_PAYMENT","05",offLineFolwNumber);
				}
			}
		}
		//PaymentOrderEntity paymentOrder = paymentOrderRepository.findPaymentOrderByOrderNo(HelpConstant.Status.delete,serviceId,orderId);
	}
	private PaymentOrderItemEntity createPaymentOrderItemEntity(PaymentOrderEntity paymentOrder,Long userId,String offLineFlowNumber){
		PaymentOrderItemEntity poi = new PaymentOrderItemEntity();
		poi.setOrderNo(paymentOrder.getOrderNo());
		poi.setServiceNo(paymentOrder.getServiceNo());
		poi.setFlowNumber(offLineFlowNumber);
		poi.setCreateUser(userId);
		/**
		 * 生成明细的时候金额还没有确认是没值 的
		 */
//		poi.setPayAmt(paymentOrder.getDiscountAmt());
		poi.setTradeStatus(TradeStatus.NORMAL.toString());
		poi.setPayStatus("05");
		poi.setPayType("OFFLINE_PAYMENT");
		return poi;
	}

	@Override
	public List<PaymentOrder> findAllByOrderNo(String orderId) {
		List<String> orderNoList = new ArrayList<>();
		List<PaymentOrder> paymentOrderList = new ArrayList<>();
		PaymentRelationEntity paymentRelationEntity = paymentRelationRepository.findByPaySingleNo(orderId);
		String[] recordArr = paymentRelationEntity.getRecordNo().split(",");
		for (String str : recordArr) {
			orderNoList.add(str);
		}
		List<PaymentOrderEntity> paymentOrderEntityList = paymentOrderRepository.findPaymentOrderByOrderNoList(HelpConstant.Status.delete, PaymentType.FINANCE_CREDIT.toString(), orderNoList);
		for (PaymentOrderEntity paymentOrderEntity : paymentOrderEntityList) {
			PaymentOrder order= new PaymentOrder();
			BeanUtils.copyProperties(paymentOrderEntity, order);
			paymentOrderList.add(order);
		}
		return paymentOrderList;
	}

	@Override
	public PaymentRelation createPaymentRelationEntity(String recordNo) {
		int hashCode = recordNo.hashCode();
		int abs = Math.abs(hashCode);
		PaymentRelationEntity findByPaySingleNo = paymentRelationRepository.findByPaySingleNo(abs+"");
		PaymentRelationEntity entity = new PaymentRelationEntity();
		if(findByPaySingleNo == null) {
			PaymentRelationEntity paymentRelationEntity = new PaymentRelationEntity();
			paymentRelationEntity.setPaySingleNo(abs+"");
			paymentRelationEntity.setRecordNo(recordNo);
			entity = paymentRelationRepository.save(paymentRelationEntity);
			
		}else {
			int i = paymentRelationRepository.updateByPaySingleNo(abs+"",recordNo);
			entity = paymentRelationRepository.findByPaySingleNo(abs+"");
		}
		PaymentRelation pr = new PaymentRelation();
		BeanUtils.copyProperties(entity, pr);
		return pr;
	}
}

