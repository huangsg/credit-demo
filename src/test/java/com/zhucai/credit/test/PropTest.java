package com.zhucai.credit.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.zhucai.credit.bank.ccbe.request.RequestBean;
import com.zhucai.credit.bank.ccbe.service.RequestFactory;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PropTest {
	
	@Autowired
	private RequestFactory requestUtils;
	
	@Test
	public void contextLoads() {
		RequestBean custId = requestUtils.getRequestBean("xxxxxx");
		System.out.println(custId);
	}
}
