package com.zhucai.credit.jxls;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.curator.shaded.com.google.common.collect.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ResourceUtils;

import com.zhucai.credit.bean.CreditApplication;
import com.zhucai.credit.utils.JxlsUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ExcelExport {
	
	@Value("${icbc.excel.path}")
	private String path;

	@Test
	public void test() throws Exception {
		//File file = ResourceUtils.getFile("classpath:jxls/icbc/customer_application.xlsx");
		ClassPathResource res = new ClassPathResource("jxls/icbc/customer_application.xlsx");
		
		File outFile = new File(path,"out.xlsx");
		List<CreditApplication> list = Lists.newArrayList();
		CreditApplication c1 = new CreditApplication();
		c1.setEnterpriseName("四建");
		CreditApplication c2 = new CreditApplication();
		c2.setEnterpriseName("八建");
		list.add(c1);
		list.add(c2);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("list", list);
		JxlsUtils.exportExcel(res.getInputStream(),outFile , model);
		System.out.println("完成");
	}

}
