package com.zhucai.credit;

import javax.jms.Queue;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zhucai.common.locks.DistributedLockFactory;

@SpringBootApplication
@EnableScheduling
@EnableTransactionManagement
@PropertySource(value = {"${external.properties.location}"}, ignoreResourceNotFound = true)
public class CreditServiceApplication {

/*	@Value("${dubbo.register.address}")
	private String registerAddress;
	
	
	*//**
	 * 用于线上环境配置多个dubbo服务的时候避免冲突，一般可以为空
	 *//*
	@Value("${dubbo.register.file}")
	private String registerFile;
	
	@Value("${dubbo.protecol.name}")
	private String protecolName;

	//这其实是RPC端口，这里沿用原配置名
	@Value("${dubbo.protecol.port}")
	private Integer protecolPort;
	
	*//**
	 * 用于指定服务提供方的host，对于一个主机上有多个IP，顺序混乱的可以通过这个配置指定
	 *//*
	@Value("${dubbo.protecol.host}")
	private String protecolHost;*/
	
	@Value("${locks.zookeeper.server}")
	private String lockServer;

	/**
	 * 注册中心
	 * @return
	 */
/*    @Bean
    public RegistryConfig registry() {
        RegistryConfig registryConfig = new RegistryConfig();
        registryConfig.setAddress(registerAddress);
		registryConfig.setId("zhucai");
        registryConfig.setCheck(false);
        registryConfig.setDefault(true);
        if(!Strings.isNullOrEmpty(registerFile)) {
        	registryConfig.setFile(registerFile);
        }
        return registryConfig;
    }
    @Bean
    public ProtocolConfig protocol() {
        ProtocolConfig protocolConfig = new ProtocolConfig();
        protocolConfig.setName(protecolName);
        protocolConfig.setPort(protecolPort);
        if(!Strings.isNullOrEmpty(protecolHost)) {
            protocolConfig.setHost(protecolHost);
        }
        return protocolConfig;
    }
    */
	
	@Value("${sms.queue.name}")
	public String queueName;
	
	@Bean
	public Queue getQueue() {
		Queue queue = new ActiveMQQueue(queueName);
		return queue;
	}
	
    @Bean
	public DistributedLockFactory lockFactory() {
		DistributedLockFactory factory = new DistributedLockFactory(lockServer, "zc_credit");
		return factory;
	}
    
	public static void main(String[] args) {
		SpringApplication.run(CreditServiceApplication.class, args);
	}

}
