package com.zhucai.credit.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.zhucai.credit.model.FinanceDebitEntity;

/**
 * 
 * @author hsg
 * 扣款的Repository
 */
@Repository
public interface FinanceDebitRepository extends
		PagingAndSortingRepository<FinanceDebitEntity, Long>,
		JpaSpecificationExecutor<FinanceDebitEntity> {

	/**
	 * 根据支用编号查询扣款记录
	 * @param delete
	 * @param payout_num
	 * @return
	 */
	FinanceDebitEntity findByStatusNotAndPayoutNum(int delete, String payout_num);

	/**
	 * 根据反馈序号查询扣款记录
	 * @param delete
	 * @param payout_num
	 * @return
	 */
	FinanceDebitEntity findBySerialNum(int serialNum);
	




}
