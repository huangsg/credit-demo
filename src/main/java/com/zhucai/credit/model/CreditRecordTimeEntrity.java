package com.zhucai.credit.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.wordnik.swagger.annotations.ApiModelProperty;
/**
 * 融资记录历时记录表
 * @author hsg
 *
 */
@Entity
@Table(name="finance_cr_record_time")
public class CreditRecordTimeEntrity extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "融资记录id")
	private Long crRecordId;
	@ApiModelProperty(value = "历时记录类型")
	private String recordingType;
	@ApiModelProperty(value = "历时记录时间")
	private Date recordingTime;
	@ApiModelProperty(value = "历时记录描述")
	private String recordingRemark;
	@ApiModelProperty(value = "历时记录间隔")
	private String recordingBetween;
	@ApiModelProperty(value = "历时记录总间隔")
	private String recordingAllBetween;
	
	@Column(nullable=true,length=120)
	public String getRecordingAllBetween() {
		return recordingAllBetween;
	}
	public void setRecordingAllBetween(String recordingAllBetween) {
		this.recordingAllBetween = recordingAllBetween;
	}
	@Column(nullable=false)
	public Long getCrRecordId() {
		return crRecordId;
	}
	public void setCrRecordId(Long crRecordId) {
		this.crRecordId = crRecordId;
	}
	@Column(nullable=false,length=40)
	public String getRecordingType() {
		return recordingType;
	}
	public void setRecordingType(String recordingType) {
		this.recordingType = recordingType;
	}
	@Column(nullable=false)
	public Date getRecordingTime() {
		return recordingTime;
	}
	public void setRecordingTime(Date recordingTime) {
		this.recordingTime = recordingTime;
	}
	@Column(nullable=true,length=250)
	public String getRecordingRemark() {
		return recordingRemark;
	}
	public void setRecordingRemark(String recordingRemark) {
		this.recordingRemark = recordingRemark;
	}
	@Column(nullable=true,length=120)
	public String getRecordingBetween() {
		return recordingBetween;
	}
	public void setRecordingBetween(String recordingBetween) {
		this.recordingBetween = recordingBetween;
	}
	
}
