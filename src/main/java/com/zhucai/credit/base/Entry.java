package com.zhucai.credit.base;

import java.util.Map;

/**
 * <p>
 * Title:键值对
 * </p>
 * @author: huangshenggen
 * @version: 1.0
 */
public class Entry<K, V> implements Map.Entry<K, V> {

	/**
	 * 文本
	 */
	private K key;

	/**
	 * 值
	 */
	private V value;

	public Entry() {

	}

	public Entry(K key, V value) {
		this.setKey(key);
		this.setValue(value);
	}

	public K getKey() {
		return this.key;
	}

	public K setKey(K key) {
		K oldKey = getKey();
		this.key = key;
		return oldKey;
	}

	public V getValue() {
		return this.value;
	}

	public V setValue(V value) {
		V oldValue = getValue();
		this.value = value;
		return oldValue;
	}
}
