package com.zhucai.credit.bank.ccbe.bean;

import java.io.Serializable;
import java.util.Date;

import com.wordnik.swagger.annotations.ApiModelProperty;
/**
 * 平台账款新增信息接收
 * 6W5574
 * @date 2017年12月08日 @user xjz
 */
public class PlatformReceive implements Serializable{

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "交易请求方组织机构代码")
	private String request_orgn_num;
	@ApiModelProperty(value = "客户组织机构代码")
	private String cust_orgn_code;
	@ApiModelProperty(value = "关联客户组织机构代码")
	private String r_cust_orgn_code;
	@ApiModelProperty(value = "关联客户名称")
	private String relate_cust_name;
	@ApiModelProperty(value = "合同编号")
	private String contract_num;
	@ApiModelProperty(value = "发票号")
	private String bill_num;
	@ApiModelProperty(value = "应收账款金额")
	private String bill_balance_amt;
	@ApiModelProperty(value = "单据日期")
	private String bill_date;
	@ApiModelProperty(value = "账期")
	private Integer credit_period;
	@ApiModelProperty(value = "账款补充信息集")
	private String b_supplement_group;
	public String getRequest_orgn_num() {
		return request_orgn_num;
	}
	public void setRequest_orgn_num(String request_orgn_num) {
		this.request_orgn_num = request_orgn_num;
	}
	public String getCust_orgn_code() {
		return cust_orgn_code;
	}
	public void setCust_orgn_code(String cust_orgn_code) {
		this.cust_orgn_code = cust_orgn_code;
	}
	public String getR_cust_orgn_code() {
		return r_cust_orgn_code;
	}
	public void setR_cust_orgn_code(String r_cust_orgn_code) {
		this.r_cust_orgn_code = r_cust_orgn_code;
	}
	public String getRelate_cust_name() {
		return relate_cust_name;
	}
	public void setRelate_cust_name(String relate_cust_name) {
		this.relate_cust_name = relate_cust_name;
	}
	public String getContract_num() {
		return contract_num;
	}
	public void setContract_num(String contract_num) {
		this.contract_num = contract_num;
	}
	public String getBill_num() {
		return bill_num;
	}
	public void setBill_num(String bill_num) {
		this.bill_num = bill_num;
	}
	public String getBill_balance_amt() {
		return bill_balance_amt;
	}
	public void setBill_balance_amt(String bill_balance_amt) {
		this.bill_balance_amt = bill_balance_amt;
	}

	public String getBill_date() {
		return bill_date;
	}
	public void setBill_date(String bill_date) {
		this.bill_date = bill_date;
	}
	public Integer getCredit_period() {
		return credit_period;
	}
	public void setCredit_period(Integer credit_period) {
		this.credit_period = credit_period;
	}
	public String getB_supplement_group() {
		return b_supplement_group;
	}
	public void setB_supplement_group(String b_supplement_group) {
		this.b_supplement_group = b_supplement_group;
	}
	
	
	
	
}
