package com.zhucai.credit.monitor.service;

import com.zhucai.credit.monitor.bean.SendMsgResult;

public interface INotificationService {
	
	SendMsgResult notification(String message);
	
}
