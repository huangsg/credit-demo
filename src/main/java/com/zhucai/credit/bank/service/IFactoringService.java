package com.zhucai.credit.bank.service;

import com.zhucai.credit.bank.bean.BankTransfer;
import com.zhucai.credit.bean.CreditRecord;

/**
 * @author taojie
 * 本接口旨在封装掉银行间保理业务差异<br>
 * 对于未来产生如票据业务，票据和保理业务流程可能很难兼容，应该另启接口<br>
 * 1.本接口只做银行交互，交互结果都应通过返回反馈给调用方，由调用方完成业务数据的持久<br>
 * 2.接口内方法的所需数据，如无特殊比较应该通过接口参数提供，不应再有其它查询行为<br>
 * 基于以上两点，目前接口所用CreditRecord对象可能是不合适的，容易被反向污染的
 * 应该另行包装专用于银行交互的内部对象，以免污染其它域
 */
public interface IFactoringService {

	
	/**
	 * 融资业务前置，用于如融资发起时，需要预先提交部分数据给银行
	 * 按目前建行和工行业务，都不需要实现本接口，仅在发起是调用即可
	 * @param creditRecord
	 * @return
	 */
	CreditRecord preHandle(BankTransfer creditRecord);
	
	/**
	 * 申请
	 * 向银行发起融资
	 * @param creditRecord
	 * @return
	 */
	CreditRecord apply(BankTransfer creditRecord);
	
	
	/**
	 * 支取
	 * 针对某一笔融资，进行支取操作，在接入招行之前，无法确定通过平台主动支取的形式<br>
	 * @param creditRecord
	 * @return
	 */  
	CreditRecord draw(BankTransfer creditRecord);
	
	/**
	 * 支付（还款）
	 * 针对某一笔融资，进行还款操作，在接入招行之前，无法确定平台主动还款的形式<br>
	 * @param creditRecord
	 * @return
	 */
	CreditRecord pay(BankTransfer creditRecord);
}
