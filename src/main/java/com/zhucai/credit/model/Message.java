package com.zhucai.credit.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * 
 * @ClassName: Message
 * @Description: 系统信息
 * @author jmshi
 * @date 2015年10月11日 上午10:47:49
 *
 */
@Entity
@Table(name = "tb_xa_message")
@ApiModel(value = "系统信息")
public class Message extends BaseEntity {

	@ApiModelProperty(value = "信息内容")
	private String content;
	
	@ApiModelProperty(value = "信息来源   ：即发送方的UserId,于2017.03.03添加的注释")
	private String source;
	
	@ApiModelProperty(value = "信息分类，如：询价信息，签约信息，竞价结束信息")
	private String type;
 
	@ApiModelProperty(value = "信息类型，系统信息1 站内信2")
	private Integer kind;

	@ApiModelProperty(value = "详细内容")
	private String url;
	
    @ApiModelProperty(value = "消息时间")
	private String sendDate;
    
    @ApiModelProperty(value = "是否读取 0 = 未读  1 = 已读")
    private Integer isNew;
	
	@Column(nullable=true,length=1024)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	 

	@Column(nullable=true,length=50)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@Column(nullable=true,length=10)
	public Integer getKind() {
		return kind;
	}

	public void setKind(Integer kind) {
		this.kind = kind;
	}
	
	@Column(nullable=true,length=100)
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	@Column(nullable=true,length=100)
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
	
	@Column(nullable=true,length=50)
	public String getSendDate() {
		return sendDate;
	}

	public void setSendDate(String sendDate) {
		this.sendDate = sendDate;
	}

	@Transient
	public Integer getIsNew() {
		return isNew;
	}

	public void setIsNew(Integer isNew) {
		this.isNew = isNew;
	}
}
