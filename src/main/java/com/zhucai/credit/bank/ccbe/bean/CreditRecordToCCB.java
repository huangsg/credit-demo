package com.zhucai.credit.bank.ccbe.bean;

import java.io.Serializable;
import java.util.Date;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * 平台账款新增信息接收
 * 6W5574
 * @date 2017年11月28日 @user hsg
 */
public class CreditRecordToCCB implements Serializable{

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "请求方组织机构代码")
	private String request_orgn_num;
	@ApiModelProperty(value = "供应商的组织机构号")
	private String cust_orgn_code;
	@ApiModelProperty(value = "核心企业的组织机构号(采购商)")
	private String r_cust_orgn_code;
	@ApiModelProperty(value = "核心企业名称")
	private String relate_cust_name;
	@ApiModelProperty(value = "合同编号")
	private String contract_num;
	@ApiModelProperty(value = "发票号，应收账款的号码（比如：发票号、订单号）")
	private String bill_num;
	@ApiModelProperty(value = "应收账款金额")
	private double bill_balance_amt;
	@ApiModelProperty(value = "发票或订单的起始日期(yyyymmdd)(发票日期不存在就是付款计划日期）")
	private Date bill_date;
	@ApiModelProperty(value = "账期(账款的账期(单位：天)，重要，会根据单据日期+账期得出账款的到期日)")
	private String credit_period;
	/**
	 * 账款的特色业务字段（在合作方的客户ID@|@币种代码@|@单据金额@|@是否融资@|@发货日期@|@物流编号@|@确认状态@|@阶段类型）
	 */
	@ApiModelProperty(value = "账款补充信息集")
	private Integer b_supplement_group;
	public String getRequest_orgn_num() {
		return request_orgn_num;
	}
	public void setRequest_orgn_num(String request_orgn_num) {
		this.request_orgn_num = request_orgn_num;
	}
	public String getCust_orgn_code() {
		return cust_orgn_code;
	}
	public void setCust_orgn_code(String cust_orgn_code) {
		this.cust_orgn_code = cust_orgn_code;
	}
	public String getR_cust_orgn_code() {
		return r_cust_orgn_code;
	}
	public void setR_cust_orgn_code(String r_cust_orgn_code) {
		this.r_cust_orgn_code = r_cust_orgn_code;
	}
	public String getRelate_cust_name() {
		return relate_cust_name;
	}
	public void setRelate_cust_name(String relate_cust_name) {
		this.relate_cust_name = relate_cust_name;
	}
	public String getContract_num() {
		return contract_num;
	}
	public void setContract_num(String contract_num) {
		this.contract_num = contract_num;
	}
	public String getBill_num() {
		return bill_num;
	}
	public void setBill_num(String bill_num) {
		this.bill_num = bill_num;
	}
	public double getBill_balance_amt() {
		return bill_balance_amt;
	}
	public void setBill_balance_amt(double bill_balance_amt) {
		this.bill_balance_amt = bill_balance_amt;
	}
	public Date getBill_date() {
		return bill_date;
	}
	public void setBill_date(Date bill_date) {
		this.bill_date = bill_date;
	}
	public String getCredit_period() {
		return credit_period;
	}
	public void setCredit_period(String credit_period) {
		this.credit_period = credit_period;
	}
	public Integer getB_supplement_group() {
		return b_supplement_group;
	}
	public void setB_supplement_group(Integer b_supplement_group) {
		this.b_supplement_group = b_supplement_group;
	}
	
}
