package com.zhucai.credit.bank.ccbe.bean;

import java.io.Serializable;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * 业务信息反馈
 * 6W5519
 * @date 2017年11月28日 @user hsg
 */
public class SignUpFeedbackTwo implements Serializable{

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "当前最大反馈序号")
	private Integer max_serial_num;
	@ApiModelProperty(value = "反馈序号")
	private Integer serial_num;
	@ApiModelProperty(value = "企业名称")
	private String cust_name;
	@ApiModelProperty(value = "合同编号")
	private String contract_num;
	@ApiModelProperty(value = "合作方客户编号")
	private String cooper_cust_num;
	@ApiModelProperty(value = "卖方一般结算帐户")
	private String clearance_account;
	@ApiModelProperty(value = "回款专户")
	private String recover_account;
	@ApiModelProperty(value = "客户组织机构代码")
	private String cust_orgn_code;
	@ApiModelProperty(value = "关联客户组织机构代码")
	private String r_cust_orgn_code;
	//返回新增字段
	@ApiModelProperty(value = "156")
	private String currency_cd;
	@ApiModelProperty(value = "100000000")
	private String contract_amt;
	@ApiModelProperty(value = "20171207")
	private String start_date;
	@ApiModelProperty(value = "20180607")
	private String exdr_date;
	@ApiModelProperty(value = "CK17C00003073")
	private String regist_num;
	public Integer getMax_serial_num() {
		return max_serial_num;
	}
	public void setMax_serial_num(Integer max_serial_num) {
		this.max_serial_num = max_serial_num;
	}
	public Integer getSerial_num() {
		return serial_num;
	}
	public void setSerial_num(Integer serial_num) {
		this.serial_num = serial_num;
	}
	public String getCust_name() {
		return cust_name;
	}
	public void setCust_name(String cust_name) {
		this.cust_name = cust_name;
	}
	public String getContract_num() {
		return contract_num;
	}
	public void setContract_num(String contract_num) {
		this.contract_num = contract_num;
	}
	public String getCooper_cust_num() {
		return cooper_cust_num;
	}
	public void setCooper_cust_num(String cooper_cust_num) {
		this.cooper_cust_num = cooper_cust_num;
	}
	public String getClearance_account() {
		return clearance_account;
	}
	public void setClearance_account(String clearance_account) {
		this.clearance_account = clearance_account;
	}
	public String getRecover_account() {
		return recover_account;
	}
	public void setRecover_account(String recover_account) {
		this.recover_account = recover_account;
	}
	public String getCust_orgn_code() {
		return cust_orgn_code;
	}
	public void setCust_orgn_code(String cust_orgn_code) {
		this.cust_orgn_code = cust_orgn_code;
	}
	public String getR_cust_orgn_code() {
		return r_cust_orgn_code;
	}
	public void setR_cust_orgn_code(String r_cust_orgn_code) {
		this.r_cust_orgn_code = r_cust_orgn_code;
	}
	public String getCurrency_cd() {
		return currency_cd;
	}
	public void setCurrency_cd(String currency_cd) {
		this.currency_cd = currency_cd;
	}
	public String getContract_amt() {
		return contract_amt;
	}
	public void setContract_amt(String contract_amt) {
		this.contract_amt = contract_amt;
	}
	public String getStart_date() {
		return start_date;
	}
	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}
	public String getExdr_date() {
		return exdr_date;
	}
	public void setExdr_date(String exdr_date) {
		this.exdr_date = exdr_date;
	}
	public String getRegist_num() {
		return regist_num;
	}
	public void setRegist_num(String regist_num) {
		this.regist_num = regist_num;
	}
	
}
