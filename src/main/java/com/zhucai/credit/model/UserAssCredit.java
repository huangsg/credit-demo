package com.zhucai.credit.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * 用户和金融关联信息表 
 * @author hsg
 *
 */
@Entity
@Table(name = "finance_user_ass_credit")
@ApiModel(value = "用户和金融关联关系表 ")
public class UserAssCredit extends BaseEntity{

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "贷款机构类型")
	private String crProdType;
	@ApiModelProperty(value = "用户id")
	private Long manUserId;
	@ApiModelProperty(value = "用户类型")
	private String manUserType;
	@ApiModelProperty(value = "用户状态")
	private String manUserStatus;
	@Column(nullable=false,length=50)
	public String getCrProdType() {
		return crProdType;
	}
	public void setCrProdType(String crProdType) {
		this.crProdType = crProdType;
	}
	@Column(nullable=false,length=50)
	public Long getManUserId() {
		return manUserId;
	}
	public void setManUserId(Long manUserId) {
		this.manUserId = manUserId;
	}
	@Column(nullable=true,length=25)
	public String getManUserType() {
		return manUserType;
	}
	public void setManUserType(String manUserType) {
		this.manUserType = manUserType;
	}
	@Column(nullable=true,length=25)
	public String getManUserStatus() {
		return manUserStatus;
	}
	public void setManUserStatus(String manUserStatus) {
		this.manUserStatus = manUserStatus;
	}


	
}
