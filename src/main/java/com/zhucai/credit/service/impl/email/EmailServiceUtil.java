package com.zhucai.credit.service.impl.email;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.zhucai.credit.bean.CreditApplication;


@Service("emailServiceUtil")
public class EmailServiceUtil{
	private static final Logger logger = LoggerFactory.getLogger(EmailServiceUtil.class);

	@Value("${icbc.mail.1.to}")
	private String to;

	@Value("${spring.mail.username}")
	private String from;
	
	@Value("${spring.mail.sendname}")
	private String sendName;
	
	@Value("${icbc.excel.path}")
	private String path;
	
	@Autowired
	private  JavaMailSender mailSender;
	
	@Autowired
	private TemplateEngine templateEngine;
	
	/**
	 * 核心企业推荐供应商发送邮件
	 * @version 2018年8月15日
	 * @param ca
	 * @return
	 */
	@Async
	public  Boolean sendEnteriseInfoEmail(CreditApplication ca){
		logger.info("### 核心企业推荐供应商发送邮件 start ca={}", ca.toString());
		System.setProperty("mail.mime.splitlongparameters", "false");
		MimeMessage message = mailSender.createMimeMessage();
		String excelPath = ca.getExcelName();
		SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
		//设置excel名称
		String excelName = ca.getApplicantEnterpriseName()+"_客户准入申请_"+sf.format(new Date())+".xlsx";
		try {
			//动态设置邮件标题
			String subject = ca.getApplicantEnterpriseName()+"_客户准入申请_"+sf.format(new Date());
			//动态设置发送邮件名称
			MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");  
			messageHelper.setFrom(sendName+"<"+from+">");  
			messageHelper.setSubject(subject);  
			messageHelper.setTo(to);
//			messageHelper.addBcc(from);
			String x = templateEngine.process("icbc/Mail1", new Context());
			messageHelper.setText(x,true);
			messageHelper.addAttachment(excelName, new File(path,excelPath));
		} catch (MessagingException e) {
			logger.error("### 核心企业推荐供应商发送邮件失败 errMsg={}", e);
			throw new RuntimeException("发送邮件失败!");
		}

		mailSender.send(message);
		logger.info("### 核心企业推荐供应商发送邮件成功 excelName={}", excelPath);
		return true;
	}
	/**
	 * 历史交易数据
	 * @version 2018年8月15日
	 * @param ca
	 * @return
	 */
	public  Boolean sendHistroyInfoEmail(CreditApplication ca){
		logger.info("### 核心企业推荐供应商发送邮件 start ca={}", ca.toString());
		System.setProperty("mail.mime.splitlongparameters", "false");
		MimeMessage message = mailSender.createMimeMessage();
		SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
		String excelPath = ca.getExcelName();
		//设置excel名称
		String excelName = ca.getApplicantEnterpriseName()+"_历史交易数据_"+sf.format(new Date())+".xlsx";
		try {
			//动态设置邮件标题
			String subject = ca.getApplicantEnterpriseName()+"_历史交易数据_"+sf.format(new Date());
			//动态设置发送邮件名称
			MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");  
			messageHelper.setFrom(sendName+"<"+from+">");  
			messageHelper.setSubject(subject);  
			messageHelper.setTo(to);  
//			messageHelper.addBcc(from);
			String x = templateEngine.process("icbc/Mail2", new Context());
			messageHelper.setText(x,true);
			messageHelper.addAttachment(excelName, new File(path,excelPath));
		} catch (MessagingException e) {
			logger.error("### 核心企业推荐供应商发送邮件失败 errMsg={}", e);
			throw new RuntimeException("发送邮件失败!");
		}
		
		mailSender.send(message);
		logger.info("### 核心企业推荐供应商发送邮件成功 excelName={}", excelPath);
		return true;
	}
	
}