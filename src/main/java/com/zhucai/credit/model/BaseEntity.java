package com.zhucai.credit.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Transient;
import javax.persistence.Version;

import com.wordnik.swagger.annotations.ApiModelProperty;
import com.zhucai.credit.base.Constant;


/**
 * @Desc: 基础类
 * @Auth: hsg
 * @Date: 2017-9-8 下午5:20:35
 */
@MappedSuperclass
public abstract class BaseEntity implements Serializable{


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键，唯一标识，")
    private Long tid;

    @ApiModelProperty(value = "状态，0为无效，1为正常,3删除 参看Constant.Status")
    private Integer status;

    @ApiModelProperty(value = "版本,hibernate维护")
    private Integer version;

    @ApiModelProperty(value = "@Fields createUser : 创建者")
    private Long createUser;

    @ApiModelProperty(value = "@Fields createTime : 创建时间")
    private Long createTime;

    @ApiModelProperty(value = "@Fields modifyUser : 修改者")
    private Long modifyUser;

    @ApiModelProperty(value = "@Fields modifyTime : 修改时间")
    private Long modifyTime;

    @ApiModelProperty(value = "@Fields modifyDescription : 修改描述")
    private String modifyDescription;

   
    @Id
  	@GeneratedValue(strategy = GenerationType.AUTO)
  	@Basic(optional = false)
  	@Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    public Long getTid() {
		return tid;
	}

	public void setTid(Long tid) {
		this.tid = tid;
	}

	/**
     * 为确保赋值增加默认值1:正常
     */
    @Column(nullable = false, columnDefinition = "int default 1")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Version
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Column(name = "createUser")
    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    @Column(name = "createTime")
    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    @Column(name = "modifyUser")
    public Long getModifyUser() {
        return modifyUser;
    }

    public void setModifyUser(Long modifyUser) {
        this.modifyUser = modifyUser;
    }

    @Column(name = "modifyTime")
    public Long getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Long modifyTime) {
        this.modifyTime = modifyTime;
    }

    @Column(name = "modifyDescription", length = 500)
    public String getModifyDescription() {
        return modifyDescription;
    }

    public void setModifyDescription(String modifyDescription) {
        this.modifyDescription = modifyDescription;
    }

    /**
     * 数据插入前的操作
     */
    @PrePersist
    public void setInsertBefore() {
//        判断是否已手动设置createTime的值，如果没有设置则使用当前时间
        if (null == this.createTime) {
            this.createTime = (new Date()).getTime();
        }
        if (status == null) {
            this.status = Constant.Status.valid;
        }
//        判断是否已手动设置createTime的值，如果没有设置则使用当前时间
        if (null == this.modifyTime) {
            this.modifyTime = (new Date()).getTime();
        }

    }

    /**
     * 数据修改前的操作
     */
    @PreUpdate
    public void setUpdateBefore() {
        this.modifyTime = (new Date()).getTime();
    }

    @Transient
    @ApiModelProperty(value = "创建时间的字符串表示")
    public String getCreateAtStr() {
        return getLongToString(createTime);
    }

    @Transient
    @ApiModelProperty(value = "修改时间的字符串表示")
    public String getUpdatedAtStr() {
        return getLongToString(modifyTime);
    }

    public static String getLongToString(Long longTime) {
        if (longTime == null) {
            return null;
        }
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(longTime);
        return sf.format(date);
    }

    public static String getLongToString2(Long longTime) {
        if (longTime == null) {
            return null;
        }
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(longTime);
        return sf.format(date);
    }

    @Transient
    @ApiModelProperty(value = "创建时间的字符串表示")
    public String getCreateAtStr2() {
        return getLongToString2(createTime);
    }


}
