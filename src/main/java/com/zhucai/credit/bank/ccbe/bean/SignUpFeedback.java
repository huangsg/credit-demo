package com.zhucai.credit.bank.ccbe.bean;

import java.io.Serializable;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * 报名信息状态反馈
 * 6W5503
 * @date 2017年11月28日 @user hsg
 */
public class SignUpFeedback implements Serializable{

	
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "当前最大反馈序号")
	private Integer max_serial_num;
	@ApiModelProperty(value = "报名状态反馈序号")
	private Integer application_state_serial_num;
	@ApiModelProperty(value = "组织机构代码")
	private String orgn_num;
	@ApiModelProperty(value = "报名编号")
	private String app_num;
	@ApiModelProperty(value = "企业名称")
	private String customer_name;
	@ApiModelProperty(value = "申请人姓名")
	private String applicant_name;
	@ApiModelProperty(value = "单选项，选项为：0:不通过；1:通过")
	private String approve_pass_ind;
	@ApiModelProperty(value = "该项为“是否审批通过”为“0:不通过”时填写")
	private String unpass_reason;
	@ApiModelProperty(value = "在合作方的客户ID")
	private String partner_customer_id;
	@ApiModelProperty(value = "营销品牌")
	private String marketing_brand_name;
	@ApiModelProperty(value = "合作企业组织机构号")
	private String Co_Entp_Org_InsNo;
	@ApiModelProperty(value = "合作公司简称")
	private String Co_Co_ShrtNm;
	@ApiModelProperty(value = "新增字段")
	private String pltfrm_Nm;
	public Integer getMax_serial_num() {
		return max_serial_num;
	}
	public void setMax_serial_num(Integer max_serial_num) {
		this.max_serial_num = max_serial_num;
	}
	public Integer getApplication_state_serial_num() {
		return application_state_serial_num;
	}
	public void setApplication_state_serial_num(Integer application_state_serial_num) {
		this.application_state_serial_num = application_state_serial_num;
	}
	public String getOrgn_num() {
		return orgn_num;
	}
	public void setOrgn_num(String orgn_num) {
		this.orgn_num = orgn_num;
	}
	public String getApp_num() {
		return app_num;
	}
	public void setApp_num(String app_num) {
		this.app_num = app_num;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public String getApplicant_name() {
		return applicant_name;
	}
	public void setApplicant_name(String applicant_name) {
		this.applicant_name = applicant_name;
	}
	public String getApprove_pass_ind() {
		return approve_pass_ind;
	}
	public void setApprove_pass_ind(String approve_pass_ind) {
		this.approve_pass_ind = approve_pass_ind;
	}
	public String getUnpass_reason() {
		return unpass_reason;
	}
	public void setUnpass_reason(String unpass_reason) {
		this.unpass_reason = unpass_reason;
	}
	public String getPartner_customer_id() {
		return partner_customer_id;
	}
	public void setPartner_customer_id(String partner_customer_id) {
		this.partner_customer_id = partner_customer_id;
	}
	public String getMarketing_brand_name() {
		return marketing_brand_name;
	}
	public void setMarketing_brand_name(String marketing_brand_name) {
		this.marketing_brand_name = marketing_brand_name;
	}
	public String getCo_Entp_Org_InsNo() {
		return Co_Entp_Org_InsNo;
	}
	public void setCo_Entp_Org_InsNo(String co_Entp_Org_InsNo) {
		Co_Entp_Org_InsNo = co_Entp_Org_InsNo;
	}
	public String getCo_Co_ShrtNm() {
		return Co_Co_ShrtNm;
	}
	public void setCo_Co_ShrtNm(String co_Co_ShrtNm) {
		Co_Co_ShrtNm = co_Co_ShrtNm;
	}
	public String getPltfrm_Nm() {
		return pltfrm_Nm;
	}
	public void setPltfrm_Nm(String pltfrm_Nm) {
		this.pltfrm_Nm = pltfrm_Nm;
	}

}
