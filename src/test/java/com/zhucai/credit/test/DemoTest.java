package com.zhucai.credit.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;


public class DemoTest {
	public static void main(String[] args) throws Exception{
		
		addM();
	}
	public static void addM() throws ParseException {
		String endDate = "2018-09-20 00:00:00.0";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date parseDate = sdf.parse(endDate);
		Date endDates = DateUtils.addMonths(parseDate, 1);
		Date startDates = DateUtils.addMonths(parseDate, -1);
		System.out.println(sdf.format(endDates)+","+sdf.format(startDates));
	}

}