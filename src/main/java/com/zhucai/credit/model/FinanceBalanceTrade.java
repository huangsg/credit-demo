package com.zhucai.credit.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.wordnik.swagger.annotations.ApiModelProperty;


/**
 * 余额交易流水表
 * @author hsg
 *
 */
@Entity
@Table(name="finance_balance_trade")
public class FinanceBalanceTrade extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = " 余额交易流水id")
    private Long balanceTradeId;

	@ApiModelProperty(value = " 余额交易流水账号id")
    private Long balanceAccountId;


	@ApiModelProperty(value = " 余额交易流水平台id")
    private Long parentBalanceTradeId;

	@ApiModelProperty(value = " 业务订单Id")
    private Long bizBillId;

	@ApiModelProperty(value = " 交易描述")
    private String tradeDescr;

	@ApiModelProperty(value = " 交易金额")
    private BigDecimal tradeAmount;

	@ApiModelProperty(value = " 处理状态")
    private String tradeStatus;

	@ApiModelProperty(value = " 变更前资金余额")
    private BigDecimal beforeBalance;

	@ApiModelProperty(value = " 变更后资金余额 ")
    private BigDecimal afterBalance;

	@ApiModelProperty(value = " 数据签名 ")
    private String dataSign;

	@Column(nullable=false,length=50)
	public Long getBalanceTradeId() {
		return balanceTradeId;
	}

	public void setBalanceTradeId(Long balanceTradeId) {
		this.balanceTradeId = balanceTradeId;
	}

	@Column(nullable=false,length=50)
	public Long getBalanceAccountId() {
		return balanceAccountId;
	}

	public void setBalanceAccountId(Long balanceAccountId) {
		this.balanceAccountId = balanceAccountId;
	}

	@Column(nullable=true,length=50)
	public Long getParentBalanceTradeId() {
		return parentBalanceTradeId;
	}

	public void setParentBalanceTradeId(Long parentBalanceTradeId) {
		this.parentBalanceTradeId = parentBalanceTradeId;
	}

	@Column(nullable=false,length=50)
	public Long getBizBillId() {
		return bizBillId;
	}

	public void setBizBillId(Long bizBillId) {
		this.bizBillId = bizBillId;
	}

	@Column(nullable=true,length=250)
	public String getTradeDescr() {
		return tradeDescr;
	}

	public void setTradeDescr(String tradeDescr) {
		this.tradeDescr = tradeDescr;
	}

	@Column(nullable=true,columnDefinition = "decimal(25,2)")
	public BigDecimal getTradeAmount() {
		return tradeAmount;
	}
	public void setTradeAmount(BigDecimal tradeAmount) {
		this.tradeAmount = tradeAmount;
	}
	@Column(nullable=true,length=50)
	public String getTradeStatus() {
		return tradeStatus;
	}

	public void setTradeStatus(String tradeStatus) {
		this.tradeStatus = tradeStatus;
	}

	@Column(nullable=true,columnDefinition = "decimal(25,2)")
	public BigDecimal getBeforeBalance() {
		return beforeBalance;
	}

	public void setBeforeBalance(BigDecimal beforeBalance) {
		this.beforeBalance = beforeBalance;
	}
	@Column(nullable=true,columnDefinition = "decimal(25,2)")
	public BigDecimal getAfterBalance() {
		return afterBalance;
	}

	public void setAfterBalance(BigDecimal afterBalance) {
		this.afterBalance = afterBalance;
	}

	@Column(nullable=false,length=50)
	public String getDataSign() {
		return dataSign;
	}

	public void setDataSign(String dataSign) {
		this.dataSign = dataSign;
	}


}