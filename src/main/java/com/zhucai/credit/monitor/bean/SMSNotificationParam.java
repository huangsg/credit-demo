package com.zhucai.credit.monitor.bean;

/**
 * @author Zen.Xu
 * @version 1.0
 * @time 16/11/8 下午1:58
 * @Description SMS通知参数
 */
public class SMSNotificationParam  {

    /**
     * 手机号
     */
    private String phone;

    /**
     * 内容
     */
    private String content;

    /**
     * sms类型  1.短信验证 2.营销通知
     */
    private int smsType;
    
    private String name;
    
    //签名
    private String signName;

    public SMSNotificationParam(String phone,String content,int smsType){
        this.phone=phone;
        this.content=content;
        this.smsType=smsType;
    }

    
    public String getSignName() {
		return signName;
	}


	public void setSignName(String signName) {
		this.signName = signName;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getType() {
        return 1;
    }

    public int getSmsType() {
        return smsType;
    }

    public void setSmsType(int smsType) {
        this.smsType = smsType;
    }
}
