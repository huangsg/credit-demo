package com.zhucai.credit.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.zhucai.credit.model.PaymentOrderItemEntity;

public interface CashierOrderItemRepository  extends
PagingAndSortingRepository<PaymentOrderItemEntity, Long>,
JpaSpecificationExecutor<PaymentOrderItemEntity> {

	/**
	 * 根据流水号查询支付明细单
	 * @param channelSerialNum
	 * @return
	 */
	@Query(value = "select * from finance_pay_order_item where order_no = ?1 and flow_number = ?2 and pay_type = ?3",nativeQuery = true)
	PaymentOrderItemEntity findOrderItemByFlowNumber(
			String orderId,String channelSerialNum,String payType);

	/**
	 * 根据融资单号查询支付成功的记录
	 * @param recordNo
	 * @return
	 */
	@Query(value = "select count(*) from finance_pay_order_item where order_no = ?1 and pay_status = '02'",nativeQuery = true)
	Integer findOrderItemByOrderNo(String recordNo);

	/**
	 * 根据订单编号模糊查询订单明细信息
	 * @param orderNo
	 * @return
	 */
	@Query(value = "select fpoi.id,fpoi.pay_amt,fpoi.flow_number,fpoi.order_no,fpo.product_remark" +
			" from finance_pay_order_item fpoi " +
			" left join finance_pay_order fpo on fpo.order_no = fpoi.order_no " +
			" where 1=1 " +
			" and fpoi.status <> 3 " +
			" and fpoi.order_no like concat('%',?1,'%') " +
			" and fpoi.pay_status = '02' " +
			" and fpoi.trade_status = 'NORMAL'" +
			" and fpoi.create_user in (?2)",nativeQuery = true)
	List<Object[]> findByOrderNoLike(String orderNo,List<Long> userIdList);

}
