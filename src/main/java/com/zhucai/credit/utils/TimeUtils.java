package com.zhucai.credit.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.util.StringUtils;


public class TimeUtils {

    public static final int YEAR_RETURN = 0;  
    
    public static final int MONTH_RETURN = 1;  
      
    public static final int DAY_RETURN = 2;  
      
    public static final int HOUR_RETURN= 3;  
      
    public static final int MINUTE_RETURN = 4;  
      
    public static final int SECOND_RETURN = 5;  
      
      
    public static final String YYYY = "yyyy";  
      
    public static final String YYYYMM = "yyyy-MM";  
      
    public static final String YYYYMMDD_A = "yyyyMMdd";  
    public static final String YYYYMMDD = "yyyy-MM-dd";  
      
    public static final String YYYYMMDDHH= "yyyy-MM-dd HH";  
      
    public static final String YYYYMMDDHHMM = "yyyy-MM-dd HH:mm";  
      
    public static final String YYYYMMDDHHMMSS = "yyyy-MM-dd HH:mm:ss";  
    
    
	public static String getLongToString(Long longTime) {
		if (longTime == null) {
			return null;
		}
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date d=new Date(longTime);
		return sf.format(d);
	}
	public static String getDateToString(Date dateTime) {
		if (dateTime == null) {
			return null;
		}
		SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd HHmmss");
		return sf.format(dateTime);
	}
	public static String getDateToString2(Date dateTime) {
		if (dateTime == null) {
			return null;
		}
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		return sf.format(dateTime);
	}
	public static String getRZINum(Date dateTime) {
		if (dateTime == null) {
			return null;
		}
		SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddHHmmss");
		return sf.format(dateTime);
	}
	public static Date getStringToDate(String dateTime) {
		try {
			if (StringUtils.isEmpty(dateTime)) {
				return null;
			}
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			return sf.parse(dateTime);
		} catch (ParseException e) {
			return null;
		}
	}
	//根据格式返回日期格式
	public static Date getDateByFormat(String dateTime,String format) {
		try {
			if (StringUtils.isEmpty(dateTime)) {
				
				return null;
			}
			SimpleDateFormat sf = new SimpleDateFormat(format);
			return sf.parse(dateTime);
		} catch (ParseException e) {
			return null;
		}
	}
	//根据格式返回日期格式
	public static Date getDateByFormat2(Date dateTime,String format) {
		try {
			if (StringUtils.isEmpty(dateTime)) {
				return null;
			}
			SimpleDateFormat sf = new SimpleDateFormat(format);
			String format2 = sf.format(dateTime);
			return sf.parse(format2);
		} catch (ParseException e) {
			return null;
		}
	}
	  
	   /** 
	    * 获取过去第几天的日期 
	    * 
	    * @param past 
	    * @return 
	    * @throws ParseException 
	    */  
	   public static Date getPastDate(int past) {  
	       Calendar calendar = Calendar.getInstance();  
	       calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - past);  
	       Date today = calendar.getTime();  
	       SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");  
	       String result = format.format(today);  
	       Date parse;
		try {
			parse = format.parse(result);
		} catch (ParseException e) {
			return null;
		}
	       return parse;  
	   }  
	  
	   /** 
	    * 获取未来 第 past 天的日期 
	    * @param past 
	    * @return 
	    */  
	   public static Date getFetureDate(int past) {  
	       Calendar calendar = Calendar.getInstance();  
	       calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) + past);  
	       Date today = calendar.getTime();  
	       SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");  
	       String result = format.format(today);  
	       Date future;
			try {
				future = format.parse(result);
			} catch (ParseException e) {
				return null;
			}
	       return future;  
	   }  
	   /** 
	    * 获取当天的最小时间
	    * @param past 
	    * @return 
	    */  
	public static Date getMinNow() {  
		   Calendar calendar = Calendar.getInstance();  
		   Date today = calendar.getTime();  
		   SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");  
		   String result = format.format(today)+" 00:00:00";  
		   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		   try {
				today = sdf.parse(result);
			} catch (ParseException e) {
				return null;
			}
		   return today;  
	   }  
	   /** 
	    * 获取当天的的最大时间
	    * @param past 
	    * @return 
	    */  
	   public static Date getMaxNow() {  
		   Calendar calendar = Calendar.getInstance();  
		   Date today = calendar.getTime();  
		   SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");  
		   String result = format.format(today)+" 23:59:59";  
		   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		   try {
				today = sdf.parse(result);
			} catch (ParseException e) {
				return null;
			}
		   return today;  
	   }  
	   /**
	     * 计算2个时间间隔 返回 **天**小时**分钟
	     * start-end
	     *
	     * @param dateStart
	     * @param dateEnd
	     * @return
	     */
	    public static String dateDiff(Date dateStart, Date dateEnd) {
	    	String rsStr = null;
	        long nd = 1000 * 24 * 60 * 60;//一天的毫秒数
	        long nh = 1000 * 60 * 60;//一小时的毫秒数
	        long nm = 1000 * 60;//一分钟的毫秒数
	        //	      获得两个时间的毫秒时间差异
	        long diff = 0;
	        if(dateStart.compareTo(dateEnd)==-1){
	        	diff = dateEnd.getTime() - dateStart.getTime();
			}else{
				diff = dateStart.getTime() - dateEnd.getTime();
			}
	        long day = diff / nd;//计算差多少天
	        long hour = diff % nd / nh;//计算差多少小时
	        long min = diff % nd % nh / nm;//计算差多少分钟
	        if (min < 0) {
	            return rsStr;
	        } else {
	        	if(day>0){
	        		rsStr = day + "天" + hour + "小时" + min + "分钟";
	        	}else{
	        		rsStr =  hour + "小时" + min + "分钟";
	        	}
	            return rsStr;
	        }
	        

	    }
	    /**
	     * 计算2个时间间隔 天数
	     * start-end
	     *
	     * @param dateStart
	     * @param dateEnd
	     * @return
	     */
	    public static Integer dateTian(Date dateStart, Date dateEnd) {
	    	if(dateStart.before(dateEnd)){
	    		return 0;
	    	}
	    	 SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");  
	    	 Integer day = 0;
			try {
				dateStart=sdf.parse(sdf.format(dateStart));  
				dateEnd=sdf.parse(sdf.format(dateEnd));
				long nd = 1000 * 24 * 60 * 60;//一天的毫秒数
		    	Long load = dateStart.getTime();
		    	Long end = dateEnd.getTime();
		    	Long da=(load-end)/nd;
		    	day = Integer.valueOf(da.toString());
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    	
			return day;
	    	
	    }
	    /**
	     * 时间转换格式
	     * start-end
	     */
	    public static Date changeDate(String parm) {
	    	
	    	SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
	    	SimpleDateFormat sf2 = new SimpleDateFormat("yyyy-MM-dd");
	    	try {
				Date d2 = sf.parse(parm);
				String d3 = sf2.format(d2);
				 Date rsDate = sf2.parse(d3);
				 return rsDate;
			} catch (ParseException e) {
				e.printStackTrace();
				return null;
			}
	    	
	    }
	    /**
	     * 时间转换格式
	     * start-end
	     */
	    public static Date changeDate3(Date parm) {
	    	
	    	SimpleDateFormat sf2 = new SimpleDateFormat("yyyy-MM-dd");
	    	try {
	    		String d3 = sf2.format(parm);
	    		Date rsDate = sf2.parse(d3);
	    		return rsDate;
	    	} catch (ParseException e) {
	    		e.printStackTrace();
	    		return null;
	    	}
	    	
	    }
	    /**
	     * 时间转换格式
	     * start-end
	     */
	    public static String changeDate2(Date parm) {
	    	SimpleDateFormat sf2 = new SimpleDateFormat("yyyyMMdd");
	    	String format = sf2.format(parm);
	    	return format;
	    }
	    
	
      
      
    public static long getBetween(String beginTime, String endTime, String formatPattern, int returnPattern) throws ParseException{  
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatPattern);  
            Date beginDate = simpleDateFormat.parse(beginTime);  
            Date endDate = simpleDateFormat.parse(endTime);  
              
            Calendar beginCalendar = Calendar.getInstance();  
            Calendar endCalendar = Calendar.getInstance();  
            beginCalendar.setTime(beginDate);  
            endCalendar.setTime(endDate);  
            switch (returnPattern) {  
            case YEAR_RETURN:  
                return TimeUtils.getByField(beginCalendar, endCalendar, Calendar.YEAR);  
            case MONTH_RETURN:  
                return TimeUtils.getByField(beginCalendar, endCalendar, Calendar.YEAR)*12 + TimeUtils.getByField(beginCalendar, endCalendar, Calendar.MONTH);  
            case DAY_RETURN:  
                return TimeUtils.getTime(beginDate, endDate)/(24*60*60*1000);  
            case HOUR_RETURN:  
                return TimeUtils.getTime(beginDate, endDate)/(60*60*1000);  
            case MINUTE_RETURN:  
                return TimeUtils.getTime(beginDate, endDate)/(60*1000);  
            case SECOND_RETURN:  
                return TimeUtils.getTime(beginDate, endDate)/1000;  
            default:  
                return 0;  
            }  
        }  
    private static long getByField(Calendar beginCalendar, Calendar endCalendar, int calendarField){  
    	long i = endCalendar.get(calendarField) - beginCalendar.get(calendarField);
    	if(i==0){
    		i=1;
    	}
    	
        return  i;
    }  
      
    private static long getTime(Date beginDate, Date endDate){  
        return endDate.getTime() - beginDate.getTime();  
    }  
    /** 
    * 获取未来 第 past 天的日期 
    * @param past 
    * @param dateFormat
    * @return 
    */  
   public static Date getFetureDate(int past,String dateFormat) {  
       Calendar calendar = Calendar.getInstance();  
       calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) + past);  
       Date today = calendar.getTime();  
       SimpleDateFormat format = new SimpleDateFormat(dateFormat);  
       String result = format.format(today);  
       Date future;
		try {
			future = format.parse(result);
		} catch (ParseException e) {
			return null;
		}
       return future;  
   }
   /**
    * 得到几天前的时间
    * @param d
    * @param day
    * @return
    */
   public static Date getDateBefore(Date d,int day){
    Calendar now =Calendar.getInstance();
    now.setTime(d);
    now.set(Calendar.DATE,now.get(Calendar.DATE)-day);
    return now.getTime();
   }
   
   /**
    * 得到几天后的时间
    * @param d
    * @param day
    * @return
    */
   public static Date getDateAfter(Date d,int day){
    Calendar now =Calendar.getInstance();
    now.setTime(d);
    now.set(Calendar.DATE,now.get(Calendar.DATE)+day);
    return now.getTime();
   }
    public static void main(String[] args) {  
        try {  
        	long between = TimeUtils.getBetween("20130101", "20130505", TimeUtils.YYYYMMDD_A, TimeUtils.YEAR_RETURN);
        	System.out.println(between);
        } catch (ParseException e) {  
            e.printStackTrace();  
        }  
          
    }
}
