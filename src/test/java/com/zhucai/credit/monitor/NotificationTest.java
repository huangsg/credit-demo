package com.zhucai.credit.monitor;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.zhucai.credit.monitor.bean.SendMsgResult;
import com.zhucai.credit.monitor.service.INotificationService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class NotificationTest {
	
	@Resource(name = "SMS")
	private INotificationService iNotificationService;
	
	@Test
	public void test() {
		SendMsgResult notification = iNotificationService.notification( "供应商审核通过的融资计划到建行审核 异常  recordNo=RZI20180612215904err=java.lang.RuntimeException: java.net.SocketException: Network is unreachable");
		System.out.println(notification.getMsg());
	}
	
	public static void main(String[] args) throws UnknownHostException {
        InetAddress addr = InetAddress.getLocalHost();  
        String ip=addr.getHostAddress().toString(); //获取本机ip  
        String hostName=addr.getHostName().toString(); //获取本机计算机名称  
        System.out.println(ip);
        System.out.println(hostName);
	}
}
