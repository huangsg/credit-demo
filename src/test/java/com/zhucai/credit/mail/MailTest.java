package com.zhucai.credit.mail;

import java.io.File;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.junit4.SpringRunner;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MailTest {
	
	@Value("${icbc.mail.1.to}")
	private String to;

	@Value("${spring.mail.username}")
	private String from;
	
	@Value("${icbc.excel.path}")
	private String path;
	
	@Autowired
	private JavaMailSender mailSender;
	
	@Autowired
	private TemplateEngine templateEngine;
	
	@Test
	public void test() throws Exception {
		System.setProperty("mail.mime.splitlongparameters", "false");
		MimeMessage message = mailSender.createMimeMessage();
		try {
			MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");  
			messageHelper.setFrom("筑材网 <"+from+">");  
			messageHelper.setSubject("陶杰测试");  
			messageHelper.setTo(to);  
			//messageHelper.setBcc(new String[]{"co@zhucai.com"});
			String x = templateEngine.process("icbc/Mail1", new Context());
			messageHelper.setText(x,true);
			messageHelper.addAttachment("客户准入申请_20180814.xlsx", new File(path,"out.xlsx"));
			

		} catch (MessagingException e) {
			e.printStackTrace();
		}

		mailSender.send(message); 
	}

}
