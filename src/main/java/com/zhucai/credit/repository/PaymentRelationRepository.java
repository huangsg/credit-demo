package com.zhucai.credit.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.zhucai.credit.model.PaymentRelationEntity;

/**
 * 融资支付关系dao
 * @author xjz
 *
 */
@Repository
public interface PaymentRelationRepository extends
PagingAndSortingRepository<PaymentRelationEntity, Long>,
JpaSpecificationExecutor<PaymentRelationEntity> {

	@Query(value = "select * from finance_payment_relation where pay_single_no = ?1",nativeQuery = true)
	PaymentRelationEntity findByPaySingleNo(String orderNo);

	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value = "update finance_payment_relation set record_no = ?2 where pay_single_no = ?1",nativeQuery = true)
	int updateByPaySingleNo(String string, String recordNo);

}
