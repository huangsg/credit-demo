package com.zhucai.credit.base;

import org.apache.commons.lang3.StringUtils;

/**
 * <p>
 * Title:文本键值对
 * </p>
 * @author: huangshenggen
 * @version: 1.0
 */
public class TextEntry extends Entry<String, String> implements Comparable<TextEntry> {

	public TextEntry() {
		super();
	}

	public TextEntry(String key, String value) {
		super(key, value);
	}

	@Override
	public int compareTo(TextEntry o) {
		if (o == null)
			return -1;
		String key = StringUtils.defaultIfEmpty(getKey(), "");
		String okey = StringUtils.defaultIfEmpty(o.getKey(), "");
		return key.compareTo(okey);
	}
}
