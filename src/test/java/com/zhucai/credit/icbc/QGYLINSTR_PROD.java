package com.zhucai.credit.icbc;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.jsoup.nodes.Document;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.zhucai.credit.utils.StrongJsoup;

@RunWith(SpringRunner.class)
@SpringBootTest
public class QGYLINSTR_PROD {
	
	@Autowired
	private TemplateEngine templateEngine;
	 

	
	/**
	 * 付款查询接口
	 */
	
	@Test
    public void build() throws Exception{
		Date d = new Date();
		SimpleDateFormat fmtTranDate = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat fmtTranTime = new SimpleDateFormat("HHmmssSSS");
		
  
		String cis = "111190001616306";
        String bankCode = "102";
        String id = "JSZCW01.y.1111";
        long fSeqno = d.getTime();
        String CODE = "QGYLINSTR";
        String tranDate = fmtTranDate.format(d);
        String tranTime = fmtTranTime.format(d);
        
        
        Context context = new Context();
		context.setVariable("CIS",  cis);
		context.setVariable("BankCode",  bankCode);
		context.setVariable("ID",  id);
        
		context.setVariable("TranDate",  tranDate);
		context.setVariable("TranTime",  tranTime);
		context.setVariable("fSeqno",  fSeqno);

		//<fSeqno>1533173456176</fSeqno>
		//<SerialNo>HNH1544401</SerialNo>
		
		//<fSeqno>1533191831307</fSeqno>
		//<SerialNo>HNH1544402</SerialNo>
		
		
		//<fSeqno>1535163616311</fSeqno>
		//<SerialNo>HNH1546401</SerialNo>
		//context.setVariable("QryfSeqno",  "1535163616311");
		context.setVariable("QrySerialNo",  "HNH1531073");
		//context.setVariable("QrySerialNo",  "HNH1531074");
		context.setVariable("QryOrderNo",  "1");
		
        
        String x = "<?xml version=\"1.0\" encoding=\"GBK\"?>"+templateEngine.process("icbc/"+CODE, context);
        System.out.println(x);
        
        
		String url = "http://58.247.178.190:448/servlet/ICBCCMPAPIReqServlet?userID="+id+"&PackageID="+fSeqno+"&SendTime="+tranDate+tranTime;
		StrongJsoup connect = StrongJsoup.connect(url);
		connect.headers(ImmutableMap.of("Content-Type", "application/x-www-form-urlencoded"));
		
		Map<String,String> params = Maps.newHashMap();
		params.put("Version", "0.0.0.1");
		params.put("TransCode",CODE);
		params.put("BankCode",bankCode);
		params.put("GroupCIS",cis);
		params.put("ID",id);
		params.put("Cert","");
		params.put("reqData",x);
		System.out.println(url);
		
		System.out.println(params);
		connect.getJsoupConn().data(params);
		
		Document post = connect.post();
		String text = post.text();
		System.out.println(text);
		
		Base64 base64 = new Base64();
		String substring = text.substring(text.indexOf("=")+1);
		System.out.println(substring);
		byte[] decode = base64.decode(substring);
		System.out.println(new String(decode,"GBK"));
        
        
        
    }


}
