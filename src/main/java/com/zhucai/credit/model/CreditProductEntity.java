package com.zhucai.credit.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * 贷款服务机构
 * 2017年9月19日
 * @author hsg
 */
@Entity
@Table(name="finance_product_type")
public class CreditProductEntity extends BaseEntity {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "服务机构产品名称,")
	private String crProdName;
	@ApiModelProperty(value = "服务机构银行,")
	private String crProdBankName;
	@ApiModelProperty(value = "服务机构类型,Enum(CrProdType)")
	private String crProdType;
	@ApiModelProperty(value = "银行官网对应的url")
	private String jumpUrlAddress;
	@ApiModelProperty(value = "贷款支取对应的跳转url")
	private String drawUrlAddress;
	@ApiModelProperty(value = "贷款还款对应的跳转url")
	private String repaymentUrlAddress;
	@ApiModelProperty(value = "合同签订的跳转url")
	private String contractUrlAddress;
	@ApiModelProperty(value = "银行云链机构样式")
	private String crBankClass;
	@ApiModelProperty(value = "银行选择默认样式")
	private String crBankChoiceClass;
	@ApiModelProperty(value = "对接是否成功，默认0没对接成功，1对接成功")
	private Integer buttStatus;
	@Column(nullable=true,length=50)
	public String getCrBankChoiceClass() {
		return crBankChoiceClass;
	}
	public void setCrBankChoiceClass(String crBankChoiceClass) {
		this.crBankChoiceClass = crBankChoiceClass;
	}
	@Column(nullable=true,length=265)
	public String getDrawUrlAddress() {
		return drawUrlAddress;
	}
	public void setDrawUrlAddress(String drawUrlAddress) {
		this.drawUrlAddress = drawUrlAddress;
	}
	@Column(nullable=true,length=265)
	public String getRepaymentUrlAddress() {
		return repaymentUrlAddress;
	}
	public void setRepaymentUrlAddress(String repaymentUrlAddress) {
		this.repaymentUrlAddress = repaymentUrlAddress;
	}
	@Column(nullable=true,length=265)
	public String getContractUrlAddress() {
		return contractUrlAddress;
	}
	public void setContractUrlAddress(String contractUrlAddress) {
		this.contractUrlAddress = contractUrlAddress;
	}
	@Column(nullable=true,length=2,columnDefinition="INT default 0")
	public Integer getButtStatus() {
		return buttStatus;
	}
	public void setButtStatus(Integer buttStatus) {
		this.buttStatus = buttStatus;
	}
	@Column(nullable = true, length = 265)
	public String getJumpUrlAddress() {
		return jumpUrlAddress;
	}
	public void setJumpUrlAddress(String jumpUrlAddress) {
		this.jumpUrlAddress = jumpUrlAddress;
	}
	@Column(nullable = true, length = 50)
	public String getCrBankClass() {
		return crBankClass;
	}
	public void setCrBankClass(String crBankClass) {
		this.crBankClass = crBankClass;
	}
	@Column(nullable = true, length = 50)
	public String getCrProdName() {
		return crProdName;
	}
	public void setCrProdName(String crProdName) {
		this.crProdName = crProdName;
	}
	@Column(nullable = false, length = 20)
	public String getCrProdType() {
		return crProdType;
	}
	public void setCrProdType(String crProdType) {
		this.crProdType = crProdType;
	}
	@Column(nullable=true,length=50)
	public String getCrProdBankName() {
		return crProdBankName;
	}
	public void setCrProdBankName(String crProdBankName) {
		this.crProdBankName = crProdBankName;
	}
	
}
