package com.zhucai.credit.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * （供应商）信用机构资质申请（e点通）
 * 2017年9月19日
 * @author hsg
 */
@Entity
@Table(name="finance_credit_application")
public class CreditApplicationEntity extends BaseEntity {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "建行的时候建行给的，工行的时候没有这个所以自定义一个")
	private String appNum;
	
	@ApiModelProperty(value = "申请金融id(供应商)")
	private Long crApplicantId;
	@ApiModelProperty(value = "申请组织机构号(供应商)")
	private String appOrgnNum;
	@ApiModelProperty(value = "申请组织客户的姓名(供应商)")
	private String applicantName;
	@ApiModelProperty(value = "申请组织客户的固定电话(供应商)")
	private String applicantPhone;
	@ApiModelProperty(value = "申请组织客户的手机号码(供应商)")
	private String applicantMobile;
	@ApiModelProperty(value = "申请组织客户的企业名称,(供应商)")
	private String applicantEnterpriseName;
	@ApiModelProperty(value = "申请组织客户的企业id(工行模式下可能先没有入驻金融),")
	private Long applicantEnterpriseId;
	@ApiModelProperty(value = "合作采购商id")
	private Long crApplicantPurId;
	@ApiModelProperty(value = "申请合作组织机构号(采购商)")
	private String cooOrgnNum;
	@ApiModelProperty(value = "贷款服务机构id(融资机构id)")
	private Long crProdId;
	@ApiModelProperty(value = "审核状态Enum(CrAprovalStatus)")
	private String crAprovalStatus;
	@ApiModelProperty(value = "驳回状态原因")
	private String rejectRemark;
	
	@ApiModelProperty(value = "对供应商评价 ,")
	private String crSupEvaluate;
	@ApiModelProperty(value = "合作状态 ,Enum(CrProdstatus)")
	private String crProdstatus;
	@ApiModelProperty(value = "合作申请时间 ,")
	private Date crApplyTime;
	@ApiModelProperty(value = "合作开始时间 ,")
	private Date crProdStartTime;
	@ApiModelProperty(value = "合作结束时间 ,")
	private Date crProdEndTime;
	@ApiModelProperty(value = "（授信额度）,")
	private BigDecimal contractAmt;
	@ApiModelProperty(value = "最近的操作记录id,")
	private Long operRecordId;
	
	/**
	 * 建设银行相关字段
	 */
	
	@ApiModelProperty(value = "核心企业建议授信额度-CCB")
	private Integer crLineOfCredit;
	@ApiModelProperty(value = "申贷金额-CCB")
	private BigDecimal applyAmt;
	@ApiModelProperty(value = "违约次数 ,（暂时和纠纷次数相同）-CCB")
	private Integer crDefaultTimes;
	@ApiModelProperty(value = "供应商的结算账户,-CCB")
	private String clearanceAccount;
	@ApiModelProperty(value = "回款专户 ,-CCB")
	private String recoverAccount;
	
	@ApiModelProperty(value = "供应商和核心企业的对应银行的合同编号-CCB,")
	private String contractNum;
	@ApiModelProperty(value = "交易活跃度 ,A,B,C,D,E(A为最好)-CCB")
	private String transactionActivity;
	@ApiModelProperty(value = "上下游稳定性 ,5,4,3,2,1(5为最好)-CCB")
	private String fluctStabilization;
	@ApiModelProperty(value = "交易年限 ,(单位月)-CCB")
	private Double tradingYears;
	@ApiModelProperty(value = "交易量评价 ,A,B,C,D,E(A为最好)-CCB")
	private String transactionEvaluate;
	@ApiModelProperty(value = "纠纷次数 ,暂时和违约次数相同-CCB")
	private Integer disputeCount;
	@ApiModelProperty(value = "本次合作有效期,-CCB")
	private Integer termOfValidity;
	
	/**
	 * 工商银行的相关字段
	 */
	@ApiModelProperty(value = "供应商监管账号,-ICBC")
	private String supEnterpriseAccount;
	@ApiModelProperty(value = "供应收款账号,-ICBC")
	private String supReceivablesAccount;
	@ApiModelProperty(value = "核心企业账号,-ICBC")
	private String enterpriseAccount;
	@ApiModelProperty(value = "供应链编号,-ICBC")
	private String supplyChainNumber;
	@ApiModelProperty(value = "历史交易年限（年）,-ICBC")
	private Double historyTransactionYears;
	@ApiModelProperty(value = "历史交易纠纷,-ICBC")
	private String historyDisputeCount;
	@ApiModelProperty(value = "历史交易坏账率(数值越低，代表坏账率越低，无坏账请填写0，必填项),-ICBC")
	private BigDecimal historyDebtRate;
	@ApiModelProperty(value = "退货率,-ICBC")
	private BigDecimal returnRate;
	@ApiModelProperty(value = "前三年首年供货总额,-ICBC")
	private BigDecimal threeYearsSupply;
	@ApiModelProperty(value = "前三年首年应收核心企业账款余额,-ICBC")
	private BigDecimal threeYearsReceivable;
	@ApiModelProperty(value = "前二年首年供货总额,-ICBC")
	private BigDecimal twoYearsSupply;
	@ApiModelProperty(value = "前二年首年应收核心企业账款余额,-ICBC")
	private BigDecimal twoYearsReceivable;
	@ApiModelProperty(value = "上年供货总额,-ICBC")
	private BigDecimal oneYearsSupply;
	@ApiModelProperty(value = "上年应收核心企业账款余额,-ICBC")
	private BigDecimal oneYearsReceivable;
	@ApiModelProperty(value = " 本年供货预计增长率(数值越大，代表增长率越大，1代表增长百分百，必填项),-ICBC")
	private BigDecimal thisYearsGrowthRate;
	@Column(nullable = true)
	
	public String getSupReceivablesAccount() {
		return supReceivablesAccount;
	}
	@Column(nullable = true,length = 50)
	public void setSupReceivablesAccount(String supReceivablesAccount) {
		this.supReceivablesAccount = supReceivablesAccount;
	}
	public Long getApplicantEnterpriseId() {
		return applicantEnterpriseId;
	}
	public void setApplicantEnterpriseId(Long applicantEnterpriseId) {
		this.applicantEnterpriseId = applicantEnterpriseId;
	}
	@Column(nullable = true,length = 250)
	public String getSupEnterpriseAccount() {
		return supEnterpriseAccount;
	}
	public void setSupEnterpriseAccount(String supEnterpriseAccount) {
		this.supEnterpriseAccount = supEnterpriseAccount;
	}
	@Column(nullable = true,length = 250)
	public String getRejectRemark() {
		return rejectRemark;
	}
	public void setRejectRemark(String rejectRemark) {
		this.rejectRemark = rejectRemark;
	}
	@Column(nullable = true,length = 50)
	public String getEnterpriseAccount() {
		return enterpriseAccount;
	}
	public void setEnterpriseAccount(String enterpriseAccount) {
		this.enterpriseAccount = enterpriseAccount;
	}
	@Column(nullable = true,length = 50)
	public String getSupplyChainNumber() {
		return supplyChainNumber;
	}
	public void setSupplyChainNumber(String supplyChainNumber) {
		this.supplyChainNumber = supplyChainNumber;
	}
	@Column(nullable = true)
	public Double getHistoryTransactionYears() {
		return historyTransactionYears;
	}
	public void setHistoryTransactionYears(Double historyTransactionYears) {
		this.historyTransactionYears = historyTransactionYears;
	}
	@Column(nullable = true,length = 55)
	public String getHistoryDisputeCount() {
		return historyDisputeCount;
	}
	public void setHistoryDisputeCount(String historyDisputeCount) {
		this.historyDisputeCount = historyDisputeCount;
	}
	@Column(nullable = true)
	public BigDecimal getHistoryDebtRate() {
		return historyDebtRate;
	}
	public void setHistoryDebtRate(BigDecimal historyDebtRate) {
		this.historyDebtRate = historyDebtRate;
	}
	@Column(nullable = true)
	public BigDecimal getReturnRate() {
		return returnRate;
	}
	public void setReturnRate(BigDecimal returnRate) {
		this.returnRate = returnRate;
	}
	@Column(nullable = true)
	public BigDecimal getThreeYearsSupply() {
		return threeYearsSupply;
	}
	public void setThreeYearsSupply(BigDecimal threeYearsSupply) {
		this.threeYearsSupply = threeYearsSupply;
	}
	@Column(nullable = true)
	public BigDecimal getThreeYearsReceivable() {
		return threeYearsReceivable;
	}
	public void setThreeYearsReceivable(BigDecimal threeYearsReceivable) {
		this.threeYearsReceivable = threeYearsReceivable;
	}
	@Column(nullable = true)
	public BigDecimal getTwoYearsSupply() {
		return twoYearsSupply;
	}
	public void setTwoYearsSupply(BigDecimal twoYearsSupply) {
		this.twoYearsSupply = twoYearsSupply;
	}
	@Column(nullable = true)
	public BigDecimal getTwoYearsReceivable() {
		return twoYearsReceivable;
	}
	public void setTwoYearsReceivable(BigDecimal twoYearsReceivable) {
		this.twoYearsReceivable = twoYearsReceivable;
	}
	@Column(nullable = true)
	public BigDecimal getOneYearsSupply() {
		return oneYearsSupply;
	}
	public void setOneYearsSupply(BigDecimal oneYearsSupply) {
		this.oneYearsSupply = oneYearsSupply;
	}
	@Column(nullable = true)
	public BigDecimal getOneYearsReceivable() {
		return oneYearsReceivable;
	}
	public void setOneYearsReceivable(BigDecimal oneYearsReceivable) {
		this.oneYearsReceivable = oneYearsReceivable;
	}
	@Column(nullable = true)
	public BigDecimal getThisYearsGrowthRate() {
		return thisYearsGrowthRate;
	}
	public void setThisYearsGrowthRate(BigDecimal thisYearsGrowthRate) {
		this.thisYearsGrowthRate = thisYearsGrowthRate;
	}
	@Column(nullable=true,columnDefinition = "decimal(20,2)")
	public BigDecimal getContractAmt() {
		return contractAmt;
	}
	public void setContractAmt(BigDecimal contractAmt) {
		this.contractAmt = contractAmt;
	}
	@Column(nullable = true, length =2)
	public String getFluctStabilization() {
		return fluctStabilization;
	}
	public void setFluctStabilization(String fluctStabilization) {
		this.fluctStabilization = fluctStabilization;
	}
	@Column(nullable = true, length = 30)
	public String getApplicantName() {
		return applicantName;
	}
	public void setApplicantName(String applicantName) {
		this.applicantName = applicantName;
	}
	@Column(nullable = true, length = 20)
	public String getApplicantPhone() {
		return applicantPhone;
	}
	public void setApplicantPhone(String applicantPhone) {
		this.applicantPhone = applicantPhone;
	}
	@Column(nullable = true, length = 12)
	public String getApplicantMobile() {
		return applicantMobile;
	}
	public void setApplicantMobile(String applicantMobile) {
		this.applicantMobile = applicantMobile;
	}
	@Column(nullable = true, length = 100)
	public String getApplicantEnterpriseName() {
		return applicantEnterpriseName;
	}
	public void setApplicantEnterpriseName(String applicantEnterpriseName) {
		this.applicantEnterpriseName = applicantEnterpriseName;
	}
	@Column(nullable = true, length = 30)
	public String getClearanceAccount() {
		return clearanceAccount;
	}
	public void setClearanceAccount(String clearanceAccount) {
		this.clearanceAccount = clearanceAccount;
	}
	@Column(nullable = true, length = 30)
	public String getRecoverAccount() {
		return recoverAccount;
	}
	public void setRecoverAccount(String recoverAccount) {
		this.recoverAccount = recoverAccount;
	}
	@Column(nullable = false, length = 20)
	public Long getCrApplicantId() {
		return crApplicantId;
	}
	public void setCrApplicantId(Long crApplicantId) {
		this.crApplicantId = crApplicantId;
	}
	@Column(nullable = false, length = 20)
	public Long getCrApplicantPurId() {
		return crApplicantPurId;
	}
	public void setCrApplicantPurId(Long crApplicantPurId) {
		this.crApplicantPurId = crApplicantPurId;
	}
	@Column(nullable = false, length = 20)
	public Long getCrProdId() {
		return crProdId;
	}
	public void setCrProdId(Long crProdId) {
		this.crProdId = crProdId;
	}
	@Column(nullable = false, length = 20)
	public String getCrAprovalStatus() {
		return crAprovalStatus;
	}
	public void setCrAprovalStatus(String crAprovalStatus) {
		this.crAprovalStatus = crAprovalStatus;
	}
	@Column(nullable = true, length = 20)
	public String getCrProdstatus() {
		return crProdstatus;
	}
	public void setCrProdstatus(String crProdstatus) {
		this.crProdstatus = crProdstatus;
	}
	@Column(nullable = true, length = 20)
	public Date getCrApplyTime() {
		return crApplyTime;
	}
	public void setCrApplyTime(Date crApplyTime) {
		this.crApplyTime = crApplyTime;
	}
	@Column(nullable = true, length = 20)
	public Date getCrProdStartTime() {
		return crProdStartTime;
	}
	public void setCrProdStartTime(Date crProdStartTime) {
		this.crProdStartTime = crProdStartTime;
	}
	@Column(nullable = true, length = 20)
	public Date getCrProdEndTime() {
		return crProdEndTime;
	}
	public void setCrProdEndTime(Date crProdEndTime) {
		this.crProdEndTime = crProdEndTime;
	}
	@Column(nullable=true,length=20)
	public Long getOperRecordId() {
		return operRecordId;
	}
	public void setOperRecordId(Long operRecordId) {
		this.operRecordId = operRecordId;
	}
	@Column(nullable=true,length=200)
	public String getCrSupEvaluate() {
		return crSupEvaluate;
	}
	public void setCrSupEvaluate(String crSupEvaluate) {
		this.crSupEvaluate = crSupEvaluate;
	}
	@Column(nullable=true,length=20,columnDefinition="INT default 0")
	public Integer getCrDefaultTimes() {
		return crDefaultTimes;
	}
	public void setCrDefaultTimes(Integer crDefaultTimes) {
		this.crDefaultTimes = crDefaultTimes;
	}
	@Column(nullable=true,length=20,columnDefinition="INT default 0")
	public Integer getCrLineOfCredit() {
		return crLineOfCredit;
	}
	public void setCrLineOfCredit(Integer crLineOfCredit) {
		this.crLineOfCredit = crLineOfCredit;
	}
	@Column(nullable=true,length=20,columnDefinition = "DOUBLE default 0")
	public Double getTradingYears() {
		return tradingYears;
	}
	public void setTradingYears(Double tradingYears) {
		this.tradingYears = tradingYears;
	}
	@Column(nullable=true,length=20,columnDefinition = "INT default 0")
	public Integer getDisputeCount() {
		return disputeCount;
	}
	public void setDisputeCount(Integer disputeCount) {
		this.disputeCount = disputeCount;
	}
	@Column(nullable=true,length=2)
	public String getTransactionActivity() {
		return transactionActivity;
	}
	public void setTransactionActivity(String transactionActivity) {
		this.transactionActivity = transactionActivity;
	}
	@Column(nullable=true,length=2)
	public String getTransactionEvaluate() {
		return transactionEvaluate;
	}
	public void setTransactionEvaluate(String transactionEvaluate) {
		this.transactionEvaluate = transactionEvaluate;
	}
	@Column(nullable=true,length=20)
	public Integer getTermOfValidity() {
		return termOfValidity;
	}
	public void setTermOfValidity(Integer termOfValidity) {
		this.termOfValidity = termOfValidity;
	}
	@Column(nullable=true,length=20)
	public String getAppOrgnNum() {
		return appOrgnNum;
	}
	public void setAppOrgnNum(String appOrgnNum) {
		this.appOrgnNum = appOrgnNum;
	}
	@Column(nullable=true,length=20)
	public String getCooOrgnNum() {
		return cooOrgnNum;
	}
	public void setCooOrgnNum(String cooOrgnNum) {
		this.cooOrgnNum = cooOrgnNum;
	}
	@Column(nullable=true,length=18)
	public String getAppNum() {
		return appNum;
	}
	public void setAppNum(String appNum) {
		this.appNum = appNum;
	}
	@Column(nullable=true,length=40)
	public String getContractNum() {
		return contractNum;
	}
	public void setContractNum(String contractNum) {
		this.contractNum = contractNum;
	}
	@Column(nullable=true,columnDefinition = "decimal(20,2)")
	public BigDecimal getApplyAmt() {
		return applyAmt;
	}
	public void setApplyAmt(BigDecimal applyAmt) {
		this.applyAmt = applyAmt;
	}
	
}
