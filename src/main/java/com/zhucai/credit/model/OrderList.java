package com.zhucai.credit.model;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * @author hss
 * @ClassName: OrderList
 * @Description: 订单验收付款计划表
 */
@Entity
@Table(name = "tb_xa_orderlist")
@ApiModel(value = "订单验收付款计划表")
public class OrderList extends BaseEntity  {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "合同Id")
    private Long contractId;
   
	@ApiModelProperty(value = "招标Id")
    private Long bidId;
	
	@ApiModelProperty(value = "合同编号")
    private String contractNumZhucai;
	
	@ApiModelProperty(value = "项目名称")
    private String projectName;
	
	@ApiModelProperty(value = "供应商名称")
    private String supplierName;
	
	@ApiModelProperty(value = "采购商名称")
    private String purchaseName;
	
	@ApiModelProperty(value = "招标合同")
    private String bidContent;
	
	@ApiModelProperty(value = "订单金额--万元")
    private Double amount;
	
	@ApiModelProperty(value = "实际付款金额")
    private Double actualAmountPaid;
	
	@ApiModelProperty(value = "交货地址")
    private String  address;

	@ApiModelProperty(value = "供应方联系人")
    private String  supplierLinkman;
	
	@ApiModelProperty(value = "供应方联系方式")
    private String  supplierContact;
	
	@ApiModelProperty(value = "验收联系人")
    private String  checkName;
	
	@ApiModelProperty(value = "验收联系方式")
    private String  checkPhone;
	
	/****** hss 2018/03/29  订单或预付款状态    ZhucaiConstant.OrderType start  ******/
	@ApiModelProperty(value = "数据类型 1：订单（即验收） 2：预付款")
    private Integer  type;
	/****** hss 2018/03/29  订单或预付款状态   ZhucaiConstant.OrderType end  ******/	
	
	@ApiModelProperty(value = "验收说明")
    private String  remark;

	@ApiModelProperty(value = "发起方类型 0:平台  1：采购商  2：供应商")
    private  Integer startAccountType;
	
	@ApiModelProperty(value = "完成时间")
	private String completeDate;
	
	@ApiModelProperty(value = "约定交货日期")
	private String deliveryDate;
	
	/****** hss 2018/03/29  订单或预付款状态    ZhucaiConstant.OrderStatus start  ******/
	@ApiModelProperty(value = "订单或预付款状态 1：待完成  2：已完成/待发起  3：已发起 ")
	private Integer orderStatus;
	/****** hss 2018/03/29  订单或预付款状态   ZhucaiConstant.OrderStatus end  ******/
	
	
    /****** PMQ 2017/12/29 update 实际付款流程发起人 start  ******/
    @ApiModelProperty(value = "实际付款发起人")
    private Long paymentStarter;
    /****** PMQ 2017/12/29 update 实际付款流程发起人 end  ******/
	
	
	/************订单的争议***************/
    @ApiModelProperty(value = "处理争议的时间")
    private String disputeDate;
    
    @ApiModelProperty(value = "发起争议人ID")
    private Long disputePersonId;
    
    /*************付款订单************/
    @ApiModelProperty(value = "付款日期,付款日期")
    private String paymentDate;
    
    @ApiModelProperty(value = "订单编号,订单编号")
    private String orderNum;
    
    @ApiModelProperty(value = "备注")
    private String reason;
    
    
    @ApiModelProperty(value = "订单支付编号")
    private String orderPayNum;
    
    
    /***********物流订单************/
    @ApiModelProperty("物流的发起类型 1自发物流 2平台物流")
    private Integer contractLogisticsOrderType;
    
    @ApiModelProperty("自发物流  -发货日期")
    private String selfLogisticDeliveryDate;
    
    @ApiModelProperty("自发物流 -承运人")
    private String selfLogisticCarrier;
    
    @ApiModelProperty("自发物流 =承运人联系方式")
    private String selfLogisticCarrierPhone;

	public Long getContractId() {
		return contractId;
	}

	public void setContractId(Long contractId) {
		this.contractId = contractId;
	}

	public Long getBidId() {
		return bidId;
	}

	public void setBidId(Long bidId) {
		this.bidId = bidId;
	}

	public String getContractNumZhucai() {
		return contractNumZhucai;
	}

	public void setContractNumZhucai(String contractNumZhucai) {
		this.contractNumZhucai = contractNumZhucai;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getPurchaseName() {
		return purchaseName;
	}

	public void setPurchaseName(String purchaseName) {
		this.purchaseName = purchaseName;
	}

	public String getBidContent() {
		return bidContent;
	}

	public void setBidContent(String bidContent) {
		this.bidContent = bidContent;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getActualAmountPaid() {
		return actualAmountPaid;
	}

	public void setActualAmountPaid(Double actualAmountPaid) {
		this.actualAmountPaid = actualAmountPaid;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSupplierLinkman() {
		return supplierLinkman;
	}

	public void setSupplierLinkman(String supplierLinkman) {
		this.supplierLinkman = supplierLinkman;
	}

	public String getSupplierContact() {
		return supplierContact;
	}

	public void setSupplierContact(String supplierContact) {
		this.supplierContact = supplierContact;
	}

	public String getCheckName() {
		return checkName;
	}

	public void setCheckName(String checkName) {
		this.checkName = checkName;
	}

	public String getCheckPhone() {
		return checkPhone;
	}

	public void setCheckPhone(String checkPhone) {
		this.checkPhone = checkPhone;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getStartAccountType() {
		return startAccountType;
	}

	public void setStartAccountType(Integer startAccountType) {
		this.startAccountType = startAccountType;
	}

	public String getCompleteDate() {
		return completeDate;
	}

	public void setCompleteDate(String completeDate) {
		this.completeDate = completeDate;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Integer getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Long getPaymentStarter() {
		return paymentStarter;
	}

	public void setPaymentStarter(Long paymentStarter) {
		this.paymentStarter = paymentStarter;
	}


	public String getDisputeDate() {
		return disputeDate;
	}

	public void setDisputeDate(String disputeDate) {
		this.disputeDate = disputeDate;
	}

	public Long getDisputePersonId() {
		return disputePersonId;
	}

	public void setDisputePersonId(Long disputePersonId) {
		this.disputePersonId = disputePersonId;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getOrderPayNum() {
		return orderPayNum;
	}

	public void setOrderPayNum(String orderPayNum) {
		this.orderPayNum = orderPayNum;
	}

	public Integer getContractLogisticsOrderType() {
		return contractLogisticsOrderType;
	}

	public void setContractLogisticsOrderType(Integer contractLogisticsOrderType) {
		this.contractLogisticsOrderType = contractLogisticsOrderType;
	}

	public String getSelfLogisticDeliveryDate() {
		return selfLogisticDeliveryDate;
	}

	public void setSelfLogisticDeliveryDate(String selfLogisticDeliveryDate) {
		this.selfLogisticDeliveryDate = selfLogisticDeliveryDate;
	}

	public String getSelfLogisticCarrier() {
		return selfLogisticCarrier;
	}

	public void setSelfLogisticCarrier(String selfLogisticCarrier) {
		this.selfLogisticCarrier = selfLogisticCarrier;
	}

	public String getSelfLogisticCarrierPhone() {
		return selfLogisticCarrierPhone;
	}

	public void setSelfLogisticCarrierPhone(String selfLogisticCarrierPhone) {
		this.selfLogisticCarrierPhone = selfLogisticCarrierPhone;
	}
    

	
}
