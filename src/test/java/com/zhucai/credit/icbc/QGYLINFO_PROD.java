package com.zhucai.credit.icbc;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.jsoup.nodes.Document;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.zhucai.credit.utils.StrongJsoup;

@RunWith(SpringRunner.class)
@SpringBootTest
public class QGYLINFO_PROD {
	
	@Autowired
	private TemplateEngine templateEngine;
	 

	
	/**
	 * 合同签订接口
	 * @version 2018年8月25日
	 * @throws Exception
	 */
	@Test
    public void build() throws Exception{
		Date d = new Date();
		SimpleDateFormat fmtTranDate = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat fmtTranTime = new SimpleDateFormat("HHmmssSSS");
		
  
		String cis = "111190001616306";
        String bankCode = "102";
        String id = "JSZCW01.y.1111";
        long fSeqno = d.getTime();
        String CODE = "QGYLINFO";
        String tranDate = fmtTranDate.format(d);
        String tranTime = fmtTranTime.format(d);
        
        
        Context context = new Context();
		context.setVariable("CIS",  cis);
		context.setVariable("BankCode",  bankCode);
		context.setVariable("ID",  id);
        
		context.setVariable("TranDate",  tranDate);
		context.setVariable("TranTime",  tranTime);
		context.setVariable("fSeqno",  fSeqno);
        context.setVariable("AccNo",  "1111425709000063395");
        //context.setVariable("MemberAccNo",  "");
        context.setVariable("GYLCode",  "GY-2013000003219");
        //context.setVariable("MemberType",  "");
        //context.setVariable("MemberCode",  "");
        context.setVariable("Status",  "1");
        context.setVariable("BeginDate",  "20180601");
        context.setVariable("EndDate",  "20181214");
        //context.setVariable("NextTag",  "");
        
        String x = "<?xml version=\"1.0\" encoding=\"GBK\"?>"+templateEngine.process("icbc/"+CODE, context);
        System.out.println(x);
        
        
		String url = "http://58.247.178.190:448/servlet/ICBCCMPAPIReqServlet?userID="+id+"&PackageID="+fSeqno+"&SendTime="+tranDate+tranTime;
		StrongJsoup connect = StrongJsoup.connect(url);
		connect.headers(ImmutableMap.of("Content-Type", "application/x-www-form-urlencoded"));
		
		Map<String,String> params = Maps.newHashMap();
		params.put("Version", "0.0.0.1");
		params.put("TransCode",CODE);
		params.put("BankCode",bankCode);
		params.put("GroupCIS",cis);
		params.put("ID",id);
		params.put("Cert","");
		params.put("reqData",x);
		System.out.println(url);
		
		System.out.println(params);
		connect.getJsoupConn().data(params);
		
		Document post = connect.post();
		String text = post.text();
		System.out.println(text);
		
		Base64 base64 = new Base64();
		String substring = text.substring(text.indexOf("=")+1);
		System.out.println(substring);
		byte[] decode = base64.decode(substring);
		System.out.println(new String(decode,"GBK"));
        
        
        
    }


}
