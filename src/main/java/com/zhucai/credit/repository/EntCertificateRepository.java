package com.zhucai.credit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zhucai.credit.model.EntCertificateEntity;

/**
 * 金融账号Repository
 */
@Repository
public interface EntCertificateRepository extends JpaRepository<EntCertificateEntity,Long> {


}
