package com.zhucai.credit.test;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.common.collect.Lists;
import com.zhucai.credit.bank.ccbe.bean.CreditRecordFeedback;
import com.zhucai.credit.bank.ccbe.bean.CreditRecordPayment;
import com.zhucai.credit.bank.ccbe.bean.PayApplicationReceiveInfo;
import com.zhucai.credit.bank.ccbe.bean.PlatformReceive;
import com.zhucai.credit.bank.ccbe.bean.PurApproveInfo;
import com.zhucai.credit.bank.ccbe.bean.SignUpFeedback;
import com.zhucai.credit.bank.ccbe.bean.SignUpFeedbackTwo;
import com.zhucai.credit.bank.ccbe.bean.SignUpInfo;
import com.zhucai.credit.bank.ccbe.service.CcbeService;
import com.zhucai.credit.bank.ccbe.service.MockCcbeService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CcbeServiceTest {

	@Autowired
	private CcbeService ccbeService;
	@Autowired
	private MockCcbeService mootCcbeService;
	
	@Test
	public void test() throws Exception {
		ccbeService.signUpInfo();
	}
	
	@Test
	public void testCreditRecordPayment() throws Exception {
		List<CreditRecordPayment> creditRecordPayment = ccbeService.creditRecordPayment();
		for (CreditRecordPayment creditRecordPayment2 : creditRecordPayment) {
			System.out.println(creditRecordPayment2);
		}
	}
	@Test
	public void testSignUpInfo() throws Exception {
		List<SignUpInfo> list = ccbeService.signUpInfo();
		for (SignUpInfo signUpInfo : list) {
			System.out.println(signUpInfo);
		}
	}
	
	@Test
	public void testPurApproveInfo() throws Exception {
		PurApproveInfo pur = new PurApproveInfo();
		pur.setApp_num("CK17C00003077");
		pur.setRequest_orgn_num("34616261-3");
		pur.setOrgn_num("66077349-8");
		pur.setFeedback_status_cd("0001");
		pur.setTrade_years(2.0);
		pur.setTrade_activity("A");
		pur.setTrade_ranking(0.7);
		pur.setDispute_times(0.01);
		pur.setMultiple_evaluate("A");
		pur.setLimit_proposal(30000000);
		ccbeService.purApproveInfo(pur);
		
	}
	

	@Test
	public void testCreditRecordFeedback() throws Exception {
		List<CreditRecordFeedback> recordFeedback = ccbeService.creditRecordFeedback();
		for (CreditRecordFeedback creditRecordFeedback : recordFeedback) {
			System.out.println(creditRecordFeedback);
		}
	}
	@Test
	public void testSignUpFeedback() throws Exception {
		List<SignUpFeedback> upFeedback = ccbeService.signUpFeedback();
		for (SignUpFeedback signUpFeedback : upFeedback) {
			System.out.println(signUpFeedback);
		}
	}
	@Test
	public void testSignUpFeedbackTwo() throws Exception {
		List<SignUpFeedbackTwo> upFeedbackTwo = ccbeService.signUpFeedbackTwo();
		for (SignUpFeedbackTwo signUpFeedbackTwo : upFeedbackTwo) {
			System.out.println(signUpFeedbackTwo);
		}
	}
	
	
	@Test
	public void testSingUpInfo() throws Exception {
		List<SignUpInfo> upInfo = mootCcbeService.signUpInfo();
		for (SignUpInfo signUpInfo : upInfo) {
			System.out.println(signUpInfo);
		}
	}
	
	@Test
	public void testPlatformReceiveList() throws Exception {
		PlatformReceive p = new PlatformReceive();
		p.setBill_balance_amt("1000000");
//		p.setBill_date(new Date());
		p.setBill_num("HZ201700000011");
		p.setContract_num("RZBH320647400201700005");
		p.setCredit_period(180);
		p.setCust_orgn_code("79455400-4");
		p.setRelate_cust_name("公司六五");
		p.setRequest_orgn_num("34616261-3");
		p.setR_cust_orgn_code("13872357-8");
		Map<String, String> receive = ccbeService.platformReceive(Lists.newArrayList(p));
		Set<String> keySet = receive.keySet();
		for (String string : keySet) {
			String key = string;
			String value = receive.get(string);
			System.out.println(key+"==="+value);
		}
	}
	
	
	@Test
	public void testPlatformReceive() throws Exception {
		PlatformReceive p = new PlatformReceive();
		p.setBill_balance_amt("1000000");
//		p.setBill_date(new Date());
		p.setBill_num("HZ201700000011");
		p.setContract_num("RZBH320647400201700005");
		p.setCredit_period(180);
		p.setCust_orgn_code("79455400-4");
		p.setRelate_cust_name("公司六五");
		p.setRequest_orgn_num("34616261-3");
		p.setR_cust_orgn_code("13872357-8");
		Map<String, String> receive = ccbeService.platformReceive(Lists.newArrayList(p));
		Set<String> keySet = receive.keySet();
		for (String string : keySet) {
			String key = string;
			String value = receive.get(string);
			System.out.println(key+"==="+value);
		}
	}
	
	@Test
	public void testPayApplicationReceiveInfo() throws Exception {
		PayApplicationReceiveInfo pai = new PayApplicationReceiveInfo();
		pai.setAccount_type("1");
		pai.setBill_num("HZ201700000011");
		pai.setContract_num("RZBH320647400201700005");
		pai.setCurrency_cd("156");
		pai.setCust_orgn_code("79455400-4");
		pai.setPayment_amt("1000000");
		pai.setPayment_date(new Date());
		pai.setRequest_orgn_num("34616261-3");
		Map<String,String> receive = ccbeService.payApplicationReceiveInfo(Lists.newArrayList(pai));
		System.out.println(111111);
	}
	
	
	
	
}
