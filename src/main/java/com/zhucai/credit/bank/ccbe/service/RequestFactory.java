package com.zhucai.credit.bank.ccbe.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.zhucai.credit.bank.ccbe.request.RequestBean;
import com.zhucai.credit.bank.ccbe.request.RequestBeans;

@Component
public class RequestFactory {
	

	@Value("${ccbe.cust-id}")
	private String custId;
	
	@Value("${ccbe.user-id}")
	private String userId;
	
	@Value("${ccbe.user-psw}")
	private String userPsw;
	
	@Value("${ccbe.org-num}")
	private String orgNum;

	public RequestBean getRequestBean(String txCode) {
		RequestBean req = new RequestBean();
		req.setCUST_ID(custId);
		req.setUSER_ID(userId);
		req.setPASSWORD(userPsw);
		req.setLANGUAGE("CN");
		req.setTX_CODE(txCode);
		req.addTxInfo("request_orgn_num", orgNum);

		return req;
	}
	
	public RequestBeans getRequestBeans(String txCode) {
		RequestBeans reqs = new RequestBeans();
		reqs.setCUST_ID(custId);
		reqs.setUSER_ID(userId);
		reqs.setPASSWORD(userPsw);
		reqs.setLANGUAGE("CN");
		reqs.setTX_CODE(txCode);
		//reqs.addTxInfo("request_orgn_num", orgNum);

		return reqs;
	}
}
