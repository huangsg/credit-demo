package com.zhucai.credit.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.wordnik.swagger.annotations.ApiModelProperty;
import com.zhucai.credit.common.CommonEnums.CertiStatus;
import com.zhucai.credit.common.EnumHelper;
import com.zhucai.credit.utils.TimeUtils;

/**
 * 授权委托书
 * 2017年9月19日
 * @author hsg
 */
@Entity
@Table(name = "finance_ent_certi_ficate")
public class EntCertificateEntity extends BaseEntity{

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "金融账号id,")
	private long accountId;
	@ApiModelProperty(value = "附件表关联id,")
	private long certiImgId;
	@ApiModelProperty(value = "最新操作人id,")
	private String userId;
	@ApiModelProperty(value = "最新操作人账号,")
	private String userName;
	@ApiModelProperty(value = "最新操作联系人,")
	private String linkeName;
	@ApiModelProperty(value = "授权委托书变更状态,")
	private String certiStatus;
	@ApiModelProperty(value = "申请时间,")
	private Date applyTime;
	@ApiModelProperty(value = "生效日期,")
	private Date takeEffectDate;
	@ApiModelProperty(value = "失效日期,")
	private Date expirationDate;
	@Column(nullable = false, length = 20)
	public String getCertiStatus() {
		return certiStatus;
	}
	public void setCertiStatus(String certiStatus) {
		this.certiStatus = certiStatus;
	}
	@Column(nullable = false, length = 20)
	public long getAccountId() {
		return accountId;
	}
	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}
	@Column(nullable = true, length = 20)
	public long getCertiImgId() {
		return certiImgId;
	}
	public void setCertiImgId(long certiImgId) {
		this.certiImgId = certiImgId;
	}
	@Column(nullable = true)
	public Date getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	@Column(nullable = true)
	public Date getTakeEffectDate() {
		return takeEffectDate;
	}
	public void setTakeEffectDate(Date takeEffectDate) {
		this.takeEffectDate = takeEffectDate;
	}
	@Column(nullable = true, length = 20)
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	@Column(nullable = true, length = 20)
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Column(nullable = true, length = 50)
	public String getLinkeName() {
		return linkeName;
	}
	public void setLinkeName(String linkeName) {
		this.linkeName = linkeName;
	}
	@Column(nullable = true)
	public Date getApplyTime() {
		return applyTime;
	}
	public void setApplyTime(Date applyTime) {
		this.applyTime = applyTime;
	}
	@Transient
	public String getApplyTimeStr() {
		return TimeUtils.getDateToString(applyTime);
	}
	@Transient
	public String getTakeEffectDateStr() {
		return TimeUtils.getDateToString(takeEffectDate);
	}
	@Transient
	public String getExpirationDateStr() {
		return TimeUtils.getDateToString(expirationDate);
	}
	@Transient
	public String getCertStatusDesc() {
		return EnumHelper.getDescribe(CertiStatus.valueOf(certiStatus));
	}
	
	
}
