package com.zhucai.credit.bank.icbc.bean;

import java.io.Serializable;
/**
 * 付款查询请求实体
 * @author xjz
 *
 */
public class QgylinstrSend implements Serializable{

	private static final long serialVersionUID = 1L;
	//待查指令包序列号(待查指令包序列号和待查平台交易序列号至少有一项有值，都有值时以平台交易序列号为准)
	private String qryfSeqno;
	//待查平台交易序列号(待查指令包序列号和待查平台交易序列号至少有一项有值，都有值时以平台交易序列号为准)
	private String qrySerialNo;
	//请求备用字段1
	private String reqReserved1;
	//请求备用字段2
	private String reqReserved2;
	//指令顺序号 * (每个包内的“指令顺序号”不能为空，并且不能重复)
	private String iSeqno;
	//待查指令包顺序号(待查提交指令包的iSeqno)
	private String qryiSeqno;
	//待查平台交易顺序号 (待查提交指令包中指令顺序号，如1,2,3等自然数)
	private String qryOrderNo;
	//请求备用字段3
	private String reqReserved3;
	//请求备用字段4
	private String reqReserved4;
	public String getQryfSeqno() {
		return qryfSeqno;
	}
	public void setQryfSeqno(String qryfSeqno) {
		this.qryfSeqno = qryfSeqno;
	}
	public String getQrySerialNo() {
		return qrySerialNo;
	}
	public void setQrySerialNo(String qrySerialNo) {
		this.qrySerialNo = qrySerialNo;
	}
	public String getReqReserved1() {
		return reqReserved1;
	}
	public void setReqReserved1(String reqReserved1) {
		this.reqReserved1 = reqReserved1;
	}
	public String getReqReserved2() {
		return reqReserved2;
	}
	public void setReqReserved2(String reqReserved2) {
		this.reqReserved2 = reqReserved2;
	}
	public String getiSeqno() {
		return iSeqno;
	}
	public void setiSeqno(String iSeqno) {
		this.iSeqno = iSeqno;
	}
	public String getQryiSeqno() {
		return qryiSeqno;
	}
	public void setQryiSeqno(String qryiSeqno) {
		this.qryiSeqno = qryiSeqno;
	}
	public String getQryOrderNo() {
		return qryOrderNo;
	}
	public void setQryOrderNo(String qryOrderNo) {
		this.qryOrderNo = qryOrderNo;
	}
	public String getReqReserved3() {
		return reqReserved3;
	}
	public void setReqReserved3(String reqReserved3) {
		this.reqReserved3 = reqReserved3;
	}
	public String getReqReserved4() {
		return reqReserved4;
	}
	public void setReqReserved4(String reqReserved4) {
		this.reqReserved4 = reqReserved4;
	}
	
	

}
