package com.zhucai.credit.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.zhucai.credit.model.FinanceDrawEntity;

/**
 * 
 * @author hsg
 *（支用实例）Repository
 */
@Repository
public interface FinanceDrawRepository extends
		PagingAndSortingRepository<FinanceDrawEntity, Long>,
		JpaSpecificationExecutor<FinanceDrawEntity> {

	List<FinanceDrawEntity> findByStatusNotAndPayoutNumAndEntType(int delete, String bill_num, String string);

	/**
	 * 根据outId查询支取记录
	 * @param delete
	 * @param tid
	 * @return
	 */
	@Query(value="select fdr.id,fea.enterprise_name,fdr.draw_time,fdr.draw_amount,fea.id accId from finance_draw_record fdr "
			+ " left join finance_ent_account fea on fea.org_num = fdr.org_num and fea.ent_type = fdr.ent_type"
			+ " where fdr.status <> ?1 and fdr.out_id = ?2",nativeQuery=true)
	List<Object[]> findByStatusNotAndOutId(int delete, Long tid);

	/**
	 * 根
	 * @param delete
	 * @param payout_num
	 * @return
	 */
	List<FinanceDrawEntity> findByStatusNotAndPayoutNum(int delete, String payout_num);

	/**
	 * 根据支取号和发票号查询支取信息
	 * @param delete
	 * @param payout_num
	 * @param invoiceNoCode
	 * @return
	 */
	FinanceDrawEntity findByStatusNotAndPayoutNumAndInvoiceNoCode(int delete,
			String payout_num, String invoiceNoCode);




}
