package com.zhucai.credit.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * 筑材网金融产品（筑保通信息表）
 * 2017年9月19日
 * @author hsg
 */
@Entity
@Table(name = "finance_ent_cr_prod")
public class EntCrProdEntity extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "组织机构代码,")
	private String orgnNum;
	@ApiModelProperty(value = "核心企业特权开通填的联系人,")
	private String linkPerson;
	@ApiModelProperty(value = "核心企业特权开通填的联系方式,")
	private String linkTel;
	@ApiModelProperty(value = "商家类型,Enum(EntType)")
	private String entType;
	@ApiModelProperty(value = "筑材网金融产品类型,Enum(ZhuCaiProdType)")
	private String zhucaiProdType;
	@ApiModelProperty(value = "金融账号Id,")
	private Long enAccId;
	@ApiModelProperty(value = "贷款服务机构关联id,")
	private Long crPrTypeId;
	@ApiModelProperty(value = "筑保通状态,Enum(EnCrStatus)")
	private String enCrStatus;
	@ApiModelProperty(value = "筑保通申请时间,（）")
	private Date applyTime;
	@ApiModelProperty(value = "状态(黑名单)默认0不是，1是")
	private Integer ecpStatus;
	@ApiModelProperty(value = "最近黑名单操作记录id")
	private Long operRecordId;
	@ApiModelProperty(value = "企业id,")
	private Long enterpriseId;
	@ApiModelProperty(value = "融资利率,")
	private BigDecimal crProdRate;
	@ApiModelProperty(value = "融资利率描述,")
	private String crProdRateStr;
	@ApiModelProperty(value = "核心企业和银行的对应授信额度,")
	private BigDecimal purCreditLine;
	
	@Column(nullable = false)
	public Long getCrPrTypeId() {
		return crPrTypeId;
	}
	public void setCrPrTypeId(Long crPrTypeId) {
		this.crPrTypeId = crPrTypeId;
	}
	@Column(nullable = true)
	public BigDecimal getCrProdRate() {
		return crProdRate;
	}
	public void setCrProdRate(BigDecimal crProdRate) {
		this.crProdRate = crProdRate;
	}
	@Column(nullable = true,length = 40)
	public String getCrProdRateStr() {
		return crProdRateStr;
	}
	public void setCrProdRateStr(String crProdRateStr) {
		this.crProdRateStr = crProdRateStr;
	}
	@Column(nullable = true)
	public BigDecimal getPurCreditLine() {
		return purCreditLine;
	}
	public void setPurCreditLine(BigDecimal purCreditLine) {
		this.purCreditLine = purCreditLine;
	}
	@Column(nullable = true, length = 40)
	public String getZhucaiProdType() {
		return zhucaiProdType;
	}
	public void setZhucaiProdType(String zhucaiProdType) {
		this.zhucaiProdType = zhucaiProdType;
	}
	@Column(nullable = false, length = 2,columnDefinition = "INT default 0")
	public Integer getEcpStatus() {
		return ecpStatus;
	}
	public void setEcpStatus(Integer ecpStatus) {
		this.ecpStatus = ecpStatus;
	}
	@Column(nullable = false, length = 20)
	public Long getEnAccId() {
		return enAccId;
	}
	public void setEnAccId(Long enAccId) {
		this.enAccId = enAccId;
	}
	@Column(nullable = true, length = 50)
	public String getLinkPerson() {
		return linkPerson;
	}
	public void setLinkPerson(String linkPerson) {
		this.linkPerson = linkPerson;
	}
	@Column(nullable = true, length = 20)
	public String getLinkTel() {
		return linkTel;
	}
	public void setLinkTel(String linkTel) {
		this.linkTel = linkTel;
	}
	@Column(nullable = false, length = 20)
	public String getEnCrStatus() {
		return enCrStatus;
	}
	public void setEnCrStatus(String enCrStatus) {
		this.enCrStatus = enCrStatus;
	}
	@Column(nullable=true,length=20)
	public Long getOperRecordId() {
		return operRecordId;
	}
	public void setOperRecordId(Long operRecordId) {
		this.operRecordId = operRecordId;
	}
	@Column(nullable=false,length=20)
	public String getEntType() {
		return entType;
	}
	public void setEntType(String entType) {
		this.entType = entType;
	}
	@Column(nullable=false,length=20)
	public Date getApplyTime() {
		return applyTime;
	}
	public void setApplyTime(Date applyTime) {
		this.applyTime = applyTime;
	}
	@Column(nullable=false,length=20)
	public String getOrgnNum() {
		return orgnNum;
	}
	public void setOrgnNum(String orgnNum) {
		this.orgnNum = orgnNum;
	}
	
	@Column(nullable=true,length=50)
	public Long getEnterpriseId() {
		return enterpriseId;
	}
	public void setEnterpriseId(Long enterpriseId) {
		this.enterpriseId = enterpriseId;
	}
	
}
