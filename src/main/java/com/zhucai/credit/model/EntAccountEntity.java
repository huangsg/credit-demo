package com.zhucai.credit.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * 驻材金融信息表
 * 2017年9月19日
 * @author hsg
 */
@Entity
@Table(name = "finance_ent_account")
public class EntAccountEntity extends BaseEntity{
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "驻材金融类型（采购商，供应商）,")
	private String entType;
	@ApiModelProperty(value = "企业id,")
	private Long enterpriseId;
	@ApiModelProperty(value = "企业名称,")
	private String enterpriseName;
	@ApiModelProperty(value = "审核状态,Enum(CheckStatus)")
	private String checkStatus;
	@ApiModelProperty(value = "申请时间,")
	private Date applyTime;
	@ApiModelProperty(value = "审核通过时间,")
	private Date approveTime;
	@ApiModelProperty(value = "审核通过人,")
	private Long approvePerson;
	@ApiModelProperty(value = "停用时间,")
	private Date terminateTime;
	@ApiModelProperty(value = "停用人,")
	private Long terminatePerson;
	@ApiModelProperty(value = "组织机构代码,")
	private String orgNum;
	@ApiModelProperty(value = "统一社会信用代码,")
	private String unityOrgNum;
	@ApiModelProperty(value = "是否阅读金融服务协议,0:否,1:是")
	private Integer readStatus;
	@ApiModelProperty(value = "自动驳回,0:否,1:是")
	private Integer autoRejectStatus;
	@ApiModelProperty(value = "自动驳回结束时间")
	private Date autoRejectTime;
	@ApiModelProperty(value = "最近审核操作记录id")
	private Long operId;
	@ApiModelProperty(value = "正在使用的委托书附件id")
	private Long certiImgId;
	@ApiModelProperty(value = "正在使用的委托书主键id")
	private Long certiId;
	@ApiModelProperty(value = "变更审核中的附件id")
	private Long certiChangeImgId;
	@ApiModelProperty(value = "变更中的委托书主键id")
	private Long certiChangeId;
	
	
	
	@Column(nullable = true, length = 50)
	public String getUnityOrgNum() {
		return unityOrgNum;
	}

	public void setUnityOrgNum(String unityOrgNum) {
		this.unityOrgNum = unityOrgNum;
	}
	@Column(nullable = true, length = 100)
	public String getEnterpriseName() {
		return enterpriseName;
	}

	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}


	@Column(nullable = false, length = 20)
	public Long getEnterpriseId() {
		return enterpriseId;
	}

	public void setEnterpriseId(Long enterpriseId) {
		this.enterpriseId = enterpriseId;
	}

	@Column(nullable = true, length = 20)
	public String getOrgNum() {
		return orgNum;
	}

	public void setOrgNum(String orgNum) {
		this.orgNum = orgNum;
	}

	@Column(nullable = true, length = 2,columnDefinition = "INT default 0")
	public Integer getAutoRejectStatus() {
		return autoRejectStatus;
	}

	public void setAutoRejectStatus(Integer autoRejectStatus) {
		this.autoRejectStatus = autoRejectStatus;
	}
	@Column(nullable = true, length = 20)
	public Date getAutoRejectTime() {
		return autoRejectTime;
	}

	public void setAutoRejectTime(Date autoRejectTime) {
		this.autoRejectTime = autoRejectTime;
	}

	@Column(nullable = true, length = 2,columnDefinition = "INT default 0")
	public Integer getReadStatus() {
		return readStatus;
	}

	public void setReadStatus(Integer readStatus) {
		this.readStatus = readStatus;
	}

	@Column(nullable = true, length = 20)
	public Long getOperId() {
		return operId;
	}

	public void setOperId(Long operId) {
		this.operId = operId;
	}

	@Column(nullable = false, length = 20)
	public String getEntType() {
		return entType;
	}

	public void setEntType(String entType) {
		this.entType = entType;
	}

	@Column(nullable = false, length = 20)
	public String getCheckStatus() {
		return checkStatus;
	}

	public void setCheckStatus(String checkStatus) {
		this.checkStatus = checkStatus;
	}
	@Column(nullable = false)
	public Date getApplyTime() {
		return applyTime;
	}

	public void setApplyTime(Date applyTime) {
		this.applyTime = applyTime;
	}
	@Column(nullable = true)
	public Date getApproveTime() {
		return approveTime;
	}

	public void setApproveTime(Date approveTime) {
		this.approveTime = approveTime;
	}
	@Column(nullable = true)
	public Date getTerminateTime() {
		return terminateTime;
	}

	public void setTerminateTime(Date terminateTime) {
		this.terminateTime = terminateTime;
	}
	@Column(nullable = true,length=20)
	public Long getApprovePerson() {
		return approvePerson;
	}

	public void setApprovePerson(Long approvePerson) {
		this.approvePerson = approvePerson;
	}
	@Column(nullable = true,length=20)
	public Long getTerminatePerson() {
		return terminatePerson;
	}

	public void setTerminatePerson(Long terminatePerson) {
		this.terminatePerson = terminatePerson;
	}

	@Column(nullable = true,length=20)
	public Long getCertiImgId() {
		return certiImgId;
	}

	public void setCertiImgId(Long certiImgId) {
		this.certiImgId = certiImgId;
	}
	@Column(nullable = true,length=20)
	public Long getCertiId() {
		return certiId;
	}

	public void setCertiId(Long certiId) {
		this.certiId = certiId;
	}
	@Column(nullable = true,length=20)
	public Long getCertiChangeImgId() {
		return certiChangeImgId;
	}

	public void setCertiChangeImgId(Long certiChangeImgId) {
		this.certiChangeImgId = certiChangeImgId;
	}
	@Column(nullable = true,length=20)
	public Long getCertiChangeId() {
		return certiChangeId;
	}

	public void setCertiChangeId(Long certiChangeId) {
		this.certiChangeId = certiChangeId;
	}


}
