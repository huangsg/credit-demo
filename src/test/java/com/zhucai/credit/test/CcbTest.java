package com.zhucai.credit.test;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.List;

import com.google.common.base.Charsets;
import com.google.common.io.Files;

public class CcbTest {
	public static void main(String[] args) throws Exception{
		Socket socket = new Socket("127.0.0.1", 12345); 
 
        OutputStreamWriter streamWriter = new OutputStreamWriter(socket.getOutputStream(), "GB18030");  
        BufferedWriter bufferedWriter = new BufferedWriter(streamWriter);  
        List<String> readLines = Files.readLines(new File("request/6W5501.xml"), Charsets.UTF_8);
        for(String l : readLines) {
        	bufferedWriter.write(l);
        }
        
        bufferedWriter.flush();  
          
        BufferedInputStream streamReader = new BufferedInputStream(socket.getInputStream());  
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(streamReader, "GB18030"));  
        String line = null;  
        while((line = bufferedReader.readLine())!= null)  
        {  
            System.out.println(line);  
        }  
        bufferedReader.close();  
        bufferedWriter.close();  
        socket.close();
		
/*		System.setProperty("javax.net.ssl.trustStore", "cbctest.jks");
		System.setProperty("javax.net.ssl.trustStorePassword", "123456"); 
		Connection connect = Jsoup.connect("https://124.127.94.45:9443/NCCB/NECV5B2BMainPlatWLPT");
		List<String> readLines = Files.readLines(new File("xxxx.xml"), Charsets.UTF_8);
		String join = Joiner.on("\r\n").join(readLines);
		System.out.println(join);
		Document post = connect.requestBody(join).post();
		System.out.println(post);*/
	}
}
