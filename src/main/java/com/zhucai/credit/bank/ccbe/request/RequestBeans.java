package com.zhucai.credit.bank.ccbe.request;

import java.util.List;
import java.util.Map;

public class RequestBeans {
	

	
	private Long REQUEST_SN;
	
	private String CUST_ID;
	private String USER_ID;
	private String PASSWORD;
	private String TX_CODE;
	private String LANGUAGE;

	//private Map<String,String> TX_INFO = Maps.newHashMap();
	private List<Map<String,String>> TX_INFO;
	



	public List<Map<String, String>> getTX_INFO() {
		return TX_INFO;
	}

	public void setTX_INFO(List<Map<String, String>> tX_INFO) {
		TX_INFO = tX_INFO;
	}

	public Long getREQUEST_SN() {
		return REQUEST_SN;
	}

	public void setREQUEST_SN(Long rEQUEST_SN) {
		REQUEST_SN = rEQUEST_SN;
	}

	public String getCUST_ID() {
		return CUST_ID;
	}

	public void setCUST_ID(String cUST_ID) {
		CUST_ID = cUST_ID;
	}

	public String getUSER_ID() {
		return USER_ID;
	}

	public void setUSER_ID(String uSER_ID) {
		USER_ID = uSER_ID;
	}

	public String getPASSWORD() {
		return PASSWORD;
	}

	public void setPASSWORD(String pASSWORD) {
		PASSWORD = pASSWORD;
	}

	public String getTX_CODE() {
		return TX_CODE;
	}

	public void setTX_CODE(String tX_CODE) {
		TX_CODE = tX_CODE;
	}

	public String getLANGUAGE() {
		return LANGUAGE;
	}

	public void setLANGUAGE(String lANGUAGE) {
		LANGUAGE = lANGUAGE;
	}

/*

	public void addTxInfo(String tag, String value) {
		this.TX_INFO.put(tag, value);
	}*/
	
}
