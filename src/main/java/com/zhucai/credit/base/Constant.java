package com.zhucai.credit.base;

import java.io.Serializable;

public class Constant implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/** 
	 * @Desc: 常用状态类型
	 * @Auth: hsg
	 * @Date: 2017-9-28 下午5:05:12
	 */  
	public static class Status implements Serializable {
		
		private static final long serialVersionUID = 1L;
		
        /**
         * @Fields delete : 删除
         */
        public static final int delete = 3;
        
        /**
         * @Fields invalid : 停用/无效状态
         */
        public static final int invalid = 0;
        /**
         * @Fields valid : 有效/正常状态
         */
        public static final int valid = 1;
    }
	
	/**
	 * @Desc: 文件后缀类型
	 * @Auth: PMQ
	 * @Date: 2017年9月14日 下午3:50:08
	 */
	public static class FileSuffix implements Serializable {
		
		private static final long serialVersionUID = 1L;
		
		public static final String POINT_XML = ".xml";
		
	}
	
	/**
	 * @Desc: 模板类别
	 * @Auth: PMQ
	 * @Date: 2017年9月20日 上午10:54:08
	 */
	public static class ProcessTemplateCategory {
		/**
		 * 标准模板
		 */
		public static final int STANDARD_VERSION = 1;
		/**
		 * 精简模板
		 */
		public static final int SIMPLIFY_VERSION = 2;
		/**
		 * 自定义模板
		 */
		public static final int SELF_DEFINE_VERSION = 3;
	
	}
	
}
