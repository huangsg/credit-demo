package com.zhucai.credit.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.wordnik.swagger.annotations.ApiModel;


/**
 * @author hsg
 */
@Entity
@Table(name = "finance_refund_order")
@ApiModel(value = "收银台退款表")
public class RefundOrderEntity extends BaseEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//业务订单id
	@Column(nullable = false)
	private Long orderId;
	//业务订单No
	@Column(nullable = false,length = 20)
	private String orderNo;
	//订单金额
	@Column(nullable = false,columnDefinition = "decimal default 0")
	private BigDecimal orderAmt;
	//付款状态
	@Column(nullable = false,length = 2)
	private String payStatus;
	//流水号
	@Column(nullable = false,length = 20)
	private String flowNumber;
	//退款商家
	@Column(nullable = false,length = 50)
	private String refundEnterpriseName;
	//退款申请金额
	@Column(nullable = false,columnDefinition = "decimal default 0")
	private BigDecimal refundApplyAmt;
	//退款金额
	@Column(nullable = true,columnDefinition = "decimal default 0")
	private BigDecimal refundAmt;
	//退款状态
	@Column(nullable = false,length = 50)
	private String refundStatus;
	//退款申请时间
	@Column(nullable = false)
	private Date refundApplyTime;
	//退款时间
	@Column(nullable = true)
	private Date refundTime;
	//退款申请原因
	@Column(nullable = true,length = 200)
	private String refundApplyReason;
	//退款备注
	@Column(nullable = true,length = 200)
	private String refundRemark;
	//创建人
	@Column(nullable = false)
	private Long createRefundUser;
	//支付方式
	@Column(nullable = false)
	private String payType;
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public String getRefundEnterpriseName() {
		return refundEnterpriseName;
	}
	public void setRefundEnterpriseName(String refundEnterpriseName) {
		this.refundEnterpriseName = refundEnterpriseName;
	}
	public BigDecimal getRefundApplyAmt() {
		return refundApplyAmt;
	}
	public void setRefundApplyAmt(BigDecimal refundApplyAmt) {
		this.refundApplyAmt = refundApplyAmt;
	}
	public BigDecimal getRefundAmt() {
		return refundAmt;
	}
	public void setRefundAmt(BigDecimal refundAmt) {
		this.refundAmt = refundAmt;
	}
	public String getRefundStatus() {
		return refundStatus;
	}
	public void setRefundStatus(String refundStatus) {
		this.refundStatus = refundStatus;
	}
	public Date getRefundApplyTime() {
		return refundApplyTime;
	}
	public void setRefundApplyTime(Date refundApplyTime) {
		this.refundApplyTime = refundApplyTime;
	}
	public Date getRefundTime() {
		return refundTime;
	}
	public void setRefundTime(Date refundTime) {
		this.refundTime = refundTime;
	}
	public String getRefundApplyReason() {
		return refundApplyReason;
	}
	public void setRefundApplyReason(String refundApplyReason) {
		this.refundApplyReason = refundApplyReason;
	}
	public String getRefundRemark() {
		return refundRemark;
	}
	public void setRefundRemark(String refundRemark) {
		this.refundRemark = refundRemark;
	}
	public String getFlowNumber() {
		return flowNumber;
	}
	public void setFlowNumber(String flowNumber) {
		this.flowNumber = flowNumber;
	}
	public Long getCreateRefundUser() {
		return createRefundUser;
	}
	public void setCreateRefundUser(Long createRefundUser) {
		this.createRefundUser = createRefundUser;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public BigDecimal getOrderAmt() {
		return orderAmt;
	}
	public void setOrderAmt(BigDecimal orderAmt) {
		this.orderAmt = orderAmt;
	}
	public String getPayStatus() {
		return payStatus;
	}
	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	
	
}
