package com.zhucai.credit.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.zhucai.credit.model.MessageUser;

/**
 * 
 * @author hsg
 * MessageUser
 */
@Repository
public interface MessageUserRepository extends
		PagingAndSortingRepository<MessageUser, Long>,
		JpaSpecificationExecutor<MessageUser> {




	





}
