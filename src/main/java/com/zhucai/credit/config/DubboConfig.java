package com.zhucai.credit.config;

import org.springframework.context.annotation.Bean;

import com.alibaba.dubbo.config.ApplicationConfig;
import com.alibaba.dubbo.config.MonitorConfig;
import com.alibaba.dubbo.config.ProtocolConfig;
import com.alibaba.dubbo.config.ProviderConfig;
import com.alibaba.dubbo.config.spring.AnnotationBean;

//@Configuration
public class DubboConfig {
	
    
    @Bean
    public ApplicationConfig application() {
        ApplicationConfig applicationConfig = new ApplicationConfig();
        applicationConfig.setName("creditService");
        return applicationConfig;
    }
    
    @Bean
    public MonitorConfig monitorConfig() {
        MonitorConfig mc = new MonitorConfig();
        mc.setProtocol("credit");
        return mc;
    }
    

    
    @Bean
    public ProtocolConfig protocol() {
        ProtocolConfig protocolConfig = new ProtocolConfig();
        protocolConfig.setName("rmi");
        return protocolConfig;
    }

    @Bean
    public ProviderConfig provider() {
        ProviderConfig providerConfig = new ProviderConfig();
        providerConfig.setMonitor(monitorConfig());
        providerConfig.setRetries(0);
        providerConfig.setTimeout(30000);
        providerConfig.setThreads(100);
        //providerConfig.setProxy("javassist");
        return providerConfig;
    }
    
    /**
     * 接口的实现++
     * @return
     */
    @Bean
    public AnnotationBean annotation() {
    	AnnotationBean providerConfig = new AnnotationBean();
    	providerConfig.setPackage("com.zhucai.credit.service.impl");
    	//providerConfig.setApplicationContext(applicationContext);
        return providerConfig;
    }


    /**
     * 这是作为消费者 声明需要使用其他的服务
     * @return
     */
  /*  @Bean
    public ReferenceBean<ICompWikiService> compWiki() {
        ReferenceBean<ICompWikiService> ref = new ReferenceBean<>();
        ref.setVersion("1.0.0");
        ref.setInterface(ICompWikiService.class);
        ref.setCheck(false);
        return ref;
    }
    
    @Bean
    public ReferenceBean<IAccountService> account() {
        ReferenceBean<IAccountService> ref = new ReferenceBean<>();
        ref.setVersion("1.0.0");
        ref.setInterface(IAccountService.class);
        ref.setCheck(false);
        return ref;
    }*/
}