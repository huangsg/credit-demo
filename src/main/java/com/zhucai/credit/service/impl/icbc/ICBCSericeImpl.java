package com.zhucai.credit.service.impl.icbc;

import org.springframework.stereotype.Service;

import com.zhucai.credit.service.ICBCSerice;



/**
 * 工行接口实现
 * @author hsg
 * @version 2018年8月8日
 *
 */
@Service("ICBCSericeImpl")
@com.alibaba.dubbo.config.annotation.Service(version="1.0.0")
public class ICBCSericeImpl implements ICBCSerice{
	
}
