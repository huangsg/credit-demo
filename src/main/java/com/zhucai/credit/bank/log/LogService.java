package com.zhucai.credit.bank.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LogService {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private LogRepository logRepository;
	
	//@Transactional(readOnly = true)
	public Long getMaxTpKey(String tpType,String tpCode) {
		return logRepository.getMaxTpKey(tpType, tpCode);
	}
	
	//@Transactional(readOnly = false, rollbackFor = Exception.class)
	public Long getLog(String tpType,String tpCode) {
		try {
			LogEntity log = new LogEntity();
			log.setTpType(tpType);
			log.setTpCode(tpCode);
			return logRepository.save(log).getTid();
		} catch (Exception e) {
			logger.warn("日志记录失败", e);
			return null;
		}
	}
	//@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void requestLog(Long logId, String request) {
		try {
			logRepository.updateRequest(logId, request);
		} catch (Exception e) {
			logger.warn("请求日志记录失败", e);
		}
	}
	//@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void responseLog(Long logId, String response,String tpKey) {
		try {
			logRepository.updateResponse(logId, response,tpKey);
		} catch (Exception e) {
			logger.warn("返回日志记录失败", e);
		}
	}
}
