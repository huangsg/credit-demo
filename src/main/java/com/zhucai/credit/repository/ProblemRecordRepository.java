package com.zhucai.credit.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.zhucai.credit.model.ProblemRecordEntrity;

/**
 * 
 * @author hsg
 * ProblemRecordEntrity
 */
@Repository
public interface ProblemRecordRepository extends
		PagingAndSortingRepository<ProblemRecordEntrity, Long>,
		JpaSpecificationExecutor<ProblemRecordEntrity> {






}
