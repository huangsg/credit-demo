package com.zhucai.credit.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * 发票实体
 * 2017年9月19日
 * @author hsg
 */
@Entity
@Table(name="tb_xa_invoice_info")
public class CreditInvoiceEntity extends BaseEntity {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "发票号,")
	private String invoiceNo;
	@ApiModelProperty(value = "发票代码,")
	private String invoiceCode;
	@ApiModelProperty(value = "发票金额,")
	private BigDecimal invoiceAmount;
	@ApiModelProperty(value = "发票日期")
	private Date invoiceDate;
	@ApiModelProperty(value = "附件id,")
	private Long invoiceFileId;
	@ApiModelProperty(value = "附件名称,")
	private String invoiceFileName;
	@ApiModelProperty(value = "附件url,")
	private String invoiceFileUrl;
	@ApiModelProperty(value = "生成时间,")
	private Date creTime;
	@ApiModelProperty(value = "是否验真通过（0：未验真，1，已验真通过，2：已验真未通过）,")
	private Integer checkTrueStatus;
	@ApiModelProperty(value = "验真返回采购商统一社会代码,")
	private String purUncert;
	@ApiModelProperty(value = "验真返回供应商统一社会代码,")
	private String supUncert;
	@ApiModelProperty(value = "验真返回发票号,")
	private String checkInvoiceNo;
	@ApiModelProperty(value = "验真返回发票代码,")
	private String checkInvoiceCode;
	@ApiModelProperty(value = "验真返回发票金额,")
	private BigDecimal checkInvoiceAmount;
	@ApiModelProperty(value = "验真返回发票日期")
	private Date checkInvoiceDate;
	@ApiModelProperty(value = "发票流（方便验证是否是同一张）,")
	private String invoiceFlow;
	
	@Column(nullable = true,length = 32)
	public String getCheckInvoiceNo() {
		return checkInvoiceNo;
	}
	public void setCheckInvoiceNo(String checkInvoiceNo) {
		this.checkInvoiceNo = checkInvoiceNo;
	}
	@Column(nullable = true,length = 32)
	public String getCheckInvoiceCode() {
		return checkInvoiceCode;
	}
	public void setCheckInvoiceCode(String checkInvoiceCode) {
		this.checkInvoiceCode = checkInvoiceCode;
	}
	@Column(nullable = true)
	public BigDecimal getCheckInvoiceAmount() {
		return checkInvoiceAmount;
	}
	
	public void setCheckInvoiceAmount(BigDecimal checkInvoiceAmount) {
		this.checkInvoiceAmount = checkInvoiceAmount;
	}
	@Column(nullable = true)
	public Date getCheckInvoiceDate() {
		return checkInvoiceDate;
	}
	public void setCheckInvoiceDate(Date checkInvoiceDate) {
		this.checkInvoiceDate = checkInvoiceDate;
	}
	@Column(nullable = true,length = 32)
	public String getInvoiceFlow() {
		return invoiceFlow;
	}
	public void setInvoiceFlow(String invoiceFlow) {
		this.invoiceFlow = invoiceFlow;
	}
	@Column(nullable = true,length = 30)
	public String getPurUncert() {
		return purUncert;
	}
	public void setPurUncert(String purUncert) {
		this.purUncert = purUncert;
	}
	@Column(nullable = true,length = 30)
	public String getSupUncert() {
		return supUncert;
	}
	public void setSupUncert(String supUncert) {
		this.supUncert = supUncert;
	}
	@Column(nullable = true,columnDefinition = "INT default 0")
	public Integer getCheckTrueStatus() {
		return checkTrueStatus;
	}
	public void setCheckTrueStatus(Integer checkTrueStatus) {
		this.checkTrueStatus = checkTrueStatus;
	}
	@Column(nullable = true, length = 50)
	public String getInvoiceFileName() {
		return invoiceFileName;
	}
	public void setInvoiceFileName(String invoiceFileName) {
		this.invoiceFileName = invoiceFileName;
	}
	@Column(nullable = false, length = 12)
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	@Column(nullable = false)
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	@Column(nullable = false, length = 12)
	public Long getInvoiceFileId() {
		return invoiceFileId;
	}
	public void setInvoiceFileId(Long invoiceFileId) {
		this.invoiceFileId = invoiceFileId;
	}
	@Column(nullable=false,columnDefinition = "decimal(20,2)")
	public BigDecimal getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(BigDecimal invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	@Column(nullable = true, length = 15)
	public String getInvoiceCode() {
		return invoiceCode;
	}
	public void setInvoiceCode(String invoiceCode) {
		this.invoiceCode = invoiceCode;
	}
	@Column(nullable = true, length = 50)
	public String getInvoiceFileUrl() {
		return invoiceFileUrl;
	}
	public void setInvoiceFileUrl(String invoiceFileUrl) {
		this.invoiceFileUrl = invoiceFileUrl;
	}
	public Date getCreTime() {
		return creTime;
	}
	public void setCreTime(Date creTime) {
		this.creTime = creTime;
	}
	
}
