package com.zhucai.credit.valid.invoice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Map;

import net.coobird.thumbnailator.Thumbnails;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zhucai.credit.utils.FileUtils;

@Service
public class InvoiceValidServiceImpl implements InvoiceValidService {
	@Autowired
	private LeShuiOcrVatInfo leShuiOcrVatInfo;
	@Override
	public String invoiceSign(String url) {
		File file = getFile(url);
		try {
			String md5Hex = DigestUtils.md5Hex(new FileInputStream(file));
			return md5Hex;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public InviceValidResult valid(String url) {
		File file = getFile(url);
		file = thumb(file);
		
		try {
			Map vatInfoByParam = leShuiOcrVatInfo.getVatInfoByParam(file.getAbsolutePath());
			System.out.println(vatInfoByParam);
			InviceValidResult invoice = new InviceValidResult();
			String invoicefalseCode = (String)vatInfoByParam.get("invoicefalseCode");
			if(invoicefalseCode != null) {
				String resultMsg = (String)vatInfoByParam.get("resultMsg");
				invoice.setResultMsg(resultMsg);
				invoice.setInvoicefalseCode(invoicefalseCode);
			} else {
				String invoiceDataCode = (String)vatInfoByParam.get("invoiceDataCode");
				String invoiceNumber = (String)vatInfoByParam.get("invoiceNumber");
				String totalTaxSum = (String)vatInfoByParam.get("totalTaxSum");
				String billingTime = (String)vatInfoByParam.get("billingTime");
				String taxpayerNumber = (String)vatInfoByParam.get("taxpayerNumber");
				String salesTaxpayerNum = (String)vatInfoByParam.get("salesTaxpayerNum");

				invoice.setInvoiceDataCode(invoiceDataCode);
				invoice.setInvoiceNumber(invoiceNumber);
				invoice.setTotalTaxSum(new BigDecimal(totalTaxSum));
				try {
					invoice.setBillingTime(DateUtils.parseDate(billingTime, "yyyy-MM-dd"));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				invoice.setTaxpayerNumber(taxpayerNumber);
				invoice.setSalesTaxpayerNum(salesTaxpayerNum);
			}
			
			return invoice;
			
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	private File thumb(File file) {
		//大于2.5兆的图片要压缩一下
		if(file.length() > 2600000l) {
			try {
				File thumb = new File(file.getParentFile(),"thumb_"+file.getName());
				Thumbnails.of(file).height(2000).outputQuality(0.9f).toFile(thumb);
				return thumb;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return file;
	}
	
	
	
	private File getFile(String url) {
		String dest = "file/invoice/";
		String name = FilenameUtils.getName(url);
		String filePath = dest+name;
		File file = new File(filePath);
		if(!file.exists()) {
			file = FileUtils.saveFileFromURL(url, filePath);
		}
		return file;
	}

	public static void main(String[] args) {
		InvoiceValidServiceImpl impl = new InvoiceValidServiceImpl();
/*		String url = "https://static.zhucai.com/online/upload/finaceInvoice/19be21df-6a6b-4b1b-a7f9-4ccaaf8a61ca.JPG";
		
		String invoiceSign = impl.invoiceSign(url);
		System.out.println(invoiceSign);*/
		
		String url = "https://static.zhucai.com/online/upload/finaceInvoice/d231e1d5-7eb3-440d-bdce-da5f7b886bbe.jpg";
		impl.valid(url);
	}

}
