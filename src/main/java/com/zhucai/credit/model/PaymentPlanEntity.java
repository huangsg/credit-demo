package com.zhucai.credit.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "tb_xa_payment_plan")
@ApiModel(value="付款计划表定义表")
public class PaymentPlanEntity extends BaseEntity{
	
		@ApiModelProperty(value="合同id ,合同id 数据库")
	private Long contractId;
		@ApiModelProperty(value="任务ID  ,任务ID  数据库生成")
	private Long missionId;
		@ApiModelProperty(value="合同编号 系统生成,合同编号 系统生成")
	private String contractNum;
		@ApiModelProperty(value="任务编号 系统,任务编号 系统")
	private String missionNum;
		@ApiModelProperty(value="项目名称,项目名称")
	private String projectName;
		@ApiModelProperty(value="项目经理,项目经理")
	private String projectManager;
		@ApiModelProperty(value="付款单位,付款单位")
	private String payerName;
		@ApiModelProperty(value="委托收款单位 网站,委托收款单位 网站")
	private String payeeTrustName;
		@ApiModelProperty(value="实际收款单位,实际收款单位")
	private String payeeName;
		@ApiModelProperty(value="创建时间 表头字段,创建时间 表头字段")
	private String dateCreate;
		@ApiModelProperty(value="合同总金额,合同总金额")
	private Double contractTotalAmount;
		@ApiModelProperty(value="已付款金额,已付款金额")
	private Double contractPaidAmount;
		@ApiModelProperty(value="本次付款金额,本次付款金额")
	private Double contractThisAmount;
	@ApiModelProperty(value="付款到平台时间")
	private String paymentDate;
		@ApiModelProperty(value="银行账号,银行账号")
	private String bankAccount;
		@ApiModelProperty(value="付款银行")
	private String bankDeposit;
		@ApiModelProperty(value="发票类型， 增票 普增票 普票 其他,发票类型， 增票 普增票 普票 其他")
	private String invoiceType;
		@ApiModelProperty(value="招标内容,招标内容")
	private String bidContent;
		@ApiModelProperty(value="合同时间 ,合同时间 ")
	private String contractTime;
		@ApiModelProperty(value="贷款说明,贷款说明")
	private String loanReason;
		@ApiModelProperty(value="付款状态")
	private Integer payState;
	
	@ApiModelProperty(value="付款状态字符串")
	private String payStateStr;
	/***  付款计划流程使用状态：2017/11/15 PMQ ADD start  ***/
	@ApiModelProperty(value="制定付款计划状态: 0.未开启, 1.启动制定, 2.制定完成 (参考常量: ZhucaiConstant.MakePaymentPlanStatus)")
	private Integer paymentPlanStatus = 0;
	/***  付款计划流程使用状态：2017/11/15 PMQ ADD end  ***/
	
		@ApiModelProperty(value="付款批次")
	private String paymentName;
		
		@ApiModelProperty(value="本次实际付款金额,本次实际付款金额")
	private Double payThisAmount;
		
		@ApiModelProperty(value="发起审核时间")
	private String initiateDate;
		
		@ApiModelProperty(value="实际付款时间")
	private String payForDate;
		
		@ApiModelProperty(value="支付方式1线上2线下") 
	private Integer payForMethod;
		
		@ApiModelProperty(value="备注")
	private String remark;
		
		@ApiModelProperty(value="审核意见")
	private String checkComments;

	@ApiModelProperty(value="平台付款管理: 状态( 0:待付款, 1:已付款, 2:付款失败 )") 
	private Integer platformPayStatus;

	@ApiModelProperty(value="平台付款管理: 付款日期") 
	private String platformPayDate;

	@ApiModelProperty(value="平台付款管理: 流水号") 
	private String platformPaySerial;

	@ApiModelProperty(value="平台付款管理: 付款类型( 0:单次付款, 1:批量付款 )") 
	private Integer platformPayCheckMethod;
	
	@ApiModelProperty(value="平台付款管理: 支付方式   1线上2线下") 
	private Integer platformPayForMethod;
	
	@ApiModelProperty(value="平台付款管理: 前台任务执行状态( 0:未执行, 1:已执行 )") 
	private Integer platformPayTaskStatus;
	@ApiModelProperty(value="平台付款管理: 付款方式( 1:委托集团付款方式, 2:商票, 3:采购单元付款，4：采购单元计划 )") 
	private String payway;
	@ApiModelProperty(value="付款方式: 房款方式( 1:直接付款, 2:委托支付)") 
	private String payChannelType;
	@ApiModelProperty(value="付款渠道: 付款渠道( 1:银行转账, 2:票据支付, 3:E点通支付，4：线下支付，5：银联在线支付 )") 
	private String payChannel;
	
	
	
	/**********订单模式 begin*************/
	@ApiModelProperty(value="付款计划类型  0预付款 1订单付款 ZhucaiConstant.OrderType") 
	private Integer payPlanType;
	
	@ApiModelProperty(value="订单编号")
	private String orderNum;
	
	@ApiModelProperty(value="订单Id")
	private Long orderId;
	/**********订单模式 end*************/
	
/**********金融发票begin*************/
	
	@ApiModelProperty(value="发票附件Id")
	private Long invoiceFileId;
	
	/**********金融发票end*************/
	
	@Transient
	public Integer getPayPlanType() {
		return payPlanType;
	}

	public Long getInvoiceFileId() {
		return invoiceFileId;
	}

	public void setInvoiceFileId(Long invoiceFileId) {
		this.invoiceFileId = invoiceFileId;
	}

	public String getPayStateStr() {
		return payStateStr;
	}

	public void setPayStateStr(String payStateStr) {
		this.payStateStr = payStateStr;
	}

	public void setPayPlanType(Integer payPlanType) {
		this.payPlanType = payPlanType;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public void setContractId(Long contractId){
		this.contractId = contractId;
	}
	
	public Long getContractId(){
		return contractId;
	}
	
	public void setMissionId(Long missionId){
		this.missionId = missionId;
	}
	
	public Long getMissionId(){
		return missionId;
	}
	
	public void setContractNum(String contractNum){
		this.contractNum = contractNum;
	}
	
	@Column(nullable=true,length=50)
	public String getContractNum(){
		return contractNum;
	}
		public void setMissionNum(String missionNum){
		this.missionNum = missionNum;
	}
	
	@Column(nullable=true,length=50)
	public String getMissionNum(){
		return missionNum;
	}
		public void setProjectName(String projectName){
		this.projectName = projectName;
	}
	
	@Column(nullable=true,length=255)
	public String getProjectName(){
		return projectName;
	}
		public void setProjectManager(String projectManager){
		this.projectManager = projectManager;
	}
	
	@Column(nullable=true,length=50)
	public String getProjectManager(){
		return projectManager;
	}
		public void setPayerName(String payerName){
		this.payerName = payerName;
	}
	
	@Column(nullable=true,length=50)
	public String getPayerName(){
		return payerName;
	}
		public void setPayeeTrustName(String payeeTrustName){
		this.payeeTrustName = payeeTrustName;
	}
	
	@Column(nullable=true,length=255)
	public String getPayeeTrustName(){
		return payeeTrustName;
	}
		public void setPayeeName(String payeeName){
		this.payeeName = payeeName;
	}
	
	@Column(nullable=true,length=255)
	public String getPayeeName(){
		return payeeName;
	}
		public void setDateCreate(String dateCreate){
		this.dateCreate = dateCreate;
	}
	
	@Column(nullable=true,length=50)
	public String getDateCreate(){
		return dateCreate;
	}
		public void setContractTotalAmount(Double contractTotalAmount){
		this.contractTotalAmount = contractTotalAmount;
	}
	
	@Column(nullable=true,length=20)
	public Double getContractTotalAmount(){
		return contractTotalAmount;
	}
		public void setContractPaidAmount(Double contractPaidAmount){
		this.contractPaidAmount = contractPaidAmount;
	}
	
	@Column(nullable=true,length=20)
	public Double getContractPaidAmount(){
		return contractPaidAmount;
	}
		public void setContractThisAmount(Double contractThisAmount){
		this.contractThisAmount = contractThisAmount;
	}
	
	@Column(nullable=true,length=20)
	public Double getContractThisAmount(){
		if(contractThisAmount==null){
			return 0.0;
		}
		return contractThisAmount;
	}
		public void setPaymentDate(String paymentDate){
		this.paymentDate = paymentDate;
	}
	
	@Column(nullable=true,length=50)
	public String getPaymentDate(){
		return paymentDate;
	}

		public void setBankAccount(String bankAccount){
		this.bankAccount = bankAccount;
	}
	
	@Column(nullable=true,length=50)
	public String getBankAccount(){
		return bankAccount;
	}
		public void setBankDeposit(String bankDeposit){
		this.bankDeposit = bankDeposit;
	}
	
	@Column(nullable=true,length=50)
	public String getBankDeposit(){
		return bankDeposit;
	}
		public void setInvoiceType(String invoiceType){
		this.invoiceType = invoiceType;
	}
	
	@Column(nullable=true,length=50)
	public String getInvoiceType(){
		return invoiceType;
	}
		public void setBidContent(String bidContent){
		this.bidContent = bidContent;
	}
	
	@Column(nullable=true,length=255)
	public String getBidContent(){
		return bidContent;
	}
		public void setContractTime(String contractTime){
		this.contractTime = contractTime;
	}
	
	@Column(nullable=true,length=50)
	public String getContractTime(){
		return contractTime;
	}
		public void setLoanReason(String loanReason){
		this.loanReason = loanReason;
	}
	
	@Column(nullable=true,length=255)
	public String getLoanReason(){
		return loanReason;
	}

	public Integer getPayState() {
		return payState;
	}

	public void setPayState(Integer payState) {
		this.payState = payState;
	}

	public String getPaymentName() {
		return paymentName;
	}

	public void setPaymentName(String paymentName) {
		this.paymentName = paymentName;
	}

	public Double getPayThisAmount() {
		return payThisAmount;
	}

	public void setPayThisAmount(Double payThisAmount) {
		this.payThisAmount = payThisAmount;
	}

	public String getInitiateDate() {
		return initiateDate;
	}

	public void setInitiateDate(String initiateDate) {
		this.initiateDate = initiateDate;
	}

	public Integer getPayForMethod() {
		return payForMethod;
	}

	public void setPayForMethod(Integer payForMethod) {
		this.payForMethod = payForMethod;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getPayForDate() {
		return payForDate;
	}

	public void setPayForDate(String payForDate) {
		this.payForDate = payForDate;
	}

	public String getCheckComments() {
		return checkComments;
	}

	public void setCheckComments(String checkComments) {
		this.checkComments = checkComments;
	}

	@Column(nullable = true, columnDefinition = "INT default 0")
	public Integer getPlatformPayStatus() {
		return platformPayStatus;
	}

	public void setPlatformPayStatus(Integer platformPayStatus) {
		this.platformPayStatus = platformPayStatus;
	}

	public String getPlatformPayDate() {
		return platformPayDate;
	}

	public void setPlatformPayDate(String platformPayDate) {
		this.platformPayDate = platformPayDate;
	}

	public String getPlatformPaySerial() {
		return platformPaySerial;
	}

	public void setPlatformPaySerial(String platformPaySerial) {
		this.platformPaySerial = platformPaySerial;
	}

	public Integer getPlatformPayCheckMethod() {
		return platformPayCheckMethod;
	}

	public void setPlatformPayCheckMethod(Integer platformPayCheckMethod) {
		this.platformPayCheckMethod = platformPayCheckMethod;
	}

	@Column(nullable = true, columnDefinition = "INT default 2")
	public Integer getPlatformPayForMethod() {
		return platformPayForMethod;
	}

	public void setPlatformPayForMethod(Integer platformPayForMethod) {
		this.platformPayForMethod = platformPayForMethod;
	}

	@Column(nullable = true, columnDefinition = "INT default 0")
	public Integer getPlatformPayTaskStatus() {
		return platformPayTaskStatus;
	}

	public void setPlatformPayTaskStatus(Integer platformPayTaskStatus) {
		this.platformPayTaskStatus = platformPayTaskStatus;
	}
	@Column(nullable=true,length=4)
	public String getPayway() {
		return payway;
	}

	public void setPayway(String payway) {
		this.payway = payway;
	}

	public String getPayChannelType() {
		return payChannelType;
	}

	public void setPayChannelType(String payChannelType) {
		this.payChannelType = payChannelType;
	}

	public String getPayChannel() {
		return payChannel;
	}

	public void setPayChannel(String payChannel) {
		this.payChannel = payChannel;
	}

	@Column(nullable = true, columnDefinition = "INT default 0")
	public Integer getPaymentPlanStatus() {
		return paymentPlanStatus;
	}

	public void setPaymentPlanStatus(Integer paymentPlanStatus) {
		this.paymentPlanStatus = paymentPlanStatus;
	}
	
}
