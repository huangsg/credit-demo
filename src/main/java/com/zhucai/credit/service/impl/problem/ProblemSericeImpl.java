package com.zhucai.credit.service.impl.problem;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zhucai.credit.model.ProblemRecordEntrity;
import com.zhucai.credit.repository.ProblemRecordRepository;



/**
 * 请求建行接口
 * 2017年11月28日
 * @author hsg
 */
@Service("problemSericeImpl")
public class ProblemSericeImpl{
	@Autowired
	ProblemRecordRepository problemRecordRepository;
	
	private static final Logger logger = LoggerFactory.getLogger(ProblemSericeImpl.class);
	
	 public void setProblem(ProblemRecordEntrity pre){
		 try {
			 problemRecordRepository.save(pre);
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("调用记录问题失败msg={}",e.getMessage());
		}
	 }
	 public void setProblem(String problem,String problemType){
		 try {
			 
			ProblemRecordEntrity pre = new ProblemRecordEntrity();
			pre.setOperPhone("18179810921");
			pre.setOperTime(new Date());
			pre.setOperlevel("A");
			pre.setOperStatus(0);
			pre.setProblemType(problemType);
			pre.setProblemRemark(problem);
			problemRecordRepository.save(pre);
		 } catch (Exception e) {
			 // TODO: handle exception
			 logger.error("调用记录问题失败msg={}",e.getMessage());
		 }
	 }
}
