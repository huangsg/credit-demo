package com.zhucai.credit.service.impl.excel;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.zhucai.credit.bean.CreditApplication;
import com.zhucai.credit.utils.JxlsUtils;

@Service("excelExportUtil")
public class ExcelExportUtil {
	
	private static final Logger logger = LoggerFactory.getLogger(ExcelExportUtil.class);
	@Value("${icbc.excel.path}")
	private  String path;

	/**
	 * 生成核心企业推荐供应商入驻excel
	 * @author shoffice
	 * @version 2018年8月15日
	 */
	public  Boolean createEnteriseInfoExcel(List<CreditApplication> caList){
		logger.info("### 生成核心企业推荐供应商入驻excel start ca={}", caList.toString());
		ClassPathResource res = new ClassPathResource("jxls/icbc/customer_application.xlsx");
		if(CollectionUtils.isEmpty(caList)){
			throw new RuntimeException("导出附件异常!");
		}
		
		//动态获取excel名称
		String fileExcelName = caList.get(0).getExcelName();
		File outFile = new File(path,fileExcelName);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("list", caList);
		try {
			JxlsUtils.exportExcel(res.getInputStream(),outFile , model);
		} catch (IOException e) {
			e.printStackTrace();
			logger.info("### 生成核心企业推荐供应商入驻excel 失败 errMsh{}", e);
			throw new RuntimeException("导出附件异常!");
		}
		logger.info("### 生成核心企业推荐供应商入驻excel 成功");
		return true;
	}
	/**
	 * 导出历史交易记录excel
	 * @version 2018年8月15日
	 */
	public  Boolean exportHistroynfoExcel(List<CreditApplication> caList){
		logger.info("### 生成核心企业推荐供应商入驻excel start ca={}", caList.toString());
		ClassPathResource res = new ClassPathResource("jxls/icbc/history_transaction.xlsx");
		if(CollectionUtils.isEmpty(caList)){
			throw new RuntimeException("导出附件异常!");
		}
		
		//动态获取excel名称
		String fileExcelName = caList.get(0).getExcelName();
		File outFile = new File(path,fileExcelName);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("list", caList);
		try {
			JxlsUtils.exportExcel(res.getInputStream(),outFile , model);
		} catch (IOException e) {
			e.printStackTrace();
			logger.info("### 生成核心企业推荐供应商入驻excel 失败 errMsh{}", e);
			throw new RuntimeException("导出附件异常!");
		}
		logger.info("### 生成核心企业推荐供应商入驻excel 成功");
		return true;
	}

}
