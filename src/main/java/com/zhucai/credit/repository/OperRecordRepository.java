package com.zhucai.credit.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.zhucai.credit.common.CommonEnums.OperType;
import com.zhucai.credit.model.OperRecordEntity;

/**
 * 
 * @author hsg
 * 操作记录的Repository
 */
@Repository
public interface OperRecordRepository extends
		PagingAndSortingRepository<OperRecordEntity, Long>,
		JpaSpecificationExecutor<OperRecordEntity> {
	



	List<OperRecordEntity> findByOutIdAndStatusNotAndOperTypeOrderByOperTimeDesc(Long tid,Integer del,String key);
	OperRecordEntity findByTid(Long tid);
	/**
	 * 根据操作类型和关联id查询操作记录
	 * @date 2017年12月11日 @user hsg
	 */
	@Query(value = " select operRecordEntity "
			+ " from OperRecordEntity operRecordEntity "
			+ " where operRecordEntity.operType = ?1 "
			+ " and operRecordEntity.outId = ?2 "
			+ " and operRecordEntity.status<>?3")
	List<OperRecordEntity> findByOperTypeAndOutIdAndStatusNot(String operType, Long tid, int delete);
   
	@Query(value = " select operRecordEntity "
			+ " from OperRecordEntity operRecordEntity "
			+ " where operRecordEntity.operType = ?1 "
			+ " and operRecordEntity.bizKey = ?2 "
			+ " and operRecordEntity.status<>?3"
			+ " and operRecordEntity.result=?4")
	List<OperRecordEntity> findByOperTypeAndBizKeyAndStatusNot(OperType drawAmount, String payout_num, int delete,String success);

}
