package com.zhucai.credit.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.wordnik.swagger.annotations.ApiModel;


/**
 * 收银台订单表
 * @author hsg
 *
 */
@Entity
@Table(name = "finance_pay_order")
@ApiModel(value = "收银台订单表")
public class PaymentOrderEntity extends BaseEntity {
	
    //业务编号
	@Column(nullable = false,length = 19)
	private String serviceNo;
	//业务订单编号
	@Column(nullable = false,length = 19)
	private String orderNo;
	//付款商家名称
	@Column(nullable = true,length = 50)
	private String payEnterpriseName;
	//付款商家id
	@Column(nullable = true)
	private Long payEnterpriseId;
	//收款商家名称
	@Column(nullable = true,length = 50)
	private String collectEnterpriseName;
	//订单金额
	@Column(nullable = false,columnDefinition = "decimal default 0")
	private BigDecimal orderAmt;
	//折扣
	@Column(nullable = true)
	private BigDecimal discount;
	//折后金额
	@Column(nullable = false)
	private BigDecimal discountAmt;
	//商品名称
	@Column(nullable = false,length = 200)
	private String productName;
	//商品描述
	@Column(nullable = true,length = 200)
	private String productRemark;
    //超时时间
	@Column(nullable = false)
	private Date orderOutTime;
	//支付时间
	@Column(nullable = false)
	private Date payTime;
	//支付状态
	@Column(nullable = false,length = 2)
	private String payStatus;
	//支付方式
	@Column(nullable = true,length = 20)
	private String payType;
	//交易状态
	@Column(nullable = false,length = 20)
	private String tradeStatus;
	//流水号
	@Column(nullable = true,length = 20)
	private String flowNumber;
	//备注
	@Column(nullable = true,length = 500)
	private String remark;
	//创建时间
	@Column(nullable = false)
	private Date createOrderTime;
	//创建人
	@Column(nullable = false)
	private Long createOrderUser;
	//融资银行
	@Column(nullable = true)
	private String financeBank;
	
	 
	public String getFinanceBank() {
		return financeBank;
	}
	public void setFinanceBank(String financeBank) {
		this.financeBank = financeBank;
	}
	public String getServiceNo() {
		return serviceNo;
	}
	public void setServiceNo(String serviceNo) {
		this.serviceNo = serviceNo;
	}
	public String getPayEnterpriseName() {
		return payEnterpriseName;
	}
	public void setPayEnterpriseName(String payEnterpriseName) {
		this.payEnterpriseName = payEnterpriseName;
	}
	public BigDecimal getOrderAmt() {
		return orderAmt;
	}
	public void setOrderAmt(BigDecimal orderAmt) {
		this.orderAmt = orderAmt;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public BigDecimal getDiscountAmt() {
		return discountAmt;
	}
	public void setDiscountAmt(BigDecimal discountAmt) {
		this.discountAmt = discountAmt;
	}
	public Long getPayEnterpriseId() {
		return payEnterpriseId;
	}
	public void setPayEnterpriseId(Long payEnterpriseId) {
		this.payEnterpriseId = payEnterpriseId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductRemark() {
		return productRemark;
	}
	public void setProductRemark(String productRemark) {
		this.productRemark = productRemark;
	}
	public Date getOrderOutTime() {
		return orderOutTime;
	}
	public void setOrderOutTime(Date orderOutTime) {
		this.orderOutTime = orderOutTime;
	}
	public Date getPayTime() {
		return payTime;
	}
	public void setPayTime(Date payTime) {
		this.payTime = payTime;
	}
	public String getPayStatus() {
		return payStatus;
	}
	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	public String getTradeStatus() {
		return tradeStatus;
	}
	public void setTradeStatus(String tradeStatus) {
		this.tradeStatus = tradeStatus;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getFlowNumber() {
		return flowNumber;
	}
	public void setFlowNumber(String flowNumber) {
		this.flowNumber = flowNumber;
	}
	public String getCollectEnterpriseName() {
		return collectEnterpriseName;
	}
	public void setCollectEnterpriseName(String collectEnterpriseName) {
		this.collectEnterpriseName = collectEnterpriseName;
	}
	public Date getCreateOrderTime() {
		return createOrderTime;
	}
	public void setCreateOrderTime(Date createOrderTime) {
		this.createOrderTime = createOrderTime;
	}
	public Long getCreateOrderUser() {
		return createOrderUser;
	}
	public void setCreateOrderUser(Long createOrderUser) {
		this.createOrderUser = createOrderUser;
	}
}
