package com.zhucai.credit.service.impl.transactional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.zhucai.credit.model.Message;
import com.zhucai.credit.model.MessageUser;
import com.zhucai.credit.model.StationMessage;
import com.zhucai.credit.repository.MessageRepository;
import com.zhucai.credit.repository.MessageUserRepository;
import com.zhucai.credit.repository.StationMessageRepository;

@Service("messageService")
@Transactional(readOnly = false, rollbackFor = Exception.class)
public class MessageServiceImpl {


	@Autowired
	private MessageRepository messageRepository;
	@Autowired
	private StationMessageRepository stationMessageRepository;
	@Autowired
	private MessageUserRepository messageUserRepository;
	/**
	 * 站内信
	 * @throws BusinessException
	 */
	public void sendMessage(String source,String number,String url,List<Long> ids,List<String> param) 
			{
		
		try{
			StationMessage stationMessage = stationMessageRepository.findStationMessage(number);
			
			//PMQ 20180106  当空时跳过处理，不让崩掉
			if(stationMessage == null || StringUtils.isEmpty(stationMessage.getContent())){
				return;
			}
			
			//获取信息模板，组装信息内容
			String content = stationMessage.getContent();
			if (!CollectionUtils.isEmpty(param)) {
				for(int i=0;i<param.size();i++){
					switch (i) {
					case 0:
						content = content.replaceAll("XXXXX", param.get(i));//替换内容模板中的XXXXX
						break;
					case 1:
						content = content.replaceAll("YYYYY", param.get(i));//替换内容模板中的YYYYY
						break;
					case 2:
						content = content.replaceAll("ZZZZZ", param.get(i));//替换内容模板中的ZZZZZ
						break;
					}
				}
			}
			if("flow_remind".equals(number) || "P53".equals(number)){
				List<Message> messageList =  new ArrayList<Message>();
				messageList = messageRepository.findMessageByContent(content);
				if(!CollectionUtils.isEmpty(messageList)){
					return;
				}
			}
			
			Message message = new Message();//new
			message.setContent(content);
			message.setKind(2);
			message.setType(stationMessage.getType());
			message.setUrl(url);
			message.setSource(source);
			message.setCreateUser(Long.parseLong(source));
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			message.setCreateTime(new java.util.Date().getTime());
			message.setSendDate(sf.format(new java.util.Date()));
			message = messageRepository.save(message);
			//站内信关联用户
			if (!CollectionUtils.isEmpty(ids)) {
				for(int k=0;k<ids.size();k++){
					MessageUser messageUser = new MessageUser();
					messageUser.setMessageId(message.getTid());
					messageUser.setUserId(ids.get(k));
					messageUser.setCreateUser(Long.parseLong(source));
					messageUser.setCreateTime(new java.util.Date().getTime());
					messageUser.setIsNew(0);
					messageUser = messageUserRepository.save(messageUser);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
}
