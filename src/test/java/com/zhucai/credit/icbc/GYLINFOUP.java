package com.zhucai.credit.icbc;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.zhucai.credit.utils.StrongJsoup;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GYLINFOUP {
	
	@Autowired
	private TemplateEngine templateEngine;
	 
	
	/**
	 * 发送融资交易信息
	 * @version 2018年8月25日
	 * @throws Exception
	 */
	@Test
    public void build() throws Exception{
		Date d = new Date();
		SimpleDateFormat fmtTranDate = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat fmtTranTime = new SimpleDateFormat("HHmmssSSS");
		
  
        String cis = "440290000079081";
        String bankCode = "102";
        String id = "lubw.y.4402";
        long fSeqno = d.getTime();
        String CODE = "GYLINFOUP";
        String tranDate = fmtTranDate.format(d);
        String tranTime = fmtTranTime.format(d);
        
        
        Context context = new Context();
		context.setVariable("CIS",  cis);
		context.setVariable("BankCode",  bankCode);
		context.setVariable("ID",  id);
        
		context.setVariable("TranDate",  tranDate);
		context.setVariable("TranTime",  tranTime);
		context.setVariable("fSeqno",  fSeqno);
		context.setVariable("TotalNum",  "1");
		context.setVariable("TotalAmt",  "20000");
		context.setVariable("TranType",  "1");
		context.setVariable("SignTime", tranDate+tranTime);
        context.setVariable("AccNo",  "4402202019100169946");
        context.setVariable("GYLCode",  "GY-2018000006476");

        context.setVariable("iSeqno",  System.currentTimeMillis());
        context.setVariable("VouchType",  "01");
        context.setVariable("VouchCode",  "9936282105");
        context.setVariable("VouchNo",  "9946616");

        
        context.setVariable("DueAmt",  "20000");
        context.setVariable("DueDate",  "20180730");
        context.setVariable("PlanDate",  "20181130");
        context.setVariable("MemberName",  "膛妇钰弯初禽艺凑词仲沈僧");
        //context.setVariable("MemberName",  "孤臊汪争蜀艺词仲沈僧");
        context.setVariable("MemberCode",  "79039566-7");
        //context.setVariable("MemberAccNo",  "1001708229006846128");
        context.setVariable("Status",  "1");

        context.setVariable("ContactNo", "ZC_HT_3599480984");
        //context.setVariable("MemberType", "1");
        //context.setVariable("BillAmt", "20000");
        
        String signUrl = "http://127.0.0.1:449";
        StrongJsoup connect = StrongJsoup.connect(signUrl);
        String x = "<?xml version=\"1.0\" encoding=\"GBK\"?>\r\n"+templateEngine.process("icbc/"+CODE, context);
        System.out.println(x);
        
		connect.headers(ImmutableMap.of("Content-Type", "INFOSEC_SIGN/1.0","Content-Length",x.getBytes().length+""));

		connect.getJsoupConn().postDataCharset("GBK").request().requestBody(x);

		Document post1 = connect.post(1);
		Elements eles = post1.getElementsByTag("sign");
		String signText = eles.text();
		//System.out.println(signText);
        
        
        
		String url = "http://127.0.0.1:448/servlet/ICBCCMPAPIReqServlet?userID="+id+"&PackageID="+fSeqno+"&SendTime="+tranDate+tranTime;
		StrongJsoup connect1 = StrongJsoup.connect(url);
		connect1.headers(ImmutableMap.of("Content-Type", "application/x-www-form-urlencoded"));
		
		Map<String,String> params = Maps.newHashMap();
		params.put("Version", "0.0.0.1");
		params.put("TransCode",CODE);
		params.put("BankCode",bankCode);
		params.put("GroupCIS",cis);
		params.put("ID",id);
		params.put("Cert","");
		params.put("reqData",signText);
		//System.out.println(url);
		
		//System.out.println(params);
		connect1.getJsoupConn().data(params);
		
		Document post = connect1.post();
		String text = post.text();
		System.out.println(post);
		
		Base64 base64 = new Base64();
		String substring = text.substring(text.indexOf("=")+1);
		//System.out.println(substring);
		byte[] decode = base64.decode(substring);
		System.out.println(new String(decode,"GBK"));
        
        
        
    }


}
