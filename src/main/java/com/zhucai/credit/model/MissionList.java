package com.zhucai.credit.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * @author 曹文波
 * @ClassName: MissionList
 * @Description: 采购 招标 合同任务表定义表
 * @date 2014年10月11日 上午10:47:49
 */
@Entity
@Table(name = "tb_xa_mission_list")
@ApiModel(value = "采购 招标 合同任务表定义表")
public class MissionList extends BaseEntity{

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "任务名称,任务名称")
    private String missionName;
    
    @ApiModelProperty(value = "任务批次")
    private String missionBatch;
    
    @ApiModelProperty(value = "任务号")
    private String missionNo;
    
    @ApiModelProperty(value = "要求验收的日期,要求验收的日期")
    private String requireDate;
    @ApiModelProperty(value = "付款日期,付款日期")
    private String paymentDate;
    @ApiModelProperty(value = "任务完成后，多少天付款,任务完成后，多少天付款")
    private String paymentDaysFromComplete;
    @ApiModelProperty(value = "付款方式 1比例 2金额,付款方式")
    private Integer paymentMethod;
    @ApiModelProperty(value = "付款比例,付款比例")
    private String paymentRatio;
    @ApiModelProperty(value = "任务状态,任务状态")
    private String missionStatus;
    @ApiModelProperty(value = "付款金额-元,付款金额")
    private Double paymentMoney;
    @ApiModelProperty(value = "合同ID,合同ID")
    private Long contractId;
    @ApiModelProperty(value = "合同标识,1=招标合同 2=采购合同")
    private Integer contractType;
    
    @ApiModelProperty(value = "验收日期")
    private String checkDate;
    
    
    @ApiModelProperty(value = "合同信息")
    private ContractList contractList;
    
    @ApiModelProperty(value = "任务详情")
    private String missionDetail;
   
    @ApiModelProperty(value = "支付的当前步骤：1：等待财务确认付款计划，2：等待项目经理审核，3：等待销售员确认")
    private String paymentStep;

    @ApiModelProperty(value = "备注")
    private String reason;
    
    @ApiModelProperty(value = "补充说明")
    private String remark;
    
    @ApiModelProperty(value = "处理争议的时间")
    private String disputeDate;
    
    @ApiModelProperty(value = "发起争议人ID")
    private Long disputePersonId;
    
    /****** PMQ 2017/12/29 update 实际付款流程发起人 start  ******/
    @ApiModelProperty(value = "实际付款发起人")
    private Long paymentStarter;
    /****** PMQ 2017/12/29 update 实际付款流程发起人 end  ******/
    
    public void setMissionName(String missionName) {
        this.missionName = missionName;
    }

    @Column(nullable = true, length = 50)
    public String getMissionName() {
        return missionName;
    }

    public void setRequireDate(String requireDate) {
        this.requireDate = requireDate;
    }

    @Column(nullable = true, length = 50)
    public String getRequireDate() {
        return requireDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    @Column(nullable = true, length = 50)
    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDaysFromComplete(String paymentDaysFromComplete) {
        this.paymentDaysFromComplete = paymentDaysFromComplete;
    }

    @Column(nullable = true, length = 50)
    public String getPaymentDaysFromComplete() {
        return paymentDaysFromComplete;
    }

    public void setPaymentRatio(String paymentRatio) {
        this.paymentRatio = paymentRatio;
    }

    @Column(nullable = true, length = 50)
    public String getPaymentRatio() {
        return paymentRatio;
    }

    public void setMissionStatus(String missionStatus) {
        this.missionStatus = missionStatus;
    }

    @Column(nullable = true, length = 50)
    public String getMissionStatus() {
        return missionStatus;
    }

    public void setPaymentMoney(Double paymentMoney) {
        this.paymentMoney = paymentMoney;
    }

    @Column(nullable = true, length = 50)
    public Double getPaymentMoney() {
        return paymentMoney;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    @Column(nullable = true, length = 50)
    public Long getContractId() {
        return contractId;
    }

    @Column(nullable = true, length = 8)
    public Integer getContractType() {
        return contractType;
    }

    public void setContractType(Integer contractType) {
        this.contractType = contractType;
    }
    @Column(nullable = true, length = 8)
    public Integer getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(Integer paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
    
    @Column(nullable = true, length = 50)
    public String getMissionBatch() {
		return missionBatch;
	}

	public void setMissionBatch(String missionBatch) {
		this.missionBatch = missionBatch;
	}
	@Column(nullable = true, length = 50)
	public String getMissionNo() {
		return missionNo;
	}

	public void setMissionNo(String missionNo) {
		this.missionNo = missionNo;
	}
	
	@Column(nullable = true, length = 50)
	public String getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}
	
	@Transient
	public ContractList getContractList() {
		return contractList;
	}

	public void setContractList(ContractList contractList) {
		this.contractList = contractList;
	}
	
	
	@Transient
	public String getMissionDetail() {
		return missionDetail;
	}

	public void setMissionDetail(String missionDetail) {
		this.missionDetail = missionDetail;
	}

	public String getPaymentStep() {
		return paymentStep;
	}

	public void setPaymentStep(String paymentStep) {
		this.paymentStep = paymentStep;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getDisputeDate() {
		return disputeDate;
	}

	public void setDisputeDate(String disputeDate) {
		this.disputeDate = disputeDate;
	}

	public Long getDisputePersonId() {
		return disputePersonId;
	}

	public void setDisputePersonId(Long disputePersonId) {
		this.disputePersonId = disputePersonId;
	}

	public Long getPaymentStarter() {
		return paymentStarter;
	}

	public void setPaymentStarter(Long paymentStarter) {
		this.paymentStarter = paymentStarter;
	}

	
	
}
