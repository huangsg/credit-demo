package com.zhucai.credit.bank.log;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * LOG Repository
 */
@Repository
public interface LogRepository extends JpaRepository<LogEntity,Long> {

	@Query(value="select tp_key from finance_tp_log where tp_type = ?1 and tp_code = ?2 order by tp_key desc limit 1",nativeQuery=true)
	Long getMaxTpKey(String tpType,String tpCode);
	
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value="update finance_tp_log set request = ?2 where id = ?1",nativeQuery=true)
	void updateRequest(Long id,String request);

	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value="update finance_tp_log set response = ?2,tp_key = ?3 where id = ?1",nativeQuery=true)
	void updateResponse(Long id,String request,String tpKey);

}
