package com.zhucai.credit.bank.ccbe.bean;

import java.io.Serializable;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * 扣款信息反馈
 * 6W5550
 * @date 2017年11月28日 @user hsg
 */
public class CreditRecordDebit implements Serializable{

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "当前最大反馈序号")
	private Integer max_serial_num;
	@ApiModelProperty(value = "反馈序号")
	private Integer serial_num;
	@ApiModelProperty(value = "申请融资客户的企业名称")
	private String cust_name;
	@ApiModelProperty(value = "合同编号")
	private String contract_num;
	@ApiModelProperty(value = "支用编号")
	private String payout_num;
	@ApiModelProperty(value = "扣款标识")
	private String deduct_ind;
	@ApiModelProperty(value = "归还贷款本金")
	private Double pay_principal_amt;
	@ApiModelProperty(value = "回款专户扣款金额")
	private Double recover_account_amt;
	@ApiModelProperty(value = "一般结算户扣款金额")
	private Double clearance_account_amt;
	@ApiModelProperty(value = "平台监管账户扣款金额")
	private Double supervise_account_amt;
	@ApiModelProperty(value = "风险池扣款金额")
	private Double risk_pool_amt;
	@ApiModelProperty(value = "保证金账户扣款金额")
	private Double guarantee_account_amt;
	@ApiModelProperty(value = "代还账户扣款金额")
	private Double platform_account_amt;
	@ApiModelProperty(value = "欠本金金额")
	private Double owe_principal_amt;
	@ApiModelProperty(value = "欠利息金额")
	private Double owe_interest_amt;
	@ApiModelProperty(value = "欠费金额")
	private Double owe_cost_amt;
	@ApiModelProperty(value = "单据号(已弃用)")
	private String bill_num;
	@ApiModelProperty(value = "币种代码")
	private String currency_cd;
	//漏掉的字段
	@ApiModelProperty(value = "归还利息")
	private Double pay_interest_amt;
	
	public Integer getMax_serial_num() {
		return max_serial_num;
	}
	public void setMax_serial_num(Integer max_serial_num) {
		this.max_serial_num = max_serial_num;
	}
	public Integer getSerial_num() {
		return serial_num;
	}
	public void setSerial_num(Integer serial_num) {
		this.serial_num = serial_num;
	}
	public String getCust_name() {
		return cust_name;
	}
	public void setCust_name(String cust_name) {
		this.cust_name = cust_name;
	}
	public String getContract_num() {
		return contract_num;
	}
	public void setContract_num(String contract_num) {
		this.contract_num = contract_num;
	}
	public String getPayout_num() {
		return payout_num;
	}
	public void setPayout_num(String payout_num) {
		this.payout_num = payout_num;
	}
	public String getDeduct_ind() {
		return deduct_ind;
	}
	public void setDeduct_ind(String deduct_ind) {
		this.deduct_ind = deduct_ind;
	}
	public Double getPay_principal_amt() {
		return pay_principal_amt;
	}
	public void setPay_principal_amt(Double pay_principal_amt) {
		this.pay_principal_amt = pay_principal_amt;
	}
	public Double getRecover_account_amt() {
		return recover_account_amt;
	}
	public void setRecover_account_amt(Double recover_account_amt) {
		this.recover_account_amt = recover_account_amt;
	}
	public Double getClearance_account_amt() {
		return clearance_account_amt;
	}
	public void setClearance_account_amt(Double clearance_account_amt) {
		this.clearance_account_amt = clearance_account_amt;
	}
	public Double getSupervise_account_amt() {
		return supervise_account_amt;
	}
	public void setSupervise_account_amt(Double supervise_account_amt) {
		this.supervise_account_amt = supervise_account_amt;
	}
	public Double getRisk_pool_amt() {
		return risk_pool_amt;
	}
	public void setRisk_pool_amt(Double risk_pool_amt) {
		this.risk_pool_amt = risk_pool_amt;
	}
	public Double getGuarantee_account_amt() {
		return guarantee_account_amt;
	}
	public void setGuarantee_account_amt(Double guarantee_account_amt) {
		this.guarantee_account_amt = guarantee_account_amt;
	}
	public Double getPlatform_account_amt() {
		return platform_account_amt;
	}
	public void setPlatform_account_amt(Double platform_account_amt) {
		this.platform_account_amt = platform_account_amt;
	}
	public Double getOwe_principal_amt() {
		return owe_principal_amt;
	}
	public void setOwe_principal_amt(Double owe_principal_amt) {
		this.owe_principal_amt = owe_principal_amt;
	}
	public Double getOwe_interest_amt() {
		return owe_interest_amt;
	}
	public void setOwe_interest_amt(Double owe_interest_amt) {
		this.owe_interest_amt = owe_interest_amt;
	}
	public Double getOwe_cost_amt() {
		return owe_cost_amt;
	}
	public void setOwe_cost_amt(Double owe_cost_amt) {
		this.owe_cost_amt = owe_cost_amt;
	}
	public String getBill_num() {
		return bill_num;
	}
	public void setBill_num(String bill_num) {
		this.bill_num = bill_num;
	}
	public String getCurrency_cd() {
		return currency_cd;
	}
	public void setCurrency_cd(String currency_cd) {
		this.currency_cd = currency_cd;
	}
	public Double getPay_interest_amt() {
		return pay_interest_amt;
	}
	public void setPay_interest_amt(Double pay_interest_amt) {
		this.pay_interest_amt = pay_interest_amt;
	}
	
}
