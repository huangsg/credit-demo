package com.zhucai.credit.common;


/**
 * Created by cui on 2015/10/23.
 */
public class ZhucaiConstant {


    /**
     * 消息状态
     *
     * @author zc
     */
    public static class MessageConstant {

        /**
         * 采购商的留言
         */
        public static final Integer purchaserLeave = 1;
        /**
         * 供应商的留言
         */
        public static final Integer supplierLeave = 2;

        /**
         * 最新状态
         */
//        public static final Integer msgIsNew =1;
        public static final Integer msgIsNew = 0;
        /**
         * 已读状态，tb_xa_bidmessage 表是这样使用的
         */
//        public static final Integer msgIsNotNew = 0;
        public static final Integer msgIsNotNew = 1;

    }

    /**
     * @Desc: 通用信息
     * @Auth: PMQ
     * @Date: 2018年4月11日 下午2:49:10
     */
    public static class CommonMessage{
    	/**
    	 * 登录失效
    	 */
    	public static final String SESSION_TIMEOUT = "当前登录状态已失效，请重新登录！";
    	
    }
    
    /**
     * 附件类型
     */
    public static class AttachmentType {
        public static final Integer bidATT = 1;
        //定标
        public static final Integer bidATT_DETERMINE = 10;

        public static final Integer contractATT = 2;

        public static final Integer inquiryATT = 3;

        public static final Integer fundATT = 4;

        public static final Integer bankCreditATT = 5;

        public static final Integer bidReplyATT = 6;

        public static final Integer paymentATT = 7;

        //销售员竞价投标是上传附件的类型
        public static final Integer bidPriceUploadFile = 11;

        /**
         * 平台付款管理
         */
        public static final Integer platformPayATT = 8;

        /**
         * 供应商合同执行
         */
        public static final Integer contractRunAtt = 9;
        /**
         * 合同终止附件。建议后以后附件类型从101以后开始，防止以前使用不规范导致重复。
         */
        public static final Integer contractTerminateAtt = 101;

        /**
         * 联盟供应商附件(图片)
         */
        public static final Integer allianceATT = 20;


        /**
         * 现场负责人验收上传图片
         */
        public static final Integer contractRunCheckATT = 30;
        
        
        /**
         * 订单验收上传(验收文件)
         */
        public static final Integer orderListATT = 50;
        
        /**
         * 订单验收上传(现场验收文件)
         */
        public static final Integer orderListCheckATT = 60;
    }

    /**
     * 用户操作 类型
     */
    public static class UserAction {
        /**
         * 保存 不发布
         */
        public static final Integer publishNo = 1;
        /**
         * 保存 发布
         */
        public static final Integer publishYes = 2;

        /**
         * 审核 通过
         */
        public static final Integer passYes = 1;

        /**
         * 审核 驳回
         */
        public static final Integer reject = 0;
    }

    public static class ContractType {
        /**
         * 招标合同
         */
        public static final Integer bidContract = 1;
        /**
         * 正式合同
         */
        public static final Integer formalContract = 2;
        /**
         * 询价合同
         */
        public static final Integer inquiryContract = 3;
    }

    /**
     * 用户类型
     *
     * @author lex
     */
    public static class AccountType {
        /**
         * 平台
         */
        public static final Integer platform = 0;

        /**
         * 采购商
         */
        public static final Integer purchaser = 1;
        /**
         * 供应商
         */
        public static final Integer supplier = 2;

        /**
         * 银行
         */
        public static final Integer bank = 3;


    }


    /**
     * 注册用户当前所在的步骤
     *
     * @author lex
     */
    public static class AccountRegistStep {
        /**
         * 第一步企业注册
         */
        public static final Integer step1_regist = 1;
        /**
         * 第二步提交基本信息
         */
        public static final Integer step2_commitBaseInfo = 2;
        /**
         * 第三步上传企业证书
         */
        public static final Integer step3_uploadBusinessCertification = 3;
        /**
         * 第四步填写银行帐号
         */
        public static final Integer step4_enterBankInfo = 4;
        /**
         * 第五步填写到帐金额
         */
        public static final Integer step5_enterMoney = 5;
        /**
         * 第六步完成企业认证
         */
        public static final Integer step6_completeCompanyAuthroized = 6;
        /**
         * 第七步后台认证失败
         */
        public static final Integer step7_authorized_failed = 7;
        /**
         * 第七步后台认证成功。
         */
        public static final Integer step8_pass = 8;

    }

    /**
     * 企业用户注册状态
     */
    public static class AccountStatus {

        /**
         * 已完成：账号完成注册，账号可正常操作
         */
        public static final String normal = "已完成";

        /**
         * 完善资料 :用户正在完善资料 账号不可操作
         */
        public static final String completing = "完善资料";

        /**
         * 已注册等待审核：完成注册未审核通过，账号不可操作。
         */
        public static final String checking_online = "已注册等待审核";

        /**
         * 在线审核完成：平台管理中心完成编号审核，账号可以正常操作
         */
        public static final String is_checked_online = "在线审核完成";

        /**
         * 资料备案：平台客户在规格时间内审核纸质资料，账号可以正常操作
         */
        public static final String checking_offline = "资料备案";
        /**
         * 已停用：账号停用，用户不可登录
         */
        public static final String is_locked = "已停用";
        /**
         * 已删除
         */
        public static final String is_deleted = "已删除";
    }

    /**
     * 招标申请 议标流程状态
     */
    public static class BidStatus {
        /**
         * 未发布
         */
        public static final String publishNo = "a";
        /**
         * 正在发布
         */
        public static final String publishYes = "b";
        /**
         * 已入围议标
         */
        public static final String inShortList = "c";
        /**
         * 竞价
         */
        public static final String bidPrice = "d";
        /**
         * 中标
         */
        public static final String bidSuccess = "e";
        /**
         * 流标
         */
        public static final String bidFailed = "f";


        /**
         * 已截标 只用于app端的查询招标接口   接口地址为：/m/bidList/findBidListEQConditionPage
         */
        public static final String hasBeen = "g";
    }
    
    
    /**
     * 招标申请 议标流程状态
     */
    public static class BidActiviCheckStatus {
    	/**
    	 * 未发布
    	 */
    	public static final String publishNo = "未发布";
    	/**
    	 * 正在发布
    	 */
    	public static final String publishYes = "招标中";
    	/**
    	 * 已入围议标
    	 */
    	public static final String inShortList = "已入围议标";
    	/**
    	 * 竞价
    	 */
    	public static final String bidPrice = "竞价";
    	/**
    	 * 中标
    	 */
    	public static final String bidSuccess = "中标";
    	/**
    	 * 流标
    	 */
    	public static final String bidFailed = "流标";
    	
    	
    	/**
    	 * 已截标 只用于app端的查询招标接口   接口地址为：/m/bidList/findBidListEQConditionPage
    	 * 
    	 */
    	public static final String hasBeen = "已截标";
    }

    /**
     * 新的招标状态，对原来的进行了细分，没有在数据库中存储信息，只是前台作为新的参数<br>
     * 需要截标时间，应标数量 这两个联合判断。<br>
     * 不应该被存入数据库！！！
     */
    public static class BidStatusNew {
        /**
         * 招标中
         */
        public static final String bidPublish = "bidPublish";
        /**
         * 已截标
         */
        public static final String bidTimeOut = "bidTimeOut";
        /**
         * 已入围
         */
        public static final String bidCandidate = "bidCandidate";
        /**
         * 竞价中
         */
        public static final String bidCompetition = "bidCompetition";
        /**
         * 已中标
         */
        public static final String bidSuccess = "bidSuccess";
        /**
         * 已流标
         */
        public static final String bidFailed = "bidFailed";
    }

    /**
     * 询价状态，没有爱数据库中储存信息，只是前台判断询价状态的参数，需要通过截止日期进行判断
     */
    public static class InquiryStatusNew {

        /**
         * 询价中
         */
        public static final String inquiryPublish = "inquiryPublish";
        /**
         * 已截止
         */
        public static final String inquiryTimeOut = "inquiryTimeOut";
    }

    public static class BidResult {

        /**
         * 入围
         */
        public static final Integer a0 = 0;
        /**
         * /**
         * 中标
         */
        public static final Integer a1 = 1;
        /**
         * 流标
         */
        public static final Integer a2 = 2;
        /**
         * 竞价
         */
        public static final Integer a3 = 3;

        /**
         * 直接定标流程
         */
        public static final Integer a4 = 4;

    }

    /**
     * 招标申请 审核状态
     */
    public static class BidCheckStatus {

        /**
         * 0 采购员提交申请  采购员提交申请
         */
//        public static final String a0 = "采购员提交采购申请";

        /**
         * 采购员发布招标文件到网站
         */
        public static final String a1 = "采购员发布招标文件到网站";

        /**
         * 0 采购员提交入围申请
         */
//        public static final String a2 = "采购员提交入围申请";

        /**
         * 未提交
         */
        public static final String a3 = "未提交";

        /**
         * 采购申请驳回
         */
        public static final String a5 = "采购申请驳回";
        /**
         * 1、	预算审核：预算员审核采购申请
         */
        public static final String budget_checking = "预算审核";
        /**
         * 2、	财务审核：财务审核采购申请，调整自有资金、银行资金数额
         */
        public static final String finance_checking = "财务审核";
        /**
         * 3、	项目经理审核：项目经理审核采购申请
         */
        public static final String pm_checking = "项目经理审核";
        /**
         * 4、	驳回采购审核：采购审核过程中驳回申请采购员重新修改，须填写理由
         */
        public static final String purchase_reject = "驳回采购审核";
        /**
         * 5、	申请中止采购：采购申请审核过程中取消采购申请
         */
        public static final String purchase_cancel = "申请中止采购";
        /**
         * 6、	招标中：采购申请审核通过，正在招标中  采购员
         */
        public static final String bidding = "招标中";

        /**
         * 7、	入围审核中：采购员提交入围名单
         */
//        public static final String shortlist_checking = "入围审核中";
        public static final String shortlist_checking = "项目经理审核";

        /**
         * 8、	入围驳回：项目经理驳回入围申请
         */

        public static final String shortlist_reject = "入围驳回";
        
        public static final String DECISION_REJECT = "定标驳回";

        /**
         * 9、	入围谈判：项目经理审核后采购员谈判
         */
        public static final String shortlist_negotiate = "入围谈判";

        /**
         * 采购员提交中标 流标申请后，需项目经理审核
         */
        public static final String a4 = "待审核";
        /**
         * 10、	竞价谈判：采购员选择进入竞价谈判流程
         */
        public static final String price_negotiate = "竞价谈判";
        /**
         * 再次竞价阶段 此阶段不能查看 定标
         */
        public static final String price_again = "再次报价";

        /**
         * 11、	初始合同：采购员选择一家生成初始合同
         */
        public static final String initial_contract = "初始合同";

        /**
         * 12、	流标：放弃本次采购申请
         */
        public static final String bid_failed = "流标";

        /**
         * 13、	中标
         */
        public static final String bid_success = "中标";
        
        /****************待办中 待办内容 begin**************/
        public static final String biding_todo = "招标截止待处理";
        
        public static final String inshortlist_todo = "已入围待处理";
        public static final String biding_end = "已截标";
        public static final String bidprice_bidding = "待发起定标";
        
        
        /****************待办中 待办内容 end**************/
        
    }

    public static class BidActionType {

        /**
         * 查看
         */
        public static final Integer view = 1;
        /**
         * 审核
         */
        public static final Integer check = 2;
        /**
         * 编辑 删除 其他
         */
        public static final Integer edit = 3;
        /**
         * 没有按钮
         */
        public static final Integer noButton = 4;

        /**
         * 待投标
         */
        public static final Integer bidReplyNo = 5;
        /**
         * 流标
         */
        public static final Integer cancel = 6;
    }

    /**
     * 投标 流程状态
     */
    public static class BidReplyStatus {
        /**
         * 待投标
         */
        public static final String bidNo = "a";
//        public static final String bidNo = "待投标";
        /**
         * 已投标
         */
        public static final String bidYes = "b";
//        public static final String bidYes = "已投标";
        /**
         * 已入围议标
         */
        public static final String inShortList = "c";
//        public static final String inShortList = "已入围议标";
        /**
         * 竞价
         */
        public static final String bidPrice = "d";
//        public static final String bidPrice = "竞价";
        /**
         * 中标
         */
        public static final String bidSuccess = "e";
//        public static final String bidSuccess = "中标";
        /**
         * 未中标
         */
        public static final String bidFailed = "f";
//        public static final String bidFailed = "未中标";
    }


    /**
     * 投标申请 审核状态
     */
    public static class BidReplyCheckStatus {
        public static final String None = "";
        public static final String bidNo = "未投标";
        public static final String salesSubmit = "提交报价";
        public static final String rejectYes = "驳回";
        public static final String RSMCheck = "区域销售经理审核";
        public static final String SDCheck = "销售总监审核";
        public static final String checkYes = "审核通过";
    }

    /**
     * 上报资金计划 流程状态
     */
    public static class FundingStatus {

        /**
         * 资金上报计划同级
         */
        public static final String fundApply_1 = "a1";
        /**
         * 资金上报计划下级
         */
        public static final String fundApply_2 = "a2";
        /**
         * 资金批复计划
         */
        public static final String fundConfirm = "b1";
        /**
         * 资金使用情况同级
         */
        public static final String fundUse_1 = "c1";
        /**
         * 资金使用情况下级
         */
        public static final String fundUse_2 = "c2";

        /**
         * 银行待批复资金计划
         */
        public static final String fundBankNo = "d1";

        /**
         * 银行已批复资金计划
         */
        public static final String fundBankYes = "d2";

    }

    /**
     * 上报资金计划 审核状态
     */
    public static class FundingCheckStatus {

        public static final String P_3_PM_check = "项目经理审核";

        public static final String P_3_FA_check = "财务提交计划";

        public static final String P_2_BX_check = "资金分调度审核";

        public static final String P_2_AX_check = "分指令审核";

        public static final String P_1_B_check = "资金总调度审核";

        public static final String P_1_A_check = "总指令审核";

        public static final String bank_check = "银行审核";

        public static final String submitNo = "待提交";

//        public static final String checkNo = "待审核";

//        public static final String rejectYes = "已驳回";

        public static final String passYes = "已通过";

        public static final String reject = "驳回";

    }

    /**
     * 授信申请 状态
     */
    public static class BankCreditStatus {
        /**
         * 采购方 授信申请中
         */
        public static final String applying = "a1";
        /**
         * 采购商 已批复授信
         */
        public static final String passYes = "a2";


        /**
         * 银行  授信申请中
         */
        public static final String applyingBank = "b1";

        /**
         * 银行 已批复授信
         */
        public static final String passYesBank = "b2";
    }

    /**
     * 授信 审核状态
     */
    public static class BankCreditCheckStatus {

        public static final String P_1_B_check = "资金总调度审核";

        public static final String P_1_A_check = "总指令审核";

        public static final String bank_check = "银行审核";

        public static final String submitNo = "待提交";

        public static final String passYes = "已通过";

        public static final String reject = "已驳回";
    }

    /**
     * 合同 审核状态
     */
    public static class ContractCheckStatus {

        /**
         * 1、	初始合同生成：采购生成初始合同
         */
        public static final String initial_contract = "采购员提交合同";

        /**
         * 2、	项目经理审核：审核初始合同
         */
        public static final String PM_check = "项目经理审核";

        /**
         * 3、	销售员审核：销售员审核
         */
        public static final String sales_check = "销售员审核";

        /**
         * 4、	上级领导会审：上级领导会审
         */
        public static final String assignment = "会审";

        /**
         * 5、	驳回合同：任何一个审核流程驳回初始合同，采购员调整初始合同再审核 (不包括银行驳回)
         */
        public static final String contract_return = "驳回合同";

        /**
         * 6、	资金总调度审核：资金调总度审核合同，划分自有资金、银行资金
         */
        public static final String P_1_B_check = "资金总调度审核";

        /**
         * 7、	总指令审核：总指令审核合同
         */
        public static final String P_1_A_check = "总指令审核";

        /**
         * 8、	银行备案：银行已经备案合同（只有供应商和采购商总指令和销售总监都确认后才向银行申报备案审核）
         */
        public static final String bank_backup = "银行备案";

        /**
         * 9、	银行审核：银行已经审核合同
         */
        public static final String bank_check = "银行审核";

        /**
         * 10、	银行驳回：银行驳回合同审核，到总资金调试调整比例
         */
        public static final String bank_return = "银行驳回";

        /**
         * 11、	签订合同：等待双方签订合同
         */
        public static final String sign_contract = "签订合同";

        /**
         * 12、	合同成立：全部自有资金合同或银行备案、审核合同后双方签订合同
         */
        public static final String contract_establish = "合同成立";

        /**
         * 13、	签订合同：等待采购员签订
         */
        public static final String sign_buyer_contract = "等待采购商签订";

        /**
         * 14、	签订合同：等待销售员签订
         */
        public static final String sign_sales_contract = "等待供应商签订";

    }


    /**
     * 合同 签订状态
     */
    public static class ContractSignStatus {
        /**
         * 0、	签署合同未开始
         */
        public static final int sing_status_zero = 0;

        /**
         * 1、	等待采购员签订
         */
        public static final int sing_status_one = 1;

        /**
         * 1、	等待供应商签订
         */
        public static final int sing_status_two = 2;

        /**
         * 1、	签订合同完成
         */
        public static final int sing_status_three = 3;


    }

    /**
     * 合同 确认完成状态
     */
    public static class ContractConfirmFinishStatus {
        /**
         * 0.合同 确认未开始
         */
        public static final int CONFIRM_FINISH_INIT = 0;

        /**
         * 1.等待采购方确认
         */
        public static final int CONFIRM_FINISH_PURCHASER = 1;

        /**
         * 2.等待供应方确认
         */
        public static final int CONFIRM_FINISH_SUPPLIER = 2;

        /**
         * 3.确认完成
         */
        public static final int CONFIRM_FINISH_END = 3;
    }

    /**
     * @Desc: 合同体量类别
     * @Auth: PMQ
     * @Date: 2017年11月18日 下午2:38:26
     */
    public static class ContractBodyType {
        /**
         * 小额合同
         */
        public static final String SMALL_CONTRACT = "small";
        /**
         * 常规合同
         */
        public static final String NORMAL_CONTRACT = "";
    }

    /**
     * 任务验收状态
     */
    public static class Mission_Confirm_Status {

        /**
         * 1、	现场负责人审核：等待现场负责人审核
         */
        public static final String P_SA_check = "现场负责人审核";

        /**
         * 2、	销项驳回：驳回验收申请重新填写
         */
        public static final String mission_return = "销项驳回";

        /**
         * 2.5、销售员重新发起验收
         */
        public static final String mission_again_check = "销售员重新发起验收";

        /**
         * 2.6、验收完成
         */
        public static final String mission_complete = "验收完成";

        /**
         * 3、	付款计划
         */
        public static final String PM_payplan = "项目经理制定付款计划";

        /**
         * 3.5 财务指定付款计划
         */
        public static final String FIN_PAYPLAN = "财务制定付款计划";

        /**
         * 项目经理确认付款计划
         */
        public static final String PM_CHECK_PAYPLAN = "项目经理审核付款计划";

        /**
         * 4、	销售审核:销售员审核付款
         */
        public static final String sales_check_payplan = "销售确定付款计划";

        /**
         * 5、	付款驳回：驳回调整后付款计划项目经理重新调整
         */
        public static final String payment_return = "付款驳回";

        /**
         * 6、	等待支付：未到支付时间前10天等待支付时间到达
         */
        public static final String wait_payment = "等待支付";

        /**
         * 7、	支付中：短信提醒总资金调度支付，平台确认支付
         */
        public static final String payment_ing = "支付中";

        /**
         * 8、	支付违约：未按期支付资金，平台追索
         */
        public static final String break_payment = "支付违约";

        /**
         * 9、	支付完成：完成支付
         */
        public static final String payment_complete = "支付完成";

        /**
         * 10、	争议：进入争议流程
         */
        public static final String dispute = "争议";


        /**
         * 11、新流程引擎 付款管理命名
         */
        public static final String payplan = "制定付款计划";
        
        /**
         * 12、付款计划变更
         */
        public static final String payment_change = "付款计划变更";


    }

    /**
     * 付款状态
     */
    public static class PaymentStatus {

        /**
         * 1、	等待付款：正在等待付款时间。
         */
        public static final Integer payment_wait = 1;

        /**
         * 2、	通知付款：付款前一天已发送短信通知。
         */
        public static final Integer payment_notice = 2;

        /**
         * 3、	已付款：采购商已经付款至平台，等等7天后平台打至供应商账户中。
         */
        public static final Integer payment_success = 3;

        /**
         * 4、	已到账：款项已如数打入供应商账户中。
         */
        public static final Integer account_success = 3;

        /**
         * 5、	超期未付款：超过付款期限未付款，平台人工处理。
         */
        public static final Integer payment_expired = 4;

    }

    /**
     * 用户级别
     *
     * @author lex
     */
    public static class TreeLevel {
        /**
         * 总公司
         */
        public static final Integer head = 1;
        /**
         * 地区总部
         */
        public static final Integer regional = 2;
        /**
         * 项目组，采购单元
         */
        public static final Integer project = 3;

    }

    /**
     * 树型结构每一级的节点类型
     *
     * @author lex
     */
    public static class TreeType {
        /**
         * 公司级别
         */
        public static final Integer company = 1;
        /**
         * 职位级别
         */
        public static final Integer position = 2;
        /**
         * 用户级别
         */
        public static final Integer user = 3;

    }

    public static class UserRole {
        /**
         * 采购商 超级管理员
         */
        public static final String P_ADMIN = "采购商/超级管理员";

        /**
         * 总指令
         */
        public static final String P_1_A = "采购商/总指令";

        /**
         * 资金总调度
         */
        public static final String P_1_B = "采购商/资金总调度";

        /**
         * 总巡视员
         */
        public static final String P_1_C = "采购商/总巡视员";

        /**
         * 分指令
         */
        public static final String P_2_AX = "采购商/分指令";

        /**
         * 资金分调度
         */
        public static final String P_2_BX = "采购商/资金分调度";

        /**
         * 分巡视员
         */
        public static final String P_2_CX = "采购商/分巡视员";

        /**
         * 项目经理
         */
        public static final String P_3_PM = "采购商/经理";

        /**
         * 采购
         */
        public static final String P_3_PA = "采购商/采购员";

        /**
         * 财务 一个采购单元只能有一个
         */
        public static final String P_3_FA = "采购商/财务";

        /**
         * 预算
         */
        public static final String P_3_BA = "采购商/预算员";

        /**
         * 现场负责人
         */
        public static final String P_3_SA = "采购商/现场负责人";

        /**
         * 其他
         */
        public static final String P_3_OA = "采购商/其他负责人";

        /**
         * 供应商 超级管理员
         */
        public static final String S_ADMIN = "供应商/超级管理员";

        /**
         * 供应商 销售总监
         */
        public static final String S_1_SD = "供应商/销售总监";

        /**
         * 供应商 财务总监
         */
        public static final String S_1_FD = "供应商/财务总监";

        /**
         * 供应商 区域销售经理
         */
        public static final String S_2_RSM = "供应商/区域销售经理";

        /**
         * 供应商 财务
         */
        public static final String S_2_FA = "供应商/区域财务经理";

        /**
         * 供应商 销售员
         */
        public static final String S_3_SALES = "供应商/销售员";

        /**
         * 筑材网 管理员
         */
        public static final String Z_ADMIN = "筑材网/超级管理员";

        /**
         * 筑材网 结算中心
         */
        public static final String Z_SC = "筑材网/结算中心";

        /**
         * 筑材网 招标管理中心
         */
        public static final String Z_BMC = "筑材网/招标管理中心";

        /**
         * 筑材网 任务管理中心
         */
        public static final String Z_JMC = "筑材网/任务管理中心";

        /**
         * 银行/操作人员
         */
        public static final String B_ADMIN = "银行/操作人员";

        /**
         * 采购商基本角色前缀
         */
        public static final String PURCHASER_PREFIX = "采购商/";

        /**
         * 供应商基本角色前缀
         */
        public static final String SUPPLIER_PREFIX = "供应商/";
        
    }

    /**
     * 每种角色可以添加用户的个数
     *
     * @author lex
     */
    public static class UserRoleCount {
        /**
         * 采购商 超级管理员
         */
        public static final Integer P_ADMIN = 1;

        /**
         * 总指令
         */
        public static final Integer P_1_A = 1;

        /**
         * 资金总调度
         */
        public static final Integer P_1_B = 1;

        /**
         * 总巡视员，可以有多个
         */
        public static final Integer P_1_C = 50;

        /**
         * 分指令
         */
        public static final Integer P_2_AX = 200;

        /**
         * 资金分调度
         */
        public static final Integer P_2_BX = 200;

        /**
         * 分巡视员，可以有多个
         */
        public static final Integer P_2_CX = 200;

        /**
         * 项目经理
         */
        public static final Integer P_3_PM = 1;

        /**
         * 采购
         */
        public static final Integer P_3_PA = 500;

        /**
         * 财务 一个采购单元只能有一个
         */
        public static final Integer P_3_FA = 1;

        /**
         * 预算,可以有多个
         */
        public static final Integer P_3_BA = 100;

        /**
         * 现场负责人
         */
        public static final Integer P_3_SA = 100;

        /**
         * 其他
         */
        public static final Integer P_3_OA = 100;

        /**
         * 供应商 超级管理员
         */
        public static final Integer S_ADMIN = 1;

        /**
         * 供应商 销售总监
         */
        public static final Integer S_1_SD = 1;

        /**
         * 供应商 财务总监
         */
        public static final Integer S_1_FD = 1;

        /**
         * 供应商 区域销售经理
         */
        public static final Integer S_2_RSM = 1;

        /**
         * 供应商 财务
         */
        public static final Integer S_2_FA = 1;

        /**
         * 供应商 销售员
         */
        public static final Integer S_3_SALES = 100;


    }

    /**
     * 用户类型， 采购商，供应商，筑材网
     */
    public static class UserType {
        /**
         * 采购商
         */
        public static final String purchaser = "采购商";

        /**
         * 供应商
         */
        public static final String supplier = "供应商";

        /**
         * 筑材网
         */
        public static final String web_admin = "筑材网";


    }

    public static class ActivitiGatewayStatus {

        public static final Integer pass = 1;

        public static final Integer passNot = 0;

        public static final Integer a0 = 0;

        public static final Integer a1 = 1;

        public static final Integer a2 = 2;

        public static final Integer a3 = 3;

        public static final Integer a4 = 4;

        public static final Integer a100 = 100;

        public static final Integer a101 = 101;
    }

    public static class ActivitiConstant {

        /**
         * 招标申请流程
         */
        public static final String purchaseApply = "purchaseApply";

        /**
         * 议标流程
         */
        public static final String bidDiscuss = "bidDiscuss";

        /**
         * 投标申请流程
         */
        public static final String bidConfirm = "bidConfirm";

        /**
         * 独立的定标申请流程
         */
        public static final String bidDecide = "bidDecide";

        /**
         * 上报资金计划流程
         */
        public static final String funding = "funding";

        public static final String fundingEdit = "fundingEdit";
        /**
         * 申请银行授信
         */
        public static final String bankCredit = "bankCredit";
    }

    /**
     * 合同处理状态
     * <p>
     * 作用于ContractList中的status字段，表示合同的状态<br>
     * 用途和BaseEntity中的status冲突！！！
     *
     * @author zc
     */
    public static class ContractListStatus {

        /**
         * 1、无效合同
         */
        public static final int INVALID = -1;

        /**
         * 1、已取消的合同/已变更的合同
         */
        public static final int DELETE = 0;

        /**
         * 2、新建合同 未生效
         */
        public static final int CREATE = 1;


        /**
         * 3、取消中的合同
         */
        public static final int IN_DELETE = 2;

        /**
         * 4、变更中的合同
         */
        public static final int IN_EDIT = 3;

        /**
         * 5、变更代替的合同
         */
        public static final int EDIT_REPLACE = 4;

        /**
         * 6、已生效合同
         */
        public static final int APPROVED = 5;

        /**
         * 7、执行中的合同
         */
        public static final int RUN = 6;

        /**
         * 8、待付款的合同
         */
        public static final int PAY_IN = 7;

        /**
         * 合同确认完成
         */
        public static final int CONFIRM_COMPLETED = 8;

        /**
         * 9、已完成的合同
         */
        public static final int COMPLETED = 9;

        /**
         * 10、争议中的合同
         */
        public static final int DISPUTE = 10;

        /**
         * 11、终止中的合同
         */
        public static final int IN_STOP = 11;

        /**
         * 12、已终止的合同
         */
        public static final int STOPPED = 12;

    }

    /**
     * 合同状态的文字表示(2017/02/20 追加 : 未追加完毕或有误的后续相继修正补充追加)
     *
     * @author pc
     */
    public static class ContractListStatusText {
        /**
         * 无效合同
         */
        public static final String STATUS_TEXT_01 = "无效合同";
        /**
         * 已变更合同
         */
        public static final String STATUS_TEXT_02 = "已变更合同";
        /**
         * 已取消合同
         */
        public static final String STATUS_TEXT_03 = "已取消合同";
        /**
         * 会审
         */
        public static final String STATUS_TEXT_04 = "会审";
        /**
         * 销售员审核
         */
        public static final String STATUS_TEXT_05 = "销售员审核";
        /**
         * 总指令审核
         */
        public static final String STATUS_TEXT_06 = "总指令审核";
        /**
         * 签订合同
         */
        public static final String STATUS_TEXT_07 = "签订合同";
        /**
         * 资金总调度审核
         */
        public static final String STATUS_TEXT_08 = "资金总调度审核";
        /**
         * 采购员提交合同
         */
        public static final String STATUS_TEXT_09 = "采购员提交合同";
        /**
         * 合同成立
         */
        public static final String STATUS_TEXT_10 = "合同成立";
        /**
         * 合同执行
         */
        public static final String STATUS_TEXT_11 = "合同执行";
        /**
         * 合同验收
         */
        public static final String STATUS_TEXT_12 = "合同验收";
        /**
         * 确认合同完成
         */
        public static final String STATUS_TEXT_13 = "确认合同完成";
        /**
         * 合同已完成
         */
        public static final String STATUS_TEXT_14 = "合同已完成";
        /**
         * 合同已终止完成
         */
        public static final String STATUS_TEXT_15 = "合同已终止";
        /**
         * 项目经理审核
         */
        public static final String STATUS_TEXT_16 = "项目经理审核";
        /**
         * 销售总监审核
         */
        public static final String STATUS_TEXT_17 = "销售总监审核";

    }

    public static class MissionListStatus {

        /**
         * 1、已取消的任务
         */
        public static final Integer DELETE = 0;

        /**
         * 2、新建/未验收
         */
        public static final Integer CREATE = 1;


        /**
         * 3、待验收的任务
         */
        public static final Integer TO_CHECK = 2;

        /**
         * 4、已验收/待付款的任务
         */
        public static final Integer CHECKED = 3;

        /**
         * 5、付款中的任务
         */
        public static final Integer IN_PAY = 4;

        /**
         * 6、已付款待评价
         */
        public static final Integer PAYED = 5;

        /**
         * 7、已完成的任务
         */
        public static final Integer COMPLETED = 6;

        /**
         * 8、争议中的任务
         */
        public static final Integer IN_DISPUTE = 9;

        /**
         * 10、付款计划变更
         */
        public static final Integer PAYMENT_CHANGE = 10;
    }

    public static class MissionListStatusStr {

        /**
         * 1、已取消的任务
         */
        public static final String DELETE = "已取消的任务";

        /**
         * 2、新建/未验收
         */
        public static final String CREATE = "新建任务";


        /**
         * 3、待验收的任务
         */
        public static final String TO_CHECK = "待验收的任务";

        /**
         * 4、已验收/待付款的任务
         */
        public static final String CHECKED = "已验收/待付款的任务";

        /**
         * 5、付款中的任务
         */
        public static final String IN_PAY = "付款中的任务";

        /**
         * 6、已付款待评价
         */
        public static final String PAYED = "已付款待评价的任务";

        /**
         * 7、已完成的任务
         */
        public static final String COMPLETED = "已完成的任务";

        /**
         * 8、争议中的任务
         */
        public static final String IN_DISPUTE = "争议中的任务";


    }


    /**
     * 图片类型
     *
     * @author lex
     */
    public static class pictureType {

        /**
         * 营业执照图片
         */
        public static final Integer businessLicense = 1;
        /**
         * 平台承诺书
         */
        public static final Integer plantformCommitment = 2;
        /**
         * 企业资质
         */
        public static final Integer companyAptitude = 3;
        /**
         * 税务登记图片
         */
        public static final Integer companyTaxRegistration = 4;
        /**
         * 组织机构图片
         */
        public static final Integer organization = 5;
        /**
         * 履约保函图片
         */
        public static final Integer impleteBaohan = 6;
        /**
         * 管理员任命书
         */
        public static final Integer adminProxy = 7;
        /**
         * 银行协议
         */
        public static final Integer bankProtocols = 8;
        /**
         * 平台合同
         */
        public static final Integer plantformContract = 9;
        /**
         * 上年审计结果
         */
        public static final Integer lastYearAudit = 10;
        /**
         * 上首页宣传图
         */
        public static final Integer HomePublicityMap = 11;
        /**
         * 供应商产品默认
         */
        public static final Integer supplierProductDefault = 12;
        /**
         * 供应商产品全部
         */
        public static final Integer supplierProduct = 13;
        /**
         * 荣誉图片
         */
        public static final Integer honor = 14;
        /**
         * 企业相册
         */
        public static final Integer supplierPicutreBox = 15;

        /**
         * 新闻图片
         */
        public static final Integer news = 16;

        /**
         * 手机评证图片
         */
        public static final Integer phone = 17;

        /**
         * 三证合一
         */
        public static final Integer unifyCert = 18;
        /**
         * 企业名称变更证明
         */
        public static final Integer enterpriseNameChangeImg = 19;
        /**
         * 金融授权委托书
         */
        public static final Integer financeCert = 20;
        /**
         * 金融发票附件
         */
        public static final Integer financeInvoice = 21;
        /**
         * 银行卡认证的图片
         */
        public static final Integer bankAuthPic = 22;

    }

    /**
     * 会员属性类型
     *
     * @author dzlia
     */
    public static class personAttributeType {

        /**
         * 企业资质
         */
        public static final Integer AttrcompanyAptitude = 1;
        /**
         * 企业荣誉
         */
        public static final Integer AttrcompanyHonor = 2;
        /**
         * 企业相册
         */
        public static final Integer AttrcompanyPhoto = 3;

    }


    /**
     * 文件类型
     *
     * @author lex
     */
    public static class fileType {
        /**
         * 平台合同
         */
        public static final Integer pdf = 1;
        /**
         * 上年审计结果
         */
        public static final Integer picture = 2;
    }

    /**
     * 筑材网平台下载文件
     *
     * @author pc
     */
    public static class PlatformFileDownLoad {
        /**
         * 平台合同文件名称
         */
//    	public static final String VALUE_CONTRACT = "筑材网平台合作合同.docx";
        /**
         * 管理员任命文件名称
         */
//    	public static final String VALUE_ADMIN = "采购商供应商申请表（超级管理员任命书）.doc";
        /**
         * 平台承若书文件名称
         */
        public static final String VALUE_PROTOCOL = "筑材网用户服务协议.docx";
//    	/**
//    	 * 平台合同文件标识符
//    	 */
//    	public static final String KEY_CONTRACT = "contractFile";

        //采购商管理员任命书文件标识符
        public static final String KEY_ADMIN_PURCHASER = "adminFile_purchaser";

        //采购商平台合作合同文件标识符
        public static final String KEY_CONTRACT_PURCHASER = "contractFile_purchaser";

        //供应商管理员任命书文件标识符
        public static final String KEY_ADMIN_SUPPLIER = "adminFile_supplier";

        //供应商平台合作合同文件标识符
        public static final String KEY_CONTRACT_SUPPLIER = "contractFile_supplier";

        public static final String VALUE_ADMIN_PURCHASER = "采购商申请表（超级管理员任命书）.doc";

        public static final String VALUE_ADMIN_SUPPLIER = "供应商申请表（超级管理员任命书）.doc";

        public static final String VALUE_CONTRACT_PURCHASER = "筑材网平台合作合同-（采购商）.docx";

        public static final String VALUE_CONTRACT_SUPPLIER = "筑材网平台合作合同-（供应商）.docx";


//    	/**
//    	 * 管理员任命文件标识符
//    	 */
//    	public static final String KEY_ADMIN = "adminFile";
        /**
         * 平台承若书文件标识符
         */
        public static final String KEY_PROTOCOL = "protocolFile";
        /**
         * 文件在服务器的相对路径
         */
        public static final String FILE_PATH = "upload/platformFiles";
    }

    /**
     * 银行卡审核状态
     *
     * @author pc
     */
    public static class PersonBankCardCheckStatus {
        /**
         * 待认证
         */
        public static final int CHECKING_STEP_ONE = 0;
        /**
         * 前台：认证中，后台：待审核
         */
        public static final int CHECKING_STEP_TWO = 1;
        /**
         * 认证失败
         */
        public static final int CHECKING_STEP_THREE = 2;
        /**
         * 已认证
         */
        public static final int CHECKING_STEP_FOUR = 3;
        /**
         * 已激活
         */
        public static final int CHECKING_STEP_FIVE = 4;

    }

    /**
     * 验证码保存名称
     *
     * @author pc
     */
    public static class PictureValidateCodeSaveName {
        /**
         * 找回密码验证码名称<br>
         * 由于使用验证码的地方较少，使用同一个session中的验证码信息。<br>
         * 已经使用的地方有：1.登陆密码错误三次之后出现的验证码;2.账号注册的验证码;3.找回密码的验证码
         */
        public static final String FINDPASS_CODE_NAME = "findpass1_save_name";
    }

    /**
     * 实际付款状态
     *
     * @author pc
     */
    public static class PaymentPlanStatus {
        /**
         * 未付款
         */
        public static final int NOT_PAYMENT_STATUS = 1;
        /**
         * 审核中(待审核)
         */
        public static final int CHECKING_PAYMENT_STATUS = 2;
        /**
         * 付款到平台
         */
        public static final int FINISHED_PAYMENT_STATUS = 3;
        /**
         * 审核不通过
         */
        public static final int CHECKED_FAIL_PAYMENT_STATUS = 4;
        /**
         * 已付款(付款到供应商)
         */
        public static final int END_PAYMENT_STATUS = 5;
        /**
         * 审核中(待采购方审核)
         */
        public static final int PURCHER_CHECKING_PAYMENT_STATUS = 6;
    }
    /**
     * 实际付款状态
     *
     * @author pc
     */
    public static class PaymentPlanStatusStr {
    	/**
    	 * 未付款
    	 */
    	public static final String NOT_PAYMENT_STATUS = "未付款";
    	/**
    	 * 审核中(待审核)
    	 */
    	public static final String CHECKING_PAYMENT_STATUS = "审核中(待审核)";
    	/**
    	 * 付款到平台
    	 */
    	public static final String FINISHED_PAYMENT_STATUS = "付款到平台";
    	/**
    	 * 审核不通过
    	 */
    	public static final String CHECKED_FAIL_PAYMENT_STATUS ="审核不通过";
    	/**
    	 * 已付款(付款到供应商)
    	 */
    	public static final String END_PAYMENT_STATUS = "已付款(付款到供应商)";
    	/**
    	 * 审核中(待采购方审核)
    	 */
    	public static final String PURCHER_CHECKING_PAYMENT_STATUS = "审核中(待采购方审核)";
    }

    /**
     * @Desc: 制定付款计划状态
     * @Auth: PMQ
     * @Date: 2017年11月15日 上午11:10:29
     */
    public static class MakePaymentPlanStatus {
    	
    	
        /**
         * 0.未开启
         */
        public static final int NOT_MAKE_STATUS = 0;
        /**
         * 1.启动制定
         */
        public static final int START_MAKE_STATUS = 1;
        /**
         * 2.制定完成
         */
        public static final int FINISHED_MAKE_STATUS = 2;
    }

    /**
     * 执行记录保存类型
     *
     * @author pc
     */
    public static class ExecuteRecordType {
        /**
         * 注册用户的审核记录
         */
        public static final int TYPE_ONE = 1;
        /**
         * 付款计划的付款执行和审核记录
         */
        public static final int TYPE_TWO = 2;
        /**
         * 平台付款管理的执行记录
         */
        public static final int TYPE_THREE = 3;
        /**
         * 企业开票信息审核的执行记录
         */
        public static final int TYPE_FOUR = 4;
    	/**
    	 *物流付款的执行记录
    	 */
    	public static final int TYPE_FIVE = 5;
    	/**
    	 *物流验收的执行记录
    	 */
    	public static final int TYPE_SIX = 6;
    }

    /**
     * 服务器类型
     *
     * @author pc
     */
    public static class ServerType {
        /**
         * 前台应用服务器
         */
        public static final int TYPE_ONE = 1;
        /**
         * 后台应用服务器
         */
        public static final int TYPE_TWO = 2;

    }

    /**
     * 问题反馈: 处理状态
     *
     * @author pc
     */
    public static class FeedbackProblemProcessStatus {
        /**
         * 未处理
         */
        public static final int WAIT_PROCESS = 0;

        /**
         * 处理中
         */
        public static final int DOING_PROCESS = 1;

        /**
         * 不处理
         */
        public static final int CANCEL_PROCESS = 2;

        /**
         * 处理完成
         */
        public static final int COMPLETED_PROCESS = 3;

    }

    /**
     * 企业信息变更: 所处状态
     *
     * @author pc
     */
    public static class EnterpriseInfoChangeStatus {
        /**
         * 未变更
         */
        public static final int NORMAL = 0;

        /**
         * 待审核
         */
        public static final int WAIT_CHECK = 1;

        /**
         * 审核通过
         */
        public static final int CHECK_SUCCESS = 2;

        /**
         * 审核不通过
         */
        public static final int CHECK_FAIL = 3;
    }

    /**
     * 企业备案: 备案状态
     *
     * @author pc
     */
    public static class EnterpriseFilingStatus {
        /**
         * 未备案
         */
        public static final int NO_FILING = 0;

        /**
         * 已备案
         */
        public static final int COMPLETED_FILING = 1;

        /**
         * 备案失败
         */
        public static final int FAIL_FILING = 2;
    }

    /**
     * 咨询管理: 栏目
     *
     * @author pc
     */
    public static class NewsInfoColumn {
        /**
         * 小筑头条
         */
        public static final int COLUMN_ONE = 1;
        /**
         * 宏观形式
         */
        public static final int COLUMN_TWO = 2;
        /**
         * 行业动态
         */
        public static final int COLUMN_THREE = 3;
        /**
         * 脑洞大开
         */
        public static final int COLUMN_FOUR = 4;
        /**
         * 信马由缰
         */
        public static final int COLUMN_FIVE = 5;
        /**
         * 筑你成功
         */
        public static final int COLUMN_SIX = 6;
        /**
         * 人物专访
         */
        public static final int COLUMN_SEVEN = 7;
    }

    /**
     * 咨询管理: 置顶
     *
     * @author pc
     */
    public static class NewsInfoSetTop {
        /**
         * 不置顶
         */
        public static final int NO_TOP = 0;
        /**
         * 首页置顶
         */
        public static final int HOME_PAGE_TOP = 1;
        /**
         * 栏目置顶
         */
        public static final int COLUMN_TOP = 2;
    }

    /**
     * 企业银行卡开启状态
     *
     * @author pc
     */
    public static class EnterpriseBankCardStatus {
        /**
         * 未初始化
         */
        public static final int NO_INIT = 0;
        /**
         * 关闭中
         */
        public static final int OFF = 1;
        /**
         * 开启中
         */
        public static final int ON = 2;
    }

    /**
     * @Desc: 付款计划
     * @Auth: PMQ
     * @Date: 2017年11月6日 下午9:55:42
     */
    public static class PaymentPlan {
        /**
         * 付款类别：(1:委托集团付款方式)
         */
        public static final int PAYWAY_ONE = 1;
        /**
         * 付款类别：(2:商票)
         */
        public static final int PAYWAY_TWO = 2;
        /**
         * 付款类别：(3:采购单元付款)
         */
        public static final int PAYWAY_THREE = 3;
        /**
         * 付款类别：(4：采购单元计划 )
         */
        public static final int PAYWAY_FOUR = 4;

        /**
         * 付款方式：(1:直接付款)
         */
        public static final int DERECT_PAY = 1;
        /**
         * 付款方式：(2:委托支付)
         */
        public static final int DELEGATE_PAY = 2;

        /**
         * 付款渠道：(1:银行转账)
         */
        public static final int PAYCHANNEL_ONE = 1;
        /**
         * 付款渠道：(2:票据支付)
         */
        public static final int PAYCHANNEL_TWO = 2;
        /**
         * 付款渠道：(3:E点通支付)
         */
        public static final int PAYCHANNEL_THREE = 3;
        /**
         * 付款渠道：(4：线下支付)
         */
        public static final int PAYCHANNEL_FOUR = 4;
        /**
         * 付款渠道：(5：银联在线支付)
         */
        public static final int PAYCHANNEL_FIVE = 5;

    }
    /**
     * @Desc: app付款计划显示
     * @Auth: young
     */
    public static class PaymentPlanAppShow {
    	/**
    	 * 付款类别：(1:委托集团付款方式)
    	 */
    	public static final String PAYWAY_ONE = "委托集团付款方式";
    	/**
    	 * 付款类别：(2:商票)
    	 */
    	public static final String PAYWAY_TWO = "商票";
    	/**
    	 * 付款类别：(3:采购单元付款)
    	 */
    	public static final String PAYWAY_THREE = "采购单元付款";
    	/**
    	 * 付款类别：(4：采购单元计划 )
    	 */
    	public static final String PAYWAY_FOUR = "采购单元计划";
    	
    	/**
    	 * 付款方式：(1:直接付款)
    	 */
    	public static final String DERECT_PAY = "直接付款";
    	/**
    	 * 付款方式：(2:委托支付)
    	 */
    	public static final String DELEGATE_PAY = "委托支付";
    	
    	/**
    	 * 付款渠道：(1:银行转账)
    	 */
    	public static final String PAYCHANNEL_ONE = "银行转账";
    	/**
    	 * 付款渠道：(2:票据支付)
    	 */
    	public static final String PAYCHANNEL_TWO = "票据支付";
    	/**
    	 * 付款渠道：(3:E点通支付)
    	 */
    	public static final String PAYCHANNEL_THREE = "E点通支付";
    	/**
    	 * 付款渠道：(4：线下支付)
    	 */
    	public static final String PAYCHANNEL_FOUR = "线下支付";
    	/**
    	 * 付款渠道：(5：银联在线支付)
    	 */
    	public static final String PAYCHANNEL_FIVE ="银联在线支付";
    	
    }

    /**
     * 平台付款管理: 状态
     *
     * @author pc
     */
    public static class PlatformPayStatus {
        /**
         * 待付款
         */
        public static final int WAIT_PAY = 0;
        /**
         * 待确认付款（凭证上传成功）
         */
        public static final int WAIT_CONFIRM_PAY = 1;
        /**
         * 已付款
         */
        public static final int SUCCESS_PAY = 2;
        /**
         * 付款失败
         */
        public static final int FAIL_PAY = 3;
        /**
         * 已收款
         */
        public static final int SUCCESS_RECEIVE = 4;
        /**
         * 收款驳回
         */
        public static final int FAIL_RECEIVE = 5;
    }

    /**
     * 平台付款管理: 付款类型
     *
     * @author pc
     */
    public static class PlatformPayCheckMethod {
        /**
         * 单次付款
         */
        public static final int SINGLE_PAY = 0;
        /**
         * 批量付款
         */
        public static final int BATCH_PAY = 1;
    }

    /**
     * 平台付款管理: 前台任务执行状态
     *
     * @author pc
     */
    public static class PlatformPayTaskStatus {
        /**
         * 未执行
         */
        public static final int NO_RUN = 0;
        /**
         * 已执行
         */
        public static final int END_RUN = 1;
    }

    /**
     * 广告图管理: 发布状态
     *
     * @author pc
     */
    public static class BannerPublishStatus {
        /**
         * 待发布
         */
        public static final int WAIT_PUBLISH = 1;
        /**
         * 已上架
         */
        public static final int SUCCESS_PUBLISH = 2;
    }

    /**
     * 广告图管理: 属性
     *
     * @author pc
     */
    public static class BannerProperty {
        /**
         * 采购商
         */
        public static final int TYPE_ONE = 1;
        /**
         * 供应商
         */
        public static final int TYPE_TWO = 2;
        /**
         * 标书
         */
        public static final int TYPE_THREE = 3;
        /**
         * 广告
         */
        public static final int TYPE_FOUR = 4;
    }

    /**
     * App版本管理: 发布状态
     *
     * @author pc
     */
    public static class AppVersionPublishStatus {
        /**
         * 待发布
         */
        public static final int WAIT_PUBLISH = 1;
        /**
         * 已上架
         */
        public static final int SUCCESS_PUBLISH = 2;
    }

    /**
     * App版本管理: 系统
     *
     * @author pc
     */
    public static class AppVersionSystemType {
        /**
         * Android
         */
        public static final int ANDROID_SYSTEM = 1;
        /**
         * IOS
         */
        public static final int IOS_SYSTEM = 2;
    }

    /**
     * 系统公告管理: 重要程度
     *
     * @author pc
     */
    public static class SysAnnImportantDegree {
        /**
         * 一般
         */
        public static final int DEGREE_ONE = 1;
        /**
         * 重要
         */
        public static final int DEGREE_TWO = 2;
        /**
         * 非常重要
         */
        public static final int DEGREE_THREE = 3;
    }

    /**
     * 系统公告管理: 推送对象
     *
     * @author pc
     */
    public static class SysAnnPushToRole {
        /**
         * 全体人员
         */
        public static final int ALL = 1;
        /**
         * 超级管理员
         */
        public static final int SUPER_ADMIN = 2;
        /**
         * 采购商全体人员
         */
        public static final int PURCHASER_ALL = 3;
        /**
         * 供应商全体人员
         */
        public static final int SUPPLIER_ALL = 4;
        /**
         * 总指令
         */
        public static final int BIG_INSTRUCTION = 5;
        /**
         * 总调度
         */
        public static final int BIG_DISPATCH = 6;
        /**
         * 总巡视
         */
        public static final int BIG_PATROL = 7;
        /**
         * 分指令
         */
        public static final int SMALL_INSTRUCTION = 8;
        /**
         * 分调度
         */
        public static final int SMALL_DISPATCH = 9;
        /**
         * 分巡视
         */
        public static final int SMALL_PATROL = 10;
        /**
         * 项目经理
         */
        public static final int PROJECT_MANAGER = 11;
        /**
         * 采购员
         */
        public static final int PURCHASER = 12;
        /**
         * 财务员
         */
        public static final int TREASURER = 13;
        /**
         * 预算员
         */
        public static final int BUDGETEER = 14;
        /**
         * 现场负责人
         */
        public static final int SCENE_RESPONSIBLE_PERSON = 15;
        /**
         * 销售总监
         */
        public static final int SALES_DIRECTOR = 16;
        /**
         * 财务总监
         */
        public static final int FINANCE_DIRECTOR = 17;
        /**
         * 区域销售经理
         */
        public static final int REGIONAL_SALES_MANAGER = 18;
        /**
         * 区域财务经理
         */
        public static final int REGIONAL_FINANCE_MANAGER = 19;
        /**
         * 销售员
         */
        public static final int SUPPLIER = 20;
        /**
         * 采购商超管
         */
        public static final int PURCHASER_SUPER_ADMIN = 21;

        /**
         * 供应商超管
         */
        public static final int SUPPLIER_SUPER_ADMIN = 22;
    }

    /**
     * 系统公告管理: 主题分类
     *
     * @author pc
     */
    public static class SysAnnThemeType {
        /**
         * 普通公告
         */
        public static final int THEME_ONE = 1;
        /**
         * 平台运维
         */
        public static final int THEME_TWO = 2;
        /**
         * 业务提醒
         */
        public static final int THEME_THREE = 3;
        /**
         * 专项公告
         */
        public static final int THEME_FOUR = 4;
        /**
         * 特殊公告
         */
        public static final int THEME_FIVE = 5;
    }

    /**
     * 系统公告管理: 置顶
     *
     * @author pc
     */
    public static class SysAnnTopSet {
        /**
         * 不置顶
         */
        public static final boolean NO_TOP = false;
        /**
         * 置顶
         */
        public static final boolean TOP = true;
    }

    /**
     * 系统公告管理: 发布状态
     *
     * @author pc
     */
    public static class SysAnnPublishStatus {
        /**
         * 待发布
         */
        public static final int WAIT_PUBLISH = 1;
        /**
         * 已发布
         */
        public static final int SUCCESS_PUBLISH = 2;
    }

    /**
     * 浏览消息的标识记录: 所属实体类型
     *
     * @author pc
     */
    public static class BrowseMessageMarkType {
        /**
         * 系统公告管理类型的消息
         */
        public static final int TYPE_ONE = 1;


        /**
         * 不显示对应用户的所有系统公告
         */
        public static final int TYPE_TWO = 2;
        //有新的业务消息类型的话，可以依次添加
    }

    /**
     * 底部广告图管理: 基图标识
     *
     * @author pc
     */
    public static class BottomBannerbaseImageFlag {
        /**
         * 0:非基图、
         */
        public static final int NO_BASE_IMAGE = 0;
        /**
         * 1:基图片
         */
        public static final int YES_BASE_IMAGE = 1;
    }

    /**
     * 对审核记录分类。
     *
     * @author pc
     */
    public static class AuditRecordType {
        /**
         * 申请
         */
        public static final String SHEN_QING = "申请";
        /**
         * 入围
         */
        public static final String RU_WEI = "入围";
        /**
         * 竞价
         */
        public static final String JING_JIA = "竞价";
        /**
         * 定标
         */
        public static final String DING_BIAO = "定标";
        /**
         * 定标-中标
         */
        public static final String DING_BIAO_SUCCESS = "定标-中标";
        /**
         * 定标-流标
         */
        public static final String DING_BIAO_FAILED = "定标-流标";
        /**
         * 废标
         */
        public static final String WASTE_BIAO = "废标";
        /**
         * 投标
         */
        public static final String SUBMIT_BIDREPLY = "投标";
    }
    
    /**
     * 对审核记录分类。
     *
     * @author pc
     */
    public static class AuditRecordTypeForApp {
    	/**
    	 * 其他
    	 */
    	public static final Integer OTHER = 0;
    	/**
    	 * 申请
    	 */
    	public static final Integer SHEN_QING = 1;
    	/**
    	 * 入围
    	 */
    	public static final Integer RU_WEI = 2;
    	/**
    	 * 竞价
    	 */
    	public static final Integer JING_JIA = 3;
    	/**
    	 * 定标
    	 */
    	public static final Integer DING_BIAO = 12;
    	/**
    	 * 定标-中标
    	 */
    	public static final Integer DING_BIAO_SUCCESS = 4;
    	/**
    	 * 定标-流标
    	 */
    	public static final Integer DING_BIAO_FAILED = 5;
    	/**
    	 * 废标
    	 */
    	public static final Integer WASTE_BIAO = 6;
    	/**
    	 * 合同签署
    	 */
    	public static final Integer SIGN_CONTRACT = 7;
    	/**
    	 * 合同变更
    	 */
    	public static final Integer CHANGE_CONTRACT = 8;
    	/**
    	 * 合同取消
    	 */
    	public static final Integer CANCLE_CONTRACT = 9;
    	/**
    	 * 合同终止
    	 */
    	public static final Integer STOP_CONTRACT = 10;
    	/**
    	 * 投标
    	 */
    	public static final Integer SUBMIT_BIDREPLY = 11;
    
    }

    /**
     * 我的收藏管理: 收藏类别
     *
     * @author pc
     */
    public static class MyCollectionType {
        /**
         * 1:采购商
         */
        public static final int PURCHER = 1;
        /**
         * 2:供应商
         */
        public static final int SUPPLIER = 2;
        /**
         * 3:招标项目
         */
        public static final int BID = 3;
        /**
         * 4:询价项目
         */
        public static final int INQUIRY = 4;
    }

    /**
     * 企业开票管理: 审核状态
     *
     * @author pc
     */
    public static class EnterpriseBillingCheckStatus {
        /**
         * 0:未认证
         */
        public static final int NOT_AUTH = 0;
        /**
         * 1:待认证
         */
        public static final int WAIT_AUTH = 1;
        /**
         * 2:已认证
         */
        public static final int SUCCESS_AUTH = 2;
        /**
         * 3:认证失败
         */
        public static final int FAIL_AUTH = 3;
        /**
         * 4:待确认
         */
        public static final int WAIT_CONFIRM = 4;
        /**
         * 5:已确认
         */
        public static final int CONFIRM_OK = 5;
        /**
         * 5:已失效
         */
        public static final int CONFIRM_FAIL = 6;
    }

    /**
     * 企业开票管理: 总公司与子公司区分标识
     *
     * @author pc
     */
    public static class EnterpriseBillingCompanyFlag {
        /**
         * 0:总公司
         */
        public static final int HEAD_OFFICE = 0;
        /**
         * 1:子公司
         */
        public static final int SUBCOMPANY = 1;

    }

    /**
     * 招标管理 -> 采购单元审核：已审核到达过的级别标识
     *
     * @author pc
     */
    public static class BidListCheckedHistoryLevel {
        /**
         * 0:未审核标识
         */
        public static final int NO_CHECKED = 0;
        /**
         * 1:预算员审核标识
         */
        public static final int FIRST_CHECKED = 1;
        /**
         * 2:财务审核标识
         */
        public static final int SECOND_CHECKED = 2;
        /**
         * 3:项目经理审核标识
         */
        public static final int THIRD_CHECKED = 3;

    }

    /**
     * 企业信息通用管理常量
     *
     * @author pc
     */
    public static class EnterpriseInfoComConstant {
        /**
         * 企业管理
         */
        public static final String ENTERPRISE_MANAGEMENT = "企业管理";
        /**
         * 企业信息维护
         */
        public static final String ENTERPRISE_INFO_UPDATE = "企业信息维护";
        /**
         * 本账户管理
         */
        public static final String SELF_ACCOUNT_ADMIN = "本账户管理";
        /**
         * 企业信息完善
         */
        public static final String ENTERPRISE_INFO_REG = "企业信息完善";
        /**
         * 企业完善注册URL
         */
        public static final String ENTERPRISE_INFO_REG_URL = "/member/enterpriseRegInfo/";

    }

    /**
     * 筑材网招商活动常量
     *
     * @author pc
     */
    public static class ZhuCaiActivityConstant {

        /**
         * 启动开关：(1:true 开启)
         */
        public static final boolean ON = true;
        /**
         * 启动开关：(0:false 关闭)
         */
        public static final boolean OFF = false;

        /**
         * 是否为平台注册用户：(1:true 是)
         */
        public static final boolean AUTH_USER = true;
        /**
         * 是否为平台注册用户：(0:false 非)
         */
        public static final boolean NO_AUTH_USER = false;

        /**
         * 访问来源：(WEB: 0)
         */
        public static final int VISIT_MODE_WEB = 0;
        /**
         * 访问来源：(APP: 1)
         */
        public static final int VISIT_MODE_APP = 1;

    }

    /**
     * 招标类型
     *
     * @author pc
     */
    public static class BidType {

        /**
         * 1:公开
         */
        public static final int PUBLIC = 1;
        /**
         * 2:定向
         */
        public static final int SPECIAL = 2;

    }

    /**
     * 企业开票：银行信息填写方式
     *
     * @author pc
     */
    public static class EnterpriseBillingBIM {

        /**
         * 1:手动输入(默认)
         */
        public static final int MANUAL_INPUT = 1;
        /**
         * 2:从已有银行卡中选择
         */
        public static final int BANK_CARD_SELECT = 2;

    }

    /**
     * 缓存锁：key前缀
     *
     * @author pc
     */
    public static class ExclusionRedisLockKeyPrefix {

        /**
         * 招标表
         */
        public static final String KEY_BIDLIST = "lock_tb_xa_bidlist_";

        /**
         * 合同表
         */
        public static final String KEY_CONTRACTLIST = "lock_tb_xa_contract_list_";

        /**
         * 任务表
         */
        public static final String KEY_MISSIONLIST = "lock_tb_xa_mission_list_";

        /**
         * 付款表
         */
        public static final String KEY_PAYMENTPLAN = "lock_tb_xa_payment_plan_";

        /**
         * 账号通用
         */
        public static final String KEY_ACCOUT_COM = "lock_accout_com_";

        /**
         * 临时保存锁key
         */
        public static final String TEMP_LOCKKEY = "temp_lockKey";
        
        /**
         * 订单表
         */
        public static final String KEY_ORDERKEY = "lock_tb_xa_order_list_";
    }

    /**
     * 排他业务：提示信息
     *
     * @author pc
     */
    public static class ExclusionProcessTopInfo {

        /**
         * 并发操作时：排他返回提示
         */
        public static final String TOP_EXCLUSION_RETURN = "该任务已由本账号在其它地点处理，操作无法完成！";

        /**
         * 招标已经更新提交时：排他返回提示
         */
        public static final String TOP_BIDLIST_UPDATE_RETURN = "亲，本招标已经被成功提交，不能再次提交或保存了！";

        /**
         * 询价已经更新提交时：排他返回提示
         */
        public static final String TOP_INQUIRY_UPDATE_RETURN = "亲，本询价已经被成功提交，不能再次提交或保存了！";

        /**
         * 数据已经不存在时：返回提示
         */
        public static final String TOP_DATA_EMPTY_RETURN = "操作失败，该条数据已经丢失或已被其他人员操作。";

        /**
         * 不能报价时：返回提示
         */
        public static final String TOP_INQUIRY_DATA_EMPTY_RETURN = "亲，询价记录已经丢失，不能报价了。";

        /**
         * 不能投标时：返回提示
         */
        public static final String TOP_BIDLIST_DATA_EMPTY_RETURN = "亲，招标记录已经丢失，不能投标了。";

        /**
         * 非业务性异常时：返回提示
         */
        public static final String TOP_NOT_BUSINESS_EXCEPTION_RETURN = "处理异常了，请向平台报告此问题，谢谢！";

    }

    /**
     * 请求URL路径：基路径
     *
     * @author pc
     */
    public static class RequestUrlBasePath {

        /**
         * 招标审核PC端
         */
        public static final String URL_BIDLIST_CHECK_FOR_PC = "/m/bidList/completeTask";

        /**
         * 招标审核APP端
         */
        public static final String URL_BIDLIST_CHECK_FOR_APP = "/m/bidList/completeTask4App";

        /**
         * 合同审核/提交PC端
         */
        public static final String URL_CONTRACT_CHECK_FOR_PC = "/workflow/completeTask/";

        /**
         * 合同审核APP端
         */
        public static final String URL_CONTRACT_CHECK_FOR_APP = "/workflow/completeTaskForApp";

        /**
         * 投标审核PC端
         */
        public static final String URL_BIDREPLY_CHECK_FOR_PC = "/m/bidReply/completeTask";

        /**
         * 投标审核APP端
         */
        public static final String URL_BIDREPLY_CHECK_FOR_APP = "/m/bidReply/completeTask4App";

        /**
         * 合同编辑PC端
         */
        public static final String URL_CONTRACT_EDIT_FOR_PC = "/workflow/setTaskVariables/";

        /**
         * 通用错误页面
         */
        public static final String URL_COM_ERROR_PAGE = "/commons/error";

    }

    /**
     * 常规业务性消息
     *
     * @author pc
     */
    public static class GeneralMsgInfo {

        /**
         * 提交数据异常！
         */
        public static final String SUBMIT_DATA_ERROR_TOP = "提交数据异常！";

        /**
         * 登录账号信息异常！
         */
        public static final String LOGIN_ACCOUNT_ERROR_TOP = "登录账号信息异常！";

        /**
         * 登录状态失效！
         */
        public static final String LOGIN_SESSION_TIMEOUT_TOP = "登录状态失效！";

        /**
         * 角色名称重复！
         */
        public static final String ROLENAME_REPEAT_TOP = "角色名称重复！";

        /**
         * 角色记录丢失！
         */
        public static final String ROLE_RECORD_LOSE_TOP = "角色记录丢失！";

        /**
         * 角色下存在子账号！
         */
        public static final String ROLE_EXIST_SUB_ACCOUNT_TOP = "角色下存在子账号！";

        /**
         * 访问资源不存在！
         */
        public static final String RESOURCE_NOT_EXIST_TOP = "访问资源不存在！";

    }

    /**
     * @Date: 2017/7/5 @Auth:zen.xu @Desc:用户消息信息
     */
    public static class UserMsgInfo {

        /**
         * @Date: 2017/7/5 @Auth:zen.xu @Desc: 账号已存在
         */
        public static final String USERNAME_EXIST = "账号已存在！";

        /**
         * @Date: 2017/7/10 @Auth:zen.xu @Desc:数据可读范围验证失败！
         */
        public static final String VIEWSCOPE_ERROR = "数据可读范围验证失败！";

        /**
         * @Date: 2017/7/10 @Auth:zen.xu @Desc:角色不可变更！
         */
        public static final String WORKFLOW_ERROR = "角色不可变更！";

        /**
         * @Date: 2017/7/10 @Auth:zen.xu @Desc:停用验证失败！
         */
        public static final String OFF_ERROR = "停用验证失败！";

        /**
         * @Date: 2017/7/12 @Auth:zen.xu @Desc:角色不存在
         */
        public static final String ROLE_NOT_EXIST = "角色不存在！";

        /**
         * @Date: 2017/7/12 @Auth:zen.xu @Desc:部门不存在
         */
        public static final String DEPARTMENT_NOT_EXIST = "部门不存在！";

        /**
         * @Date: 2018/4/11 @Auth: PMQ @Desc: 编辑账号数据异常！
         */
        public static final String EDIT_ACCOUNT_DATA_ERROR = "编辑账号数据异常！";

        /**
         * @Date: 2017/7/12 @Auth:zen.xu @Desc:状态正常
         */
        public static final String STATUS_VALID = "正常";

        /**
         * @Date: 2017/7/12 @Auth:zen.xu @Desc:状态停用
         */
        public static final String STATUS_INVALID = "停用";
    }

    /**
     * @Date: 2017/7/5 @Auth:zen.xu @Desc:用户修改标志
     */
    public static class  UserUpdateFlag{
        /**
         * @Date: 2018/3/30 @Auth:cxf @Desc:修改角色
         */
        public static final String ROLE = "role";
        /**
         * @Date: 2018/3/30 @Auth:cxf @Desc:修改角色
         */
        public static final String DEP = "dep";
    }


    /**
     * @Desc: 角色账号类型
     * @Auth: PMQ
     * @Date: 2017年7月5日 下午4:37:01
     */
    public static class RoleAccountType {
        /**
         * 1.采购商
         */
        public static final int PURCHASER = 1;
        /**
         * 2.供应商
         */
        public static final int SUPPLIER = 2;
        /**
         * 3.银行
         */
        public static final int BANK = 3;

    }

    /**
     * @Desc: 角色使用类型
     * @Auth: PMQ
     * @Date: 2017年7月5日 下午4:41:13
     */
    public static class RoleUseType {
        /**
         * 1.普通角色
         */
        public static final int COMMON = 1;
        /**
         * 2.管理角色
         */
        public static final int ADMIN = 2;

    }

    /**
     * @Desc: 角色是否固定
     * @Auth: PMQ
     * @Date: 2017年7月6日 下午1:51:07
     */
    public static class RoleIsFixed {
        /**
         * true: 固定角色
         */
        public static final boolean FIXED = true;
        /**
         * false: 不固定角色
         */
        public static final boolean NOT_FIXED = false;
    }

    /**
     * @Desc: 角色来源分类
     * @Auth: PMQ
     * @Date: 2018年3月14日 下午3:02:47
     */
    public static class RoleOriginCategory {
    	/**
    	 * 1.平台配置的基本角色
    	 */
    	public static final int BASE_ROLE = 1;
    	/**
    	 * 2.客户自定义角色
    	 */
    	public static final int SELF_DEF_ROLE = 2;
    }
    
    /**
     * @Desc: 资源显示类型
     * @Auth: PMQ
     * @Date: 2017年7月12日 下午3:04:08
     */
    public static class ResourceShowType {
        /**
         * 1：菜单级资源
         */
        public static final int MENU_LEVEL = 1;
        /**
         * 2: tab级别资源
         */
        public static final int TAB_LEVEL = 2;
        /**
         * 3: 按钮级别资源
         */
        public static final int BUTTON_LEVEL = 3;
    }

    /**
     * @Desc: TAB分类级别
     * @Auth: PMQ
     * @Date: 2017年7月24日 下午3:30:23
     */
    public static class TabCategoryLevel {
        /**
         * 0: 一级分类
         */
        public static final int FIRST_LEVEL = 0;
        /**
         * 1: 二级分类
         */
        public static final int SECOND_LEVEL = 1;
    }

    /**
     * @Desc: 常规或动态使用资源
     * @Auth: PMQ
     * @Date: 2017年7月24日 下午5:33:15
     */
    public static class ResourceCommonUse {
        /**
         * false: 常规使用资源
         */
        public static final boolean COMMON_USE = false;
        /**
         * true: 动态配置资源 dynamic
         */
        public static final boolean DYNAMIC_USE = true;
    }

    /**
     * @Desc: 页面按钮资源KEY集合
     * @Auth: cxf
     * @Date: 2017年12月13日 下午3:50:43
     */
    public static class MenuResourceKeys {

        /**
         * 采购商招标管理
         */
        public static final String P_MENU_BID = "p_menu_bid";
        /**
         * 采购商合同管理
         */
        public static final String P_MENU_CONTRACT = "p_menu_contract";
        /**
         * 采购商合同执行管理
         */
        public static final String P_MENU_CONTRACT_PERFORM = "p_menu_contract_perform";
        /**
         * 采购商付款管理
         */
        public static final String P_MENU_PAYMENT = "p_menu_payment";
        /**
         * 采购商询价管理
         */
        public static final String P_MENU_INQUERY = "p_menu_inquery";
        /**
         * 采购商企业信息维护
         */
        public static final String P_MENU_ENTERPRISE_INFORMATION = "p_menu_enterprise_information";
        /**
         * 采购商子账户管理
         */
        public static final String P_MENU_ACCOUNTS_MANAGEMENT = "p_menu_accounts_management";
        /**
         * 采购商流程管理
         */
        public static final String P_MENU_PROCESS_MANAGEMENT = "p_menu_process_management";
        /**
         * 采购商项目管理
         */
        public static final String P_MENU_PROJECT_MANAGEMENT = "p_menu_project_management";
        /**
         * 采购商新闻管理
         */
        public static final String P_MENU_NEWS_MANAGEMENT = "p_menu_news_management";
        
        
        
        /**
         * 供应商投标管理
         */
        public static final String S_MENU_BIDREPLY = "s_menu_bidreply";
        /**
         * 供应商合同管理
         */
        public static final String S_MENU_CONTRACT = "s_menu_contract";
        /**
         * 供应商合同执行管理
         */
        public static final String S_MENU_CONTRACT_PERFORM = "s_menu_contract_perform";
        /**
         * 供应商收款管理
         */
        public static final String S_MENU_PAYMENT = "s_menu_payment";
        /**
         * 供应商询价管理
         */
        public static final String S_MENU_INQUERY = "s_menu_inquery";
        /**
         * 供应商企业信息维护
         */
        public static final String S_MENU_ENTERPRISE_INFORMATION = "s_menu_enterprise_information";
        /**
         * 供应商子账户管理
         */
        public static final String S_MENU_ACCOUNTS_MANAGEMENT = "s_menu_accounts_management";
        /**
         * 供应商流程管理
         */
        public static final String S_MENU_PROCESS_MANAGEMENT = "s_menu_process_management";
        /**
         * 供应商流程管理
         */
        public static final String S_MENU_PRODUCTS_MANAGEMENT = "s_menu_products_management";
    }

    /**
     * @Desc: 筑材金融资源KEY集合
     * @Auth: PMQ
     * @Date: 2018年3月29日 下午3:05:36
     */
    public static class ZhucaiFinanceResourceKeys{
    	
    	/**
         * 供应商：筑材金融资源全部KEYS
         */
    	public static final String S_FINANCE_RESOURCE_KEYS = "f_finance_sup_zcjr,f_finance_sup_wdzcjr,f_finance_sup_wdzbt";
    	/**
         * 采购商：筑材金融资源全部KEYS
         */
    	public static final String P_FINANCE_RESOURCE_KEYS = "f_finance_pur_zcjr,f_finance_pur_wdzcjr,f_finance_pur_wdzbt";

    	/**
         * 供应商：我的筑材金融KEY
         */
    	public static final String S_FINANCE_WDZCJR_KEY = "f_finance_sup_wdzcjr";
    	/**
         * 供应商：我的筑保通KEY
         */
    	public static final String S_FINANCE_WDZBT_KEY = "f_finance_sup_wdzbt";
    	/**
         * 采购商：我的筑材金融KEY
         */
    	public static final String P_FINANCE_WDZCJR_KEY = "f_finance_pur_wdzcjr";
    	/**
         * 采购商：我的筑保通KEY
         */
    	public static final String P_FINANCE_WDZBT_KEY = "f_finance_pur_wdzbt";
    	
    }
    
    /**
     * @Desc: 页面按钮资源KEY集合
     * @Auth: cxf
     * @Date: 2017年12月13日 下午3:50:43
     */
    public static class ButtonResourceKeys {

        /**
         * 发布招标按钮权限
         */
        public static final String P_PUBLISH_BID = "p_publish_bid";

        /**
         * 创建合同按钮权限
         */
        public static final String P_CREATE_CONTRACT = "p_create_contract";

        /**
         * 发布询价按钮权限
         */
        public static final String P_PUBLISH_INQUERY = "p_publish_inquery";

        /**
         * 发布投标按钮权限
         */
        public static final String S_PUBLISH_BIDREPLY = "s_publish_bidreply";

        /**
         * 发布验收按钮权限
         */
        public static final String S_PUBLISH_CHECK = "s_publish_check";
    }

    /**
     * @Desc: 页面TAB资源KEY集合
     * @Auth: PMQ
     * @Date: 2017年7月14日 下午3:50:43
     */
    public static class TabResourceKeys {
        /**
         * 采购商企业信息维护页面TAB资源KEY集合
         */
        public static final String PURCHASER_COMPANYINFO_TAB_KEYS = "p_companyinfo_qyrzglq,p_companyinfo_jbxxglq,p_companyinfo_zzzsglq,p_companyinfo_qyryglq,p_companyinfo_qyxcglq,p_companyinfo_yhzhglq,p_companyinfo_csszglq,p_companyinfo_kpglq";
        /**
         * 采购商子账号页面TAB资源KEY集合
         */
        public static final String PURCHASER_SUBACCOUNT_TAB_KEYS = "p_subaccount_zhwhq,p_subaccount_bmglq,p_subaccount_zzhglq,p_subaccount_jsglq";
        /**
         * 采购商项目管理页面TAB资源KEY集合
         */
        public static final String PURCHASER_NEWSMANAGE_TAB_KEYS = "p_itemmanage_xmglq";
        /**
         * 采购商新闻管理页面TAB资源KEY集合
         */
        public static final String PURCHASER_ITEMMANAGE_TAB_KEYS = "p_newsmanage_xwglq";
        /**
         * 采购商招标管理页面TAB资源KEY集合
         */
        public static final String PURCHASER_BIDMANAGE_TAB_KEYS = "p_bidmanage_wfbzbckq,p_bidmanage_zzfbzbckq,p_bidmanage_yrwzbckq,p_bidmanage_jjzzbckq,p_bidmanage_yzbzbckq,p_bidmanage_ylbzbckq,p_bidmanage_bjjlckq,p_bidmanage_fbjdshjlckq,p_bidmanage_rwjdshjlckq,p_bidmanage_jjjdshjlckq,p_bidmanage_dbjdshjlckq,p_bidmanage_zblyckq,p_publish_bid";
        /**
         * 采购商合同管理页面TAB资源KEY集合
         */
        public static final String PURCHASER_CONTRACTMANAGE_TAB_KEYS = "p_contractmanage_wsxhtckq,p_contractmanage_ysxhtckq,p_contractmanage_bgqxzhtckq,p_contractmanage_zyzhtckq,p_contractmanage_dqrwchtckq,p_contractmanage_ywchtckq,p_contractmanage_htbgjlckq,p_contractmanage_xhtshjlckq,p_contractmanage_htbgshjlckq,p_contractmanage_htzzshjlckq,p_create_contract";
        /**
         * 采购商合同执行页面TAB资源KEY集合
         */
        public static final String PURCHASER_CONTRACTRUN_TAB_KEYS = "p_contractrun_zxzrwckq,p_contractrun_ywcrwckq,p_contractrun_htzxshjlckq";
        /**
         * 采购商付款管理页面TAB资源KEY集合
         */
        public static final String PURCHASER_PAYMANAGE_TAB_KEYS = "p_paymanage_dqrfkjhckq,p_paymanage_sjfkjhckq,p_paymanage_drfkjhckq,p_paymanage_tkjhckq,p_paymanage_fkjhshjlckq,p_paymanage_cjyfg";
        /**
         * 采购商询价管理页面TAB资源KEY集合
         */
        public static final String PURCHASER_INQUIRYMANAGE_TAB_KEYS = "p_inquirymanage_wfbxjckq,p_inquirymanage_zzfbxjckq,p_inquirymanage_yjsxjckq,p_publish_inquery";
        /**
         * 供应商企业信息页面TAB资源KEY集合
         */
        public static final String SUPPLIER_COMPANYINFO_TAB_KEYS = "s_companyinfo_qyrzglq,s_companyinfo_jbxxglq,s_companyinfo_zzzsglq,s_companyinfo_qyryglq,s_companyinfo_qyxcglq,s_companyinfo_yhzhglq";
        /**
         * 供应商子账号页面TAB资源KEY集合
         */
        public static final String SUPPLIER_SUBACCOUNT_TAB_KEYS = "s_subaccount_zhwhq,s_subaccount_bmglq,s_subaccount_zzhglq,s_subaccount_jsglq";
        /**
         * 供应商产品管理页面TAB资源KEY集合
         */
        public static final String SUPPLIER_PRODUCTMANAGE_TAB_KEYS = "s_productmanage_cpglq";
        /**
         * 供应商投标管理页面TAB资源KEY集合
         */
        public static final String SUPPLIER_BIDREPLYMANAGE_TAB_KEYS = "s_bidreplymanage_dtbckq,s_bidreplymanage_ytbckq,s_bidreplymanage_yrwckq,s_bidreplymanage_jjzckq,s_bidreplymanage_yzbckq,s_bidreplymanage_wzbckq,s_bidreplymanage_tbshjlckq,s_bidreplymanage_tblyckq,s_publish_bidreply";
        /**
         * 供应商合同管理页面TAB资源KEY集合
         */
        public static final String SUPPLIER_CONTRACTMANAGE_TAB_KEYS = "s_contractmanage_wsxhtckq,s_contractmanage_ysxhtckq,s_contractmanage_bgqxzhtckq,s_contractmanage_zyzhtckq,s_contractmanage_dqrwchtckq,s_contractmanage_ywchtckq,s_contractmanage_htbgjlckq,s_contractmanage_xhtshjlckq,s_contractmanage_htbgshjlckq,s_contractmanage_htzzshjlckq";
        /**
         * 供应商合同执行管理页面TAB资源KEY集合
         */
        public static final String SUPPLIER_CONTRACTRUN_TAB_KEYS = "s_contractrun_zxzrwckq,s_contractrun_ywcrwckq,s_contractrun_htzxshjlckq,s_publish_check";
        /**
         * 供应商收款管理页面TAB资源KEY集合
         */
        public static final String SUPPLIER_PAYMANAGE_TAB_KEYS = "s_paymanage_dqrskjhckq,s_paymanage_sjskjhckq,s_paymanage_drskjhckq,s_paymanage_skjhshjlckq";
        /**
         * 供应商询价管理页面TAB资源KEY集合
         */
        public static final String SUPPLIER_INQUIRYMANAGE_TAB_KEYS = "s_inquirymanage_jxzckq,s_inquirymanage_yjsckq";
        /**
         * 企业认证管理权
         */
        public static final String P_COMPANYINFO_QYRZGLQ = "p_companyinfo_qyrzglq";
        /**
         * 基本信息管理权
         */
        public static final String P_COMPANYINFO_JBXXGLQ = "p_companyinfo_jbxxglq";
        /**
         * 资质证书管理权
         */
        public static final String P_COMPANYINFO_ZZZSGLQ = "p_companyinfo_zzzsglq";
        /**
         * 企业荣誉管理权
         */
        public static final String P_COMPANYINFO_QYRYGLQ = "p_companyinfo_qyryglq";
        /**
         * 企业相册管理权
         */
        public static final String P_COMPANYINFO_QYXCGLQ = "p_companyinfo_qyxcglq";
        /**
         * 银行账号管理权
         */
        public static final String P_COMPANYINFO_YHZHGLQ = "p_companyinfo_yhzhglq";
        /**
         * 参数设置管理权
         */
        public static final String P_COMPANYINFO_CSSZGLQ = "p_companyinfo_csszglq";
        /**
         * 开票管理权
         */
        public static final String P_COMPANYINFO_KPGLQ = "p_companyinfo_kpglq";

        /**
         * 账户维护权
         */
        public static final String P_SUBACCOUNT_ZHWHQ = "p_subaccount_zhwhq";
        /**
         * 部门管理权
         */
        public static final String P_SUBACCOUNT_BMGLQ = "p_subaccount_bmglq";
        /**
         * 子账号管理权
         */
        public static final String P_SUBACCOUNT_ZZHGLQ = "p_subaccount_zzhglq";
        /**
         * 角色管理权
         */
        public static final String P_SUBACCOUNT_JSGLQ = "p_subaccount_jsglq";
        /**
         * 项目管理权
         */
        public static final String P_ITEMMANAGE_XMGLQ = "p_itemmanage_xmglq";

        /**
         * 新闻管理权
         */
        public static final String P_NEWSMANAGE_XWGLQ = "p_newsmanage_xwglq";
        /**
         * 未发布招标查看权
         */
        public static final String P_BIDMANAGE_WFBZBCKQ = "p_bidmanage_wfbzbckq";
        /**
         * 正在发布招标查看权
         */
        public static final String P_BIDMANAGE_ZZFBZBCKQ = "p_bidmanage_zzfbzbckq";
        /**
         * 已入围招标查看权
         */
        public static final String P_BIDMANAGE_YRWZBCKQ = "p_bidmanage_yrwzbckq";
        /**
         * 竞价中招标查看权
         */
        public static final String P_BIDMANAGE_JJZZBCKQ = "p_bidmanage_jjzzbckq";
        /**
         * 已中标招标查看权
         */
        public static final String P_BIDMANAGE_YZBZBCKQ = "p_bidmanage_yzbzbckq";
        /**
         * 已流标招标查看权
         */
        public static final String P_BIDMANAGE_YLBZBCKQ = "p_bidmanage_ylbzbckq";
        /**
         * 报价记录查看权
         */
        public static final String P_BIDMANAGE_BJJLCKQ = "p_bidmanage_bjjlckq";
        /**
         * 发布阶段审核记录查看权
         */
        public static final String P_BIDMANAGE_FBJDSHJLCKQ = "p_bidmanage_fbjdshjlckq";
        /**
         * 入围阶段审核记录查看权
         */
        public static final String P_BIDMANAGE_RWJDSHJLCKQ = "p_bidmanage_rwjdshjlckq";
        /**
         * 竞价阶段审核记录查看权
         */
        public static final String P_BIDMANAGE_JJJDSHJLCKQ = "p_bidmanage_jjjdshjlckq";
        /**
         * 定标阶段审核记录查看权
         */
        public static final String P_BIDMANAGE_DBJDSHJLCKQ = "p_bidmanage_dbjdshjlckq";
        /**
         * 未生效合同查看权
         */
        public static final String P_CONTRACTMANAGE_WSXHTCKQ = "p_contractmanage_wsxhtckq";
        /**
         * 已生效合同查看权
         */
        public static final String P_CONTRACTMANAGE_YSXHTCKQ = "p_contractmanage_ysxhtckq";
        /**
         * 变更／取消中合同查看权
         */
        public static final String P_CONTRACTMANAGE_BGQXZHTCKQ = "p_contractmanage_bgqxzhtckq";
        /**
         * 争议中合同查看权
         */
        public static final String P_CONTRACTMANAGE_ZYZHTCKQ = "p_contractmanage_zyzhtckq";
        /**
         * 待确认完成合同查看权
         */
        public static final String P_CONTRACTMANAGE_DQRWCHTCKQ = "p_contractmanage_dqrwchtckq";
        /**
         * 已完成合同查看权
         */
        public static final String P_CONTRACTMANAGE_YWCHTCKQ = "p_contractmanage_ywchtckq";
        /**
         * 合同变更记录查看权
         */
        public static final String P_CONTRACTMANAGE_HTBGJLCKQ = "p_contractmanage_htbgjlckq";
        /**
         * 新合同审核记录查看权
         */
        public static final String P_CONTRACTMANAGE_XHTSHJLCKQ = "p_contractmanage_xhtshjlckq";
        /**
         * 合同变更审核记录查看权
         */
        public static final String P_CONTRACTMANAGE_HTBGSHJLCKQ = "p_contractmanage_htbgshjlckq";
        /**
         * 合同终止审核记录查看权
         */
        public static final String P_CONTRACTMANAGE_HTZZSHJLCKQ = "p_contractmanage_htzzshjlckq";
        /**
         * 执行中任务查看权
         */
        public static final String P_CONTRACTRUN_ZXZRWCKQ = "p_contractrun_zxzrwckq";
        /**
         * 已完成任务查看权
         */
        public static final String P_CONTRACTRUN_YWCRWCKQ = "p_contractrun_ywcrwckq";
        /**
         * 合同执行审核记录查看权
         */
        public static final String P_CONTRACTRUN_HTZXSHJLCKQ = "p_contractrun_htzxshjlckq";
        /**
         * 合同付款计划查看权
         */
        public static final String P_PAYMANAGE_HTFKJHCKQ = "p_paymanage_htfkjhckq";
        /**
         * 待确认付款计划查看权
         */
        public static final String P_PAYMANAGE_DQRFKJHCKQ = "p_paymanage_dqrfkjhckq";
        /**
         * 实际付款计划查看权
         */
        public static final String P_PAYMANAGE_SJFKJHCKQ = "p_paymanage_sjfkjhckq";
        /**
         * 当日付款计划查看权
         */
        public static final String P_PAYMANAGE_DRFKJHCKQ = "p_paymanage_drfkjhckq";
        /**
         * 退款计划查看权
         */
        public static final String P_PAYMANAGE_TKJHCKQ = "p_paymanage_tkjhckq";
        /**
         * 付款计划审核记录查看权
         */
        public static final String P_PAYMANAGE_FKJHSHJLCKQ = "p_paymanage_fkjhshjlckq";
        /**
         * 未发布询价查看权
         */
        public static final String P_INQUIRYMANAGE_WFBXJCKQ = "p_inquirymanage_wfbxjckq";
        /**
         * 正在发布询价查看权
         */
        public static final String P_INQUIRYMANAGE_ZZFBXJCKQ = "p_inquirymanage_zzfbxjckq";
        /**
         * 已结束询价查看权
         */
        public static final String P_INQUIRYMANAGE_YJSXJCKQ = "p_inquirymanage_yjsxjckq";
        /**
         * 企业认证管理权
         */
        public static final String S_COMPANYINFO_QYRZGLQ = "s_companyinfo_qyrzglq";
        /**
         * 基本信息管理权
         */
        public static final String S_COMPANYINFO_JBXXGLQ = "s_companyinfo_jbxxglq";
        /**
         * 资质证书管理权
         */
        public static final String S_COMPANYINFO_ZZZSGLQ = "s_companyinfo_zzzsglq";
        /**
         * 企业荣誉管理权
         */
        public static final String S_COMPANYINFO_QYRYGLQ = "s_companyinfo_qyryglq";
        /**
         * 企业相册管理权
         */
        public static final String S_COMPANYINFO_QYXCGLQ = "s_companyinfo_qyxcglq";
        /**
         * 银行账号管理权
         */
        public static final String S_COMPANYINFO_YHZHGLQ = "s_companyinfo_yhzhglq";
        /**
         * 账户维护权
         */
        public static final String S_SUBACCOUNT_ZHWHQ = "s_subaccount_zhwhq";
        /**
         * 部门管理权
         */
        public static final String S_SUBACCOUNT_BMGLQ = "s_subaccount_bmglq";
        /**
         * 子账号管理权
         */
        public static final String S_SUBACCOUNT_ZZHGLQ = "s_subaccount_zzhglq";
        /**
         * 角色管理权
         */
        public static final String S_SUBACCOUNT_JSGLQ = "s_subaccount_jsglq";
        /**
         * 产品管理权
         */
        public static final String S_PRODUCTMANAGE_CPGLQ = "s_productmanage_cpglq";
        /**
         * 待投标查看权
         */
        public static final String S_BIDREPLYMANAGE_DTBCKQ = "s_bidreplymanage_dtbckq";
        /**
         * 已投标查看权
         */
        public static final String S_BIDREPLYMANAGE_YTBCKQ = "s_bidreplymanage_ytbckq";
        /**
         * 已入围查看权
         */
        public static final String S_BIDREPLYMANAGE_YRWCKQ = "s_bidreplymanage_yrwckq";
        /**
         * 竞价中查看权
         */
        public static final String S_BIDREPLYMANAGE_JJZCKQ = "s_bidreplymanage_jjzckq";
        /**
         * 已中标查看权
         */
        public static final String S_BIDREPLYMANAGE_YZBCKQ = "s_bidreplymanage_yzbckq";
        /**
         * 未中标查看权
         */
        public static final String S_BIDREPLYMANAGE_WZBCKQ = "s_bidreplymanage_wzbckq";
        /**
         * 投标审核记录查看权
         */
        public static final String S_BIDREPLYMANAGE_TBSHJLCKQ = "s_bidreplymanage_tbshjlckq";
        /**
         * 投标留言查看权
         */
        public static final String S_BIDREPLYMANAGE_TBLYCKQ = "s_bidreplymanage_tblyckq";
        /**
         * 未生效合同查看权
         */
        public static final String S_CONTRACTMANAGE_WSXHTCKQ = "s_contractmanage_wsxhtckq";
        /**
         * 已生效合同查看权
         */
        public static final String S_CONTRACTMANAGE_YSXHTCKQ = "s_contractmanage_ysxhtckq";
        /**
         * 变更／取消中合同查看权
         */
        public static final String S_CONTRACTMANAGE_BGQXZHTCKQ = "s_contractmanage_bgqxzhtckq";
        /**
         * 争议中合同查看权
         */
        public static final String S_CONTRACTMANAGE_ZYZHTCKQ = "s_contractmanage_zyzhtckq";
        /**
         * 待确认完成合同查看权
         */
        public static final String S_CONTRACTMANAGE_DQRWCHTCKQ = "s_contractmanage_dqrwchtckq";
        /**
         * 已完成合同查看权
         */
        public static final String S_CONTRACTMANAGE_YWCHTCKQ = "s_contractmanage_ywchtckq";
        /**
         * 合同变更记录查看权
         */
        public static final String S_CONTRACTMANAGE_HTBGJLCKQ = "s_contractmanage_htbgjlckq";
        /**
         * 新合同审核记录查看权
         */
        public static final String S_CONTRACTMANAGE_XHTSHJLCKQ = "s_contractmanage_xhtshjlckq";
        /**
         * 合同变更审核记录查看权
         */
        public static final String S_CONTRACTMANAGE_HTBGSHJLCKQ = "s_contractmanage_htbgshjlckq";
        /**
         * 合同终止审核记录查看权
         */
        public static final String S_CONTRACTMANAGE_HTZZSHJLCKQ = "s_contractmanage_htzzshjlckq";
        /**
         * 执行中任务查看权
         */
        public static final String S_CONTRACTRUN_ZXZRWCKQ = "s_contractrun_zxzrwckq";
        /**
         * 已完成任务查看权
         */
        public static final String S_CONTRACTRUN_YWCRWCKQ = "s_contractrun_ywcrwckq";
        /**
         * 合同执行审核记录查看权
         */
        public static final String S_CONTRACTRUN_HTZXSHJLCKQ = "s_contractrun_htzxshjlckq";
        /**
         * 合同收款计划查看权
         */
        public static final String S_PAYMANAGE_HTSKJHCKQ = "s_paymanage_htskjhckq";
        /**
         * 待确认收款计划查看权
         */
        public static final String S_PAYMANAGE_DQRSKJHCKQ = "s_paymanage_dqrskjhckq";
        /**
         * 实际收款计划查看权
         */
        public static final String S_PAYMANAGE_SJSKJHCKQ = "s_paymanage_sjskjhckq";
        /**
         * 当日收款计划查看权
         */
        public static final String S_PAYMANAGE_DRSKJHCKQ = "s_paymanage_drskjhckq";
        /**
         * 未到款查看权
         */
        public static final String S_PAYMANAGE_WDKCKQ = "s_paymanage_wdkckq";
        /**
         * 收款计划审核记录查看权
         */
        public static final String S_PAYMANAGE_SKJHSHJLCKQ = "s_paymanage_skjhshjlckq";
        /**
         * 进行中查看权
         */
        public static final String S_PAYMANAGE_JXZCKQ = "s_paymanage_jxzckq";
        /**
         * 已结束查看权
         */
        public static final String S_PAYMANAGE_YJSCKQ = "s_paymanage_yjsckq";
        /**
         * 招标留言查看权
         */
        public static final String P_BIDMANAGE_ZBLYCKQ = "p_bidmanage_zblyckq";

        public static final String TAB_PAGE_CURRENT_VIEW_MAP_KEY = "viewTabKey";
        
        /**
         * 发布投标按钮权
         */
        public static final String S_PUBLISH_BIDREPLY = "s_publish_bidreply";
        
        /**
         * 发布验收按钮权
         */
        public static final String S_PUBLISH_CHECK = "s_publish_check";
        
        /**
         * 发布询价按钮
         */
        public static final String P_PUBLISH_INQUERY = "p_publish_inquery";

        /**
         * 询价报价按钮
         */
        public static final String S_INQUIRY_OFFER = "s_inquiry_offer";
        /**
         * 供应商询价进行中询价
         */
        public static final String S_INQUIRYMANAGE_JXZCKQ = "s_inquirymanage_jxzckq";
        /**
         *供应商询价已结束询价
         */
        public static final String S_INQUIRYMANAGE_YJSCKQ = "s_inquirymanage_yjsckq";
        
        /**
         * 供应商的物流管理权限
         */
        public static final String S_LOGISTICS_MANAGE = "s_logistics_manage";
        /**
         * 供应商的物流管理--常用装货地址管理
         */
        public static final String P_LOGISTICS_CYZHDZ = "p_logistics_cyzhdz";
        /**
         * 供应商的物流管理--我的订单
         */
        public static final String P_LOGISTICS_WDDD = "p_logistics_wddd";

        /**
         * 采购商招标流程管理权
         */
        public static final String P_PROCESS_ZBLCGLQ = "p_process_zblcglq";
        /**
         * 采购商合同流程管理权
         */
        public static final String P_PROCESS_HTLCGLQ = "p_process_htlcglq";
        /**
         * 采购商付款流程管理权
         */
        public static final String P_PROCESS_FKLCGLQ = "p_process_fklcglq";
        /**
         * 供应商投标流程管理权
         */
        public static final String S_PROCESS_TBLCGLQ = "s_process_tblcglq";
        /**
         * 供应商合同流程管理权
         */
        public static final String S_PROCESS_HTLCGLQ = "s_process_htlcglq";
        /**
         * 供应商付款流程管理权
         */
        public static final String S_PROCESS_SKLCGLQ = "s_process_sklcglq";
        /**
         * 采购商流程管理页面TAB资源KEY集合
         */
        public static final String PURCHASER_PROCESS_TAB_KEYS = "p_process_zblcglq,p_process_htlcglq,p_process_fklcglq";
        /**
         * 供应商流程管理页面TAB资源KEY集合
         */
        public static final String SUPPLIER_PROCESS_TAB_KEYS = "s_process_tblcglq,s_process_htlcglq,s_process_sklcglq";

        /**
         * 采购商导出招标数据页面TAB资源KEY集合
         */
        public static final String PURCHASER_EXPORT_BID_TAB_KEYS = "p_bidmanage_wfbzbckq,p_bidmanage_zzfbzbckq,p_bidmanage_yrwzbckq,p_bidmanage_jjzzbckq,p_bidmanage_yzbzbckq,p_bidmanage_ylbzbckq";
        /**
         * 采购商导出合同数据页面TAB资源KEY集合
         */
        public static final String PURCHASER_EXPORT_CONTRACT_TAB_KEYS = "p_contractmanage_wsxhtckq,p_contractmanage_ysxhtckq,p_contractmanage_bgqxzhtckq,p_contractmanage_zyzhtckq,p_contractmanage_dqrwchtckq,p_contractmanage_ywchtckq";
    }

    /**
     * @Desc: 通用角色标识值
     * @Auth: PMQ
     * @Date: 2017年7月26日 下午4:38:32
     */
    public static class CommonRoleFlag {
        /**
         * 超级管理员
         */
        public static final int ADMIN = 1;
        /**
         * 总指令
         */
        public static final int BIG_INSTRUCTION = 2;
        /**
         * 资金总调度
         */
        public static final int BIG_DISPATCH = 3;
        /**
         * 总巡视员
         */
        public static final int BIG_PATROL = 4;
        /**
         * 分指令
         */
        public static final int SMALL_INSTRUCTION = 5;
        /**
         * 资金分调度
         */
        public static final int SMALL_DISPATCH = 6;
        /**
         * 分巡视员
         */
        public static final int SMALL_PATROL = 7;
        /**
         * 经理
         */
        public static final int PROJECT_MANAGER = 8;
        /**
         * 采购员
         */
        public static final int PURCHASER = 9;
        /**
         * 财务
         */
        public static final int TREASURER = 10;
        /**
         * 预算员
         */
        public static final int BUDGETEER = 11;
        /**
         * 现场负责人
         */
        public static final int SCENE_RESPONSIBLE_PERSON = 12;
        /**
         * 其他负责人
         */
        public static final int OTHER_RESPONSIBLE_PERSON = 13;
        /**
         * 销售总监
         */
        public static final int SALES_DIRECTOR = 14;
        /**
         * 财务总监
         */
        public static final int FINANCE_DIRECTOR = 15;
        /**
         * 区域销售经理
         */
        public static final int REGIONAL_SALES_MANAGER = 16;
        /**
         * 区域财务经理
         */
        public static final int REGIONAL_FINANCE_MANAGER = 17;
        /**
         * 销售员
         */
        public static final int SUPPLIER = 18;

    }

    /**
     * @Desc: 通用角色名称
     * @Auth: PMQ
     * @Date: 2017年7月26日 下午5:17:40
     */
    public static class CommonRoleName {
        /**
         * 超级管理员
         */
        public static final String ADMIN = "超级管理员";

    }

    /**
     * @Desc: 部门消息
     * @Auth: GaoLin
     * @Date: 2017-7-10 下午4:19:40
     */
    public static class DepartmentMsgInfo {

        /**
         * @Date: 2017-7-10 @Auth:GaoLin @Desc(V/B):上级部门相同的同级部门,部门名称不能重复.
         */
        public static final String DEPARTMENT_NAME_NOT_REPEAT = "你输入的部门名称已经存在，请重新输入！";

        /**
         * @Date: 2017-7-10 @Auth:GaoLin @Desc(V/B):总公司不能被编辑
         */
        public static final String DEPARTMENT_NOT_UPDATE = "该部门不可编辑！";

        /**
         * @Date: 2017-7-10 @Auth:GaoLin @Desc(V/B):上级部门不能为当前编辑部门的子部门。
         */
        public static final String DEPARTMENT_NOT_CHILD = "上级部门不能为当前编辑部门的子部门！";

        /**
         * @Date: 2017-7-21 @Auth:GaoLin @Desc(V/B):总部门不能编辑！
         */
        public static final String HEAD_DEPARTMENT_NOT_EDIT = "总部门不能编辑！";

        /**
         * @Date: 2017-7-10 @Auth:GaoLin @Desc(V/B):当前部门或其子部门存在子账户时，该部门不能删除.
         */
        public static final String DEPARTMENT_NOT_DELETE = "当前部门或其子部门存在子账户不能删除！";

        /**
         * @Date: 2017-7-14 @Auth:GaoLin @Desc(V/B):总部门不能删除
         */
        public static final String HEAD_DEPARTMENT_NOT_DELETE = "总部门不能删除！";

        /**
         * @Date: 2017-7-11 @Auth:GaoLin @Desc(V/B):当前部门不能移动。
         */
        public static final String DEPARTMENT_NOT_MOVE = "当前部门不能移动！";

        /**
         * @Date: 2017-7-12 @Auth:GaoLin @Desc(V/B):当前对象不存在
         */
        public static final String DEPARTMENT_NOT_FIND = "当前对象不存在！";
        /**
         * @Date: 2017-7-12 @Auth:GaoLin @Desc(V/B):上级部门不存在
         */
        public static final String DEPARTMENT_NOT_PARENT = "上级部门不存在！";

    }

    /**
     * @author pc
     */
    public static class LogisticsInterface {

        /**
         * 询价接口
         */
        public static final String INQUIRY = "/quote/search";


        /**
         * 下单接口
         */
        public static final String ORDER = "/order/create";

    }

    public static class InterfaceConstant {

        /**
         * @Date: 2017-7-4 @Auth:hss @Desc(V7.0.3):常量ios app_key
         */
        public static final String PUSH_MSG_URL = "http://msg.umeng.com/api/send";



        public static final String TITLE = "新待办";
    }


    public static class TaskStatus {
        /**
         * 待审核
         */
        public static final String check = "待审核";

        /**
         * 待处理
         */
        public static final String handle = "待处理";
    }


    public static class OldProcessId {


        /**
         * 授信流程
         */
        public static final String bankCredit = "bankCredit";


        /**
         * 招标流标流程
         */
        public static final String bidCancel = "bidCancel";

        /**
         * 采购员发起中标流标
         */
        public static final String myProcess = "myProcess";

        /**
         * 申请投标流程
         */
        public static final String bidConfirm = "bidConfirm";

        /**
         * 招标议标流程
         */
        public static final String bidDiscuss = "bidDiscuss";


        /**
         * 合同取消流程
         */
        public static final String contractCancel = "contractCancel";

        /**
         * 合同修改流程
         */
        public static final String contractEdit = "contractEdit";

        /**
         * 合同签订流程
         */
        public static final String contractInitial = "contractInitial";

        /**
         * 合同验收流程
         */
        public static final String contractRun = "contractRun";

        /**
         * 合同终止流程
         */
        public static final String contractTerminate = "contractTerminate";

        /**
         * 上报资金计划流程
         */
        public static final String funding = "funding";

        /**
         * 合同付款流程
         */
        public static final String payment = "payment";

        /**
         * 采购申请流程
         */
        public static final String purchaseApply = "purchaseApply";
    }


    public static class AllAppTaskName {


        /**
         * 预算员审核
         */
        public static final String P_3_BA_CHECK = "预算员审核";


        /**
         * 财务审核
         */
        public static final String P_3_FA_CHECK = "财务审核";

        /**
         * 项目经理审核
         */
        public static final String P_3_PM_CHECK = "项目经理审核";

        /**
         * 竞价项目经理审核
         */
        public static final String BID_P_3_PM_CHECK = "竞价项目经理审核";

        /**
         * 采购员提交入围申请
         */
        public static final String RUWEI_P_3_PA_CHECK = "采购员提交入围申请";


        /**
         * 采购员查看报价 申请中标 流标
         */
        public static final String LOOK_BID_FLOW_P_3_PA_CHECK = "采购员查看报价 申请中标 流标";

        /**
         * 采购员提交商议结果
         */
        public static final String BID_RESULT_P_3_PM_CHECK = "采购员提交商议结果";

        /**
         * 采购员提交选标申请
         */
        public static final String BID_SUBMIT_P_3_PM_CHECK = "采购员提交选标申请";

        /**
         * 重新修改入围申请
         */
        public static final String UPDATE_P_3_PM_CHECK = "重新修改入围申请";

        /**
         * 项目经理确认入围
         */
        public static final String CONFRIM_P_3_PM_CHECK = "项目经理确认入围";

        /**
         * 外加发起争议
         */
        public static final String START_DISPUTE = "外加发起争议";

        /**
         * 采购员输入密码
         */
        public static final String PUR_CONFIRM_PASSWORD = "采购员输入密码";

        /**
         * 销售员输入密码
         */
        public static final String SALE_CONFIRM_PASSWORD = "销售员输入密码";

        /**
         * 现场验收人员确认收货
         */
        public static final String COMFRIM_P_3_SA = "现场验收人员确认收货";

        /**
         * 供应商销售员审核
         */
        public static final String S_3_SALES_CHECK = "供应商销售员审核";

        /**
         * 总指令审核
         */
        public static final String P_1_A_CHECK = "总指令审核";

        /**
         * 资金总调度审核
         */
        public static final String P_1_B_CHECK = "资金总调度审核";

        /**
         * 区域销售经理审核
         */
        public static final String S_2_RSM_CHECK = "区域销售经理审核";

        /**
         * 销售总监审核
         */
        public static final String S_1_SD_CHECK = "销售总监审核";


        /**
         * 销售员修改报价
         */
        public static final String S_3_SALES_UPDATE = "销售员修改报价";


        /**
         * 供应商再次报价形成最终报价
         */
        public static final String FINAL_OFFER = "供应商再次报价形成最终报价";

        /**
         * 采购商确认合同完成
         */
        public static final String PURCHASER_CONFIRM = "采购商确认合同完成";

        /**
         * 供应商确认合同完成
         */
        public static final String SUPPLIER_CONFIRM = "供应商确认合同完成";

        /**
         * 项目经理审核流标
         */
        public static final String AUDIT_FLOW_BID = "项目经理审核流标";

        /**
         * 项目经理审核中标
         */
        public static final String AUDIT_BID = "项目经理审核中标";

        /**
         * 上级领导审核(会审)
         */
        public static final String HEARING = "上级领导审核(会审)";


    }

    /**
     * url权限
     */
    public static class Url {

        //企业信息维护
        public static final String purchaser_company_info = "/member/buyer_admin/company/info";
        public static final String supplier_company_info = "/member/supplier_admin/company/info";

        //子账户管理
        public static final String subAccountIndex = "/subAccountIndex";

        //项目管理
        public static final String purchaser_company_project = "/member/buyer_admin/company/project";

        //产品管理
        public static final String supplier_company_product = "/member/supplier_admin/company/product";
    }

    /**
     * 待办统计分类
     */
    public static class Handle {

        //待处理
        public static final String toProcessed = "1";

        //待审批
        public static final String pending = "2";
    }

    /**
     * 标书统计分类
     */
    public static class ProcessType {

        //招标管理
        public static final String bidList = "1";

        //投标管理
        public static final String bidReply = "2";

        //合同管理
        public static final String contractList = "3";

        //付款管理
        public static final String payment = "4";
    }

    public static class ClientType {

        //pc端
        public static final String PC = "PC";

        //app端
        public static final String APP = "APP";
    }


    /**
     * app第一期特殊处理 所需变量
     */
    public static class PublicVar {

        //签署合同前缀
        public static final String APP_CONTRACT_VAL = "contractId";

        //签署合同前缀分隔符
        public static final String APP_CONTRACT_FLAG = "_";

        //确认合同前缀
        public static final String APP_CONFIRM_CONTRACT = "confirmContract";

        //确认合同前缀分隔符
        public static final String APP_CONFIRM_CONTRACT_FLAG = "_";

        //新建合同中任务
        public static final String PC_CHINESE_NEW_MISS = "新建任务";

        //新建合同中任务
        public static final String PC_CHINESE_MISS = "任务";

        //新建合同中任务中内容
        public static final String PC_CHINESE_CONTENT_TEXT = "根据甲方要求提供清单商品合计价值累计超过";

        //信息发送失败按钮
        public static final String SEND_FAIL_TEXT = "发送信息失败";
        //销售总监/区域销售经理前缀
        public static final String APP_REPLY_PREFIX = "modelId=";

        //销售总监/区域销售经理后缀
        public static final String APP_REPLY_SUFFIX = "&taskId";
        //平台確認付款到供应商
        public static final String PAY_TO_PURCHASER = "平台确认付款到供应方";

        //合同审核记录
        public static final String CONTRACT_INITIAL = "contractInitial";
        public static final String CONTRACT_EDIT = "contractEdit";
        public static final String CONTRACT_TERMINATION = "contractTermination";


    }

    /**
     * 便捷操作采购商的权限的url
     */
    public static class PurchaseUrl {
        //发布招标 ,对应招标管理
        public static String PURCHASE_INVITATION_BIDS = "/member/buyer/invitation_bids";

        //指定付款计划，对应付款管理
        public static String PURCHASE_PAYMENT = "/member/buyer/payment";

        //添加新闻，对应新闻管理
        public static String PURCHASE_NEWS = "/member/buyer_admin/company/news";

        //新增项目，项目管理
        public static String PURCHASE_PROJECT = "/member/buyer_admin/company/project";

        //组织架构管理,角色管理，对应子账户管理
        public static String PURCHASE_SUBACCOUNTINDEX = "/subAccountIndex";

        //创建合同，对应合同管理
        public static String PURCHASE_CONTRACT = "/member/buyer/contract";
    }

    /**
     * 便捷操作供应商的权限url
     */
    public static class SupplierUrl {

        //发起验收，对应合同执行管理
        public static String SUPPLIER_CONTRACT_PERFORM = "/member/supplier/contract_perform";

        //组织架构管理,角色管理，对应子账户管理
        public static String SUPPLIER_SUBACCOUNTINDEX = "/subAccountIndex";
    }

    /**
     * @Auth: GaoLin
     * @Date: 2017-11-18 下午5:48:38    Desc:评价类型
     */
    public static class EvaluationType {
        //1采购评价供应
        public static Integer PURCHASE_EVALUATE = 1;

        // 2供应评价采购
        public static Integer SUPPLIER_EVALUATE = 2;

    }

    /**
     * APP招标中用到的相关常量
     *
     * @author young
     */
    public static class bidAppConstont {
        /**
         * 议标未读
         */
        public static final Integer BID_NEGONATION_UNREAD = 0;
        /**
         * 议标已读
         */
        public static final Integer BID_NEGONATION_READ = 1;
        /**
         * 不是最新报价
         */
        public static final Integer NO_LASTED_QUOTE_PRICE = 0;
        /**
         * 是最新报价
         */
        public static final Integer YES_LASTED_QUOTE_PRICE = 1;

        /**
         * 未读留言
         */
        public static final Integer MESG_UNREAD = 0;
        /**
         * 没有未读留言
         */
        public static final Integer MESG_READ = 1;
    }

    /**
     * APP投标中用到的相关常量
     *
     * @author young
     * @Date 2017/12/07
     */
    public static class bidReplyAppConstont {

        /**
         * 未提交
         */
        public static final Integer IS_OPEN_NO = 0;
        /**
         * 已提交
         */
        public static final Integer IS_OPEN_YES = 1;
    }
    /**
     * @Desc 流程发起人，审核人的权限集合
     * @author liheng
     * @Date 2017/12/14
     */
    public static class processTabKeys{
    	/**
         * 招标发起人
         */
    	public static final String P_BID_START_PERSON="p_menu_bid,p_menu_contract,p_menu_contract_perform,p_menu_payment,p_menu_inquery,p_bidmanage_wfbzbckq,p_bidmanage_zzfbzbckq,p_bidmanage_yrwzbckq,p_bidmanage_jjzzbckq,p_bidmanage_yzbzbckq,p_bidmanage_ylbzbckq,p_bidmanage_bjjlckq,p_bidmanage_fbjdshjlckq,p_bidmanage_rwjdshjlckq,p_bidmanage_jjjdshjlckq,p_bidmanage_dbjdshjlckq,p_contractmanage_wsxhtckq,p_contractmanage_ysxhtckq,p_contractmanage_bgqxzhtckq,p_contractmanage_zyzhtckq,p_contractmanage_dqrwchtckq,p_contractmanage_ywchtckq,p_contractmanage_htbgjlckq,p_contractmanage_xhtshjlckq,p_contractmanage_htbgshjlckq,p_contractmanage_htzzshjlckq,p_contractrun_zxzrwckq,p_contractrun_ywcrwckq,p_contractrun_htzxshjlckq,p_paymanage_htfkjhckq,p_paymanage_dqrfkjhckq,p_paymanage_sjfkjhckq,p_paymanage_drfkjhckq,p_paymanage_tkjhckq,p_paymanage_fkjhshjlckq,p_inquirymanage_wfbxjckq,p_inquirymanage_zzfbxjckq,p_inquirymanage_yjsxjckq,p_bidmanage_zblyckq,p_publish_bid,p_create_contract,p_publish_inquery";
    	/**
         * 招标审核人
         */
    	public static final String P_BID_CHECK_PERSON="p_menu_bid,p_bidmanage_wfbzbckq,p_bidmanage_zzfbzbckq,p_bidmanage_bjjlckq,p_bidmanage_fbjdshjlckq,p_bidmanage_zblyckq";
    	/**
         * 入围审核人
         */
    	public static final String P_CANDIDATE_CHECK_PERSON="p_menu_bid,p_bidmanage_zzfbzbckq,p_bidmanage_yrwzbckq,p_bidmanage_bjjlckq,p_bidmanage_fbjdshjlckq,p_bidmanage_rwjdshjlckq,p_bidmanage_zblyckq";
    	/**
         * 竞价审核人
         */
    	public static final String P_BID_PRICE_CHECK_PERSON="p_menu_bid,p_bidmanage_yrwzbckq,p_bidmanage_jjzzbckq,p_bidmanage_bjjlckq,p_bidmanage_fbjdshjlckq,p_bidmanage_rwjdshjlckq,p_bidmanage_jjjdshjlckq,p_bidmanage_zblyckq";
    	/**
         * 定标审核人
         */
    	public static final String P_DING_BIAO_CHECK_PERSON="p_menu_bid,p_bidmanage_zzfbzbckq,p_bidmanage_yrwzbckq,p_bidmanage_jjzzbckq,p_bidmanage_yzbzbckq,p_bidmanage_bjjlckq,p_bidmanage_fbjdshjlckq,p_bidmanage_rwjdshjlckq,p_bidmanage_jjjdshjlckq,p_bidmanage_dbjdshjlckq,p_bidmanage_zblyckq";
    	/**
         * 采购-废标审核人
         */
    	public static final String P_WASTE_BIAO_CHECK_PERSON="p_menu_bid,p_bidmanage_yzbzbckq,p_bidmanage_ylbzbckq,p_bidmanage_bjjlckq,p_bidmanage_fbjdshjlckq,p_bidmanage_rwjdshjlckq,p_bidmanage_jjjdshjlckq,p_bidmanage_dbjdshjlckq,p_bidmanage_zblyckq";
    	/**
         * 采购-签署合同审核人
         */
    	public static final String P_CONTRACT_SIGN_CHECK_PERSON="p_menu_contract,p_contractmanage_wsxhtckq,p_contractmanage_ysxhtckq,p_contractmanage_bgqxzhtckq,p_contractmanage_zyzhtckq,p_contractmanage_dqrwchtckq,p_contractmanage_ywchtckq,p_contractmanage_htbgjlckq,p_contractmanage_xhtshjlckq,p_contractmanage_htbgshjlckq,p_contractmanage_htzzshjlckq";
    	/**
         * 采购-合同验收审核人
         */
    	public static final String P_CONTRACT_VERIFY_CHECK_PERSON="p_menu_contract_perform,p_contractrun_zxzrwckq,p_contractrun_ywcrwckq,p_contractrun_htzxshjlckq";
    	/**
         * 付款计划审核人
         */
    	public static final String P_PAY_PLAN_CHECK_PERSON="p_menu_payment,p_paymanage_htfkjhckq,p_paymanage_dqrfkjhckq,p_paymanage_fkjhshjlckq";
    	/**
         * 付款发起人
         */
    	public static final String P_PAY_START_PERSON="p_menu_payment,p_paymanage_sjfkjhckq,p_paymanage_drfkjhckq,p_paymanage_fkjhshjlckq";
    	/**
         * 付款审核人
         */
    	public static final String P_PAY_CHECK_PERSON="p_menu_payment,p_paymanage_sjfkjhckq,p_paymanage_drfkjhckq,p_paymanage_fkjhshjlckq";
    	/**
         * 投标发起人
         */
    	public static final String S_BID_START_PERSON="s_menu_bidreply,s_menu_contract,s_menu_contract_perform,s_menu_payment,s_menu_inquery,s_bidreplymanage_dtbckq,s_bidreplymanage_ytbckq,s_bidreplymanage_yrwckq,s_bidreplymanage_jjzckq,s_bidreplymanage_yzbckq,s_bidreplymanage_wzbckq,s_bidreplymanage_tbshjlckq,s_bidreplymanage_tblyckq,s_contractmanage_wsxhtckq,s_contractmanage_ysxhtckq,s_contractmanage_bgqxzhtckq,s_contractmanage_zyzhtckq,s_contractmanage_dqrwchtckq,s_contractmanage_ywchtckq,s_contractmanage_htbgjlckq,s_contractmanage_xhtshjlckq,s_contractmanage_htbgshjlckq,s_contractmanage_htzzshjlckq,s_contractrun_zxzrwckq,s_contractrun_ywcrwckq,s_contractrun_htzxshjlckq,s_paymanage_htskjhckq,s_paymanage_dqrskjhckq,s_paymanage_sjskjhckq,s_paymanage_drskjhckq,s_paymanage_wdkckq,s_paymanage_skjhshjlckq,s_inquirymanage_jxzckq,s_inquirymanage_yjsckq,s_publish_bidreply,s_publish_check,s_inquiry_offer,s_logistics_manage,p_logistics_cyzhdz,p_logistics_wddd,f_finance_sup_zcjr,f_finance_sup_wdzcjr,f_finance_sup_wdzbt";

    	/**
         * 投标审核人
         */
    	public static final String S_BID_CHECK_PERSON="s_menu_bidreply,s_bidreplymanage_dtbckq,s_bidreplymanage_ytbckq,s_bidreplymanage_yrwckq,s_bidreplymanage_jjzckq,s_bidreplymanage_yzbckq,s_bidreplymanage_wzbckq,s_bidreplymanage_tbshjlckq,s_bidreplymanage_tblyckq";
    	/**
         * 供应-废标审核人
         */
    	public static final String S_WASTE_BIAO_CHECK_PERSON="s_menu_bidreply,s_bidreplymanage_yzbckq,s_bidreplymanage_wzbckq,s_bidreplymanage_tbshjlckq,s_bidreplymanage_tblyckq";
    	/**
         * 供应-签署合同审核人
         */
    	public static final String S_CONTRACT_SIGN_CHECK_PERSON="s_menu_contract,s_contractmanage_wsxhtckq,s_contractmanage_ysxhtckq,s_contractmanage_bgqxzhtckq,s_contractmanage_zyzhtckq,s_contractmanage_dqrwchtckq,s_contractmanage_ywchtckq,s_contractmanage_htbgjlckq,s_contractmanage_xhtshjlckq,s_contractmanage_htbgshjlckq,s_contractmanage_htzzshjlckq";
    	/**
         * 供应-合同验收审核人
         */
    	public static final String S_CONTRACT_VERIFY_CHECK_PERSON="s_menu_contract_perform,s_contractrun_zxzrwckq,s_contractrun_ywcrwckq,s_contractrun_htzxshjlckq";
    	/**
         * 收款计划审核人
         */
    	public static final String S_RECEIVE_PLAN_CHECK_PERSON="s_menu_payment,s_paymanage_htskjhckq,s_paymanage_dqrskjhckq,s_paymanage_skjhshjlckq";
    	/**
         * 收款审核人
         */
    	public static final String S_RECEIVE_CHECK_PERSON="s_menu_payment,s_paymanage_sjskjhckq,s_paymanage_drskjhckq,s_paymanage_wdkckq,s_paymanage_skjhshjlckq";
    }
    
    /**
     * @Desc 模板前缀
     * @author liheng
     * @Date 2017/12/15
     */
    public static class TempletJeIdConstont{
		
    	/**
		 *  开始前缀
		 */
		public static final String  START = "startevent";
		
		/**
		 *  开始前缀
		 */
		public static final String  END = "endevent";
		
    	
		/**
		 *  任务前缀
		 */
		public static final String USER_TASK = "usertask";
		
		/**
		 *  容器前缀
		 */
		public static final String CONTAINER = "manualtask";
		
		/**
		 *  网关前缀
		 */
		public static final String GATEWAY = "exclusivegateway";
		
		/**
		 *  平行网关前缀
		 */
		public static final String PARA_GATEWAY = "parallelgateway";
		
		
		/**
		 *  主流程前缀
		 */
		public static final String  SERVICETASK = "servicetask";
		
		/**
		 *  线前缀
		 */
		public static final String  FLOW = "flow";
	}
	    /**
     * @author young
     *	app的待办的种类
     */
    public static class ToDoTypeForApp {
    	/**
    	 * 招标的待办
    	 */
    	public static final String BIDLIST = "bidList";
    	/**
    	 * 合同相关的待办
    	 */
    	public static final String CONTRACTLIST = "contractList";
    	/**
    	 * 资金相关的待办
    	 */
    	public static final String FUND = "fund";
    }
    /**
     * @author young
     *	合同的种类
     */
    public static class ContractFormType {
    	/**
    	 * 固定总价
    	 */
    	public static final String FIXATION_TOTAL_PRICE = "0";
    	/**
    	 * 固定单价
    	 */
    	public static final String FIXATION_PRICE = "1";
    	/**
    	 * 固定费率
    	 */
    	public static final String FIXATION_TAX = "2";
    	/**
    	 * 用户自定义
    	 */
    	public static final String CUSTOMERSIZE = "3";
    }
    /**
     * @author young
     *	APP的展示的合同的类型
     */
    public static class ContractFormTypeShow {
    	/**
    	 * 招标的待办
    	 */
    	public static final String FIXATION_TOTAL_PRICE = "固定总价";
    	/**
    	 * 合同相关的待办
    	 */
    	public static final String FIXATION_PRICE = "固定单价";
    	/**
    	 * 合同相关的待办
    	 */
    	public static final String FIXATION_TAX = "固定费率";
    	/**
    	 * 资金相关的待办
    	 */
    	public static final String CUSTOMERSIZE = "用户自定义";
    }

	/**
	 * 答疑留言标状态常量
	 * @author hss
	 * @Date 2017/12/16
	 * 
	 */
	public static class LeaveMsgConstont{

		/**
		 * 正在招标留言状态
		 */
		public static final Integer BID_B=2;
		
		/**
		 * 已入围 议标
		 */
		public static final Integer BID_C=3;
	}
	/**
	 * app获取任务审核的页面
	 * @author young 
	 * @Date 2017/12/19
	 * 
	 */
	public static class ApproveTypeForAPP{
		
		/**
		 * 招标的
		 */
		public static final String BIDLIST="bidList";
		
		/**
		 * 投标的
		 */
		public static final String BIDREPLY="bidReply";
		/**
		 * 合同
		 */
		public static final String CONTRACTLIST="contractList";
		
		/**
		 * 合同执行
		 */
		public static final String CONTRACTRUNLIST="contractRunList";
		/**
		 * 付款计划
		 */
		public static final String PAYMENTPLAN="paymentPlan";
		
	}
	
	/**
	 * app获取任务审核的页面
	 * @author young 
	 * @Date 2017/12/19
	 * 
	 */
	public static class ContractTabResourceIdForAPP{
		
		/**
		 * 未生效
		 */
		public static final Integer[] UNDO={68,109};
		
		/**
		 * 已生效
		 */
		public static final Integer[] APPROVED={69,110};
		/**
		 * 变更/取消中
		 */
		public static final Integer[] EDITCANCEL={70,111};
		
		/**
		 * 已变更已取消
		 */
		public static final Integer[] EDITEDCANCELED={0};
		/**
		 * 争议中
		 */
		public static final Integer[] DISPUTE={71,112};
		/**
		 * 确认合同完成
		 */
		public static final Integer[] CONFIRMCOMPLETED={72,113};
		/**
		 * 已完成
		 */
		public static final Integer[] FINISHED={73,114};
		
	}
	/**
	 * app 招标的tab页面
	 * @author young 
	 * @Date 2017/12/19
	 * 
	 */
	public static class BidListTabResourceIdForAPP{
		
		/**
		 * 未发布
		 */
		public static final Integer UNPUBLISH= 57;
		
		/**
		 * 正在发布
		 */
		public static final Integer PUBLISHING=58;
		/**
		 * 已入围议标
		 */
		public static final Integer TENDERFILTER=59;
		
		/**
		 * 竞价
		 */
		public static final Integer COMPETEPRICE=60;
		/**
		 * 中标
		 */
		public static final Integer DECISIONBID=61;
		/**
		 * 流标
		 */
		public static final Integer WASTEBID=62;
	}
	
	/**
	 * app 投标的tab页面
	 * @author young 
	 * @Date 2017/12/19
	 * 
	 */
	public static class BidReplyTabResourceIdForAPP{
		
		/**
		 * 未发布
		 */
		public static final Integer UNPUBLISH= 101;
		
		/**
		 * 正在发布
		 */
		public static final Integer PUBLISHING=102;
		/**
		 * 已入围议标
		 */
		public static final Integer TENDERFILTER=103;
		
		/**
		 * 竞价
		 */
		public static final Integer COMPETEPRICE=104;
		/**
		 * 中标
		 */
		public static final Integer DECISIONBID=105;
		/**
		 * 未中标
		 */
		public static final Integer WASTEBID=106;
	}
	
	/**
	 * app 询价的tab页面
	 * @author young 
	 * @Date 2017/12/19
	 * 
	 */
	public static class EnquiryTabResourceIdForAPP{
		
		/**
		 * 采购商未发布的询价
		 */
		public static final Integer PURCHARSE_UNPUBLISH_ENQUIRY= 87;
		
		/**
		 * 采购商正在发布的询价
		 */
		public static final Integer PURCHARSE_PUBLISHING_ENQUIRY=88;
		/**
		 * 采购商结束的询价
		 */
		public static final Integer PURCHARSE_FINISH_ENQUIRY=89;
		
		/**
		 *采购商发布询价的权限
		 */
		public static final Integer PURCHARSE_QUOTE_ENQUIRY=145;
		
		/**
		 * 供应商进行中的询价
		 */
		public static final Integer SUPPLIER_PUBLISHING_ENQUIRY=128;
		/**
		 * 供应商已结束询价
		 */
		public static final Integer SUPPLIER_FINISH_ENQUIRY=129;
		
		/**
		 *供应商询价报价按钮
		 */
		public static final Integer SUPPLIER_QUOTE_ENQUIRY=152;
	}
	
	/**
	 * app 询价的tab页面
	 * @author young 
	 * @Date 2017/12/19
	 * 
	 */
	public static class BankAuthType{
		
		/**
		 * 传统
		 */
		public static final Integer TRADITIONAL_AUTH_WAY= 1;
		
		/**
		 * 紧急
		 */
		public static final Integer EMERGENCY_AUTH_WAY=2;
	}


		/***
		 * 物流下单订单的状态
		 * @author young
		 */
		 public static class LogisticsOrderStatus{
			 /**
			  * 等待报价
			  */
			 public static final Integer WAITING_BID=1;
			 /**
			  * 待确认
			  */
			 public static final Integer WAITING_CONFIRM=2;
			 /**
			  * 待提货
			  */
			 public static final Integer WAITING_DELIVERY=3;
			 /**
			  * 在途中
			  */
			 public static final Integer ON_WAY=4;
			 
			 /**
			  * 已送达
			  */
			 public static final Integer BEEN_DELIVERY=5;
			 
			 /**
			  * 已完成
			  */
			 public static final Integer BEEN_PAY=7;
			 
			 /**
			  * 已取消
			  */
			 public static final Integer BEEN_CANCALE=8;
			 
			 /**
			  * 已过期
			  */
			 public static final Integer BEEN_EXPIRED=9;
			 
			 
		 }
		 /**
		  * 订单的物流的支付和验收状态
		  * @author dep
		  *
		  */
		 public static class LogisticsPayAndCheckStatus{
			 /**
			  * 待付款审核
			  */
			 public static final Integer WATTING_PAY_CHECK=1;
			 /**
			  * 付款审核中
			  */
			 public static final Integer PAY_CHECKING= 2;
			 
			 /**
			  * 付款审核被驳回
			  */
			 public static final Integer PAY_CHECK_REJECT= 3;
			 /**
			  * 付款审核通过--并设置主线为已完成
			  */
			 public static final Integer PAY_CHECK_PASS=4;
			 /**
			  * 待验收 --关联合同的发起验收
			  */
			 public static final Integer WAITING_CHECK=5;
			 /**
			  * 验收审核中
			  */
			 public static final Integer CHECKING=6;
			 /**
			  * 验收驳回
			  */
			 public static final Integer CHECKING_REJECT=7;
			 /**
			  * 已验收
			  */
			 public static final Integer APPROVED=8;
		 }
		 /**
		  * 物流的tab类型
		  * @author young
		  *
		  */
		 public static class LogisticsTabType{
			 /**
			  * 进行中
			  */
			 public static final String UNDERWAY="进行中"; 
			 /**
			  * 送达待支付
			  */
			 public static final String DELIVERY_TO_PAY="已送达待支付"; 
			 /**
			  * 已完成
			  */
			 public static final String COMPLETED="已完成";
			 /**
			  * 已取消/已过期 
			  */
			 public static final String CANCLE_EXPIRED="已过期/已取消"; 
		 }
		 
		 /**
		  * 物流相关的票据
		  * @author young
		  *
		  */
		 public static class LogisticsPictureType{
			 /**
			  * 运费票
			  */
			 public static final Integer FREIGHT_TICKET =1;
			 /**
			  * 仓库票
			  */
			 public static final Integer STORAGE_TICKET =2;
			 /**
			  * 仓库票据
			  */
			 public static final Integer STORAGE_INVOCE =5;
			 /**
			  * 签收单
			  */
			 public static final Integer SIGNATURE_INVOCE=6; 
			 /**
			  * 其他
			  */
			 public static final Integer OTHER_INVOCE=10;
			 /**
			  * 付款凭证信息
			  */
			 public static final Integer PAY_CERTIFICATION=11; 
		 }
		 
		 /**
		  *黑明单
		  * @author dep
		  *
		  */
		 public static class LogisticsIsBlackListStatus{
			 /**
			  * 在黑明单
			  */
			 public static final Integer ISBLACK=1;
			 
			 /**
			  * 不在黑明单
			  */
			 public static final Integer NOBLACK= 0;
			 
		 }
		 
		 /**
		  *付款是否开票
		  * @author dep
		  *
		  */
		 public static class LogisticsTicketStatus{
			 /**
			  * 未开票
			  */
			 public static final Integer NOT_TICKET=0;
			 
			 /**
			  * 已开票
			  */
			 public static final Integer TICKET= 1;
			 
		 }
		 
		 /**
		  * 物流相关的票据
		  * @author young
		  *
		  */
		 public static class LogisticsOverAndCancleReason{
			 /**
			  * 报价未确认过期
			  */
			 public static final Integer OVER_CONFIRM_TIME =1;
			 /**
			  * 无承运商报价过期
			  */
			 public static final Integer NO_QUTOED_PRICE =2;
			 /**
			  * 用户自主取消运单
			  */
			 public static final Integer CANCLE_ORDER =3;
		 }
		 
		 
		 public static class LogisticsExpectedPriceType{
			 /**
			  * 一口价
			  */
			 public static final Integer TOTAL_PRICE =1;
			 /**
			  * 单价
			  */
			 public static final Integer PRICE =2;
			 /**
			  * 其他
			  */
			 public static final Integer OTHER_CASE =3;
		 }
		 
		 
		 public static class LogisticsCanPublicsh{
			 /**
			  * 不可以发起
			  */
			 public static final Integer NOT_ALLOW =0;
			 /**
			  * 可以发起
			  */
			 public static final Integer ALLOW =1;
			 /**
			  * 被屏蔽
			  */
			 public static final Integer SHIELD =2;
		 }
		 
		 
		 public static class OrderStatus{

			 /**
		       * 新建/未验收
		      */
		    public static final Integer CREATE = 1;
		    
			/**
			  * 待完成/待验收
			  */
			public static final Integer STAY_COMPLETE = 2;
			 
			/**
			  * 已验收/待付款
			 */
			public static final Integer COMPLETED = 3;
			
			 
	        /**
	         * 5、付款中的任务
	         */
	        public static final Integer IN_PAY = 4;

	        /**
	         * 6、已付款待评价
	         */
	        public static final Integer PAYED = 5;

	        /**
	         * 7、已完成的订单
	         */
	        public static final Integer COMPLETED_ORDER = 6;
	        
	        /**
	         * 争议中的
	         */
	        public static final Integer IN_DISPUTE = 9;

	        /**
	         * 付款计划变更
	         */
	        public static final Integer PAYMENT_CHANGE = 10;
		 }

		 
		 public static class OrderType{
			 /**
			  * 订单
			  */
			 public static final Integer ORDER = 1;
			 
			 /**
			  * 预付款
			  */
			 public static final Integer ADVANCE = 2;
		 }
		 
		 /**
		  * 合同订单的物流的类型
		  * @author dep
		  *
		  */
		 public static class ContractLogisticsOrderType{
			 /**
			  * 自发物流
			  */
			 public static final Integer SELF = 1;
			 
			 /**
			  * 平台物流
			  */
			 public static final Integer PLATFORM = 2;
		 }
}
