package com.zhucai.credit.utils;

public class FormatTransfUtils {

	public static Long stringToLong(String str) {
		if(str != null && !"".equals(str)) {
			return Long.parseLong(str);
		}
		return 0l;
	}

}
