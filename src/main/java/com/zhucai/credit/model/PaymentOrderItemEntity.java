package com.zhucai.credit.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 支付订单明细表
 * @author xjz
 *
 */
@Entity
@Table(name = "finance_pay_order_item")
public class PaymentOrderItemEntity extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//业务编号
	@Column(nullable = false)
	private String serviceNo;
	//订单编号
	@Column(nullable = false)
	private String orderNo;
	//付款金额
	@Column(nullable = false)
	private BigDecimal payAmt;
	//付款时间
	@Column(nullable = false)
	private Date payTime;
	//订单完成时间
	@Column(nullable = true)
	private Date finishTime;
	//支付状态
	@Column(nullable = false,length = 2)
	private String payStatus;
	//支付方式
	@Column(nullable = false)
	private String payType;
	//流水号
	@Column(nullable = true,length = 100)
	private String flowNumber;
	//交易状态
	@Column(nullable = false,length = 20)
	private String tradeStatus;
	//备注
	@Column(nullable = true,length = 500)
	private String remark;
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getServiceNo() {
		return serviceNo;
	}
	public void setServiceNo(String serviceNo) {
		this.serviceNo = serviceNo;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public BigDecimal getPayAmt() {
		return payAmt;
	}
	public void setPayAmt(BigDecimal payAmt) {
		this.payAmt = payAmt;
	}
	public Date getPayTime() {
		return payTime;
	}
	public void setPayTime(Date payTime) {
		this.payTime = payTime;
	}
	public Date getFinishTime() {
		return finishTime;
	}
	public void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}
	public String getPayStatus() {
		return payStatus;
	}
	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}
	public String getFlowNumber() {
		return flowNumber;
	}
	public void setFlowNumber(String flowNumber) {
		this.flowNumber = flowNumber;
	}
	public String getTradeStatus() {
		return tradeStatus;
	}
	public void setTradeStatus(String tradeStatus) {
		this.tradeStatus = tradeStatus;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
}
