package com.zhucai.credit.bank.log;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.zhucai.credit.model.BaseEntity;

@Entity
@Table(name="finance_tp_log")
public class LogEntity extends BaseEntity {

	private static final long serialVersionUID = 1L;
	
	
	/**
	 * 第三方类型，比如ccb.e 表示建行e点通产品
	 */
	private String tpType;
	
	/**
	 * 第三方交易类型，比如 6W5503
	 */
	private String tpCode;
	
	/**
	 * 发起请求报文
	 */
	private String request;
	
	/**
	 * 返回的第三方KEY
	 */
	private String tpKey;
	
	/**
	 * 第三方返回
	 */
	private String response;

	@Column(length = 20)
	public String getTpType() {
		return tpType;
	}

	public void setTpType(String tpType) {
		this.tpType = tpType;
	}

	@Column(length = 50)
	public String getTpCode() {
		return tpCode;
	}

	public void setTpCode(String tpCode) {
		this.tpCode = tpCode;
	}

	@Column(nullable = true, length = 50)
	public String getTpKey() {
		return tpKey;
	}

	public void setTpKey(String tpKey) {
		this.tpKey = tpKey;
	}

	@Column(nullable = true,length = 4000)
	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	@Column(nullable = true, length = 8000)
	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
	
	
}
