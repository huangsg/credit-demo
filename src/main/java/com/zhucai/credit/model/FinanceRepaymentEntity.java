package com.zhucai.credit.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * 付款记录信息表
 * 2017年9月19日
 * @author hsg
 */
@Entity
@Table(name = "finance_payment_record")
public class FinanceRepaymentEntity extends BaseEntity{
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "还款商家类型（采购商，供应商）,")
	private String entType;
	@ApiModelProperty(value = "还款企业名称,")
	private String enterpriseName;
	@ApiModelProperty(value = "企业id,")
	private Long enterpriseId;
	@ApiModelProperty(value = "还款时间,")
	private Date paymentTime;
	@ApiModelProperty(value = "还款金额,")
	private BigDecimal paymentAmount;
	@ApiModelProperty(value = "组织机构代码,")
	private String orgNum;
	@ApiModelProperty(value = "发票号和发票代码,")
	private String invoiceNoCode;
	@ApiModelProperty(value = "关联的融资记录id")
	private Long outId;
	@ApiModelProperty(value = "反馈序号")
	private Integer serialNum;
	@Column(nullable = false, length = 12)
	public String getEntType() {
		return entType;
	}
	public void setEntType(String entType) {
		this.entType = entType;
	}
	@Column(nullable = true, length = 255)
	public String getEnterpriseName() {
		return enterpriseName;
	}
	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}
	@Column(nullable = true, length = 20)
	public Long getEnterpriseId() {
		return enterpriseId;
	}
	public void setEnterpriseId(Long enterpriseId) {
		this.enterpriseId = enterpriseId;
	}
	@Column(nullable = true, length = 12)
	public String getOrgNum() {
		return orgNum;
	}
	public void setOrgNum(String orgNum) {
		this.orgNum = orgNum;
	}
	@Column(nullable = false, length = 20)
	public Long getOutId() {
		return outId;
	}
	public void setOutId(Long outId) {
		this.outId = outId;
	}
	@Column(nullable = false, length = 20)
	public Date getPaymentTime() {
		return paymentTime;
	}
	public void setPaymentTime(Date paymentTime) {
		this.paymentTime = paymentTime;
	}
	@Column(nullable=true,columnDefinition = "decimal(20,2)")
	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	@Column(nullable = true, length = 50)
	public Integer getSerialNum() {
		return serialNum;
	}
	public void setSerialNum(Integer serialNum) {
		this.serialNum = serialNum;
	}
	@Column(nullable = true, length = 50)
	public String getInvoiceNoCode() {
		return invoiceNoCode;
	}
	public void setInvoiceNoCode(String invoiceNoCode) {
		this.invoiceNoCode = invoiceNoCode;
	}
	
}
