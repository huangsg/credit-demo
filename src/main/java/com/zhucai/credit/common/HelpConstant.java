package com.zhucai.credit.common;

import java.io.Serializable;


public class HelpConstant implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static class Status implements Serializable {
        /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
        /**
         */
        public static final int delete = 3;
        
        /**
         * @Fields invalid : 过期状态
         */
        public static final int overdue = 4;
      
    }
	/**
	 * @date 2017年11月29日 @user hsg
	 */
	public static class CurrencyFile implements Serializable {
		private static final long serialVersionUID = 1L;
		
		 /**
         * @Fields invalid : 江苏足财电子商务有限公司统一社会信用代码:913206913461626134
         */
		public static final String zhucai_num = "34616261-3";
	}
	/**
	 * 贷款机构资质申请是否过期
	 * @date 2017年11月29日 @user hsg
	 */
	public static class OverdueStatus implements Serializable {
		private static final long serialVersionUID = 1L;
		
		/**
		 * 是
		 * @Fields invalid : 
		 */
		public static final int yes = 1;
		/**
		 * 否
		 * @Fields invalid : 
		 */
		public static final int no = 0;
	}
	/**
	 * 贷款机构资质申请是否成功过
	 * @date 2017年11月29日 @user hsg
	 */
	public static class OverSuccess implements Serializable {
		private static final long serialVersionUID = 1L;
		
		/**
		 * 是
		 * @Fields invalid : 
		 */
		public static final int yes = 1;
		/**
		 * 否
		 * @Fields invalid : 
		 */
		public static final int no = 0;
	}

	
}
