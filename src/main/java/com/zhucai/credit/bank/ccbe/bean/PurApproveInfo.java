package com.zhucai.credit.bank.ccbe.bean;

import java.io.Serializable;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * 采购商审核供应商提交到建行接口的bean
 * 6W5502
 * @date 2017年11月28日 @user hsg
 */
public class PurApproveInfo implements Serializable{

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "请求方组织机构代码")
	private String request_orgn_num;
	@ApiModelProperty(value = "报名编号")
	private String app_num;
	@ApiModelProperty(value = "组织机构代码")
	private String orgn_num;
	@ApiModelProperty(value = "企业名称")
	private String customer_name;
	@ApiModelProperty(value = "在合作方的客户ID")
	private String partner_customer_id;
	@ApiModelProperty(value = "反馈状态")
	private String feedback_status_cd;
	@ApiModelProperty(value = "交易年限(交易年限，大于等于0的数字，单位为月)")
	private Double trade_years;
	@ApiModelProperty(value = "加贸年限")
	private Integer jmnx;
	@ApiModelProperty(value = "交易活跃度申请融资客户在平台的交易活跃度单选项。选项为：A B C D E(以上A为交易活跃度等级最高，E为交易活跃度等级最低）")
	private String trade_activity;
	@ApiModelProperty(value = "加贸行业排名")
	private Integer t_industry_ranking;
	@ApiModelProperty(value = "交易额排名比例（范围要求在大于0，小于等于1之间）")
	private Double trade_ranking;
	@ApiModelProperty(value = "入库量或交易量评价")
	private String trade_evaluate;
	@ApiModelProperty(value = "纠纷记录次数")
	private Integer dispute_times_num;
	@ApiModelProperty(value = "纠纷记录次数比例(范围要求在大于等于0，小于等于1之间)")
	private Double dispute_times;
	/**
	 * 在平台交易的交易对手的稳定性,单选项。选项为：
	 *	A-"5"
	 *	B-"4"      
	 *	C-"3"      
	 *	D-"2"      
	 *	E-"1"
	 *	（由建行确定编码规则）
	 *	（以上"5"为上下游稳定性最好，"1"为上下游稳定性最差）
	 */
	@ApiModelProperty(value = "上下游稳定性在平台交易的交易对手的稳定性,单选项。选项为：A-5 B-4 C-3 D-2 E-1")
	private String fluct_stabilization;
	@ApiModelProperty(value = "近两年国外交易对手的数量")
	private Integer trans_opponent_num;
	@ApiModelProperty(value = "上年加贸出口额占总销售额比重(范围要求在大于0，小于等于1之间)")
	private Double trade_amt_ranking;
	@ApiModelProperty(value = "配额许可证　")
	private Integer amt_licence;
	@ApiModelProperty(value = "近两年核销合同总量")
	private Integer contract_total_num;
	/**
	 * 质物变现能力,单选项：
	 *	A-"很强"
	 *	B-"强"      
	 *	C-"一般" 
	 *	D-"较弱" 
	 *	E-"很差" 
	 */
	@ApiModelProperty(value = "质物变现能力")
	private String cetr_sale_ability;
	/**
	 * 资金服务量评价,单选项。选项为： 
	 *	A：A
	 *	B：B
	 *	C：C
	 *	D：D
	 *	E：E
	 *	（以上A为资金服务量评价最高，E为资金服务量评价最低）
	 */
	@ApiModelProperty(value = "资金服务量评价")
	private String fund_serve_evaluate;
	/**
	 * 平台对客户的综合评价，（由建行确定编码规则，通知合作方遵守）
	 *	反馈状态为“0001:信用指标正常返回”时必填。
     *  单选项。
	 *	A：A
	 *	B：B
	 *	C：C
	 *	D：D
	 *	E：E
	 *	（以上A为综合评价最高，E为综合评价最低）
	 */
	@ApiModelProperty(value = "综合评价")
	private String multiple_evaluate;
	@ApiModelProperty(value = "建议额度值")
	private Integer limit_proposal;
	@ApiModelProperty(value = "经销商融资最高额度(经销商在各家银行合计的最高融资额度)")
	private Integer loan_most_limit;
	@ApiModelProperty(value = "经销商上一年订单总金额")
	private Integer last_year_order_amt;
	public String getRequest_orgn_num() {
		return request_orgn_num;
	}
	public void setRequest_orgn_num(String request_orgn_num) {
		this.request_orgn_num = request_orgn_num;
	}
	public String getApp_num() {
		return app_num;
	}
	public void setApp_num(String app_num) {
		this.app_num = app_num;
	}
	public String getOrgn_num() {
		return orgn_num;
	}
	public void setOrgn_num(String orgn_num) {
		this.orgn_num = orgn_num;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public String getPartner_customer_id() {
		return partner_customer_id;
	}
	public void setPartner_customer_id(String partner_customer_id) {
		this.partner_customer_id = partner_customer_id;
	}
	public String getFeedback_status_cd() {
		return feedback_status_cd;
	}
	public void setFeedback_status_cd(String feedback_status_cd) {
		this.feedback_status_cd = feedback_status_cd;
	}
	public Double getTrade_years() {
		return trade_years;
	}
	public void setTrade_years(Double trade_years) {
		this.trade_years = trade_years;
	}
	public Integer getJmnx() {
		return jmnx;
	}
	public void setJmnx(Integer jmnx) {
		this.jmnx = jmnx;
	}
	public String getTrade_activity() {
		return trade_activity;
	}
	public void setTrade_activity(String trade_activity) {
		this.trade_activity = trade_activity;
	}
	public Integer getT_industry_ranking() {
		return t_industry_ranking;
	}
	public void setT_industry_ranking(Integer t_industry_ranking) {
		this.t_industry_ranking = t_industry_ranking;
	}
	public Double getTrade_ranking() {
		return trade_ranking;
	}
	public void setTrade_ranking(Double trade_ranking) {
		this.trade_ranking = trade_ranking;
	}
	public String getTrade_evaluate() {
		return trade_evaluate;
	}
	public void setTrade_evaluate(String trade_evaluate) {
		this.trade_evaluate = trade_evaluate;
	}
	public Integer getDispute_times_num() {
		return dispute_times_num;
	}
	public void setDispute_times_num(Integer dispute_times_num) {
		this.dispute_times_num = dispute_times_num;
	}
	public Double getDispute_times() {
		return dispute_times;
	}
	public void setDispute_times(Double dispute_times) {
		this.dispute_times = dispute_times;
	}
	public String getFluct_stabilization() {
		return fluct_stabilization;
	}
	public void setFluct_stabilization(String fluct_stabilization) {
		this.fluct_stabilization = fluct_stabilization;
	}
	public Integer getTrans_opponent_num() {
		return trans_opponent_num;
	}
	public void setTrans_opponent_num(Integer trans_opponent_num) {
		this.trans_opponent_num = trans_opponent_num;
	}
	public Double getTrade_amt_ranking() {
		return trade_amt_ranking;
	}
	public void setTrade_amt_ranking(Double trade_amt_ranking) {
		this.trade_amt_ranking = trade_amt_ranking;
	}
	public Integer getAmt_licence() {
		return amt_licence;
	}
	public void setAmt_licence(Integer amt_licence) {
		this.amt_licence = amt_licence;
	}
	public Integer getContract_total_num() {
		return contract_total_num;
	}
	public void setContract_total_num(Integer contract_total_num) {
		this.contract_total_num = contract_total_num;
	}
	public String getCetr_sale_ability() {
		return cetr_sale_ability;
	}
	public void setCetr_sale_ability(String cetr_sale_ability) {
		this.cetr_sale_ability = cetr_sale_ability;
	}
	public String getFund_serve_evaluate() {
		return fund_serve_evaluate;
	}
	public void setFund_serve_evaluate(String fund_serve_evaluate) {
		this.fund_serve_evaluate = fund_serve_evaluate;
	}
	public String getMultiple_evaluate() {
		return multiple_evaluate;
	}
	public void setMultiple_evaluate(String multiple_evaluate) {
		this.multiple_evaluate = multiple_evaluate;
	}
	public Integer getLimit_proposal() {
		return limit_proposal;
	}
	public void setLimit_proposal(Integer limit_proposal) {
		this.limit_proposal = limit_proposal;
	}
	public Integer getLoan_most_limit() {
		return loan_most_limit;
	}
	public void setLoan_most_limit(Integer loan_most_limit) {
		this.loan_most_limit = loan_most_limit;
	}
	public Integer getLast_year_order_amt() {
		return last_year_order_amt;
	}
	public void setLast_year_order_amt(Integer last_year_order_amt) {
		this.last_year_order_amt = last_year_order_amt;
	}
	
}
