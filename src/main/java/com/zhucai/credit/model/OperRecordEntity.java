package com.zhucai.credit.model;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.wordnik.swagger.annotations.ApiModelProperty;
import com.zhucai.credit.common.CommonEnums.OperType;
import com.zhucai.credit.common.EnumHelper;

/**
 * 操作记录实体 2017年9月20日
 * 
 * @author hsg
 */
@Entity
@Table(name = "finance_oper_record")
public class OperRecordEntity extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "操作类型,Enum(OperType)")
	private String operType;
	@ApiModelProperty(value = "操作关键类型,")
	private String bizType;
	@ApiModelProperty(value = "操作关键字,")
	private String bizKey;
	@ApiModelProperty(value = "操作金额,")
	private BigDecimal operAmount;
	@ApiModelProperty(value = "操作结果,")
	private String result;
	@ApiModelProperty(value = "操作人id,")
	private Long operator;
	@ApiModelProperty(value = "操作人name,")
	private String operatorName;
	@ApiModelProperty(value = "操作描述,")
	private String review;
	@ApiModelProperty(value = "结果原因,")
	private String resultReason;
	@ApiModelProperty(value = "外部关联id,")
	private Long outId;
	@ApiModelProperty(value = "操作时间,")
	private Date operTime;

	/**
	 * 建行字段
	 */
	@ApiModelProperty(value = "交易活跃度 ,A,B,C,D,E(A为最好)")
	private String transactionActivity;
	@ApiModelProperty(value = "上下游稳定性 ,5,4,3,2,1(5为最好)")
	private String fluctStabilization;
	@ApiModelProperty(value = "交易年限 ,(单位年)")
	private Double tradingYears;
	@ApiModelProperty(value = "交易评价 ,A,B,C,D,E(A为最好)")
	private String transactionEvaluate;
	@ApiModelProperty(value = "纠纷次数 ,暂时和违约次数相同")
	private Integer disputeCount;

	/**
	 * 工行字段
	 */
	@ApiModelProperty(value = "历史交易年限（年）,-ICBC")
	private Double historyTransactionYears;
	@ApiModelProperty(value = "历史交易纠纷,-ICBC")
	private String historyDisputeCount;
	@ApiModelProperty(value = "历史交易坏账率(数值越低，代表坏账率越低，无坏账请填写0，必填项),-ICBC")
	private BigDecimal historyDebtRate;
	@ApiModelProperty(value = "退货率,-ICBC")
	private BigDecimal returnRate;
	@ApiModelProperty(value = "前三年首年供货总额,-ICBC")
	private BigDecimal threeYearsSupply;
	@ApiModelProperty(value = "前三年首年应收核心企业账款余额,-ICBC")
	private BigDecimal threeYearsReceivable;
	@ApiModelProperty(value = "前二年首年供货总额,-ICBC")
	private BigDecimal twoYearsSupply;
	@ApiModelProperty(value = "前二年首年应收核心企业账款余额,-ICBC")
	private BigDecimal twoYearsReceivable;
	@ApiModelProperty(value = "上年供货总额,-ICBC")
	private BigDecimal oneYearsSupply;
	@ApiModelProperty(value = "上年应收核心企业账款余额,-ICBC")
	private BigDecimal oneYearsReceivable;
	@ApiModelProperty(value = " 本年供货预计增长率(数值越大，代表增长率越大，1代表增长百分百，必填项),-ICBC")
	private BigDecimal thisYearsGrowthRate;
	
	@Column(nullable = true, length = 255)
	public String getResultReason() {
		return resultReason;
	}

	public void setResultReason(String resultReason) {
		this.resultReason = resultReason;
	}

	@Column(nullable = false, length = 20)
	public String getOperType() {
		return operType;
	}

	public void setOperType(String operType) {
		this.operType = operType;
	}

	@Column(nullable = false, length = 20)
	public String getBizKey() {
		return bizKey;
	}

	public void setBizKey(String bizKey) {
		this.bizKey = bizKey;
	}

	@Column(nullable = false, length = 255)
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@Column(nullable = false, length = 20)
	public Long getOperator() {
		return operator;
	}

	public void setOperator(Long operator) {
		this.operator = operator;
	}

	@Column(nullable = true, length = 200)
	public String getReview() {
		return review;
	}

	public void setReview(String review) {
		this.review = review;
	}

	@Column(nullable = false)
	public Date getOperTime() {
		return operTime;
	}

	public void setOperTime(Date operTime) {
		this.operTime = operTime;
	}

	@Column(nullable = true, length = 20)
	public Long getOutId() {
		return outId;
	}

	public void setOutId(Long outId) {
		this.outId = outId;
	}

	@Column(nullable = true, length = 50)
	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	@Column(nullable = true, columnDefinition = "decimal(20,2)")
	public BigDecimal getOperAmount() {
		return operAmount;
	}

	public void setOperAmount(BigDecimal operAmount) {
		this.operAmount = operAmount;
	}

	@Column(nullable = true, length = 20)
	public String getBizType() {
		return bizType;
	}

	public void setBizType(String bizType) {
		this.bizType = bizType;
	}

	@Column(nullable = true, length = 20)
	public String getTransactionActivity() {
		return transactionActivity;
	}

	public void setTransactionActivity(String transactionActivity) {
		this.transactionActivity = transactionActivity;
	}

	@Column(nullable = true, length = 20)
	public String getFluctStabilization() {
		return fluctStabilization;
	}

	public void setFluctStabilization(String fluctStabilization) {
		this.fluctStabilization = fluctStabilization;
	}

	@Column(nullable = true, length = 20)
	public Double getTradingYears() {
		return tradingYears;
	}

	public void setTradingYears(Double tradingYears) {
		this.tradingYears = tradingYears;
	}

	@Column(nullable = true, length = 20)
	public String getTransactionEvaluate() {
		return transactionEvaluate;
	}

	public void setTransactionEvaluate(String transactionEvaluate) {
		this.transactionEvaluate = transactionEvaluate;
	}

	@Column(nullable = true, length = 20)
	public Integer getDisputeCount() {
		return disputeCount;
	}

	public void setDisputeCount(Integer disputeCount) {
		this.disputeCount = disputeCount;
	}

	@Column(nullable = true)
	public Double getHistoryTransactionYears() {
		return historyTransactionYears;
	}

	public void setHistoryTransactionYears(Double historyTransactionYears) {
		this.historyTransactionYears = historyTransactionYears;
	}

	@Column(nullable = true, length = 50)
	public String getHistoryDisputeCount() {
		return historyDisputeCount;
	}

	public void setHistoryDisputeCount(String historyDisputeCount) {
		this.historyDisputeCount = historyDisputeCount;
	}

	@Column(nullable = true)
	public BigDecimal getHistoryDebtRate() {
		return historyDebtRate;
	}

	public void setHistoryDebtRate(BigDecimal historyDebtRate) {
		this.historyDebtRate = historyDebtRate;
	}

	@Column(nullable = true)
	public BigDecimal getReturnRate() {
		return returnRate;
	}

	public void setReturnRate(BigDecimal returnRate) {
		this.returnRate = returnRate;
	}

	@Column(nullable = true)
	public BigDecimal getThreeYearsSupply() {
		return threeYearsSupply;
	}

	public void setThreeYearsSupply(BigDecimal threeYearsSupply) {
		this.threeYearsSupply = threeYearsSupply;
	}

	@Column(nullable = true)
	public BigDecimal getThreeYearsReceivable() {
		return threeYearsReceivable;
	}

	public void setThreeYearsReceivable(BigDecimal threeYearsReceivable) {
		this.threeYearsReceivable = threeYearsReceivable;
	}

	@Column(nullable = true)
	public BigDecimal getTwoYearsSupply() {
		return twoYearsSupply;
	}

	public void setTwoYearsSupply(BigDecimal twoYearsSupply) {
		this.twoYearsSupply = twoYearsSupply;
	}

	@Column(nullable = true)
	public BigDecimal getTwoYearsReceivable() {
		return twoYearsReceivable;
	}

	public void setTwoYearsReceivable(BigDecimal twoYearsReceivable) {
		this.twoYearsReceivable = twoYearsReceivable;
	}

	@Column(nullable = true)
	public BigDecimal getOneYearsSupply() {
		return oneYearsSupply;
	}

	public void setOneYearsSupply(BigDecimal oneYearsSupply) {
		this.oneYearsSupply = oneYearsSupply;
	}

	@Column(nullable = true)
	public BigDecimal getOneYearsReceivable() {
		return oneYearsReceivable;
	}

	public void setOneYearsReceivable(BigDecimal oneYearsReceivable) {
		this.oneYearsReceivable = oneYearsReceivable;
	}

	@Column(nullable = true)
	public BigDecimal getThisYearsGrowthRate() {
		return thisYearsGrowthRate;
	}

	public void setThisYearsGrowthRate(BigDecimal thisYearsGrowthRate) {
		this.thisYearsGrowthRate = thisYearsGrowthRate;
	}

	public static String getLongToString(Date longTime) {
		if (longTime == null) {
			return null;
		}
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sf.format(longTime);
	}

	@Transient
	public String getOperTimeStr() {
		return getLongToString(operTime);
	}

	@Transient
	public String getOperTypeDesc() {
		EnumHelper.getDescribe(OperType.valueOf(operType));
		return EnumHelper.getDescribe(OperType.valueOf(operType));
	}
}
