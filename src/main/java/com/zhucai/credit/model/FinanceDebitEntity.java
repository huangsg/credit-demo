package com.zhucai.credit.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * 扣款记录信息表
 * 2017年9月19日
 * @author hsg
 */
@Entity
@Table(name = "finance_debit_record")
public class FinanceDebitEntity extends BaseEntity{
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "扣款商家类型（采购商，供应商）,")
	private String entType;
	@ApiModelProperty(value = "扣款企业名称,")
	private String enterpriseName;
	@ApiModelProperty(value = "企业id,")
	private Long enterpriseId;
	@ApiModelProperty(value = "扣款时间,")
	private Date debitTime;
	@ApiModelProperty(value = "组织机构代码,")
	private String orgNum;
	@ApiModelProperty(value = "支用编号,")
	private String payoutNum;
	@ApiModelProperty(value = "建行的合同编号,")
	private String contractNumCcb;
	@ApiModelProperty(value = "关联的融资记录id")
	private Long outId;
	@ApiModelProperty(value = "扣款类型")
	private String deductInd;
	@ApiModelProperty(value = "反馈序号")
	private Integer serialNum;
	@ApiModelProperty(value = "归还贷款本金")
	private BigDecimal pay_principal_amt;
	@ApiModelProperty(value = "一般结算户扣款金额")
	private BigDecimal clearance_account_amt;
	@ApiModelProperty(value = "风险池扣款金额")
	private BigDecimal risk_pool_amt;
	@ApiModelProperty(value = "保证金账户扣款金额")
	private BigDecimal guarantee_account_amt;
	@ApiModelProperty(value = "代还账户扣款金额")
	private BigDecimal platform_account_amt;
	@ApiModelProperty(value = "此次所有扣款金额")
	private BigDecimal allAmt;
	@Column(nullable = false, length = 12)
	public String getEntType() {
		return entType;
	}
	public void setEntType(String entType) {
		this.entType = entType;
	}
	@Column(nullable = true, length = 50)
	public String getEnterpriseName() {
		return enterpriseName;
	}
	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}
	@Column(nullable = true, length = 20)
	public Long getEnterpriseId() {
		return enterpriseId;
	}
	public void setEnterpriseId(Long enterpriseId) {
		this.enterpriseId = enterpriseId;
	}
	@Column(nullable = true, length = 20)
	public String getOrgNum() {
		return orgNum;
	}
	public void setOrgNum(String orgNum) {
		this.orgNum = orgNum;
	}
	@Column(nullable = false, length = 50)
	public String getPayoutNum() {
		return payoutNum;
	}
	public void setPayoutNum(String payoutNum) {
		this.payoutNum = payoutNum;
	}
	@Column(nullable = true, length = 20)
	public Long getOutId() {
		return outId;
	}
	public void setOutId(Long outId) {
		this.outId = outId;
	}
	@Column(nullable = true, length = 50)
	public String getContractNumCcb() {
		return contractNumCcb;
	}
	public void setContractNumCcb(String contractNumCcb) {
		this.contractNumCcb = contractNumCcb;
	}
	@Column(nullable = true)
	public Date getDebitTime() {
		return debitTime;
	}
	
	public void setDebitTime(Date debitTime) {
		this.debitTime = debitTime;
	}
	@Column(nullable = true, length = 20)
	public String getDeductInd() {
		return deductInd;
	}
	public void setDeductInd(String deductInd) {
		this.deductInd = deductInd;
	}
	@Column(nullable = true, length = 50)
	public Integer getSerialNum() {
		return serialNum;
	}
	public void setSerialNum(Integer serialNum) {
		this.serialNum = serialNum;
	}
	@Column(nullable=true,columnDefinition = "decimal(20,2)")
	public BigDecimal getPay_principal_amt() {
		return pay_principal_amt;
	}
	public void setPay_principal_amt(BigDecimal pay_principal_amt) {
		this.pay_principal_amt = pay_principal_amt;
	}
	@Column(nullable=true,columnDefinition = "decimal(20,2)")
	public BigDecimal getClearance_account_amt() {
		return clearance_account_amt;
	}
	public void setClearance_account_amt(BigDecimal clearance_account_amt) {
		this.clearance_account_amt = clearance_account_amt;
	}
	@Column(nullable=true,columnDefinition = "decimal(20,2)")
	public BigDecimal getRisk_pool_amt() {
		return risk_pool_amt;
	}
	public void setRisk_pool_amt(BigDecimal risk_pool_amt) {
		this.risk_pool_amt = risk_pool_amt;
	}
	@Column(nullable=true,columnDefinition = "decimal(20,2)")
	public BigDecimal getGuarantee_account_amt() {
		return guarantee_account_amt;
	}
	public void setGuarantee_account_amt(BigDecimal guarantee_account_amt) {
		this.guarantee_account_amt = guarantee_account_amt;
	}
	@Column(nullable=true,columnDefinition = "decimal(20,2)")
	public BigDecimal getPlatform_account_amt() {
		return platform_account_amt;
	}
	public void setPlatform_account_amt(BigDecimal platform_account_amt) {
		this.platform_account_amt = platform_account_amt;
	}
	@Column(nullable=true,columnDefinition = "decimal(20,2)")
	public BigDecimal getAllAmt() {
		return allAmt;
	}
	public void setAllAmt(BigDecimal allAmt) {
		this.allAmt = allAmt;
	}
	
	
}
