package com.zhucai.credit.service.impl.transactional;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zhucai.credit.bean.PaymentOrderItem;
import com.zhucai.credit.common.CommonEnums.CreditStatus;
import com.zhucai.credit.model.CreditRecordEntrity;
import com.zhucai.credit.model.PaymentOrderItemEntity;
import com.zhucai.credit.repository.CashierOrderItemRepository;
import com.zhucai.credit.repository.CreditRecordRepository;
import com.zhucai.credit.repository.CreditRecordTimeRepository;
import com.zhucai.credit.service.cashier.CashierOrderItemService;
import com.zhucai.credit.utils.SqlResultUtil;

@Service("cashierOrderItemService")
@com.alibaba.dubbo.config.annotation.Service(version="1.0.0")
public class CashierOrderItemServiceImplT implements CashierOrderItemService{
	
	@Autowired
	private CashierOrderItemRepository cashierOrderItemRepository;
	@Autowired
	private CreditRecordTimeRepository creditRecordTimeRepository;
	@Autowired
	private CreditRecordRepository creditRecordRepository;
	@Autowired
	private CreditUserServiceUtil creditUserServiceUtil;

	@Override
	public void save(PaymentOrderItem item) {
		//根据融资单号查询融资纪录
		CreditRecordEntrity entrity = creditRecordRepository.findByRecordNo(item.getOrderNo());
		//新增线上付款成功历时融资记录
		creditUserServiceUtil.createRecordTime(entrity.getTid(), CreditStatus.WAIT_PAY.toString());
		

		//封装并保存支付明细表
		PaymentOrderItemEntity entity = new PaymentOrderItemEntity();
		entity.setFlowNumber(item.getFlowNumber());
		entity.setOrderNo(item.getOrderNo());
		entity.setServiceNo(item.getServiceNo());
		entity.setPayAmt(item.getPayAmt());
		entity.setPayStatus(item.getPayStatus());
		entity.setPayType(item.getPayType());
		entity.setTradeStatus(item.getTradeStatus());
		entity.setPayTime(item.getPayTime());
		entity.setCreateUser(item.getUserId());
		cashierOrderItemRepository.save(entity);
	}

	@Override
	public int findOrderItemByFlowNumber(String orderId,String channelSerialNum,String payType) {
//		PaymentOrderItemEntity list = null;
		if("unionpay".equals(payType)) {
			PaymentOrderItemEntity list = cashierOrderItemRepository.findOrderItemByFlowNumber(orderId,channelSerialNum,"UNIONPAY");
			if(list != null) {
				return 1;
			}else {
				return 0;
			}
		}else {
			PaymentOrderItemEntity list = cashierOrderItemRepository.findOrderItemByFlowNumber(orderId,channelSerialNum,"ALIPAY");
			if(list != null) {
				return 1;
			}else {
				return 0;
			}
		}
		
	}

	@Override
	public List<PaymentOrderItem> findByOrderNoLike(String orderNo,List<Long> createUserList) {
		List<Object[]> objList = cashierOrderItemRepository.findByOrderNoLike(orderNo,createUserList);
		//fpoi.id,fpoi.pay_amt,fpoi.flow_number,fpoi.order_no,fpo.product_remark
		List<PaymentOrderItem> itemList = new ArrayList<>();
		if(objList != null && objList.size() > 0) {
			for (Object[] obj : objList) {
				PaymentOrderItem item = new PaymentOrderItem();
				item.setId(SqlResultUtil.getSqlResultLong(obj[0]));
				item.setPayAmt(SqlResultUtil.getSqlResultBigDecimal(obj[1]));
				item.setFlowNumber(SqlResultUtil.getSqlResultString(obj[2]));
				item.setOrderNo(SqlResultUtil.getSqlResultString(obj[3]));
				item.setProductName(SqlResultUtil.getSqlResultString(obj[4]));
				itemList.add(item);
			}
		}
		return itemList;
	}

}
