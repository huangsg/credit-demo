package com.zhucai.credit.icbc;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.zhucai.credit.utils.StrongJsoup;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GYLINFOUP_PROD {
	
	@Autowired
	private TemplateEngine templateEngine;
	 
	
	/**
	 * 生产测试用的
	 * @version 2018年8月25日
	 * @throws Exception
	 */
	@Test
    public void build() throws Exception{
		Date d = new Date();
		SimpleDateFormat fmtTranDate = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat fmtTranTime = new SimpleDateFormat("HHmmssSSS");
		
  
        //String cis = "111190001617073";
		String cis = "111190001616306";
        String bankCode = "102";
        String id = "JSZCW01.y.1111";
        long fSeqno = d.getTime();
        String CODE = "GYLINFOUP";
        String tranDate = fmtTranDate.format(d);
        String tranTime = fmtTranTime.format(d);
        
        
        Context context = new Context();
		context.setVariable("CIS",  cis);
		context.setVariable("BankCode",  bankCode);
		context.setVariable("ID",  id);
        
		context.setVariable("TranDate",  tranDate);
		context.setVariable("TranTime",  tranTime);
		context.setVariable("fSeqno",  fSeqno);
		context.setVariable("TotalNum",  "1");
		context.setVariable("TotalAmt",  "11161862");
		//context.setVariable("TotalAmt",  "2146216");
		
		context.setVariable("TranType",  "1");
		context.setVariable("SignTime",  tranDate+tranTime);
        context.setVariable("AccNo",  "1111425709000063395");
        context.setVariable("GYLCode",  "GY-2013000003219");

        context.setVariable("iSeqno",  System.currentTimeMillis());
        context.setVariable("VouchType",  "01");
        context.setVariable("VouchCode",  "3200172130");
        context.setVariable("VouchNo",  "22355751");
        //context.setVariable("VouchNo",  "22355752");

        
        context.setVariable("DueAmt",  "11161862");
        //context.setVariable("DueAmt",  "2146216");
        
        context.setVariable("DueDate",  "20180425");
        context.setVariable("PlanDate",  "20181024");
        context.setVariable("MemberName",  "江苏足财供应链管理有限公司");
        context.setVariable("MemberCode",  "MA1P5RBW-4");
       // context.setVariable("MemberAccNo",  "1001708229006846128");
        context.setVariable("Status",  "1");


        
        String ip = "58.247.178.190";
       // String ip = "192.168.1.14";
		String signUrl = "http://" + ip + ":449";
        StrongJsoup connect = StrongJsoup.connect(signUrl);
        String x = "<?xml version=\"1.0\" encoding=\"GBK\"?>\r\n"+templateEngine.process("icbc/"+CODE, context);
        System.out.println(x);
        
		connect.headers(ImmutableMap.of("Content-Type", "INFOSEC_SIGN/1.0","Content-Length",x.getBytes().length+""));
		
		connect.getJsoupConn().postDataCharset("GBK").request().requestBody(x);
		
		Document post1 = connect.post(1);
		Elements eles = post1.getElementsByTag("sign");
		String signText = eles.text();
		//System.out.println(signText);
        
        
        
		String url = "http://"+ip+":448/servlet/ICBCCMPAPIReqServlet?userID="+id+"&PackageID="+fSeqno+"&SendTime="+tranDate+tranTime;
		StrongJsoup connect1 = StrongJsoup.connect(url);
		connect1.headers(ImmutableMap.of("Content-Type", "application/x-www-form-urlencoded"));
		
		Map<String,String> params = Maps.newHashMap();
		params.put("Version", "0.0.0.1");
		params.put("TransCode",CODE);
		params.put("BankCode",bankCode);
		params.put("GroupCIS",cis);
		params.put("ID",id);
		params.put("Cert","");
		params.put("reqData",signText);
		//System.out.println(url);
		
		//System.out.println(params);
		connect1.getJsoupConn().data(params);
		
		Document post = connect1.post();
		String text = post.text();
		System.out.println(post);
		
		Base64 base64 = new Base64();
		String substring = text.substring(text.indexOf("=")+1);
		//System.out.println(substring);
		byte[] decode = base64.decode(substring);
		System.out.println(new String(decode,"GBK"));
        
        
        
    }


}
