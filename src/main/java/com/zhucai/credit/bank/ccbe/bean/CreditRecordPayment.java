package com.zhucai.credit.bank.ccbe.bean;

import java.io.Serializable;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * 付款信息反馈
 * 6W5564
 * @date 2017年11月28日 @user hsg
 */
public class CreditRecordPayment implements Serializable{

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "当前最大反馈序号")
	private Integer max_serial_num;
	@ApiModelProperty(value = "反馈序号")
	private Integer serial_num;
	@ApiModelProperty(value = "企业名称")
	private String cust_name;
	@ApiModelProperty(value = "在合作方的客户ID")
	private String cooper_cust_num;
	@ApiModelProperty(value = "单据号(客户的发票号，多张(发票号@|@发票号@|@...))")
	private String bill_num;
	@ApiModelProperty(value = "付款金额（此次付款每张发票的付款金额，与上面单据号一一对应，用@|@分隔）")
	private String bill_amt;
	@ApiModelProperty(value = "归还本金")
	private Double return_princial_amt;
	@ApiModelProperty(value = "归还利息")
	private Double return_interest_amt;
	@ApiModelProperty(value = "尾款金额")
	private Double balance_amt;
	@ApiModelProperty(value = "本次付款，是否成功销账的标识(1-是;0-否)")
	private String write_off_ind;
	@ApiModelProperty(value = "币种代码156人民币元840美元")
	private String currency_cd;
	public Integer getMax_serial_num() {
		return max_serial_num;
	}
	public void setMax_serial_num(Integer max_serial_num) {
		this.max_serial_num = max_serial_num;
	}
	public Integer getSerial_num() {
		return serial_num;
	}
	public void setSerial_num(Integer serial_num) {
		this.serial_num = serial_num;
	}
	public String getCust_name() {
		return cust_name;
	}
	public void setCust_name(String cust_name) {
		this.cust_name = cust_name;
	}
	public String getCooper_cust_num() {
		return cooper_cust_num;
	}
	public void setCooper_cust_num(String cooper_cust_num) {
		this.cooper_cust_num = cooper_cust_num;
	}
	public String getBill_num() {
		return bill_num;
	}
	public void setBill_num(String bill_num) {
		this.bill_num = bill_num;
	}
	public String getBill_amt() {
		return bill_amt;
	}
	public void setBill_amt(String bill_amt) {
		this.bill_amt = bill_amt;
	}
	public double getReturn_princial_amt() {
		return return_princial_amt;
	}
	public void setReturn_princial_amt(double return_princial_amt) {
		this.return_princial_amt = return_princial_amt;
	}
	public double getReturn_interest_amt() {
		return return_interest_amt;
	}
	public void setReturn_interest_amt(double return_interest_amt) {
		this.return_interest_amt = return_interest_amt;
	}
	public double getBalance_amt() {
		return balance_amt;
	}
	public void setBalance_amt(double balance_amt) {
		this.balance_amt = balance_amt;
	}
	public String getWrite_off_ind() {
		return write_off_ind;
	}
	public void setWrite_off_ind(String write_off_ind) {
		this.write_off_ind = write_off_ind;
	}
	public String getCurrency_cd() {
		return currency_cd;
	}
	public void setCurrency_cd(String currency_cd) {
		this.currency_cd = currency_cd;
	}
	
}
