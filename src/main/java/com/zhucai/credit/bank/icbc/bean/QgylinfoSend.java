package com.zhucai.credit.bank.icbc.bean;

import java.io.Serializable;
/**
 * 合同签订查询请求实体
 * @author xjz
 *
 */
public class QgylinfoSend implements Serializable{

	private static final long serialVersionUID = 1L;
	
	//核心企业账号 *
	private String accNo;
	//会员企业账号
	private String memberAccNo;
	//供应链编号 *
	private String gylCode;
	//会员企业代码类型 (1、企业法人代码证书代码 ,2、营业执照 , 3、行政机关 , 4、社会团体  （本期只支持上送1）)
	private Integer memberType;
	//会员企业单位代码
	private String memberCode;
	//应付账款状态 * (1 未付清 , 2 已付清 , 3 已确认 , 4 待确认 , 5 可融资)
	private String status;
	//起始日期 * (格式YYYYMMDD)
	private String beginDate;
	//结束日期 * (格式YYYYMMDD)
	private String endDate;
	//查询下页标识 (查询首页上送空；其他页需与银行返回包提供的一致)
	private String nextTag;
	//请求备用字段1
	private String reqReserved1;
	//请求备用字段2
	private String reqReserved2;
	public String getAccNo() {
		return accNo;
	}
	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}
	public String getMemberAccNo() {
		return memberAccNo;
	}
	public void setMemberAccNo(String memberAccNo) {
		this.memberAccNo = memberAccNo;
	}
	public String getGylCode() {
		return gylCode;
	}
	public void setGylCode(String gylCode) {
		this.gylCode = gylCode;
	}
	public Integer getMemberType() {
		return memberType;
	}
	public void setMemberType(Integer memberType) {
		this.memberType = memberType;
	}
	public String getMemberCode() {
		return memberCode;
	}
	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getNextTag() {
		return nextTag;
	}
	public void setNextTag(String nextTag) {
		this.nextTag = nextTag;
	}
	public String getReqReserved1() {
		return reqReserved1;
	}
	public void setReqReserved1(String reqReserved1) {
		this.reqReserved1 = reqReserved1;
	}
	public String getReqReserved2() {
		return reqReserved2;
	}
	public void setReqReserved2(String reqReserved2) {
		this.reqReserved2 = reqReserved2;
	}
	

}
