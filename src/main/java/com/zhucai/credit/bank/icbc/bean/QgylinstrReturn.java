package com.zhucai.credit.bank.icbc.bean;

import java.io.Serializable;

/**
 * 付款查询返回实体
 * @author xjz
 *
 */
public class QgylinstrReturn implements Serializable{

	private static final long serialVersionUID = 1L;
	//待查指令包序列号
	private String qryfSeqno;
	//待查平台交易序列号
	private String qrySerialNo;
	//响应备用字段1
	private String repReserved1;
	//响应备用字段2
	private String repReserved2;
	//指令包顺序号
	private String iSeqno;
	//待查指令包顺序号
	private String qryiSeqno;
	//待查平台交易顺序号
	private String qryOrderNo;
	//凭证种类(01 发票   99 其他)
	private String vouchType;
	//凭证代码
	private String vouchCode;
	//凭证号码
	private String vouchNo;
	//票面金额
	private Long billAmt;
	//应付账款金额
	private Long dueAmt;
	//验收单号
	private String billNo;
	//合同编号
	private String contactNo;
	//应付账款生成日期
	private String dueDate;
	//应付账款编号
	private String dueCode;
	//计划付款日期
	private String planDate;
	//供应商全称
	private String memberName;
	//供应商监管账号
	private String memAccNo;
	//会员企业代码类型
	private String memberType;
	//会员企业单位代码
	private String memberCode;
	//状态
	private String status;
	//宽限期
	private String delayDay;
	//指令状态    	  0:提交成功,等待银行处理（当确认应付账款，提交人直接拒绝时使用）
    //			  1:授权成功, 等待银行处理
	//	          2:等待授权
	//	          5:超期作废
	//	          6:指令处理失败
	//	          7:处理成功
	//	          8:指令被拒绝授权
	private String result;
	//返回码
	private String instrRetCode;
	//交易返回描述
	private String instrRetMsg;
	//查询返回码
	private String iRetCode;
	//查询返回描述
	private String iRetMsg;
	//响应备用字段3
	private String repReserved3;
	//响应备用字段4
	private String repReserved4;
	public String getQryfSeqno() {
		return qryfSeqno;
	}
	public void setQryfSeqno(String qryfSeqno) {
		this.qryfSeqno = qryfSeqno;
	}
	public String getQrySerialNo() {
		return qrySerialNo;
	}
	public void setQrySerialNo(String qrySerialNo) {
		this.qrySerialNo = qrySerialNo;
	}
	public String getRepReserved1() {
		return repReserved1;
	}
	public void setRepReserved1(String repReserved1) {
		this.repReserved1 = repReserved1;
	}
	public String getRepReserved2() {
		return repReserved2;
	}
	public void setRepReserved2(String repReserved2) {
		this.repReserved2 = repReserved2;
	}
	public String getiSeqno() {
		return iSeqno;
	}
	public void setiSeqno(String iSeqno) {
		this.iSeqno = iSeqno;
	}
	public String getQryiSeqno() {
		return qryiSeqno;
	}
	public void setQryiSeqno(String qryiSeqno) {
		this.qryiSeqno = qryiSeqno;
	}
	public String getQryOrderNo() {
		return qryOrderNo;
	}
	public void setQryOrderNo(String qryOrderNo) {
		this.qryOrderNo = qryOrderNo;
	}
	public String getVouchType() {
		return vouchType;
	}
	public void setVouchType(String vouchType) {
		this.vouchType = vouchType;
	}
	public String getVouchCode() {
		return vouchCode;
	}
	public void setVouchCode(String vouchCode) {
		this.vouchCode = vouchCode;
	}
	public String getVouchNo() {
		return vouchNo;
	}
	public void setVouchNo(String vouchNo) {
		this.vouchNo = vouchNo;
	}
	public Long getBillAmt() {
		return billAmt;
	}
	public void setBillAmt(Long billAmt) {
		this.billAmt = billAmt;
	}
	public Long getDueAmt() {
		return dueAmt;
	}
	public void setDueAmt(Long dueAmt) {
		this.dueAmt = dueAmt;
	}
	public String getBillNo() {
		return billNo;
	}
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getDueCode() {
		return dueCode;
	}
	public void setDueCode(String dueCode) {
		this.dueCode = dueCode;
	}
	public String getPlanDate() {
		return planDate;
	}
	public void setPlanDate(String planDate) {
		this.planDate = planDate;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getMemAccNo() {
		return memAccNo;
	}
	public void setMemAccNo(String memAccNo) {
		this.memAccNo = memAccNo;
	}
	public String getMemberType() {
		return memberType;
	}
	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}
	public String getMemberCode() {
		return memberCode;
	}
	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDelayDay() {
		return delayDay;
	}
	public void setDelayDay(String delayDay) {
		this.delayDay = delayDay;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getInstrRetCode() {
		return instrRetCode;
	}
	public void setInstrRetCode(String instrRetCode) {
		this.instrRetCode = instrRetCode;
	}
	public String getInstrRetMsg() {
		return instrRetMsg;
	}
	public void setInstrRetMsg(String instrRetMsg) {
		this.instrRetMsg = instrRetMsg;
	}
	public String getiRetCode() {
		return iRetCode;
	}
	public void setiRetCode(String iRetCode) {
		this.iRetCode = iRetCode;
	}
	public String getiRetMsg() {
		return iRetMsg;
	}
	public void setiRetMsg(String iRetMsg) {
		this.iRetMsg = iRetMsg;
	}
	public String getRepReserved3() {
		return repReserved3;
	}
	public void setRepReserved3(String repReserved3) {
		this.repReserved3 = repReserved3;
	}
	public String getRepReserved4() {
		return repReserved4;
	}
	public void setRepReserved4(String repReserved4) {
		this.repReserved4 = repReserved4;
	}
	
	

}
