package com.zhucai.credit.bank.ccbe.bean;

import java.io.Serializable;
import java.util.Date;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * 账款受让信息反馈
 * 6W5549
 * @date 2017年11月28日 @user hsg
 */
public class CreditRecordFeedback  implements Serializable{

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "当前最大反馈序号")
	private Integer max_serial_num;
	@ApiModelProperty(value = "反馈序号")
	private Integer serial_num;
	@ApiModelProperty(value = "企业名称")
	private String cust_name;
	@ApiModelProperty(value = "在合作方的客户ID")
	private String cooper_cust_num;
	@ApiModelProperty(value = "单据号")
	private String bill_num;
	@ApiModelProperty(value = "账款到期日")
	private Date ar_due_date;
	@ApiModelProperty(value = "是否生效(是否生效的标识(1：受让成功；2：拒绝受让))")
	private String success_ind;
	@ApiModelProperty(value = "币种代码156人民币元840美元")
	private String currency_cd;
	//新增字段
	@ApiModelProperty(value = "")
	private String origctr_id;
	@ApiModelProperty(value = "020002909483")
	private String credit_no;
	@ApiModelProperty(value = "RZBH320647400201700008")
	private String contract_num;
	
	public Integer getMax_serial_num() {
		return max_serial_num;
	}
	public void setMax_serial_num(Integer max_serial_num) {
		this.max_serial_num = max_serial_num;
	}
	public Integer getSerial_num() {
		return serial_num;
	}
	public void setSerial_num(Integer serial_num) {
		this.serial_num = serial_num;
	}
	public String getCust_name() {
		return cust_name;
	}
	public void setCust_name(String cust_name) {
		this.cust_name = cust_name;
	}
	public String getCooper_cust_num() {
		return cooper_cust_num;
	}
	public void setCooper_cust_num(String cooper_cust_num) {
		this.cooper_cust_num = cooper_cust_num;
	}
	public String getBill_num() {
		return bill_num;
	}
	public void setBill_num(String bill_num) {
		this.bill_num = bill_num;
	}
	
	public Date getAr_due_date() {
		return ar_due_date;
	}
	public void setAr_due_date(Date ar_due_date) {
		this.ar_due_date = ar_due_date;
	}
	public String getSuccess_ind() {
		return success_ind;
	}
	public void setSuccess_ind(String success_ind) {
		this.success_ind = success_ind;
	}
	public String getCurrency_cd() {
		return currency_cd;
	}
	public void setCurrency_cd(String currency_cd) {
		this.currency_cd = currency_cd;
	}
	public String getOrigctr_id() {
		return origctr_id;
	}
	public void setOrigctr_id(String origctr_id) {
		this.origctr_id = origctr_id;
	}
	public String getCredit_no() {
		return credit_no;
	}
	public void setCredit_no(String credit_no) {
		this.credit_no = credit_no;
	}
	public String getContract_num() {
		return contract_num;
	}
	public void setContract_num(String contract_num) {
		this.contract_num = contract_num;
	}
	
}
