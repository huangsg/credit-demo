package com.zhucai.credit.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.zhucai.credit.bank.log.LogService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LogTest {

	@Autowired
	private LogService logService;
	
	@Test
	public void contextLoads() {
		Long newRequest = logService.getLog("ccb.e", "6W5503");
		logService.requestLog(newRequest, "reqeust");
		logService.responseLog(newRequest, "response", "123");
	}

}
