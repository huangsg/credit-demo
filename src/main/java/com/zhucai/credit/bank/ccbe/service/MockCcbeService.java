package com.zhucai.credit.bank.ccbe.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.xml.sax.InputSource;

import com.google.common.collect.Lists;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import com.thoughtworks.xstream.io.xml.XppDriver;
import com.zhucai.credit.bank.ccbe.bean.CreditRecordDebit;
import com.zhucai.credit.bank.ccbe.bean.CreditRecordFeedback;
import com.zhucai.credit.bank.ccbe.bean.CreditRecordPayment;
import com.zhucai.credit.bank.ccbe.bean.DrawFinancingMoney;
import com.zhucai.credit.bank.ccbe.bean.PayApplicationReceiveInfo;
import com.zhucai.credit.bank.ccbe.bean.PlatformReceive;
import com.zhucai.credit.bank.ccbe.bean.PurApproveInfo;
import com.zhucai.credit.bank.ccbe.bean.SignUpFeedback;
import com.zhucai.credit.bank.ccbe.bean.SignUpFeedbackTwo;
import com.zhucai.credit.bank.ccbe.bean.SignUpInfo;
import com.zhucai.credit.bank.ccbe.request.RequestBean;
import com.zhucai.credit.bank.ccbe.request.RequestBeans;
import com.zhucai.credit.bank.utils.MapEntryConverter;

@PropertySource(value = {"${external.properties.location}"}, ignoreResourceNotFound = true)
@Service
public class MockCcbeService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private RequestFactory requestFactory;
	
	@Value("${mock.path.name}")
	private  String mockPath;
	
	
	public List<SignUpInfo> signUpInfo() throws Exception {
		final String txCode = "6W5501";

		RequestBean requestBean = requestFactory.getRequestBean(txCode);
		// 结束号都给空
		requestBean.addTxInfo("end_serial_num", "");
		// 补充参数
		// request 序列化
		String request = getRequestXML(requestBean);

		logger.debug(request);
		String fileName = "6W5501.txt";
		String filePathName = "response";
		String response =  getFileInfo(filePathName,fileName);
		if(response == null){
			return null;
		}
		logger.debug(response);
		// 转对象
		try {
			Element txInfo = getTxInfo(response);
//			String maxSerialNum = txInfo.getChildText("max_serial_num");

//			logService.responseLog(logId, response, maxSerialNum);
			List<SignUpInfo> list = Lists.newArrayList();
			List<Element> children = txInfo.getChildren("LIST");
			for(Element e : children) {
				SignUpInfo result = new SignUpInfo();
				String app_num = e.getChild("app_num").getValue();
				String customer_app_serial_num = e.getChild("customer_app_serial_num").getValue();
				String orgn_num = e.getChild("orgn_num").getValue();
				String customer_name = e.getChild("customer_name").getValue();
				String applicant_name = e.getChild("applicant_name").getValue();
				String applicant_fix_phone = e.getChild("applicant_fix_phone").getValue();
				String applicant_phone = e.getChild("applicant_phone").getValue();
				String partner_customer_id = e.getChild("partner_customer_id").getValue();
				String apply_amt = e.getChild("apply_amt").getValue();
				String customer_short_name = e.getChild("customer_short_name").getValue();
				String xssr_snd = e.getChild("xssr_snd").getValue();
				String belong_industry = e.getChild("belong_industry").getValue();
				String co_Entp_Org_InsNo = e.getChild("Co_Entp_Org_InsNo").getValue();
				String pltfrm_Nm = e.getChild("Pltfrm_Nm").getValue();
				
				result.setApp_num(app_num);
				result.setApplicant_fix_phone(applicant_fix_phone);
				result.setApplicant_name(applicant_name);
				result.setApplicant_phone(applicant_phone);
				result.setApply_amt(BigDecimal.valueOf(Double.parseDouble(apply_amt)));
				result.setBelong_industry(belong_industry);
				result.setCo_Entp_Org_InsNo(co_Entp_Org_InsNo);
				result.setCustomer_app_serial_num(Integer.parseInt(customer_app_serial_num));
				result.setCustomer_name(customer_name);
				result.setCustomer_short_name(customer_short_name);
				result.setOrgn_num(orgn_num);
				result.setPartner_customer_id(partner_customer_id);
				result.setPltfrm_Nm(pltfrm_Nm);
				result.setXssr_snd(BigDecimal.valueOf(Double.parseDouble(xssr_snd)));
				
				
				list.add(result);
				
			}

			
			return list;
		} catch (Exception e) {
//			logService.responseLog(logId, response, null);
			throw e;
		}

	}
	
	
	public void purApproveInfo(PurApproveInfo purApproveInfo) throws Exception {
		final String txCode = "6W5502";
		
		RequestBean requestBean = requestFactory.getRequestBean(txCode);
		// 结束号都给空
		requestBean.addTxInfo("end_serial_num", "");

	
		// 补充参数
		requestBean.addTxInfo("request_orgn_num", purApproveInfo.getRequest_orgn_num());
		requestBean.addTxInfo("app_num", purApproveInfo.getApp_num());
		requestBean.addTxInfo("orgn_num", purApproveInfo.getOrgn_num());
		requestBean.addTxInfo("partner_customer_id", purApproveInfo.getPartner_customer_id());
		requestBean.addTxInfo("feedback_status_cd", purApproveInfo.getFeedback_status_cd());
		if(purApproveInfo.getTrade_years()==null){
			requestBean.addTxInfo("trade_years", "");
		}else {
			requestBean.addTxInfo("trade_years", Double.toString(purApproveInfo.getTrade_years()));
		}
		if(purApproveInfo.getJmnx()==null) {
			requestBean.addTxInfo("jmnx", "");
		}else {
			requestBean.addTxInfo("jmnx", purApproveInfo.getJmnx().toString());
		}
		requestBean.addTxInfo("trade_activity", purApproveInfo.getTrade_activity());
		if(purApproveInfo.getT_industry_ranking()==null) {
			requestBean.addTxInfo("t_industry_ranking","");
		}else {
			requestBean.addTxInfo("t_industry_ranking",purApproveInfo.getT_industry_ranking().toString());
		}
		if(purApproveInfo.getTrade_ranking()==null) {
			requestBean.addTxInfo("trade_ranking","");
		}else {
			requestBean.addTxInfo("trade_ranking",Double.toString(purApproveInfo.getTrade_ranking()));
		}
		requestBean.addTxInfo("trade_evaluate",purApproveInfo.getTrade_evaluate());
		if(purApproveInfo.getDispute_times_num()==null) {
			requestBean.addTxInfo("dispute_times_num","");
		}else {
			requestBean.addTxInfo("dispute_times_num",purApproveInfo.getDispute_times_num().toString());
		}
		if(purApproveInfo.getDispute_times()==null) {
			requestBean.addTxInfo("dispute_times","");
		}else {
			requestBean.addTxInfo("dispute_times",Double.toString(purApproveInfo.getDispute_times()));
		}
		requestBean.addTxInfo("fluct_stabilization",purApproveInfo.getFluct_stabilization());
		if(purApproveInfo.getTrans_opponent_num()==null) {
			requestBean.addTxInfo("trans_opponent_num","");
		}else {
			requestBean.addTxInfo("trans_opponent_num",purApproveInfo.getTrans_opponent_num().toString());
		}
		if(purApproveInfo.getTrade_amt_ranking()==null) {
			requestBean.addTxInfo("trade_amt_ranking","");
		}else {
			requestBean.addTxInfo("trade_amt_ranking",Double.toString(purApproveInfo.getTrade_amt_ranking()));
		}
		if(purApproveInfo.getTrade_amt_ranking()==null) {
			requestBean.addTxInfo("trade_amt_ranking","");
		}else {
			requestBean.addTxInfo("trade_amt_ranking",Double.toString(purApproveInfo.getTrade_amt_ranking()));
		}
		if(purApproveInfo.getAmt_licence()==null) {
			requestBean.addTxInfo("amt_licence","");
		}else {
			requestBean.addTxInfo("amt_licence",purApproveInfo.getAmt_licence().toString());
		}
		if(purApproveInfo.getContract_total_num()==null) {
			requestBean.addTxInfo("contract_total_num","");
		}else {
			requestBean.addTxInfo("contract_total_num",purApproveInfo.getContract_total_num().toString());
		}
		requestBean.addTxInfo("cetr_sale_ability",purApproveInfo.getCetr_sale_ability());
		requestBean.addTxInfo("fund_serve_evaluate",purApproveInfo.getFund_serve_evaluate());
		requestBean.addTxInfo("multiple_evaluate",purApproveInfo.getMultiple_evaluate());
		if(purApproveInfo.getLimit_proposal()==null) {
			requestBean.addTxInfo("limit_proposal","");
		}else {
			requestBean.addTxInfo("limit_proposal",purApproveInfo.getLimit_proposal().toString());
		}
		requestBean.addTxInfo("limit_proposal",purApproveInfo.getLimit_proposal().toString());
		if(purApproveInfo.getLoan_most_limit()==null) {
			requestBean.addTxInfo("loan_most_limit","");
		}else {
			requestBean.addTxInfo("loan_most_limit",Double.toString(purApproveInfo.getLoan_most_limit()));
		}
		if(purApproveInfo.getLast_year_order_amt()==null) {
			requestBean.addTxInfo("last_year_order_amt","");
		}else {
			requestBean.addTxInfo("last_year_order_amt",Double.toString(purApproveInfo.getLast_year_order_amt()));
		}
		
		// request 序列化
		String request = getRequestXML(requestBean);
		
		logger.debug(request);
		String fileName = "6W5502.xml";
		String filePathName = "request";
		String response =  getFileInfo(filePathName,fileName);
		if(response == null){
			return;
		}
		logger.debug(response);
		// 转对象
		try {
			Element txInfo = getTxInfo(response);
//			String maxSerialNum = txInfo.getChildText("max_serial_num");
			
//			logService.responseLog(logId, response, maxSerialNum);
//			List<PurApproveInfo> list = Lists.newArrayList();
			List<Element> children = txInfo.getChildren();
			for(Element e : children) {
//				PurApproveInfo result = new PurApproveInfo();
				//TODO
				
				System.out.println(e);
			}
			
		} catch (Exception e) {
//			logService.responseLog(logId, response, null);
			throw e;
		}
		
	}
	
	public List<SignUpFeedback> signUpFeedback() throws Exception {
		final String txCode = "6W5503";

		RequestBean requestBean = requestFactory.getRequestBean(txCode);
		// 结束号都给空
		requestBean.addTxInfo("end_serial_num", "");
		
		// 补充参数
		// request 序列化
		String request = getRequestXML(requestBean);

		logger.debug(request);
		String fileName = "6W5503.txt";
		String filePathName = "response";
		String response =  getFileInfo(filePathName,fileName);
		if(response == null){
			return null;
		}
		logger.debug(response);
		// 转对象
		try {
			Element txInfo = getTxInfo(response);
//			String maxSerialNum = txInfo.getChildText("max_serial_num");

//			logService.responseLog(logId, response, maxSerialNum);
			List<SignUpFeedback> list = Lists.newArrayList();
			List<Element> children = txInfo.getChildren("LIST");
			for(Element e : children) {
				SignUpFeedback result = new SignUpFeedback();
				String application_state_serial_num = e.getChild("application_state_serial_num").getValue();
				String orgn_num = e.getChild("orgn_num").getValue();
				String app_num = e.getChild("app_num").getValue();
				String customer_name = e.getChild("customer_name").getValue();
				String approve_pass_ind = e.getChild("approve_pass_ind").getValue();
				String unpass_reason = e.getChild("unpass_reason").getValue();
				String partner_customer_id = e.getChild("partner_customer_id").getValue();
				String marketing_brand_name = e.getChild("marketing_brand_name").getValue();
				String co_Entp_Org_InsNo = e.getChild("Co_Entp_Org_InsNo").getValue();
				String pltfrm_Nm = e.getChild("Pltfrm_Nm").getValue();
				
				
				
				result.setApp_num(app_num);
				result.setApplication_state_serial_num(Integer.parseInt(application_state_serial_num));
				result.setApprove_pass_ind(approve_pass_ind);
				result.setCo_Entp_Org_InsNo(co_Entp_Org_InsNo);
				result.setCo_Entp_Org_InsNo(co_Entp_Org_InsNo);
				result.setCustomer_name(customer_name);
				result.setMarketing_brand_name(marketing_brand_name);
				result.setOrgn_num(orgn_num);
				result.setPartner_customer_id(partner_customer_id);
				result.setUnpass_reason(unpass_reason);
				result.setPltfrm_Nm(pltfrm_Nm);
				System.out.println(e);
				list.add(result);
			}
			
			return list;
		} catch (Exception e) {
//			logService.responseLog(logId, response, null);
			throw e;
		}

	}
	
	public List<SignUpFeedbackTwo> signUpFeedbackTwo() throws Exception {
		final String txCode = "6W5519";

		RequestBean requestBean = requestFactory.getRequestBean(txCode);
		// 结束号都给空
		requestBean.addTxInfo("end_serial_num", "");
		
		
		// 补充参数
		// request 序列化
		String request = getRequestXML(requestBean);

		logger.debug(request);
		String fileName = "6W5519.txt";
		String filePathName = "response";
		String response =  getFileInfo(filePathName,fileName);
		if(response == null){
			return null;
		}
		logger.debug(response);
		// 转对象
		try {
			Element txInfo = getTxInfo(response);
//			String maxSerialNum = txInfo.getChildText("max_serial_num");

//			logService.responseLog(logId, response, maxSerialNum);
			List<SignUpFeedbackTwo> list = Lists.newArrayList();
			List<Element> children = txInfo.getChildren("LIST");
			for(Element e : children) {
				SignUpFeedbackTwo result = new SignUpFeedbackTwo();
				String serial_num = e.getChild("serial_num").getValue();
				String cust_name = e.getChild("cust_name").getValue();
				String contract_num = e.getChild("contract_num").getValue();
				String cooper_cust_num = e.getChild("cooper_cust_num").getValue();
				String clearance_account = e.getChild("clearance_account").getValue();
				String recover_account = e.getChild("recover_account").getValue();
				String currency_cd = e.getChild("currency_cd").getValue();
				String cust_orgn_code = e.getChild("cust_orgn_code").getValue();
				String r_cust_orgn_code = e.getChild("r_cust_orgn_code").getValue();
				String start_date = e.getChild("start_date").getValue();
				String exdr_date = e.getChild("exdr_date").getValue();
				String regist_num = e.getChild("regist_num").getValue();
				String contract_amt = e.getChild("contract_amt").getValue();
				
				result.setClearance_account(clearance_account);
				result.setContract_amt(contract_amt);
				result.setContract_num(contract_num);
				result.setCooper_cust_num(cooper_cust_num);
				result.setCurrency_cd(currency_cd);
				result.setCust_name(cust_name);
				result.setR_cust_orgn_code(r_cust_orgn_code);
				result.setCust_orgn_code(cust_orgn_code);
				result.setExdr_date(exdr_date);
				result.setRecover_account(recover_account);
				result.setRegist_num(regist_num);
				result.setSerial_num(Integer.parseInt(serial_num));
				result.setStart_date(start_date);
				list.add(result);
				
			}
			
			return list;
		} catch (Exception e) {
//			logService.responseLog(logId, response, null);
			throw e;
		}

	}
	
	public Map<String,String> platformReceive(List<PlatformReceive> list) throws Exception {
		final String txCode = "6W5574";
		
		RequestBeans requestBeans = requestFactory.getRequestBeans(txCode);
		List<Map<String,String>> requestBeanList = new ArrayList<>();
		// 补充参数
		for (PlatformReceive platformReceive : list) {
			Map<String,String> map = new HashMap<>();
			map.put("request_orgn_num", platformReceive.getRequest_orgn_num());
			map.put("cust_orgn_code", platformReceive.getCust_orgn_code());
			map.put("r_cust_orgn_code", platformReceive.getR_cust_orgn_code());
			map.put("relate_cust_name", platformReceive.getRelate_cust_name());
			map.put("contract_num", platformReceive.getContract_num());
			map.put("bill_num", platformReceive.getBill_num());
			map.put("bill_balance_amt", platformReceive.getBill_balance_amt());
			map.put("bill_date", platformReceive.getBill_date().toString());
			map.put("credit_period", platformReceive.getCredit_period().toString());
			map.put("b_supplement_group", platformReceive.getB_supplement_group());
			requestBeanList.add(map);
			
		}
		requestBeans.setTX_INFO(requestBeanList);
		// request 序列化
		String request = getRequestXML(requestBeans);
		
		logger.debug(request);
		String fileName = "6W5574.txt";
		String filePathName = "response";
		String response =  getFileInfo(filePathName,fileName);
		if(response == null){
			return null;
		}
		logger.debug(response);
		// 转对象
		try {
			Element txInfo = getTxInfo(response);
//			String maxSerialNum = txInfo.getChildText("max_serial_num");
			
//			logService.responseLog(logId, response, maxSerialNum);
			Map<String,String> map = new HashMap<String,String>();
			List<Element> children = txInfo.getChildren("TEXT_INFO");
			for(Element e : children) {
				//TODO
				String receive_state = e.getChild("receive_state").getValue();
				String fail_reason = e.getChild("fail_reason").getValue();
				map.put("fail_reason", fail_reason);
				map.put("receive_state", receive_state);
				System.out.println(e);
			}
			return map;
			
		} catch (Exception e) {
//			logService.responseLog(logId, response, null);
			throw e;
		}
	}
	
	public List<CreditRecordFeedback> creditRecordFeedback() throws Exception {
		final String txCode = "6W5549";

		RequestBean requestBean = requestFactory.getRequestBean(txCode);
		
		// 结束号都给空
		requestBean.addTxInfo("end_serial_num", "");
	
		// 补充参数
		// request 序列化
		String request = getRequestXML(requestBean);

		logger.debug(request);
		String fileName = "6W5549.txt";
		String filePathName = "response";
		String response =  getFileInfo(filePathName,fileName);
		if(response == null){
			return null;
		}
		logger.debug(response);
		// 转对象
		try {
			Element txInfo = getTxInfo(response);
//			String maxSerialNum = txInfo.getChildText("max_serial_num");

//			logService.responseLog(logId, response, maxSerialNum);
			List<CreditRecordFeedback> list = Lists.newArrayList();
			List<Element> children = txInfo.getChildren("LIST");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			for(Element e : children) {
				CreditRecordFeedback result = new CreditRecordFeedback();
				//TODO
				String serial_num = e.getChild("serial_num").getValue();
				String cust_name = e.getChild("cust_name").getValue();
				String cooper_cust_num = e.getChild("cooper_cust_num").getValue();
				String bill_num = e.getChild("bill_num").getValue();
				String ar_due_date = e.getChild("ar_due_date").getValue();
				//将字符串转换成日期
				Date date = sdf.parse(ar_due_date);
				String success_ind = e.getChild("success_ind").getValue();
				String currency_cd = e.getChild("currency_cd").getValue();
				String origctr_id = e.getChild("origctr_id").getValue();
				String credit_no = e.getChild("credit_no").getValue();
				String contract_num = e.getChild("contract_num").getValue();
				
				result.setSerial_num(Integer.parseInt(serial_num));
				result.setCust_name(cust_name);
				result.setCooper_cust_num(cooper_cust_num);
				result.setBill_num(bill_num);
				result.setAr_due_date(date);
				result.setSuccess_ind(success_ind);
				result.setCurrency_cd(currency_cd);
				result.setOrigctr_id(origctr_id);
				result.setCredit_no(credit_no);
				result.setContract_num(contract_num);
				list.add(result);
				
			}
			
			return list;
		} catch (Exception e) {
//			logService.responseLog(logId, response, null);
			throw e;
		}

	}
	
	public List<DrawFinancingMoney> drawFinancingMoney() throws Exception {
		final String txCode = "6W5528";
		
		RequestBean requestBean = requestFactory.getRequestBean(txCode);
		// 结束号都给空
		requestBean.addTxInfo("end_serial_num", "");

		// 补充参数
		// request 序列化
		String request = getRequestXML(requestBean);
		
		logger.debug(request);
		String fileName = "6W5528.txt";
		String filePathName = "response";
		String response =  getFileInfo(filePathName,fileName);
		if(response == null){
			return null;
		}
		logger.debug(response);
		// 转对象
		try {
			Element txInfo = getTxInfo(response);
//			String maxSerialNum = txInfo.getChildText("max_serial_num");
			
//			logService.responseLog(logId, response, maxSerialNum);
			List<DrawFinancingMoney> list = Lists.newArrayList();
			List<Element> children = txInfo.getChildren("LIST");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			for(Element e : children) {
				DrawFinancingMoney result = new DrawFinancingMoney();
				//TODO
				String serial_num = e.getChild("serial_num").getValue();
				String bill_num = e.getChild("bill_num").getValue();
				String cust_name = e.getChild("cust_name").getValue();
				String contract_num = e.getChild("contract_num").getValue();
				String cooper_cust_num = e.getChild("cooper_cust_num").getValue();
				String payout_num = e.getChild("payout_num").getValue();
				String currency_cd = e.getChild("currency_cd").getValue();
				String prepayment_amt = e.getChild("prepayment_amt").getValue();
				String entity_amt = e.getChild("entity_amt").getValue();
				String loan_date = e.getChild("loan_date").getValue();
				String prepayment_due_date = e.getChild("prepayment_due_date").getValue();
				String loan_ind = e.getChild("loan_ind").getValue();
				String fail_reason = e.getChild("fail_reason").getValue();
				String origctr_id = e.getChild("origctr_id").getValue();
				String intrt = e.getChild("intrt").getValue();
				String fnc_rto = e.getChild("fnc_rto").getValue();
				String credit_no = e.getChild("credit_no").getValue();
				
				result.setBill_num(bill_num);
				result.setContract_num(contract_num);
				result.setCooper_cust_num(cooper_cust_num);
				result.setCredit_no(credit_no);
				result.setCurrency_cd(currency_cd);
				result.setCust_name(cust_name);
				result.setEntity_amt(Double.parseDouble(entity_amt));
				result.setFail_reason(fail_reason);
				result.setFnc_rto(fnc_rto);
				result.setIntrt(intrt);
				result.setLoan_date(sdf.parse(loan_date));
				result.setOrigctr_id(origctr_id);
				result.setPayout_num(payout_num);
				result.setPrepayment_amt(Double.parseDouble(prepayment_amt));
				result.setPrepayment_due_date(sdf.parse(prepayment_due_date));
				result.setSerial_num(Integer.parseInt(serial_num));
				result.setLoan_ind(loan_ind);
				list.add(result);
			}
			
			return list;
		} catch (Exception e) {
//			logService.responseLog(logId, response, null);
			throw e;
		}
		
	}
	
	public List<CreditRecordDebit> creditRecordDebit() throws Exception {
		final String txCode = "6W5550";
		
		RequestBean requestBean = requestFactory.getRequestBean(txCode);
		// 结束号都给空
		requestBean.addTxInfo("end_serial_num", "");
		
		// 补充参数
		// request 序列化
		String request = getRequestXML(requestBean);
		
		logger.debug(request);
		String fileName = "6W5550.txt";
		String filePathName = "response";
		String response =  getFileInfo(filePathName,fileName);
		if(response == null){
			return null;
		}
		logger.debug(response);
		// 转对象
		try {
			Element txInfo = getTxInfo(response);
//			String maxSerialNum = txInfo.getChildText("max_serial_num");
			
//			logService.responseLog(logId, response, maxSerialNum);
			List<CreditRecordDebit> list = Lists.newArrayList();
			List<Element> children = txInfo.getChildren("LIST");
			for(Element e : children) {
				CreditRecordDebit result = new CreditRecordDebit();
				//TODO
				String serial_num = e.getChild("serial_num").getValue();
				String bill_num = e.getChild("bill_num").getValue();
				String cust_name = e.getChild("cust_name").getValue();
				String contract_num = e.getChild("contract_num").getValue();
				String payout_num = e.getChild("payout_num").getValue();
				String deduct_ind = e.getChild("deduct_ind").getValue();
				String currency_cd = e.getChild("currency_cd").getValue();
				String pay_principal_amt = e.getChild("pay_principal_amt").getValue();
				String pay_interest_amt = e.getChild("pay_interest_amt").getValue();
				String recover_account_amt = e.getChild("recover_account_amt").getValue();
				String clearance_account_amt = e.getChild("clearance_account_amt").getValue();
				String supervise_account_amt = e.getChild("supervise_account_amt").getValue();
				String risk_pool_amt = e.getChild("risk_pool_amt").getValue();
				String guarantee_account_amt = e.getChild("guarantee_account_amt").getValue();
				String platform_account_amt = e.getChild("platform_account_amt").getValue();
				String owe_principal_amt = e.getChild("owe_principal_amt").getValue();
				String owe_interest_amt = e.getChild("owe_interest_amt").getValue();
				String owe_cost_amt = e.getChild("owe_cost_amt").getValue();
				
				result.setSerial_num(Integer.parseInt(serial_num));
				result.setBill_num(bill_num);
				result.setCust_name(cust_name);
				result.setContract_num(contract_num);
				result.setPayout_num(payout_num);
				result.setDeduct_ind(deduct_ind);
				result.setCurrency_cd(currency_cd);
				result.setPay_principal_amt(Double.parseDouble(pay_principal_amt));
				result.setPay_interest_amt(Double.parseDouble(pay_interest_amt));
				result.setRecover_account_amt(Double.parseDouble(recover_account_amt));
				result.setClearance_account_amt(Double.parseDouble(clearance_account_amt));
				result.setSupervise_account_amt(Double.parseDouble(supervise_account_amt));
				result.setRisk_pool_amt(Double.parseDouble(risk_pool_amt));
				result.setGuarantee_account_amt(Double.parseDouble(guarantee_account_amt));
				result.setPlatform_account_amt(Double.parseDouble(platform_account_amt));
				result.setOwe_principal_amt(Double.parseDouble(owe_principal_amt));
				result.setOwe_interest_amt(Double.parseDouble(owe_interest_amt));
				result.setOwe_cost_amt(Double.parseDouble(owe_cost_amt));
				list.add(result);
			}
			
			return list;
		} catch (Exception e) {
//			logService.responseLog(logId, response, null);
			throw e;
		}
		
	}
	public Map<String,String> payApplicationReceiveInfo(List<PayApplicationReceiveInfo> list) throws Exception {
		final String txCode = "6W5548";
		
		
		RequestBeans requestBeans = requestFactory.getRequestBeans(txCode);
		List<Map<String,String>> requestBeanList = new ArrayList<>();
		// 补充参数
		for (PayApplicationReceiveInfo payApplicationReceiveInfo : list) {
			Map<String,String> map = new HashMap<>();
			map.put("request_orgn_num", payApplicationReceiveInfo.getRequest_orgn_num());
			map.put("customer_source", payApplicationReceiveInfo.getCustomer_source());
			map.put("contract_num", payApplicationReceiveInfo.getContract_num());
			map.put("cooper_cust_num", payApplicationReceiveInfo.getCooper_cust_num());
			map.put("cust_orgn_code", payApplicationReceiveInfo.getCust_orgn_code());
			map.put("bill_num", payApplicationReceiveInfo.getBill_num());
			map.put("currency_cd", payApplicationReceiveInfo.getCurrency_cd());
			map.put("payment_amt", payApplicationReceiveInfo.getPayment_amt());
			if(payApplicationReceiveInfo.getPayment_date()!=null) {
				map.put("payment_date", payApplicationReceiveInfo.getPayment_date().toString());
			}else {
				map.put("payment_date", "");
			}
			map.put("payment_mode", payApplicationReceiveInfo.getPayment_mode());
			if(payApplicationReceiveInfo.getPayment_cash_amt()!=null) {
				map.put("payment_cash_amt", String.valueOf(payApplicationReceiveInfo.getPayment_cash_amt()));
			}else {
				map.put("payment_cash_amt", "");
			}
			if(payApplicationReceiveInfo.getPayment_cash_amt()!=null) {
				map.put("payment_silver_amt", String.valueOf(payApplicationReceiveInfo.getPayment_silver_amt()));
			}else {
				map.put("payment_silver_amt", "");
			}
			map.put("account_type", payApplicationReceiveInfo.getAccount_type());
			requestBeanList.add(map);
			
		}
		requestBeans.setTX_INFO(requestBeanList);
		
		// request 序列化
		String request = getRequestXML(requestBeans);
		
		logger.debug(request);
		String fileName = "6W5548.xml";
		String filePathName = "request";
		String response =  getFileInfo(filePathName,fileName);
		if(response == null){
			return null;
		}
		logger.debug(response);
		// 转对象
		try {
			Element txInfo = getTxInfo(response);
//			String maxSerialNum = txInfo.getChildText("max_serial_num");
			
//			logService.responseLog(logId, response, maxSerialNum);
			Map<String,String> map = new HashMap<String,String>();
			List<Element> children = txInfo.getChildren("TEXT_INFO");
			for(Element e : children) {
				//PurApproveInfo result = new PurApproveInfo();
				//TODO
				String receive_state = e.getChild("receive_state").getValue();
				String fail_reason = e.getChild("fail_reason").getValue();
				map.put("fail_reason", fail_reason);
				map.put("receive_state", receive_state);
				System.out.println(e);
			}
			return map;
			
		} catch (Exception e) {
//			logService.responseLog(logId, response, null);
			throw e;
		}
	}
	public List<CreditRecordPayment> creditRecordPayment() throws Exception {
		final String txCode = "6W5564";
		
		RequestBean requestBean = requestFactory.getRequestBean(txCode);
		// 结束号都给空
		requestBean.addTxInfo("end_serial_num", "");
		
		// 补充参数
		// request 序列化
		String request = getRequestXML(requestBean);
		
		logger.debug(request);
		String fileName = "6W5564.txt";
		String filePathName = "response";
		String response =  getFileInfo(filePathName,fileName);
		if(response == null){
			return null;
		}
		logger.debug(response);
		// 转对象
		try {
			Element txInfo = getTxInfo(response);
//			String maxSerialNum = txInfo.getChildText("max_serial_num");
			
//			logService.responseLog(logId, response, maxSerialNum);
			List<CreditRecordPayment> list = Lists.newArrayList();
			List<Element> children = txInfo.getChildren("LIST");
			for(Element e : children) {
				CreditRecordPayment result = new CreditRecordPayment();
				//TODO
				String serial_num = e.getChild("serial_num").getValue();
				String cust_name = e.getChild("cust_name").getValue();
				String cooper_cust_num = e.getChild("cooper_cust_num").getValue();
				String bill_num = e.getChild("bill_num").getValue();
				String bill_amt = e.getChild("bill_amt").getValue();
				String return_princial_amt = e.getChild("return_princial_amt").getValue();
				String return_interest_amt = e.getChild("return_interest_amt").getValue();
				String balance_amt = e.getChild("balance_amt").getValue();
				String write_off_ind = e.getChild("write_off_ind").getValue();
				String currency_cd = e.getChild("currency_cd").getValue();
				
				result.setSerial_num(Integer.parseInt(serial_num));
				result.setCust_name(cust_name);
				result.setCooper_cust_num(cooper_cust_num);
				result.setBill_amt(bill_amt);
				result.setBill_num(bill_num);
				result.setReturn_princial_amt(Double.parseDouble(return_princial_amt));
				result.setReturn_interest_amt(Double.parseDouble(return_interest_amt));
				result.setBalance_amt(Double.parseDouble(balance_amt));
				result.setWrite_off_ind(write_off_ind);
				result.setCurrency_cd(currency_cd);
				list.add(result);
			
			}
			
			return list;
		} catch (Exception e) {
//			logService.responseLog(logId, response, null);
			throw e;
		}
		
	}
	
	
	
	
	
	


	


	private Element getTxInfo(String response) {
		SAXBuilder saxBuilder = new SAXBuilder();
		try {
			StringReader read = new StringReader(response.trim());

			InputSource source = new InputSource(read);
			Document doc = saxBuilder.build(source);
			Element root = doc.getRootElement();
			String returnCode = root.getChildText("RETURN_CODE");
			if (!"000000".equals(returnCode)) {
				//有错，抛出异常
				String returnMsg = root.getChildText("RETURN_MSG");
				throw new RuntimeException(returnMsg);
			}
			Element txInfo = root.getChild("TX_INFO");
			return txInfo;
		} catch (JDOMException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	private String getRequestXML(RequestBean requestBean) { 
		// 解决tag中出现的下划线被转义成2个下划线
		XStream xStream = new XStream(new XppDriver(new XmlFriendlyNameCoder("_-", "_")));
		xStream.registerConverter(new MapEntryConverter());
		xStream.alias("TX", RequestBean.class);
		String request = xStream.toXML(requestBean);
		return request;
	}
	
	private String getRequestXML(RequestBeans requestBeans) { 
		// 解决tag中出现的下划线被转义成2个下划线
		XStream xStream = new XStream(new XppDriver(new XmlFriendlyNameCoder("_-", "_")));
		xStream.registerConverter(new MapEntryConverter());
		xStream.alias("TX", RequestBeans.class);
		xStream.alias("LIST", Map.class);
		String request = xStream.toXML(requestBeans);
		return request;
	}

	//获取文件公共方法
	private  String getFileInfo(String filePathName,String fileName){
		try {
			String filePath;
		    filePath = mockPath+"/"+filePathName+"/"+fileName;
			FileReader in = new FileReader(new File(filePath));
			BufferedReader br = new BufferedReader(in);
			StringBuilder sb = new StringBuilder();
			String str = null;
			try {
				while((str = br.readLine()) != null) {
					sb.append("\r\n").append(str);
				}
				br.close();  
				return sb.toString();
			} catch (IOException e) {
				
				throw new RuntimeException(e);
			}
		} catch (FileNotFoundException e) {
			
			return null;
		}
	}

}
