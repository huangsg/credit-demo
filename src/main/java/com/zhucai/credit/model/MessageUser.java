package com.zhucai.credit.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * 
 * @ClassName: MessageUser
 * @Description: 系统信息,用户关联表
 * @author jmshi
 * @date 2015年10月11日 上午10:47:49
 *
 */
@Entity
@Table(name = "tb_xa_message_user")
@ApiModel(value = "系统信息,用户关联表")
public class MessageUser extends BaseEntity {

	@ApiModelProperty(value = "用户ID")
	private Long userId;

	@ApiModelProperty(value = "消息ID")
	private Long messageId;
	
	@ApiModelProperty(value = "是否读取 0 = 未读  1 = 已读")
	private Integer isNew = 0;
	
	@Column(nullable=true,length=10)
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	@Column(nullable=true,length=10)
	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public Integer getIsNew() {
		return isNew;
	}

	public void setIsNew(Integer isNew) {
		this.isNew = isNew;
	}
	
	 

}
