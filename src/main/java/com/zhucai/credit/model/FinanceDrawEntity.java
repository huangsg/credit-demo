package com.zhucai.credit.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * 支取记录表
 * 2017年9月19日
 * @author hsg
 */
@Entity
@Table(name = "finance_draw_record")
public class FinanceDrawEntity extends BaseEntity{
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "支取商家类型（采购商，供应商）,")
	private String entType;
	@ApiModelProperty(value = "支取企业名称,")
	private String enterpriseName;
	@ApiModelProperty(value = "企业id,")
	private Long enterpriseId;
	@ApiModelProperty(value = "支取时间(接口获取的时间),")
	private Date drawTime;
	@ApiModelProperty(value = "发放日期(接口返回的),")
	private Date loanDate;
	@ApiModelProperty(value = "支取金额,")
	private BigDecimal drawAmount;
	@ApiModelProperty(value = "支取率,")
	private BigDecimal drawAmountRate;
	@ApiModelProperty(value = "组织机构代码,")
	private String orgNum;
	@ApiModelProperty(value = "支用编号,")
	private String payoutNum;
	@ApiModelProperty(value = "支取金额的发票号和发票代码")
	private String invoiceNoCode;
	@ApiModelProperty(value = "支取发票的的本身金额")
	private BigDecimal invoiceAmount;
	@ApiModelProperty(value = "关联的融资记录id")
	private Long outId;
	@Column(nullable = false, length = 12)
	public String getEntType() {
		return entType;
	}
	public void setEntType(String entType) {
		this.entType = entType;
	}
	@Column(nullable = true, length = 255)
	public String getEnterpriseName() {
		return enterpriseName;
	}
	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}
	@Column(nullable = true, length = 20)
	public Long getEnterpriseId() {
		return enterpriseId;
	}
	public void setEnterpriseId(Long enterpriseId) {
		this.enterpriseId = enterpriseId;
	}
	@Column(nullable = false, length = 20)
	public Date getDrawTime() {
		return drawTime;
	}
	public void setDrawTime(Date drawTime) {
		this.drawTime = drawTime;
	}
	@Column(nullable=true,columnDefinition = "decimal(20,2)")
	public BigDecimal getDrawAmount() {
		return drawAmount;
	}
	public void setDrawAmount(BigDecimal drawAmount) {
		this.drawAmount = drawAmount;
	}
	@Column(nullable = true, length = 20)
	public String getOrgNum() {
		return orgNum;
	}
	public void setOrgNum(String orgNum) {
		this.orgNum = orgNum;
	}
	@Column(nullable = false, length = 50)
	public String getPayoutNum() {
		return payoutNum;
	}
	public void setPayoutNum(String payoutNum) {
		this.payoutNum = payoutNum;
	}
	@Column(nullable=true,columnDefinition = "decimal(20,2)")
	public BigDecimal getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(BigDecimal invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	@Column(nullable = false, length = 20)
	public Long getOutId() {
		return outId;
	}
	public void setOutId(Long outId) {
		this.outId = outId;
	}
	@Column(nullable = true, length = 20)
	public Date getLoanDate() {
		return loanDate;
	}
	public void setLoanDate(Date loanDate) {
		this.loanDate = loanDate;
	}
	@Column(nullable=true,columnDefinition = "decimal(20,2)")
	public BigDecimal getDrawAmountRate() {
		return drawAmountRate;
	}
	public void setDrawAmountRate(BigDecimal drawAmountRate) {
		this.drawAmountRate = drawAmountRate;
	}
	@Column(nullable = true, length = 50)
	public String getInvoiceNoCode() {
		return invoiceNoCode;
	}
	public void setInvoiceNoCode(String invoiceNoCode) {
		this.invoiceNoCode = invoiceNoCode;
	}
	
	
}
