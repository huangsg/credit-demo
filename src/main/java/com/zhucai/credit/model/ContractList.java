package com.zhucai.credit.model;

import java.text.DecimalFormat;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import scala.reflect.macros.Attachments;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * @author 曹文波
 * @ClassName: ContractList
 * @Description: 采购合同表定义表
 * @date 2014年10月11日 上午10:47:49
 */
@Entity
@Table(name = "tb_xa_contract_list")
@ApiModel(value = "采购合同表定义表")
public class ContractList extends BaseEntity{

    @ApiModelProperty(value = "合同编号  筑材网生成,合同编号  筑材网生成")
    private String contractNumZhucai;
    @ApiModelProperty(value = "合同编号 企业自定义输入,合同编号 企业自定义输入")
    private String contractNumSelf;
    @ApiModelProperty(value = "采购商名称,采购商名称")
    private String purchaserName;
    @ApiModelProperty(value = "项目名称 项目地址,项目名称 项目地址")
    private String purchaseProjectName;
    @ApiModelProperty(value = "报价截止日期,报价截止日期")
    private String dateExpired;
    @ApiModelProperty(value = "采购任务id,采购任务id")
    private Integer purchaseMissionId;
    @ApiModelProperty(value = "采购联系人,采购联系人")
    private String personContact;
    @ApiModelProperty(value = "采购简介,采购简介")
    private String purchaseIntro;
    @ApiModelProperty(value = "采购简介详细id=值,采购简介")
    private String bidContentDetail;
    @ApiModelProperty(value = "上传的附件 url,上传的附件 url")
    private String attachmentUrl;
    @ApiModelProperty(value = "采购预估价格,采购预估价格")
    private Double moneyEstimated;


    @ApiModelProperty(value = "供应商ID,供应商ID")
    private Long supplierId;
    @ApiModelProperty(value = "采购商ID,采购商ID")
    private Long purchaserId;
    @ApiModelProperty(value = "供应商销售员ID,供应商销售员ID")
    private Long supplierSalesId;
    @ApiModelProperty(value = "供应商名称,供应商名称")
    private String supplierName;
    
    @ApiModelProperty(value = "送货方式,送货方式")
    private String deliveryMethod;

    @ApiModelProperty(value = "现场联系人员,现场联系人员")
    private String personSite;
    @ApiModelProperty(value = "采购模式，公开采购，邀标采购,采购模式，公开采购，邀标采购")
    private String purchaseMethod;
    @ApiModelProperty(value = "采购要求,采购要求")
    private String purchaseRequire;
    @ApiModelProperty(value = "供应商要求,供应商要求")
    private String supplierRequire;
    @ApiModelProperty(value = "发票要求 增值税发票 普通发票,发票要求 增值税发票 普通发票")
    private Integer invoiceRequire;
    @ApiModelProperty(value = "联系人手机,联系人手机")
    private String phoneContact;
    @ApiModelProperty(value = "是否含税 0不含 1含税,是否含税 0不含 1含税")
    private Integer taxInclude;
    
    
    @ApiModelProperty(value = "付款形式,付款形式")
    private String paymentMethod;
    @ApiModelProperty(value = "补充说明,补充说明")
    private String remark;
    @ApiModelProperty(value = "合同状态,合同状态")
    private String contractStatus;

    @ApiModelProperty(value = "采购所属部门,所属部门")
    private Long deptId;
    
    @ApiModelProperty(value = "供应所属部门,所属部门")
    private Long supplierDeptId;
    
    
    @ApiModelProperty(value = "旧合同ID,旧合同ID")
    private String oldContractId;
    @ApiModelProperty(value = "验收方联系电话,验收方联系电话")
    private String phoneChecked;
    @ApiModelProperty(value = "供应方联系人,供应方联系人")
    private String supplierContact;
    @ApiModelProperty(value = "供应方联系电话,供应方联系电话")
    private String phoneSupplier;
    @ApiModelProperty(value = "合同形式,合同形式")
    private String contractForm;
    @ApiModelProperty(value = "是否购买付款保证金")
    private String isPayForCash;
    @ApiModelProperty(value = "付款比例")
    private String payForRate;
    @ApiModelProperty(value = "送货地点,送货地点(完整)")
    private String address;
    @ApiModelProperty(value = "(id)")
    private String deliveryAddress;
    @ApiModelProperty(value="已付款金额,已付款金额")
	private Double contractPaidAmount;
    
    
    @ApiModelProperty(value = "合同执行开始时间")
    private String runTime;
    @ApiModelProperty(value = "合同生效时间")
    private String effectiveTime;
    @ApiModelProperty(value = "合同完成时间")
    private String finishedTime;
    @ApiModelProperty(value = "合同类型")
    private String contractType;
    
    
    
    @ApiModelProperty(value = "期望的收货时间,期望的收货时间")
    private String receivingDateExpect;
    
    @ApiModelProperty(value = "付款比例,付款比例")
    private String paymentRatio;
    @ApiModelProperty(value = "筛选符合数量的供应商,筛选符合数量的供应商")
    private Integer purchaseProductTypeNum;
    @ApiModelProperty(value = "供应商资质,供应商资质")
    private String supplierQualification;
    @ApiModelProperty(value = "采购质量要求,采购质量要求")
    private String purchaseQualityRequire;
    @ApiModelProperty(value = "推送的供应商数量,推送的供应商数量")
    private Integer filterSupplierProductNum;
    @ApiModelProperty(value = "筛选条件,筛选条件")
    private String filterCondition;
    @ApiModelProperty(value = "推送符合条件的供应商数量,推送符合条件的供应商数量")
    private Integer pushNumSupplier;
    
    @ApiModelProperty(value = "附件列表")
    private List<Attachments> attachmentsList;
    
    @ApiModelProperty(value = "任务列表json字符串")
    private String jsonMissionlst;
    @ApiModelProperty(value = "税务登记号")
    private String taxRegistrationCode;
    
    @ApiModelProperty(value = "当前任务")
    private String missionName;
    
    @ApiModelProperty(value = "合同简介")
    private String contractIntro;
    
    private String moneyValueStr;
    
    /*****************   合同选择的企业开票信息字段 start   *****************************/

	@ApiModelProperty(value = "企业开票：开票公司名称")
	private String companyNameForBilling;
	@ApiModelProperty(value = "企业开票：开票电话")
	private String phoneForBilling;
	@ApiModelProperty(value = "企业开票：税务登记号")
	private String taxRegistrationCodeForBilling;
	@ApiModelProperty(value = "企业开票：公司地址")
	private String addressForBilling;
	@ApiModelProperty(value = "企业开票：开户银行")
	private String bankNameForBilling;
	@ApiModelProperty(value = "企业开票：开户账号")
	private String accountNumberForBilling;
	@ApiModelProperty(value = "流程方向：1:终止。2:取消")
	private String direct;

    /*****************   合同选择的企业开票信息字段 end   *****************************/

	@ApiModelProperty(value = "增值税专用发票的税率(只有增值税专用发票才有此选项)，使用Double可扩展")
    private Double taxRate ;
	
	@ApiModelProperty(value = "招标编号")
	private String bidNumZhucai;//招标编号（冗余字段，方便查询处理）
	
	
	@ApiModelProperty(value = "签订合同状态 0:为初始，1:为等待采购员签订，2为等待销售员签订，3为已完成")
	private Integer signStatus = 0;
	
	
	@ApiModelProperty(value = "统一社会信用代码")
	private String unifyCert;
	    
	@ApiModelProperty(value = "是否三证合一：0.否，1.是")
	private Integer unifyCertStatus;
	
	/** @Date: 2017年10月31日 @Auth: PMQ add start  **/
	@ApiModelProperty(value = "合同签署密码")
	private String contractSignPassword;
	/** @Date: 2017年10月31日 @Auth: PMQ add end  **/

	/** @Date: 2017年11月17日 @Auth: PMQ add start  **/
	@ApiModelProperty(value = "确认合同完成状态：0.初始值, 1.等待采购方确认, 2.等待供应方确认, 3.确认完成 ( 参考常量: ZhucaiConstant.ContractConfirmFinishStatus )")
	private Integer confirmFinishStatus = 0;
	/** @Date: 2017年11月17日 @Auth: PMQ add end  **/
	
	/** @Date: 2017年11月17日 @Auth: Gaolin add start  **/
	@ApiModelProperty(value = "合同验收人")
	private Long contractRunApprover;
	/** @Date: 2017年11月17日 @Auth: Gaolin add end  **/
	
	
	@ApiModelProperty(value = "流程线用户  保存格式：{userId},{userId}...")
	private String  processUserIds;

	@ApiModelProperty(value = "超级管理员操作投标业务关联部门ID")
	private Long superRelationDeptId;
	
	@Column(nullable = true, length = 50)
    public String getBidNumZhucai() {
		return bidNumZhucai;
	}

	public void setBidNumZhucai(String bidNumZhucai) {
		this.bidNumZhucai = bidNumZhucai;
	}

	public void setContractNumZhucai(String contractNumZhucai) {
        this.contractNumZhucai = contractNumZhucai;
    }

    @Column(nullable = true, length = 50)
    public String getContractNumZhucai() {
        return contractNumZhucai;
    }

    public void setContractNumSelf(String contractNumSelf) {
        this.contractNumSelf = contractNumSelf;
    }

    @Column(nullable = true, length = 50)
    public String getContractNumSelf() {
        return contractNumSelf;
    }

    public void setPurchaserName(String purchaserName) {
        this.purchaserName = purchaserName;
    }

    @Column(nullable = true, length = 50)
    public String getPurchaserName() {
        return purchaserName;
    }

    public void setPurchaseProjectName(String purchaseProjectName) {
        this.purchaseProjectName = purchaseProjectName;
    }

    @Column(nullable = true, length = 50)
    public String getPurchaseProjectName() {
        return purchaseProjectName;
    }

    public void setDateExpired(String dateExpired) {
        this.dateExpired = dateExpired;
    }

    @Column(nullable = true, length = 50)
    public String getDateExpired() {
        return dateExpired;
    }

    public void setPurchaseMissionId(Integer purchaseMissionId) {
        this.purchaseMissionId = purchaseMissionId;
    }

    @Column(nullable = true, length = 8)
    public Integer getPurchaseMissionId() {
        return purchaseMissionId;
    }

    public void setPersonContact(String personContact) {
        this.personContact = personContact;
    }

    @Column(nullable = true, length = 50)
    public String getPersonContact() {
        return personContact;
    }

    public void setPurchaseIntro(String purchaseIntro) {
        this.purchaseIntro = purchaseIntro;
    }

    public String getPurchaseIntro() {
        return purchaseIntro;
    }

    public void setAttachmentUrl(String attachmentUrl) {
        this.attachmentUrl = attachmentUrl;
    }

    @Column(nullable = true, length = 50)
    public String getAttachmentUrl() {
        return attachmentUrl;
    }

    public void setMoneyEstimated(Double moneyEstimated) {
        this.moneyEstimated = moneyEstimated;
    }

    @Column(nullable = true, length = 50)
    public Double getMoneyEstimated() {
        return moneyEstimated;
    }

    public void setPaymentRatio(String paymentRatio) {
        this.paymentRatio = paymentRatio;
    }

    @Column(nullable = true, length = 50)
    public String getPaymentRatio() {
        return paymentRatio;
    }

    public void setFilterSupplierProductNum(Integer filterSupplierProductNum) {
        this.filterSupplierProductNum = filterSupplierProductNum;
    }

    @Column(nullable = true, length = 8)
    public Integer getFilterSupplierProductNum() {
        return filterSupplierProductNum;
    }

    public void setFilterCondition(String filterCondition) {
        this.filterCondition = filterCondition;
    }

    @Column(nullable = true, length = 50)
    public String getFilterCondition() {
        return filterCondition;
    }

    public void setPushNumSupplier(Integer pushNumSupplier) {
        this.pushNumSupplier = pushNumSupplier;
    }

    @Column(nullable = true, length = 50)
    public Integer getPushNumSupplier() {
        return pushNumSupplier;
    }

    public void setReceivingDateExpect(String receivingDateExpect) {
        this.receivingDateExpect = receivingDateExpect;
    }

    @Column(nullable = true, length = 50)
    public String getReceivingDateExpect() {
        return receivingDateExpect;
    }

    public void setDeliveryMethod(String deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
    }

    @Column(nullable = true, length = 50)
    public String getDeliveryMethod() {
        return deliveryMethod;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    @Column(nullable = true, length = 500)
    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setPersonSite(String personSite) {
        this.personSite = personSite;
    }

    @Column(nullable = true, length = 50)
    public String getPersonSite() {
        return personSite;
    }

    public void setPurchaseMethod(String purchaseMethod) {
        this.purchaseMethod = purchaseMethod;
    }

    @Column(nullable = true, length = 50)
    public String getPurchaseMethod() {
        return purchaseMethod;
    }

    public void setPurchaseRequire(String purchaseRequire) {
        this.purchaseRequire = purchaseRequire;
    }

    @Column(nullable = true, length = 50)
    public String getPurchaseRequire() {
        return purchaseRequire;
    }

    public void setSupplierRequire(String supplierRequire) {
        this.supplierRequire = supplierRequire;
    }

    @Column(nullable = true, length = 255)
    public String getSupplierRequire() {
        return supplierRequire;
    }

    public void setInvoiceRequire(Integer invoiceRequire) {
        this.invoiceRequire = invoiceRequire;
    }

    @Column(nullable = true, length = 50)
    public Integer getInvoiceRequire() {
        return invoiceRequire;
    }

    public void setSupplierQualification(String supplierQualification) {
        this.supplierQualification = supplierQualification;
    }

    @Column(nullable = true, length = 255)
    public String getSupplierQualification() {
        return supplierQualification;
    }

    public void setPurchaseQualityRequire(String purchaseQualityRequire) {
        this.purchaseQualityRequire = purchaseQualityRequire;
    }

    @Column(nullable = true, length = 50)
    public String getPurchaseQualityRequire() {
        return purchaseQualityRequire;
    }

    public void setPhoneContact(String phoneContact) {
        this.phoneContact = phoneContact;
    }

    @Column(nullable = true, length = 50)
    public String getPhoneContact() {
        return phoneContact;
    }

    public void setPurchaseProductTypeNum(Integer purchaseProductTypeNum) {
        this.purchaseProductTypeNum = purchaseProductTypeNum;
    }

    @Column(nullable = true, length = 8)
    public Integer getPurchaseProductTypeNum() {
        return purchaseProductTypeNum;
    }

    public void setTaxInclude(Integer taxInclude) {
        this.taxInclude = taxInclude;
    }

    @Column(nullable = true, length = 8)
    public Integer getTaxInclude() {
        return taxInclude;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    @Column(nullable = true, length = 255)
    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Column(nullable = true, length = 255)
    public String getRemark() {
        return remark;
    }

    public void setContractStatus(String contractStatus) {
        this.contractStatus = contractStatus;
    }

    @Column(nullable = true, length = 50)
    public String getContractStatus() {
        return contractStatus;
    }

    @Column(nullable = true, length = 50)
    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    @Column(nullable = true, length = 255)
    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getOldContractId() {
        return oldContractId;
    }

    public void setOldContractId(String oldContractId) {
        this.oldContractId = oldContractId;
    }

    public String getPhoneChecked() {
        return phoneChecked;
    }

    public void setPhoneChecked(String phoneChecked) {
        this.phoneChecked = phoneChecked;
    }

    public String getSupplierContact() {
        return supplierContact;
    }

    public void setSupplierContact(String supplierContact) {
        this.supplierContact = supplierContact;
    }

    public String getPhoneSupplier() {
        return phoneSupplier;
    }

    public void setPhoneSupplier(String phoneSupplier) {
        this.phoneSupplier = phoneSupplier;
    }

    public String getContractForm() {
        return contractForm;
    }

    public void setContractForm(String contractForm) {
        this.contractForm = contractForm;
    }
    

    public String getIsPayForCash() {
		return isPayForCash;
	}

	public void setIsPayForCash(String isPayForCash) {
		this.isPayForCash = isPayForCash;
	}

	public String getPayForRate() {
		return payForRate;
	}

	public void setPayForRate(String payForRate) {
		this.payForRate = payForRate;
	}

	@Column(nullable = true, length = 50)
    public Long getPurchaserId() {
        return purchaserId;
    }
    public void setPurchaserId(Long purchaserId) {
        this.purchaserId = purchaserId;
    }
    
    @Column(nullable = true, length = 50)
    public Long getSupplierSalesId() {
        return supplierSalesId;
    }

    public void setSupplierSalesId(Long supplierSalesId) {
        this.supplierSalesId = supplierSalesId;
    }
    
    public Long getSupplierDeptId() {
		return supplierDeptId;
	}

	public void setSupplierDeptId(Long supplierDeptId) {
		this.supplierDeptId = supplierDeptId;
	}


	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Double getContractPaidAmount() {
		if(contractPaidAmount==null){
			return 0.00;
		}
		return contractPaidAmount;
	}

	public void setContractPaidAmount(Double contractPaidAmount) {
		this.contractPaidAmount = contractPaidAmount;
	}

	public String getEffectiveTime() {
		return effectiveTime;
	}

	public void setEffectiveTime(String effectiveTime) {
		this.effectiveTime = effectiveTime;
	}

	public String getFinishedTime() {
		return finishedTime;
	}

	public void setFinishedTime(String finishedTime) {
		this.finishedTime = finishedTime;
	}

	public String getContractType() {
		return contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}

	public String getRunTime() {
		return runTime;
	}

	public void setRunTime(String runTime) {
		this.runTime = runTime;
	}
	

	public String getTaxRegistrationCode() {
		return taxRegistrationCode;
	}

	public void setTaxRegistrationCode(String taxRegistrationCode) {
		this.taxRegistrationCode = taxRegistrationCode;
	}

	@Transient
	public List<Attachments> getAttachmentsList() {
		return attachmentsList;
	}

	public void setAttachmentsList(List<Attachments> attachmentsList) {
		this.attachmentsList = attachmentsList;
	}
	
	@Transient
	public String getJsonMissionlst() {
		return jsonMissionlst;
	}

	public void setJsonMissionlst(String jsonMissionlst) {
		this.jsonMissionlst = jsonMissionlst;
	}

	public String getMissionName() {
		return missionName;
	}

	public void setMissionName(String missionName) {
		this.missionName = missionName;
	}

	public String getBidContentDetail() {
		return bidContentDetail;
	}

	public void setBidContentDetail(String bidContentDetail) {
		this.bidContentDetail = bidContentDetail;
	}
	
	
	@Transient
	public String getContractIntro() {
		return contractIntro;
	}

	public void setContractIntro(String contractIntro) {
		this.contractIntro = contractIntro;
	}
	
	@Transient
	public String getMoneyValueStr() {
		if(moneyEstimated!=null){
			String i = DecimalFormat.getInstance().format(moneyEstimated);  
	        String result = i.replaceAll(",", "");  
	        return result;  
		}
		return moneyValueStr;
	}

	public void setMoneyValueStr(String moneyValueStr) {
		this.moneyValueStr = moneyValueStr;
	}

	@Column(nullable = true, length = 100)
	public String getCompanyNameForBilling() {
		return companyNameForBilling;
	}

	public void setCompanyNameForBilling(String companyNameForBilling) {
		this.companyNameForBilling = companyNameForBilling;
	}

	@Column(nullable = true, length = 50)
	public String getPhoneForBilling() {
		return phoneForBilling;
	}

	public void setPhoneForBilling(String phoneForBilling) {
		this.phoneForBilling = phoneForBilling;
	}

	@Column(nullable = true, length = 100)
	public String getTaxRegistrationCodeForBilling() {
		return taxRegistrationCodeForBilling;
	}

	public void setTaxRegistrationCodeForBilling(String taxRegistrationCodeForBilling) {
		this.taxRegistrationCodeForBilling = taxRegistrationCodeForBilling;
	}

	@Column(nullable = true, length = 255)
	public String getAddressForBilling() {
		return addressForBilling;
	}

	public void setAddressForBilling(String addressForBilling) {
		this.addressForBilling = addressForBilling;
	}

	@Column(nullable = true, length = 100)
	public String getBankNameForBilling() {
		return bankNameForBilling;
	}

	public void setBankNameForBilling(String bankNameForBilling) {
		this.bankNameForBilling = bankNameForBilling;
	}

	@Column(nullable = true, length = 100)
	public String getAccountNumberForBilling() {
		return accountNumberForBilling;
	}

	public void setAccountNumberForBilling(String accountNumberForBilling) {
		this.accountNumberForBilling = accountNumberForBilling;
	}
	@Column(nullable = true ,columnDefinition = " double default 0 ")
    public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	public String getDirect() {
		return direct;
	}

	public void setDirect(String direct) {
		this.direct = direct;
	}
	
	@Column(nullable = true, length = 11)
	public Integer getSignStatus() {
		return signStatus;
	}

	public void setSignStatus(Integer signStatus) {
		this.signStatus = signStatus;
	}
	
	@Column(nullable = true, length = 100)
	public String getUnifyCert() {
		return unifyCert;
	}

	public void setUnifyCert(String unifyCert) {
		this.unifyCert = unifyCert;
	}

	@Column(nullable = true, length = 11)
	public Integer getUnifyCertStatus() {
		return unifyCertStatus;
	}

	public void setUnifyCertStatus(Integer unifyCertStatus) {
		this.unifyCertStatus = unifyCertStatus;
	}

	@Column(nullable = true, length = 100)
	public String getContractSignPassword() {
		return contractSignPassword;
	}

	public void setContractSignPassword(String contractSignPassword) {
		this.contractSignPassword = contractSignPassword;
	}

	@Column(nullable = true, columnDefinition = "INT default 0")
	public Integer getConfirmFinishStatus() {
		return confirmFinishStatus;
	}

	public void setConfirmFinishStatus(Integer confirmFinishStatus) {
		this.confirmFinishStatus = confirmFinishStatus;
	}

	public Long getContractRunApprover() {
		return contractRunApprover;
	}

	public void setContractRunApprover(Long contractRunApprover) {
		this.contractRunApprover = contractRunApprover;
	}

	@Column(nullable = true, length = 500)
	public String getProcessUserIds() {
		return processUserIds;
	}

	public void setProcessUserIds(String processUserIds) {
		this.processUserIds = processUserIds;
	}

	public Long getSuperRelationDeptId() {
		return superRelationDeptId;
	}

	public void setSuperRelationDeptId(Long superRelationDeptId) {
		this.superRelationDeptId = superRelationDeptId;
	}



	
}
