package com.zhucai.credit.bank.service.cron;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.zhucai.common.locks.DistributedLockFactory;
import com.zhucai.common.locks.DistributedReentrantLock;
import com.zhucai.credit.bank.icbc.bean.QgylinfoReturn;
import com.zhucai.credit.bank.icbc.bean.QgylinfoSend;
import com.zhucai.credit.bank.icbc.bean.QgylinstrReturn;
import com.zhucai.credit.bank.icbc.bean.QgylinstrSend;
import com.zhucai.credit.bank.icbc.service.IcbcService;
import com.zhucai.credit.common.CommonEnums.CrProdType;
import com.zhucai.credit.common.CommonEnums.CreditStatus;
import com.zhucai.credit.common.CommonEnums.CreditUnusualStatus;
import com.zhucai.credit.common.CommonEnums.EntType;
import com.zhucai.credit.common.HelpConstant;
import com.zhucai.credit.common.ZhucaiConstant;
import com.zhucai.credit.model.ContractList;
import com.zhucai.credit.model.CreditApplicationEntity;
import com.zhucai.credit.model.CreditRecordEntrity;
import com.zhucai.credit.model.CreditRecordTimeEntrity;
import com.zhucai.credit.model.FinanceDrawEntity;
import com.zhucai.credit.model.FinanceRepaymentEntity;
import com.zhucai.credit.model.OrderList;
import com.zhucai.credit.model.PaymentPlanEntity;
import com.zhucai.credit.repository.ContractListRepository;
import com.zhucai.credit.repository.CreditApplicationRepository;
import com.zhucai.credit.repository.CreditRecordRepository;
import com.zhucai.credit.repository.CreditRecordTimeRepository;
import com.zhucai.credit.repository.FinanceDrawRepository;
import com.zhucai.credit.repository.FinanceRepaymentRepository;
import com.zhucai.credit.repository.OrderListRepository;
import com.zhucai.credit.repository.PaymentPlanRepository;
import com.zhucai.credit.service.impl.problem.ProblemSericeImpl;
import com.zhucai.credit.service.impl.transactional.CreditUserServiceUtil;
import com.zhucai.credit.utils.SqlResultUtil;

/**
 * 金融接口的定时循环任务 2017年11月28日
 * 
 * @author hsg
 */
//@Service
public class ICBCLoopService {
	private static final Logger logger = LoggerFactory.getLogger(ICBCLoopService.class);

	@Autowired
	private ContractListRepository contractListRepository;

	@Autowired
	private CreditRecordRepository creditRecordRepository;

	@Autowired
	private FinanceDrawRepository financeDrawRepository;

	@Autowired
	private FinanceRepaymentRepository financeRepaymentRepository;

	@Autowired
	CreditApplicationRepository creditApplicationRepository;

	@Autowired
	private PaymentPlanRepository paymentPlanRepository;

	@Autowired
	private DistributedLockFactory distributedLockFactory;

	@Autowired
	private IcbcService icbcService;

	@Autowired
	private ProblemSericeImpl problemSericeImpl;

	@Autowired
	private OrderListRepository orderListRepository;

	@Autowired
	private CreditUserServiceUtil creditUserServiceUtil;

	@Autowired
	private CreditRecordTimeRepository creditRecordTimeRepository;
	
	/**
	 * 工商银行合同签订任务
	 * @version 2018年9月22日
	 */
	@Scheduled(cron = "0 0/20 * * * ? ")//15分钟
	@Async
	public void signContractTask() {
		logger.trace("signIcbcContractTask start");
		DistributedReentrantLock lock = distributedLockFactory.buildLock("signIcbcContractTask");
		if (lock.tryLock(1000)) {
			try {
				signContract();
			}finally{
				lock.unlock();
			}
		} else {
			logger.trace("signIcbcContractTask is running in other jvm ,this thread end");
		}
	}

	/**
	 * 工商银行账款信息查询接口
	 * @version 2018年9月22日
	 */
	@Scheduled(cron = "0 0/30 * * * ? ")//30分钟
	@Async
	public void accountsInfoTask() {
		logger.trace("accountsInfoTask start");
		DistributedReentrantLock lock = distributedLockFactory.buildLock("accountsInfoTask");
		if (lock.tryLock(1000)) {
			try {
				accountsInfo();
			} finally{
				lock.unlock();
			}
		} else {
			logger.trace("loop：signUpTask is running in other jvm ,this thread end");
		}
	}
	/**
	 * 工商银行合同签订任务 QGYLINFO
	 * 
	 * @version 2018年8月17日
	 */
	
	private void signContract() {
		logger.info("### 工商银行签订合同循环任务start");
		try {
			// 查询可以发起工行查询的接口

			List<String> creditStatusList = new ArrayList<String>();
			creditStatusList.add("SUBSTITUTE_CONTRACT");
			List<CreditRecordEntrity> crList = creditRecordRepository.findByCreditStatusAndStatusNotAndSeqNo(creditStatusList);
			for (CreditRecordEntrity creditRecordEntrity : crList) {
				QgylinstrSend send = new QgylinstrSend();
				send.setQryfSeqno(creditRecordEntrity.getQryfSeqno());
				send.setQryOrderNo("1");
				List<QgylinstrReturn> qgylinstrReturnList = icbcService.getQgylinstrReturnList(send);
				if (CollectionUtils.isEmpty(qgylinstrReturnList)) {
					continue;
				}
				QgylinstrReturn qgylinstrReturn = qgylinstrReturnList.get(0);
				String icbcInvoiceNoCode = qgylinstrReturn.getVouchCode() + qgylinstrReturn.getVouchNo();
				if (!creditRecordEntrity.getInvoiceNoCode().equals(icbcInvoiceNoCode)) {
					logger.error("### 工商银行签订合同循环任务 工行接口返回发票号代码和融资记录的不对应 record_invoice_no_code={}，icbc_invoice_no_code={}", creditRecordEntrity.getInvoiceNoCode(),
							icbcInvoiceNoCode);
					String problem = "工商银行签订合同循环任务 工行接口返回发票号代码和融资记录的不对应 record_invoice_no_code=" + creditRecordEntrity.getInvoiceNoCode() + "icbc_invoice_no_code="
							+ icbcInvoiceNoCode;
					problemSericeImpl.setProblem(problem, "icbcSignContract");
					continue;
				}
				String result = qgylinstrReturn.getResult();

				// 成功
				if ("7".equals(result)) {
					creditRecordEntrity.setCreditStatus(CreditStatus.PENDING_WITHDRAWAL.toString());
					Integer coo = creditRecordRepository.updateBySignContract(creditRecordEntrity.getTid(), CreditUnusualStatus.NORMAL.toString(),
							null, CreditStatus.PENDING_WITHDRAWAL.toString());
					if (coo != 1) {
						logger.error("### 工商银行签订合同循环任务 recordId={},更新为待支取失败 coo={}", creditRecordEntrity.getTid(), coo);
						String problem = "工商银行签订合同循环任务 recordNo=" + creditRecordEntrity.getRecordNo() + ",更新为待支取失败 coo=" + coo;
						problemSericeImpl.setProblem(problem, "icbcSignContract");
						continue;
					}

					changePlan(creditRecordEntrity);
					// 新增合同签订的历时记录
					creditUserServiceUtil.createRecordTime(creditRecordEntrity.getTid(), CreditStatus.SUBSTITUTE_CONTRACT.toString());
					// 失败
				} else if ("8".equals(result) || "5".equals(result) || "6".equals(result)) {
					/**
					 * 实际付款状态改为未付款
					 */
					paymentPlanRepository.updatePayState(creditRecordEntrity.getPaymentId(), ZhucaiConstant.PaymentPlanStatus.NOT_PAYMENT_STATUS);
					logger.info("### 工行驳回合同,融资编号recordNo={}对应的付款计划id={}的付款计划状态改为未付款成功payState={}", creditRecordEntrity.getRecordNo(), creditRecordEntrity.getPaymentId(),
							ZhucaiConstant.PaymentPlanStatus.NOT_PAYMENT_STATUS);

					creditRecordEntrity.setCreditStatus(CreditStatus.WAIT_SUBMIT.toString());
					creditRecordEntrity.setCreditUnusualStatus(CreditUnusualStatus.CONTRACT_REJECT.toString());
					//交易返回描述
					String instrRetMsg = qgylinstrReturn.getInstrRetMsg();
					creditRecordEntrity.setRejectReason(instrRetMsg);
					// 审核状态更新为合同签署失败
					Integer coo = creditRecordRepository.updateUnusualStatus(HelpConstant.Status.delete, creditRecordEntrity.getTid(),
							CreditUnusualStatus.CONTRACT_REJECT.toString(), "工商银行驳回", CreditStatus.WAIT_SUBMIT.toString());
					if (coo != 1) {
						logger.error("### 工商银行签订合同循环任务 recordId={},更新为待支取失败 coo={}", creditRecordEntrity.getTid(), coo);
						String problem = "工商银行签订合同循环任务 recordNo=" + creditRecordEntrity.getRecordNo() + ",更新为待支取失败 coo=" + coo;
						problemSericeImpl.setProblem(problem, "icbcSignContract");
					} else {
						// 删除融资历时记录
						creditRecordTimeRepository.deleteRecordTimeByRecordId(creditRecordEntrity.getTid());
					}
				} 
			}
		} catch (Exception e) {
			logger.error("### 工商银行签订合同循环任务 发送异常", e);
			String problem = "工商银行签订合同循环任务 发生异常" + e;
			problemSericeImpl.setProblem(problem, "icbcSignContract");
		}
		logger.info("### 工商银行签订合同循环任务success end");
	}

	
	// 更新计划付款
	private void changePlan(CreditRecordEntrity creditRecordEntrity) {
		PaymentPlanEntity ppe = paymentPlanRepository.findOne(creditRecordEntrity.getPaymentId());
		// 修改计划付款的支付状态
		if (ppe.getPayState() != 5 && ppe.getPlatformPayStatus() != 4) {
			// 更新计划付款更新
			SimpleDateFormat sf2 = new SimpleDateFormat("yyyy-MM-dd");
			String nowDay = sf2.format(new Date());
			// 实际付款时间和收款时间
			ppe.setPayForDate(nowDay);
			// 采购商付款
			ppe.setPayState(5);
			// 供应商收款
			ppe.setPlatformPayStatus(4);
			ppe.setPayForMethod(1);
			// 委托集团付款方式
			ppe.setPayway("1");
			// 直接付款
			ppe.setPayChannelType("1");
			// 已付款金额
			ppe.setContractPaidAmount(ppe.getContractThisAmount());
			// 更新合同
			ContractList cl = contractListRepository.findBytidAndStatusNot(Long.valueOf(ppe.getContractId()), ZhucaiConstant.ContractListStatus.INVALID);
			// 原来的合同付款金额（无实际用处，用来打印日志）
			Double oldContractAmt = cl.getContractPaidAmount();

			Double contractPaidAmount = cl.getContractPaidAmount();
			if (contractPaidAmount == null) {
				cl.setContractPaidAmount(ppe.getContractThisAmount());
			} else {
				cl.setContractPaidAmount(contractPaidAmount + ppe.getContractThisAmount());
			}
			Integer unfinishedNum = paymentPlanRepository.findUnfinishedCountByOrderIdAndStatusNot(ppe.getOrderId(), new Long(ZhucaiConstant.PaymentPlanStatus.END_PAYMENT_STATUS),
					HelpConstant.Status.delete);
			if (unfinishedNum.equals(0) && ppe.getOrderId() != null) { // 任务完成状态更新
				OrderList orderList = orderListRepository.findBytIdAndStatusNot(ppe.getOrderId(), HelpConstant.Status.delete);
				orderList.setOrderStatus(ZhucaiConstant.OrderStatus.COMPLETED_ORDER);
				orderListRepository.save(orderList);
				logger.info("### 工行签署合同成功，更新订单状态为已完成的订单成功OrderStatus={}", ZhucaiConstant.OrderStatus.COMPLETED_ORDER);
			}

			contractListRepository.save(cl);
			logger.info("### 工行签署合同成功，更新合同成功付款金额成功原来的付款金额oldContractAmt={},新增的付款金额newContractAmt={},相加后的付款金额nowContractAmt={}", oldContractAmt, ppe.getContractThisAmount(),
					cl.getContractPaidAmount());
			paymentPlanRepository.save(ppe);
			logger.info("### 工行签署合同成功，更新计划付款成功payment={}", ppe);
		}
	}

	
	
	/**
	 * 工商银行账款信息查询接口 QGYLINSTR
	 * 
	 * @version 2018年8月17日
	 */
	private void accountsInfo() {
		logger.info("### 工商银行账款信息查任务start");

		try {
			List<String> creditStatusList = new ArrayList<String>();
			creditStatusList.add("PENDING_WITHDRAWAL");
			creditStatusList.add("REPAYMENT");

			// 查询工行企业信息
			List<CreditApplicationEntity> rsList = creditApplicationRepository.findByIcbcEnterList(CrProdType.ICBC_R_E_J.toString());

			for (CreditApplicationEntity cae : rsList) {

				String startDate = null;
				String endDate = null;

				// 获取最大最小的计划付款日期
				List<Object[]> findMaxMinTimeList = creditRecordRepository.findMaxMinTime(creditStatusList, cae.getCrApplicantPurId());
				if (CollectionUtils.isEmpty(findMaxMinTimeList)) {
					return;
				}
				Object[] obj = findMaxMinTimeList.get(0);
				String maxDate = SqlResultUtil.getSqlResultString(obj[0]);
				String minDate = SqlResultUtil.getSqlResultString(obj[1]);
				SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");
				// 最大最小日期相应的加减1个月
				endDate = sdf2.format(DateUtils.addMonths(sdf1.parse(maxDate), 1));
				startDate = sdf2.format(DateUtils.addMonths(sdf1.parse(minDate), -3));
				
				QgylinfoSend qgylinfoSend = new QgylinfoSend();
				// 核心企业账号
				qgylinfoSend.setAccNo(cae.getEnterpriseAccount());
				qgylinfoSend.setBeginDate(startDate);
				qgylinfoSend.setEndDate(endDate);
				qgylinfoSend.setStatus("1");
				// 供应链编号
				qgylinfoSend.setGylCode(cae.getSupplyChainNumber());
				List<QgylinfoReturn> qgylinfoReturnList = icbcService.getQgylinfoReturnList(qgylinfoSend);
				if (CollectionUtils.isEmpty(qgylinfoReturnList)) {
					continue;
				}
				for (QgylinfoReturn qgylinfo : qgylinfoReturnList) {
					String vouchType = qgylinfo.getVouchType();
					// 暂时只支持发票
					if (!"01".equals(vouchType)) {
						logger.error("### 工商银行账款信息查询 返回 凭证种类 不是 01（发票）vouchType={}", vouchType);
						String problem = "工商银行账款信息查询 返回 凭证种类 不是 01（发票）vouchType=" + vouchType;
						problemSericeImpl.setProblem(problem, "accountsInfo");
						continue;
					}
					String vouchCode = qgylinfo.getVouchCode();
					String vouchNo = qgylinfo.getVouchNo();
					// 发票号代码
					String invoiceNoCode =vouchCode + vouchNo;
					// 根据发票号代码查询融资记录
					CreditRecordEntrity cre = creditRecordRepository.findByInvoiceNoCode(invoiceNoCode);
					if (cre == null) {
						logger.error("### 工商银行账款信息查询 根据发票号代码查询融资记录失败  invoiceNoCode={}", invoiceNoCode);
						continue;
					}
					if (cre.getCreditStatus().equals(CreditStatus.REPAYMENTS.toString())) {
						continue;
					}
					// 查询账款金额（工行融资金额）
					Long dueAmt = qgylinfo.getDueAmt();
					// 查询账款剩余金额（待还款的金额）
					Long dueBal = qgylinfo.getDueBal();
					// 已还款金额(核心企业还款)
					BigDecimal purRepaymentAmount = BigDecimal.valueOf((dueAmt - dueBal) / 100).setScale(2, BigDecimal.ROUND_HALF_UP);
					cre.setPurRepaymentAmount(purRepaymentAmount);
					BigDecimal purRepaymentAmountZCW = cre.getPurRepaymentAmount();
					// 判断此账款信息是否被更新过
					if (purRepaymentAmount.compareTo(purRepaymentAmountZCW) == 0 && cre.getCreditStatus().equals(CreditStatus.REPAYMENT.toString()) && cre.getLoanDateCcb() != null
							&& cre.getDrawAmount() != null) {
						// 如果还款金额一样表示被更新过 不予操作
						continue;
					}
					// 供应商收款账号
					String memberRecAcc = qgylinfo.getMemberRecAcc();
					// 核心企业名称
					String purEntName = qgylinfo.getFullName();
					// 供应商名称
					String supEntName = qgylinfo.getMemberName();
					// 融资到期日
					String planDate = qgylinfo.getPlanDate();
					cre.setLoanDateCcb(DateUtils.parseDate(planDate, "yyyyMMdd", "yyyy-MM-dd"));

					cae.setSupReceivablesAccount(memberRecAcc);
					cae.setClearanceAccount(memberRecAcc);
					// 融资金额（已支取金额）
					BigDecimal creditAmt = BigDecimal.valueOf(dueAmt / 100).setScale(2, BigDecimal.ROUND_HALF_UP);
					cre.setCreditAmount(creditAmt);
					BigDecimal drawAmount = cre.getDrawAmount() == null ? BigDecimal.ZERO : cre.getDrawAmount();
					// 判断支取信息是否更新过
					// 新增支取记录
					if (drawAmount.compareTo(creditAmt) == -1) {
						createDrawRecord(cre, supEntName);
					}
					// 融资记录的支取金额
					cre.setDrawAmount(creditAmt);

					// 账款金额（筑材网的）
					BigDecimal creditGetAmount = cre.getCreditGetAmount();
					// 算支取率（融资金额除以筑材网的账款金额）
					BigDecimal setScale = creditAmt.divide(creditGetAmount, 2, BigDecimal.ROUND_HALF_UP);
					cre.setDrawRate(setScale);

					/**
					 * 1 未付清2 已付清3 已确认4 待确认5.可融资
					 */
					String payStatus = qgylinfo.getStatus();

					if ("1".equals(payStatus)) {

						cre.setCreditStatus(CreditStatus.REPAYMENT.toString());

					} else if ("2".equals(payStatus)) {
						cre.setCreditStatus(CreditStatus.REPAYMENTS.toString());
					}
					// 新增还款记录
					if (purRepaymentAmount.compareTo(purRepaymentAmountZCW) == 1) {
						createPaymentRecord(cre, purEntName, purRepaymentAmount);
					}

					// 更新供应商收款账号
					creditApplicationRepository.updateSupReceivables(memberRecAcc, cae.getTid());

					Integer coo = creditRecordRepository.updatePaymentInfo(cre.getTid(), cre.getCreditAmount(), cre.getDrawAmount(), cre.getDrawRate(),
							cre.getPurRepaymentAmount(), cre.getCreditStatus(), cre.getLoanDateCcb());

					if (coo != 1) {
						logger.error("### 工商银行账款信息查询 更新融资信息返回个数异常coo={}，recordId={}", coo, cre.getTid());
						String problem = "### 工商银行账款信息查询 更新融资信息返回个数异常coo=" + coo + "，recordId=" + cre.getTid();
						problemSericeImpl.setProblem(problem, "accountsInfo");
					}

				}
			}
		} catch (Exception e) {
			logger.error("### 工商银行账款信息查询 发生异常 errMsg={}", e);
			String problem = "### 工商银行账款信息查询 发生异常 errMsg=" + e;
			problemSericeImpl.setProblem(problem, "accountsInfo");
		}

		logger.info("### 工商银行账款信息查询任务success end");
	}

	/**
	 * 新增支取记录
	 * 
	 * @version 2018年8月20日
	 * @return
	 */
	private Boolean createDrawRecord(CreditRecordEntrity cre, String supEntName) {
		logger.info("### 工商银行新增支取记录 start,recordId={}", cre.getTid());
		try {

			// 判断此融资单是否被支取过
			List<Object[]> findByStatusNotAndOutId = financeDrawRepository.findByStatusNotAndOutId(HelpConstant.Status.delete, cre.getTid());
			if (CollectionUtils.isNotEmpty(findByStatusNotAndOutId)) {
				return true;
			}

			// 定义支用的实体
			FinanceDrawEntity fde = new FinanceDrawEntity();
			fde.setOutId(cre.getTid());
			fde.setDrawTime(new Date());
			fde.setEnterpriseName(supEntName);
			// 支取的发票号发票代码
			fde.setInvoiceNoCode(cre.getInvoiceNoCode());
			// 支取的企业id
			fde.setEnterpriseId(cre.getParticipatorEnterpriseId());
			// 商家类型
			fde.setEntType(EntType.SUPPLIER.toString());
			// 支取的组织机构编号
			fde.setOrgNum(cre.getOriginatorOrgnCode());
			// 支用编号(由于工行没有支取编号为了一致加上自定义的)
			SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddHHmmss");
			String payoutNum = "ICBC" + sf.format(new Date());
			fde.setPayoutNum(payoutNum);
			fde.setOrgNum(cre.getParticipatorOrgnCode());
			// 支取金额
			fde.setDrawAmount(cre.getCreditAmount());

			financeDrawRepository.save(fde);

			// 判断是否有支取历时记录
			List<CreditRecordTimeEntrity> findByCrRecordIdAndRecordStatus = creditRecordTimeRepository.findByCrRecordIdAndRecordStatus(cre.getTid(),
					CreditStatus.PENDING_WITHDRAWAL.toString());
			if (CollectionUtils.isEmpty(findByCrRecordIdAndRecordStatus)) {
				// 新增支取历时记录
				creditUserServiceUtil.createRecordTime(cre.getTid(), CreditStatus.PENDING_WITHDRAWAL.toString());
			}
		} catch (Exception e) {
			logger.error("### 工商银行新增支取记录 失败,recordId={}，errMsg={}", cre.getTid(), e);
			String problem = "### 工商银行新增支取记录 失败,recordId=" + cre.getTid() + ",errMsg=" + e;
			problemSericeImpl.setProblem(problem, "icbc_createDrawRecord");
			return false;
		}

		logger.info("### 工商银行新增支取记录 end,recordId={}", cre.getTid());
		return true;

	}

	/**
	 * 新增还款记录
	 * 
	 * @version 2018年8月20日
	 * @return
	 */
	private Boolean createPaymentRecord(CreditRecordEntrity cre, String purEntName, BigDecimal payAmt) {
		logger.info("### 工商银行新增还款记录 start recordId={},payAmt={},purEntName", cre.getTid(), payAmt, purEntName);
		try {
			BigDecimal subtract = payAmt.subtract(cre.getRepaymentAmount());
			if (subtract.compareTo(BigDecimal.ZERO) == -1 || subtract.compareTo(BigDecimal.ZERO) == 0) {
				return false;
			}
			// 付款的实体
			FinanceRepaymentEntity fre = new FinanceRepaymentEntity();
			fre.setEntType(EntType.PURCHASER.toString());
			fre.setPaymentTime(new Date());
			// 设置采购商名称
			fre.setEnterpriseName(purEntName);
			fre.setEnterpriseId(cre.getOriginatorEnterpriseId());
			fre.setOrgNum(cre.getOriginatorOrgnCode());
			fre.setInvoiceNoCode(cre.getInvoiceNoCode());
			// 设置操作记录的外界关联id
			fre.setOutId(cre.getTid());

			// 设置还款金额
			fre.setPaymentAmount(subtract);

			financeRepaymentRepository.save(fre);
			if (payAmt.compareTo(cre.getCreditAmount()) == 1 || payAmt.compareTo(cre.getCreditAmount()) == 0) {
				// 新增还款历时记录
				creditUserServiceUtil.createRecordTime(cre.getTid(), CreditStatus.REPAYMENT.toString());
			}
		} catch (Exception e) {
			logger.error("### 工商银行新增还款记录 失败,recordId={}，errMsg={}", cre.getTid(), e);
			String problem = "### 工商银行新增还款记录 失败,recordId=" + cre.getTid() + ",errMsg=" + e;
			problemSericeImpl.setProblem(problem, "icbc_createPaymentRecord");
			return false;
		}
		logger.info("### 工商银行新增还款记录 end recordId={},payAmt={},purEntName", cre.getTid(), payAmt, purEntName);
		return true;

	}
}
