package com.zhucai.credit.service.impl.ccb;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zhucai.credit.bank.ccbe.bean.PurApproveInfo;
import com.zhucai.credit.bank.ccbe.service.CcbeService;
import com.zhucai.credit.bean.CreditApplication;
import com.zhucai.credit.common.HelpConstant;
import com.zhucai.credit.exception.SubmitCcbException;
import com.zhucai.credit.repository.CreditApplicationRepository;
import com.zhucai.credit.repository.CreditInvoiceRepository;
import com.zhucai.credit.repository.CreditRecordRepository;
import com.zhucai.credit.repository.EntAccountRepository;
import com.zhucai.credit.repository.EntCrProdEntityRepository;
import com.zhucai.credit.service.CCBSerice;
import com.zhucai.credit.service.impl.problem.ProblemSericeImpl;



/**
 * 请求建行接口
 * 2017年11月28日
 * @author hsg
 */
@Service("CCBSericeImpl")
@com.alibaba.dubbo.config.annotation.Service(version="1.0.0")
public class CCBSericeImpl implements CCBSerice{
	@Autowired
	EntAccountRepository entAccountRepository;
	@Autowired
	EntCrProdEntityRepository entCrProdEntityRepository;
	@Autowired
	CreditApplicationRepository creditApplicationRepository;
	@Autowired
	CcbeService ccbeService;
	@Autowired
	CreditRecordRepository creditRecordRepository;
	@Autowired
	CreditInvoiceRepository creditInvoiceRepository;
	@Autowired
	ProblemSericeImpl problemSericeImpl;
	private static final Logger logger = LoggerFactory.getLogger(CCBSericeImpl.class);
	 /**
	  * 标准指标信息传递 6W5502
	  * 采购商审核供应商调用建行的接口
	 * @throws IOException 
	 * @throws Exception 
	  * @date 2017年11月30日 @user hsg
	  */
	@Override
	 public void purApproveToCCb(CreditApplication ca) throws SubmitCcbException, IOException{
		
		PurApproveInfo pai=new PurApproveInfo();
		pai.setApp_num(ca.getAppNum());
		pai.setOrgn_num(ca.getAppOrgnNum());
		pai.setRequest_orgn_num(HelpConstant.CurrencyFile.zhucai_num);
		pai.setPartner_customer_id(ca.getCrApplicantId().toString());
		pai.setFeedback_status_cd("0001");
		//交易年限
		pai.setTrade_years(ca.getTradingYears()*12);
		//交易活跃度
		pai.setTrade_activity(ca.getTransactionActivity());
		//交易量评价
		pai.setTrade_evaluate(ca.getTransactionEvaluate());
		//纠纷次数
		pai.setDispute_times_num(ca.getDisputeCount());
		//上下游稳定性
		pai.setFluct_stabilization(ca.getFluctStabilization());
		pai.setMultiple_evaluate("A");
		pai.setDispute_times(0.01);
		pai.setTrade_ranking(0.1);
		Integer.valueOf(ca.getCrLineOfCredit());
		pai.setLimit_proposal(ca.getCrLineOfCredit()*10000);
		 //根据建行接口返回的信息
		logger.info("### 标准指标信息传递 6W5502 start");
		ccbeService.purApproveInfo(pai);
		logger.info("### 标准指标信息传递 6W5502 end");
	 }
}
