package com.zhucai.credit.bank.icbc.bean;

import java.io.Serializable;

/**
 * 发送融资交易实体
 * @author xjz
 *
 */
public class GylinfoupSend implements Serializable{

	private static final long serialVersionUID = 1L;
	
	//总笔数 *
	private Integer totalNum;
	//总金额  *(票面金额、应付账款金额支持负金额，总金额=每一笔的应付账款金额绝对值的和)
	private Long totalAmt;
	//核心企业账号 *
	private String accNo;
	//操作类型  * (1、 新增 , 2、 修改 , 3、 删除)
	private Integer tranType;
	//供应链编号 *
	private String gylCode;
	//签名时间 *
	private String signTime;
	//请求包备用字段1
	private String reqReserved1;
	//请求包备用字段2
	private String reqReserved2;
	//指令顺序号 * 
	private Long iSeqno;
	//凭证种类 * (01 发票  , 99 其他 （本期只允许上送01）)
	private String vouchType;
	//凭证代码 * 
	private String vouchCode;
	//凭证号码 *
	private String vouchNo;
	//票面金额 (发票的票面金额(暂不使用),支持负金额)
	private Long billAmt;
	//应付账款金额 * (该笔发票对应的实际应付账款金额（以支持已有预付部分金额的情况）,支持负金额)
	private Long dueAmt;
	//验收单号
	private String billNo;
	//关联凭证种类(可以为空，若上送只能送02)
	private String crvouhType;
	//关联凭证代码(若关联凭证种类为02，则送订单代码)
	private String crvouhCode;
	//关联凭证号码(若关联凭证种类为02，则送订单编号)
	private String crvouhNo;
	//合同编号
	private String contactNo;
	//应付账款生成日期 * (YYYYMMDD)
	private String dueDate;
	//应付账款编号
	private String dueCode;
	//计划付款日期 * (YYYYMMDD)
	private String planDate;
	//供应商全称 * 
	private String memberName;
	//供应商监管账号
	private String memberAccNo;
	//会员企业代码类型 (如不输入，默认为1。)
	private Integer memberType;
	//会员企业单位代码  * (如上一项输入1，则该项代表企业的组织机构代码；否则为其他类型的单位代码。（本期只实现代码类型为1的情况）)
	private String memberCode;
	//备注
	private String notes;
	//状态 * (0未确认  1已确认)
	private Integer status;
	//宽限期 (YYYYMMDD)
	private String delayDay;
	//请求包备用字段3
	private String reqReserved3;
	//请求包备用字段4
	private String reqReserved4;
	public Integer getTotalNum() {
		return totalNum;
	}
	public void setTotalNum(Integer totalNum) {
		this.totalNum = totalNum;
	}
	public Long getTotalAmt() {
		return totalAmt;
	}
	public void setTotalAmt(Long totalAmt) {
		this.totalAmt = totalAmt;
	}
	public String getAccNo() {
		return accNo;
	}
	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}
	public Integer getTranType() {
		return tranType;
	}
	public void setTranType(Integer tranType) {
		this.tranType = tranType;
	}
	public String getGylCode() {
		return gylCode;
	}
	public void setGylCode(String gylCode) {
		this.gylCode = gylCode;
	}
	public String getSignTime() {
		return signTime;
	}
	public void setSignTime(String signTime) {
		this.signTime = signTime;
	}
	public String getReqReserved1() {
		return reqReserved1;
	}
	public void setReqReserved1(String reqReserved1) {
		this.reqReserved1 = reqReserved1;
	}
	public String getReqReserved2() {
		return reqReserved2;
	}
	public void setReqReserved2(String reqReserved2) {
		this.reqReserved2 = reqReserved2;
	}
	public Long getiSeqno() {
		return iSeqno;
	}
	public void setiSeqno(Long iSeqno) {
		this.iSeqno = iSeqno;
	}
	public String getVouchType() {
		return vouchType;
	}
	public void setVouchType(String vouchType) {
		this.vouchType = vouchType;
	}
	public String getVouchCode() {
		return vouchCode;
	}
	public void setVouchCode(String vouchCode) {
		this.vouchCode = vouchCode;
	}
	public String getVouchNo() {
		return vouchNo;
	}
	public void setVouchNo(String vouchNo) {
		this.vouchNo = vouchNo;
	}
	public Long getBillAmt() {
		return billAmt;
	}
	public void setBillAmt(Long billAmt) {
		this.billAmt = billAmt;
	}
	public Long getDueAmt() {
		return dueAmt;
	}
	public void setDueAmt(Long dueAmt) {
		this.dueAmt = dueAmt;
	}
	public String getBillNo() {
		return billNo;
	}
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	public String getCrvouhType() {
		return crvouhType;
	}
	public void setCrvouhType(String crvouhType) {
		this.crvouhType = crvouhType;
	}
	public String getCrvouhCode() {
		return crvouhCode;
	}
	public void setCrvouhCode(String crvouhCode) {
		this.crvouhCode = crvouhCode;
	}
	public String getCrvouhNo() {
		return crvouhNo;
	}
	public void setCrvouhNo(String crvouhNo) {
		this.crvouhNo = crvouhNo;
	}
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getDueCode() {
		return dueCode;
	}
	public void setDueCode(String dueCode) {
		this.dueCode = dueCode;
	}
	public String getPlanDate() {
		return planDate;
	}
	public void setPlanDate(String planDate) {
		this.planDate = planDate;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getMemberAccNo() {
		return memberAccNo;
	}
	public void setMemberAccNo(String memberAccNo) {
		this.memberAccNo = memberAccNo;
	}
	public Integer getMemberType() {
		return memberType;
	}
	public void setMemberType(Integer memberType) {
		this.memberType = memberType;
	}
	public String getMemberCode() {
		return memberCode;
	}
	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getDelayDay() {
		return delayDay;
	}
	public void setDelayDay(String delayDay) {
		this.delayDay = delayDay;
	}
	public String getReqReserved3() {
		return reqReserved3;
	}
	public void setReqReserved3(String reqReserved3) {
		this.reqReserved3 = reqReserved3;
	}
	public String getReqReserved4() {
		return reqReserved4;
	}
	public void setReqReserved4(String reqReserved4) {
		this.reqReserved4 = reqReserved4;
	}
	
	
	
}
