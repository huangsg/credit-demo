package com.zhucai.credit.base;

/** 
 * @Desc: 获取Id的接口
 */  
public interface Identifyable {
	
	Long getId();

}
