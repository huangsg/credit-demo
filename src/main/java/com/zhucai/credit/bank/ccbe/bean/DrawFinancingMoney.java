package com.zhucai.credit.bank.ccbe.bean;

import java.io.Serializable;
import java.util.Date;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * 支用信息反馈
 * 6W5528
 * @date 2017年11月28日 @user hsg
 */
public class DrawFinancingMoney implements Serializable{

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "当前最大反馈序号")
	private Integer max_serial_num;
	@ApiModelProperty(value = "反馈序号")
	private Integer serial_num;
	@ApiModelProperty(value = "企业名称")
	private String cust_name;
	@ApiModelProperty(value = "合同编号")
	private String contract_num;
	@ApiModelProperty(value = "供应商在合作平台的注册ID")
	private String cooper_cust_num;
	@ApiModelProperty(value = "支用编号")
	private String payout_num;
	@ApiModelProperty(value = "币种代码156人民币元840美元")
	private String currency_cd;
	@ApiModelProperty(value = "预付款金额")
	private double prepayment_amt;
	@ApiModelProperty(value = "实际入账金额")
	private double entity_amt;
	@ApiModelProperty(value = "发放日期(yyyymmdd)")
	private Date loan_date;
	@ApiModelProperty(value = "预付款到期日(yyyymmdd)")
	private Date prepayment_due_date;
	@ApiModelProperty(value = "融资是否成功的标识（1-是;0-否）")
	private String loan_ind;
	@ApiModelProperty(value = "融资失败的原因（00-额度不足;01-账款已到期;02-欠费）")
	private String fail_reason;
	@ApiModelProperty(value = "单据号,应收账款的号码，多张(发票号@|@发票号@|@...)")
	private String bill_num;
	//新增字段
	@ApiModelProperty(value = "")
	private String origctr_id;
	@ApiModelProperty(value = "0")
	private String intrt;
	@ApiModelProperty(value = "0.89999998")
	private String fnc_rto;
	@ApiModelProperty(value = "020002909483")
	private String credit_no;
	public Integer getMax_serial_num() {
		return max_serial_num;
	}
	public void setMax_serial_num(Integer max_serial_num) {
		this.max_serial_num = max_serial_num;
	}
	public Integer getSerial_num() {
		return serial_num;
	}
	public void setSerial_num(Integer serial_num) {
		this.serial_num = serial_num;
	}
	public String getCust_name() {
		return cust_name;
	}
	public void setCust_name(String cust_name) {
		this.cust_name = cust_name;
	}
	public String getContract_num() {
		return contract_num;
	}
	public void setContract_num(String contract_num) {
		this.contract_num = contract_num;
	}
	public String getCooper_cust_num() {
		return cooper_cust_num;
	}
	public void setCooper_cust_num(String cooper_cust_num) {
		this.cooper_cust_num = cooper_cust_num;
	}
	public String getPayout_num() {
		return payout_num;
	}
	public void setPayout_num(String payout_num) {
		this.payout_num = payout_num;
	}
	public String getCurrency_cd() {
		return currency_cd;
	}
	public void setCurrency_cd(String currency_cd) {
		this.currency_cd = currency_cd;
	}
	public double getPrepayment_amt() {
		return prepayment_amt;
	}
	public void setPrepayment_amt(double prepayment_amt) {
		this.prepayment_amt = prepayment_amt;
	}
	public double getEntity_amt() {
		return entity_amt;
	}
	public void setEntity_amt(double entity_amt) {
		this.entity_amt = entity_amt;
	}
	public Date getLoan_date() {
		return loan_date;
	}
	public void setLoan_date(Date loan_date) {
		this.loan_date = loan_date;
	}
	public Date getPrepayment_due_date() {
		return prepayment_due_date;
	}
	public void setPrepayment_due_date(Date prepayment_due_date) {
		this.prepayment_due_date = prepayment_due_date;
	}
	public String getLoan_ind() {
		return loan_ind;
	}
	public void setLoan_ind(String loan_ind) {
		this.loan_ind = loan_ind;
	}
	public String getFail_reason() {
		return fail_reason;
	}
	public void setFail_reason(String fail_reason) {
		this.fail_reason = fail_reason;
	}
	public String getBill_num() {
		return bill_num;
	}
	public void setBill_num(String bill_num) {
		this.bill_num = bill_num;
	}
	public String getOrigctr_id() {
		return origctr_id;
	}
	public void setOrigctr_id(String origctr_id) {
		this.origctr_id = origctr_id;
	}
	public String getIntrt() {
		return intrt;
	}
	public void setIntrt(String intrt) {
		this.intrt = intrt;
	}
	public String getFnc_rto() {
		return fnc_rto;
	}
	public void setFnc_rto(String fnc_rto) {
		this.fnc_rto = fnc_rto;
	}
	public String getCredit_no() {
		return credit_no;
	}
	public void setCredit_no(String credit_no) {
		this.credit_no = credit_no;
	}
	
}
