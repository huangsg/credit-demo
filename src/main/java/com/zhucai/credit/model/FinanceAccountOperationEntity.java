package com.zhucai.credit.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * 金融信息关联表(记录以后加的跟金融相关的辅助信息)
 * 2018年07月11日
 * @author hsg
 */
@Entity
@Table(name = "finance_account_operation")
public class FinanceAccountOperationEntity extends BaseEntity{
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "核心企业筑材金融Id,")
	private Long accPurId;
	@ApiModelProperty(value = "供应商企业筑材金融Id,")
	private Long accSupId;
	@ApiModelProperty(value = "默认银行关联id,")
	private Long bankId;
	@ApiModelProperty(value = "创建时间,")
	private Date creDate;
	
	@Column(nullable=false)
	public Date getCreDate() {
		return creDate;
	}
	public void setCreDate(Date creDate) {
		this.creDate = creDate;
	}
	@Column(nullable=false)
	public Long getAccPurId() {
		return accPurId;
	}
	public void setAccPurId(Long accPurId) {
		this.accPurId = accPurId;
	}
	@Column(nullable=false)
	public Long getAccSupId() {
		return accSupId;
	}
	public void setAccSupId(Long accSupId) {
		this.accSupId = accSupId;
	}
	@Column(nullable=false)
	public Long getBankId() {
		return bankId;
	}
	public void setBankId(Long bankId) {
		this.bankId = bankId;
	}
	
}
