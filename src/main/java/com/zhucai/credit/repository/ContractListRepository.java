package com.zhucai.credit.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.zhucai.credit.model.ContractList;

/**
 * 
 * @author hsg
 * ContractListRepository
 */
@Repository
public interface ContractListRepository extends
		PagingAndSortingRepository<ContractList, Long>,
		JpaSpecificationExecutor<ContractList> {


	 @Query(value = "select count(*) from tb_xa_mission_list txm where txm.contract_id=?1 and txm.contract_type=?2 and txm.status<>0 and txm.status<>6",nativeQuery = true)
    public Integer findMissionCountByUnfinished(Integer integer, Integer contractType);

	public ContractList findBytidAndStatusNot(Long valueOf, int invalid);


	





}
