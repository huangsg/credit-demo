package com.zhucai.credit.valid.invoice;

import java.math.BigDecimal;
import java.util.Date;

public class InviceValidResult {

	//发票code
	private String invoiceDataCode;
	//发票代码
	private String invoiceNumber;
	//发票金额
	private BigDecimal totalTaxSum;
	//发票时间
	private Date billingTime;
	//采购商统一社会代码
	private String taxpayerNumber;
	//供应商统一社会代码
	private String salesTaxpayerNum;

	
	private String resultMsg;
	//返回数据是：{"resultMsg":"余额不足，请充值。","invoicefalseCode":"240", 发短信
	private String invoicefalseCode;
	
	public String getResultMsg() {
		return resultMsg;
	}

	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}

	public String getInvoicefalseCode() {
		return invoicefalseCode;
	}

	public void setInvoicefalseCode(String invoicefalseCode) {
		this.invoicefalseCode = invoicefalseCode;
	}

	public String getInvoiceDataCode() {
		return invoiceDataCode;
	}

	public void setInvoiceDataCode(String invoiceDataCode) {
		this.invoiceDataCode = invoiceDataCode;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public BigDecimal getTotalTaxSum() {
		return totalTaxSum;
	}

	public void setTotalTaxSum(BigDecimal totalTaxSum) {
		this.totalTaxSum = totalTaxSum;
	}

	public Date getBillingTime() {
		return billingTime;
	}

	public void setBillingTime(Date billingTime) {
		this.billingTime = billingTime;
	}

	public String getTaxpayerNumber() {
		return taxpayerNumber;
	}

	public void setTaxpayerNumber(String taxpayerNumber) {
		this.taxpayerNumber = taxpayerNumber;
	}

	public String getSalesTaxpayerNum() {
		return salesTaxpayerNum;
	}

	public void setSalesTaxpayerNum(String salesTaxpayerNum) {
		this.salesTaxpayerNum = salesTaxpayerNum;
	}
	
	
}
