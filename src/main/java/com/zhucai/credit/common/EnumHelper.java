package com.zhucai.credit.common;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.util.CollectionUtils;

import com.zhucai.credit.base.TextEntry;

public class EnumHelper {

	private static Map<Enum<?>, String> DESCRIBE_CACHE = new ConcurrentHashMap<>();

	/**
	 * 获取枚举描述文本.
	 * @param enumVal
	 * @return
	 */
	public static <E extends Enum<E>> String getDescribe(Enum<E> enumVal) {
		try {
			if (enumVal == null)
				return null;

			// 缓存读取
			if (DESCRIBE_CACHE.containsKey(enumVal))
				return DESCRIBE_CACHE.get(enumVal);

			Class<E> declaringClass = enumVal.getDeclaringClass();
			Field field = declaringClass.getField(enumVal.name());
			if (field == null)
				return null;

			Text enumDescribe = field.getAnnotation(Text.class);
			if (enumDescribe == null)
				return null;

			// 更新缓存
			String describeText = enumDescribe.value();
			DESCRIBE_CACHE.put(enumVal, describeText);
			return describeText;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 构建指定枚举类型成  EntryBean
	 * @param enumVal
	 * @return
	 */
	public static <E extends Enum<E>> TextEntry buildTextEntry(Enum<E> enumVal) {
		return new TextEntry(enumVal.name(), getDescribe(enumVal));
	}

	public static <E extends Enum<E>> List<TextEntry> buildTextEntry(Class<E> enumClass) {
		return buildTextEntryList(enumClass, false);
	}

	public static <E extends Enum<E>> List<TextEntry> buildTextEntryList(Class<E> enumClass, boolean addBlank) {
		return buildTextEntryList(enumClass, addBlank, "");
	}

	public static <E extends Enum<E>> List<TextEntry> buildTextEntryList(Class<E> enumClass, boolean addBlank, String blankText) {
		EnumSet<E> enumSet = EnumSet.allOf(enumClass);
		List<TextEntry> result = new ArrayList<TextEntry>();
		if (addBlank)
			result.add(new TextEntry(null, blankText));
		if (CollectionUtils.isEmpty(enumSet))
			return result;
		for (E e : enumSet)
			result.add(buildTextEntry(e));
		return result;
	}
}
