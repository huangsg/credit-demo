package com.zhucai.credit.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * 
 * @ClassName: Message
 * @Description: 系统信息
 * @author jmshi
 * @date 2015年10月11日 上午10:47:49
 *
 */
@Entity
@Table(name = "tb_xa_station_message")
@ApiModel(value = "站内信内容模板内容")
public class StationMessage extends BaseEntity {
	
	@ApiModelProperty(value = "信息编号：即站内信的模板编号")
	private String number;
	
	@ApiModelProperty(value = "信息类型， 1供应商 2采购商。1/2代表信息的接收方")
	private Integer accountType;
	  
	@ApiModelProperty(value = "信息分类，如：询价信息，签约信息，竞价结束信息")
	private String type;
	
	@ApiModelProperty(value = "触发条件")
	private String triggers;
	
	@ApiModelProperty(value = "信息内容")
	private String content;
	
	@Column(nullable = true, length = 50)
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	
	@Column(nullable = true, length = 50)
	public Integer getAccountType() {
		return accountType;
	}
	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}
	
	@Column(nullable=true,length=50)
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@Column(nullable = true, length = 500)
	public String getTriggers() {
		return triggers;
	}
	public void setTriggers(String triggers) {
		this.triggers = triggers;
	}
	
	@Column(nullable=true,length=1000)
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
}
